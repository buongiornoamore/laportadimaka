<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo COMPANY_NAME . ' - ADMIN'; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
<!-- JS / CSS added to GROCERY CRUD template -->
<?php 
// import grocery and codigniter css
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<!-- END JS / CSS added to GROCERY CRUD template --> 
</head>
<body>
	<div class="wrapper">
        <? include('include/left_menu.php'); ?>
        <div class="main-panel">
            <? include('include/navbar_top.php'); ?>  
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
            			<div class="col-md-12">
                            <div class="card">
                            	<div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">send</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title">Newsletter&nbsp;&nbsp;<a href="" id="guide-btn" title="Visualizza la guida"><i class="fa fa-info-circle"></i></a></h4>
	                                <div class="table-responsive">
                                    	<?php echo $output; ?>
                                    </div>
                                 </div>   
                        	</div>          
                        </div>    
                    </div>
                 </div>
            </div>        
     		<? include('include/footer.php'); ?>
        </div>
    </div>
    <form name="newsletterForm" id="newsletterForm" action="<? echo base_url(); ?>admin/newsletter/email" method="post">
    	<input type="hidden" name="newsletter_list" value="" id="newsletter_list" />
    </form>
</body>
<? include('include/footer_js_admin.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
		$('.header-tools > .floatL').append('<a id="send-news-btn" href="#" class="btn btn-default gc-list-active-button hidden"><i class="fa fa-envelope"></i> &nbsp;&nbsp;Invia newsletter</a>');
		
		// stesa azione che si fa in gcrud.datagrid.js
		$('a[href$="disabled-action"]').attr('disabled', 'disabled');
		$('a[href$="disabled-action"]').bind('click', false);
		
		$("#send-news-btn").on("click", function(e) {
			e.preventDefault();
			var delete_selected = [];
		//	var data_to_send = {};
			$('.select-row:checked').each(function () {
				delete_selected.push($(this).data('id'));
			});
			// call action
			console.log(delete_selected);
			$('#newsletter_list').val(JSON.stringify(delete_selected)); //store array
			$('#newsletterForm').submit();
		});
			
		$('.btn.btn-simple.btn-danger.btn-icon').has('.fa-envelope').on('click', function(e){
		  e.preventDefault();
		  var email_to = $(this).attr('href');
		  swal({
			  title: 'Email libera a ' + email_to,
			  width: 800,
			  customClass: 'html-modal-swal',
			  html:
				'<input id="swal-input1" class="swal2-input" placeholder="Inserisci il soggetto della email">' +
				'<textarea id="swal-input2" class="swal2-input html-textarea" placeholder="Inserisci il testo HTML della email"></textarea>',
			  focusConfirm: false,
			  showCancelButton: true,
			  showConfirmButton: true,
			  confirmButtonText: 'Invia',
			  cancelButtonText: 'Annulla',
			  preConfirm: function () {
				return new Promise(function (resolve, reject) {
					setTimeout(function() {
						if ($('#swal-input1').val() === '' || $('#swal-input2').val() === '') {
						  reject('Inserisci soggetto e testo HTML della email.')
						} else {
						  resolve()
						}
				  	}, 1000)
				})
			  },
			  allowOutsideClick: false
			}).then(function (result) {
			  var subject = $('#swal-input1').val();
			  var text = $('#swal-input2').val();
			  if(subject != '' && text) {
					swal({
					  title: 'Sei sicuro?',
					  text: "La tua email verrà inviata a " + email_to,
					  type: 'warning',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Si, inviala!'
					}).then(function () {
					    $('.se-pre-con').show();
						var postData = {
						  'subject_email': subject,	
						  'testo_email': text,
						  'to_email' : email_to,
						  'lingua_id' : '1'
						};
						$.ajax({
							url: '<? echo base_url();?>admin/send_email_libera',
							type: 'POST',
							dataType: "HTML",
							async: true,
							data: postData,
							success: function(response) {
								$(".se-pre-con").delay(200).fadeOut("slow"); 
								swal({
									title: 'Email inviata',
									text: 'La tua email è stata inviata con successo !',
									type: 'success',
									confirmButtonClass: "btn btn-success",
									buttonsStyling: false
								});
							},
							error: function () {
								$(".se-pre-con").delay(200).fadeOut("slow"); 
								swal({
									title: 'Errore!',
									text: 'Errore durante l\'invio della email.',
									type: 'error',
									confirmButtonClass: "btn btn-success",
									buttonsStyling: false
									});
							}
						});
					});
				}
			}).catch(swal.noop);
		});
		
    	// sweet guide
		$("#guide-btn").on("click", function(e) {
			e.preventDefault();
			swal({
			  title: '<b>Guida all\'uso</b>',
			  width: 800,
			  type: 'question',
			  html: '<p align="justify">La funzionalità newsletter permette di gestire i contatti e le newsletter del sito.E\' possibile inserire nuovi contatti oltre che modificare, visualizzare e nascondere gli stessi.</p><p align="justify">Ogni contatto è composto da un insieme di campi, alcuni obbligatori e altri facoltativi.</p><p align="justify">Nelle tabelle di elenco sono presenti filtri di ricerca generali <i class="fa fa-search" style="color: #9c27b0"></i> oppure specifici sulle singole colonne. Le colonne possono essere ordinate (drill down) cliccando direttamente sul nome della colonna (es. Nome). Le liste tabellari possono essere stampate o esportate in excel per utilizzi esterni.</p><p align="justify">Se si intende escludere un contatto si può procedere modificandone lo stato o cancellandolo dal SITO.</p><p align="justify">La funzionalità di invio newsletter permette di inviare email libere o da template a tutti i contatti selezionati. Nel testo delle email (template) si può utilizzare la variabile VAR_NOME_CLIENTE per far inserire in automatico dal sistema il nome del cliente.</p>',
			  showCloseButton: true,
			  showCancelButton: false,
			  confirmButtonText:
				'<i class="fa fa-close"></i> Chiudi'
			});
		});
	});
</script>
</html>