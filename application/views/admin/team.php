<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo COMPANY_NAME . ' - ADMIN'; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
<!-- JS / CSS added to GROCERY CRUD template -->
<?php 
// import grocery and codigniter css
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<!-- END JS / CSS added to GROCERY CRUD template --> 
</head>
<body>
	<div class="wrapper">
        <? include('include/left_menu.php'); ?>
        <div class="main-panel">
            <? include('include/navbar_top.php'); ?>  
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
            			<div class="col-md-12">
                            <div class="card">
                            	<div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">supervisor_account</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title">Team&nbsp;&nbsp;<a href="" id="guide-btn" title="Visualizza la guida"><i class="fa fa-info-circle"></i></a></h4>
	                                <div class="table-responsive">
                                    	<?php echo $output; ?>
                                    </div>
                                 </div>   
                        	</div>          
                        </div>    
                    </div>
                 </div>
            </div>        
     		<? include('include/footer.php'); ?>
        </div>
    </div>
</body>
<? include('include/footer_js_admin.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
    	// sweet guide
		$("#guide-btn").on("click", function(e) {
			e.preventDefault();
			swal({
			  title: '<b>Guida all\'uso</b>',
			  width: 800,
			  type: 'question',
			  html: '<p align="justify">La funzionalità prodotti permette di gestire il catalogo dei prodotti presenti nello <b>shop</b>. E\' possibile inserire nuovi prodotti e/o varianti prodotti oltre che modificare, visualizzare e nascondere gli stessi.</p><p align="justify">Ogni prodotto è composto da un insieme di campi, alcuni obbligatori e altri facoltativi, oltre che da immagini di diverse dimensioni (si consiglia di utilizzare immagini con le dimensioni esatte indicate nella scheda prodotto).</p><p align="justify">I prodotti principali possono avere delle varianti prodotto che si differenziano dal prodotto principale per colore, prezzo, codice e stato. Se il prodotto principale viene disattivato, rimosso o cancellato le varianti prodotto non saranno più visibili e seguiranno lo stato del prodotto principale.</p><p align="justify">Nelle tabelle di elenco sono presenti filtri di ricerca generali <i class="fa fa-search" style="color: #9c27b0"></i> oppure specifici sulle singole colonne. Le colonne possono essere ordinate (drill down) cliccando direttamente sul nome della colonna (es. Codice). Le liste tabellari possono essere stampate o esportate in excel per utilizzi esterni.</p><p align="justify">In generale non è prevista la cancellazione di prodotti o varianti prodotto inserite nel sistema in quanto queste potrebbero essere legate ad ordini effettuati in passato (storico ordini) e quindi restano necessarie ai fini della storicizzazione dei dati e alla produzione di statistiche di ampio raggio (mensili, annuali, etc.). Se si intende escludere un prodotto o una variante prodotto nello shop si può procedere entrando nella modalita di <b>modifica</b> <i class="fa fa-pencil text-info"></i> e aggiornando lo stato a <b>sospeso</b> oppure <b>cancellato</b>.</p><p align="justify">In questo modo il prodotto non sarà visibile agli utenti dello SHOP ma resterà modificabile nella sezione <b>admin</b>. Si consiglia di aggiornare lo stato a <b>sospeso</b> per i prodotti momentaneamente indisponibili o non vendibile e <b>cancellato</b> per i prodotti che si intende mettere da parte anche se sarà sempre possibili aggiornare nuovamente lo stato qualora fosse necessario.</p><p align="justify">Per gestire le traduzioni e i campi <b>descrizione</b> e <b>descrizione breve</b> di ogni prodotto è disponibile la funzionalità <b>traduzioni prodotto</b> <i class="fa fa-file-text text-danger"></i>.</p><p align="justify">Per gestire le varianti di ogni prodotto è disponibile la funzionalità <b>varianti prodotto</b> <i class="fa fa-shopping-bag text-success"></i>.</p><p align="justify">L\'inserimento delle immagini del prodotto o della variante può essere fatto solo dopo aver inserito il prodotto entrando nella modalità di <b>modifica</b> <i class="fa fa-pencil text-info"></i>.</p>',
			  showCloseButton: true,
			  showCancelButton: false,
			  confirmButtonText:
				'<i class="fa fa-close"></i> Chiudi'
			});
		});
	});
</script>
</html>