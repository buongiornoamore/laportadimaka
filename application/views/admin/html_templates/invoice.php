<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title><?php echo $template_name . ' - ' . COMPANY_NAME; ?></title>
  <link rel="apple-touch-icon" sizes="76x76" href="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/apple-icon.png" />
  <link rel="icon" type="image/png" href="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/favicon.png" />	

  <style type="text/css">
    /* Take care of image borders and formatting, client hacks */
    img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
    a img { border: none; }
    table { border-collapse: collapse !important;}
    #outlook a { padding:0; }
    .ReadMsgBody { width: 100%; }
    .ExternalClass { width: 100%; }
    .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }
    table td { border-collapse: collapse; }
    .ExternalClass * { line-height: 115%; }
    .container-for-gmail-android { min-width: 600px; }


    /* General styling */
    * {
      font-family: Helvetica, Arial, sans-serif;
    }

    body {
      -webkit-font-smoothing: antialiased;
      -webkit-text-size-adjust: none;
      width: 100% !important;
      margin: 0 !important;
      height: 100%;
      color: #676767;
    }

    td {
      font-family: Helvetica, Arial, sans-serif;
      font-size: 14px;
      color: #777777;
      text-align: center;
      line-height: 21px;
    }

    a {
      color: #676767;
      text-decoration: none !important;
    }

    .pull-left {
      text-align: left;
    }

    .pull-right {
      text-align: right;
    }

    .header-lg,
    .header-md,
    .header-sm {
      font-size: 32px;
      font-weight: 700;
      line-height: normal;
      padding: 35px 0 0;
      color: #4d4d4d;
    }

    .header-md {
      font-size: 24px;
    }

    .header-sm {
      padding: 5px 0;
      font-size: 18px;
      line-height: 1.3;
    }

    .content-padding {
      padding: 20px 0 5px;
    }

    .mobile-header-padding-right {
      width: 290px;
      text-align: right;
      padding-left: 10px;
    }

    .mobile-header-padding-left {
      width: 290px;
      text-align: left;
      padding-left: 10px;
    }

    .free-text {
      width: 100% !important;
      padding: 10px 60px 0px;
    }

    .button {
      padding: 30px 0;
    }

    .mini-block {
      border: 1px solid #e5e5e5;
      border-radius: 5px;
      background-color: #ffffff;
      padding: 12px 15px 15px;
      text-align: left;
      width: 253px;
    }

    .mini-container-left {
      width: 278px;
      padding: 10px 0 10px 15px;
    }

    .mini-container-right {
      width: 278px;
      padding: 10px 14px 10px 15px;
    }

    .product {
      text-align: left;
      vertical-align: top;
      width: 175px;
    }

    .total-space {
      padding-bottom: 8px;
      display: inline-block;
    }

    .item-table {
      padding: 50px 20px;
      width: 560px;
    }

    .item {
      width: 300px;
    }

    .mobile-hide-img {
      text-align: left;
      width: 125px;
    }

    .mobile-hide-img img {
      border: 1px solid #e6e6e6;
      border-radius: 4px;
    }

    .title-dark {
      text-align: left;
      border-bottom: 1px solid #cccccc;
      color: #4d4d4d;
      font-weight: 700;
      padding-bottom: 5px;
    }

    .item-col {
      padding-top: 20px;
      text-align: left;
      vertical-align: top;
    }

    .force-width-gmail {
      min-width:600px;
      height: 0px !important;
      line-height: 1px !important;
      font-size: 1px !important;
    }

  </style>

  <style type="text/css" media="screen">
    @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);
  </style>

  <style type="text/css" media="screen">
    @media screen {
      /* Thanks Outlook 2013! */
      * {
        font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
      }
    }
  </style>

  <style type="text/css" media="only screen and (max-width: 480px)">
    /* Mobile styles */
    @media only screen and (max-width: 480px) {

      table[class*="container-for-gmail-android"] {
        min-width: 290px !important;
        width: 100% !important;
      }

      img[class="force-width-gmail"] {
        display: none !important;
        width: 0 !important;
        height: 0 !important;
      }

      table[class="w320"] {
        width: 320px !important;
      }

      td[class*="mobile-header-padding-left"] {
        width: 160px !important;
        padding-left: 0 !important;
      }

      td[class*="mobile-header-padding-right"] {
        width: 160px !important;
        padding-right: 0 !important;
      }

      td[class="header-lg"] {
        font-size: 24px !important;
        padding-bottom: 5px !important;
      }

      td[class="content-padding"] {
        padding: 5px 0 5px !important;
      }

       td[class="button"] {
        padding: 5px 5px 30px !important;
      }

      td[class*="free-text"] {
        padding: 10px 18px 30px !important;
      }

      td[class~="mobile-hide-img"] {
        display: none !important;
        height: 0 !important;
        width: 0 !important;
        line-height: 0 !important;
      }

      td[class~="item"] {
        width: 140px !important;
        vertical-align: top !important;
      }

      td[class~="quantity"] {
        width: 50px !important;
      }

      td[class~="price"] {
        width: 90px !important;
      }

      td[class="item-table"] {
        padding: 30px 20px !important;
      }

      td[class="mini-container-left"],
      td[class="mini-container-right"] {
        padding: 0 15px 15px !important;
        display: block !important;
        width: 290px !important;
      }

    }
  </style>
</head>

<body bgcolor="#f7f7f7">
<table align="center" cellpadding="0" cellspacing="0" class="container-for-gmail-android" width="100%">
  <tr>
    <td align="left" valign="top" width="100%" style="background:repeat-x url(<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/4E687TRe69Ld95IDWyEg_bg_top_02.jpg) #ffffff;">
      <center>
      <img src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/SBb2fQPrQ5ezxmqUTgCr_transparent.png" class="force-width-gmail" />
        <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" background="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/4E687TRe69Ld95IDWyEg_bg_top_02.jpg" style="background-color:transparent">
          <tr>
            <td width="100%" height="80" valign="top" style="text-align: center; vertical-align:middle;">
            <!--[if gte mso 9]>
            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;height:80px; v-text-anchor:middle;">
              <v:fill type="tile" src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/4E687TRe69Ld95IDWyEg_bg_top_02.jpg" color="#ffffff" />
              <v:textbox inset="0,0,0,0">
            <![endif]-->
              <center>
                 <table cellpadding="0" cellspacing="0" width="600" class="w320">
                  <tr>
                    <td class="pull-left mobile-header-padding-left" style="vertical-align: middle;">
                      <a href="<?php echo site_url(lang('PAGE_HOME_URL_EMAIL')); ?>">
                      <img width="137" height="47" src="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/logo_mail.png" alt="<?php echo COMPANY_NAME; ?>" />
                      </a>
                    </td>
                    <td class="pull-right mobile-header-padding-right" style="color: #4d4d4d;">
                    	<? if(INSTAGRAM_LINK != '') { ?>
                    	<a href="<? echo INSTAGRAM_LINK; ?>"><img width="38" height="47" src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/socials/instagram.png" alt="instagram" /></a>
                        <? } ?>	
                        <? if(FACEBOOK_LINK != '') { ?>
                        <a href="<? echo FACEBOOK_LINK; ?>"><img width="38" height="47" src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/socials/facebook.png" alt="facebook" /></a>
                        <? } ?>	
                        <? if(PINTEREST_LINK != '') { ?>
                        <a href="<? echo PINTEREST_LINK; ?>"><img width="38" height="47" src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/socials/pinterest.png" alt="pinterest" /></a>
                        <? } ?>	
                        <? if(YOUTUBE_LINK != '') { ?>
                        <a href="<? echo YOUTUBE_LINK; ?>"><img width="38" height="47" src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/socials/youtube.png" alt="youtube" /></a>
                        <? } ?>	
                        <? if(TWITTER_LINK != '') { ?>
                        <a href="<? echo TWITTER_LINK; ?>"><img width="38" height="47" src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/socials/twitter.png" alt="twitter" /></a>
                        <? } ?>	
                        <? if(GPLUS_LINK != '') { ?>
                    	<a href="<? echo GPLUS_LINK; ?>"><img width="38" height="47" src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/socials/google_plus.png" alt="google+" /></a>         
                        <? } ?>	
                    </td>
                  </tr>
                </table>
              </center>
              <!--[if gte mso 9]>
              </v:textbox>
            </v:rect>
            <![endif]-->
            </td>
          </tr>
        </table>
      </center>
    </td>
  </tr>
  <tr>
    <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
      <center>
        <table cellspacing="0" cellpadding="0" width="600" class="w320">
          <tr>
            <td class="header-lg">
              <? echo lang('LABEL_INVOICE_THANKS'); ?>
            </td>
          </tr>
          <?php if($order_payment_type == 2 && $order_payment_status != 1) {?>
           <tr>
            <td class="free-text">
              <? echo lang('LABEL_INVOICE_CC_PAYMENT_FAIL'); ?>
            </td>
           </tr>
           <tr>
            <td class="button">
              <div><!--[if mso]>
                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:155px;" arcsize="15%" strokecolor="#ffffff" fillcolor="#ff6f6f">
                  <w:anchorlock/>
                  <center style="color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;">My Account</center>
                </v:roundrect>
              <![endif]--><a href="<? echo site_url('paynow/'.$order_number); ?>"
              style="background-color:#ff6f6f;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Cabin', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;"><? echo lang('LABEL_PAY_NOW'); ?></a></br><i><?php echo lang('LABEL_PAYMENT_METHOD_CC'); ?></i></div>	
            </td>
          </tr>
          <?php } else if($customer_user_id > 0) {?>
          <tr>
            <td class="free-text">
              <? echo lang('LABEL_INVOICE_THANKS_TEXT'); ?>
            </td>
          </tr>
          <tr>
            <td class="button">
              <div><!--[if mso]>
                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:155px;" arcsize="15%" strokecolor="#ffffff" fillcolor="#ff6f6f">
                  <w:anchorlock/>
                  <center style="color:#ffffff;font-family:Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;">My Account</center>
                </v:roundrect>
              <![endif]--><a href="<? echo site_url(lang('PAGE_ACCOUNT_URL_EMAIL')); ?>"
              style="background-color:#ff6f6f;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Cabin', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;"><? echo lang('LABEL_MY_ACCOUNT_EMAIL'); ?></a></div>	
            </td>
          </tr>
          <?php } else { ?>
          <tr>
            <td class="free-text">
              <? echo lang('LABEL_INVOICE_THANKS_TEXT_NOUSER'); ?>
            </td>
          </tr>
          <?php } ?>
          <tr>
            <td class="w320">
              <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td class="mini-container-left">
                    <table cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td class="mini-block-padding">
                          <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:separate !important;">
                            <tr>
                              <td class="mini-block">
                                <span class="header-sm"><? echo lang('LABEL_SHIPPING_ADDRESS_EMAIL'); ?></span><br />
                                <? echo $shipping_address; ?>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class="mini-container-right">
                    <table cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td class="mini-block-padding">
                          <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:separate !important;">
                            <tr>
                              <td class="mini-block">
                             	<span class="header-sm"><? echo lang('LABEL_ORDER_EMAIL'); ?></span> #<? echo $order_number;?><br/>
                                <? echo $customer_name; ?>
                                <br/><br/>
                                <span class="header-sm"><? echo lang('LABEL_ORDER_DATE_EMAIL'); ?></span><br />
                                <? echo formattaData($order_date, 'd/m/Y'); ?><br /> 
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <? if($shipping_address_notes != '' || $order_notes != '') { ?>
                <tr>
                  <td class="mini-container-left" colspan="2" style="padding-right:15px !important">
                    <table cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td class="mini-block-padding">
                          <table cellspacing="0" cellpadding="0" width="100%" style="border-collapse:separate !important;">
                            <tr>
                              <td class="mini-block">
                              	<? 
								if($shipping_address_notes != '') {
                              		echo '<span class="header-sm">'. lang('LABEL_ADDRESS_NOTES_EMAIL').'</span><br/>';
                                	echo $shipping_address_notes . '<br/><br/>';
                                }
								if($order_notes != '') {
                                	echo '<span class="header-sm">'. lang('LABEL_ORDER_NOTES_EMAIL').'</span><br/>';
                                	echo $order_notes; 
                                }
                                ?>    
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <? } ?>
              </table>
            </td>
          </tr>
        </table>
      </center>
    </td>
  </tr>
  <tr>
    <td align="center" valign="top" width="100%" style="background-color: #ffffff;  border-top: 1px solid #e5e5e5; border-bottom: 1px solid #e5e5e5;">
      <center>
        <table cellpadding="0" cellspacing="0" width="600" class="w320">
            <tr>
              <td class="item-table">
                <table cellspacing="0" cellpadding="0" width="100%">
                  <tr>
                    <td class="title-dark" width="300">
                    	<? echo lang('LABEL_DESCRIPTION_EMAIL'); ?>
                    </td>
                    <td class="title-dark" width="163" style="text-align:center">
                        <? echo lang('LABEL_QTY_EMAIL'); ?>
                    </td>
                    <td class="title-dark" width="97">
                        <? echo lang('LABEL_TOTAL_EMAIL'); ?>
                    </td>
                  </tr>
				  
                  <!-- order_items -->
				  <? 
                    foreach($order_items_products as $item) {
                    ?>
                        <tr>
                            <td class="item-col item">
                              <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                  <td class="mobile-hide-img">
                                   <!-- <a href="<?// echo $detail_link;?>">-->
                                    <img width="110" height="92" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/cart/<? echo $item->url_img_piccola;?>" alt="item1">
                                   <!-- </a> -->
                                  </td>
                                  <td class="product">
                                    <span style="color: #4d4d4d; font-weight:bold;"><? echo $item->nome;?></span> <br />
                                    <? echo $item->codice;?><br />
                                    <? if($item->taglia != '')
                                         echo lang('LABEL_SIZE_EMAIL') . ': ' . $item->taglia . '<br />';
                                       if($item->colore_prodotto != '')	 
                                         echo lang('LABEL_COLOR_EMAIL') . ': ' .$item->colore_prodotto;
                                    ?>	 
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td class="item-col quantity" style="text-align:center">
                              <? echo $item->qty;?>
                            </td>
                            <td class="item-col">
                              <? echo stampaValutaHtml(($item->prezzo_scontato > 0 ? $item->prezzo_scontato : $item->prezzo), true, true);?>
                            </td>
                      </tr>
                    <?	
                    }
                    
                    foreach($order_items_variants as $item) {
                    ?>
                        <tr>
                            <td class="item-col item">
                              <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                  <td class="mobile-hide-img">
                                   <!-- <a href="<?// echo $detail_link;?>">-->
                                    <img width="110" height="92" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/cart/<? echo $item->url_img_piccola;?>" alt="item1">
                                   <!-- </a> -->
                                  </td>
                                  <td class="product">
                                    <span style="color: #4d4d4d; font-weight:bold;"><? echo $item->nome;?></span> <br />
                                    <? echo $item->codice;?><br />
                                    <? if($item->taglia != '')
                                         echo lang('LABEL_SIZE_EMAIL') . ': ' . $item->taglia . '<br />';
                                       if($item->colore_prodotto != '')	 
                                         echo lang('LABEL_COLOR_EMAIL') . ': ' .$item->colore_prodotto;
                                    ?>	 
                                  </td>
                                </tr>
                              </table>
                            </td>
                            <td class="item-col quantity" style="text-align:center">
                              <? echo $item->qty;?>
                            </td>
                            <td class="item-col">
                              <? echo stampaValutaHtml(($item->prezzo_scontato > 0 ? $item->prezzo_scontato : $item->prezzo), true, true);?>
                            </td>
                      </tr>
                    <?	
                    }
                ?>
                  <tr>
                    <td class="item-col item mobile-row-padding"></td>
                    <td class="item-col quantity"></td>
                    <td class="item-col price"></td>
                  </tr>
                  <tr>
                    <td class="item-col item">
                    </td>
                   <td class="item-col quantity" style="text-align:right; padding-right: 10px; border-top: 1px solid #cccccc;">
                      <span class="total-space"><? echo lang('LABEL_SUBTOTAL_ORDER_EMAIL'); ?></span><br/>
                      <? if($order_coupon_total > 0) {?>
                      <span class="total-space"><? echo lang('LABEL_COUPON_APPLY_EMAIL'); ?></span><br/>
                      <? } ?>
                      <span class="total-space"><? echo lang('LABEL_SHIPPING_EMAIL'); ?></span><br/>
                      <span class="total-space" style="font-weight: bold; color: #4d4d4d"><? echo lang('LABEL_TOTAL_EMAIL'); ?></span>
                    </td>
                    <td class="item-col price" style="text-align: left; border-top: 1px solid #cccccc;">
                      <span class="total-space"><? echo stampaValutaHtml($order_subtotal, true, true); ?></span><br />
                      <? if($order_coupon_total > 0) {?>
                      <span class="total-space" style="color:red"><? echo stampaValutaHtml($order_coupon_total, true, true);?></span><br/>
                      <? } ?>
                      <span class="total-space"><? echo stampaValutaHtml($order_shipping_total, true, true); ?></span> <br />
                      <span class="total-space" style="font-weight:bold; color: #4d4d4d"><? echo stampaValutaHtml($order_total, true, true); ?></span>
                    </td>
                  </tr>  
                </table>
              </td>
            </tr>
        </table>
      </center>
    </td>
  </tr>
  <tr>
    <td align="center" valign="top" width="100%" style="background-color: #f7f7f7; height: 100px;">
      <center>
        <table cellspacing="0" cellpadding="0" width="600" class="w320">
          <tr>
            <td style="padding: 25px 0 25px">
              <strong>&copy;<? echo date("Y") . ' ' . COMPANY_NAME; ?></strong> - <? echo lang('TEXT_EMAIL_FOOTER_RESERVED'); ?><br />
              <? echo COMPANY_ADDRESS; ?> <br />
              <? echo COMPANY_PHONE . ' - ' . COMPANY_EMAIL; ?>
            </td>
          </tr>
        </table>
      </center>
    </td>
  </tr>
</table>
</div>
</body>
</html>