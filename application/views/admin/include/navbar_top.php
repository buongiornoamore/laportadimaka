<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container-fluid">
        <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                <i class="material-icons visible-on-sidebar-mini">view_list</i>
            </button>
        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">&nbsp;<? echo $current_page_title; ?> </a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              <!--  <li>
                    <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="material-icons">dashboard</i>
                        <p class="hidden-lg hidden-md">Dashboard</p>
                    </a>
                </li>
               <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="material-icons">notifications</i>
                        <span class="notification">5</span>
                        <p class="hidden-lg hidden-md">
                            Notifications
                            <b class="caret"></b>
                        </p>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#">Mike John responded to your email</a>
                        </li>
                        <li>
                            <a href="#">You have 5 new tasks</a>
                        </li>
                        <li>
                            <a href="#">You're now friend with Andrew</a>
                        </li>
                        <li>
                            <a href="#">Another Notification</a>
                        </li>
                        <li>
                            <a href="#">Another One</a>
                        </li>
                    </ul>
                </li>-->
                <li>
                	<div class="btn-group" role="group" aria-label="..." style="margin-right:25px">
                		<a href="<?php echo site_url('it/home'); ?>" class="btn btn-sm btn-primary" id="admin-preview" target="_blank">Preview sito</a>
                    </div>
                </li>
                <li>
                	<div class="btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-sm btn-default btn-switch" data-value="frontend/Home" id="switch-online">Online</button>
                        <button type="button" class="btn btn-sm btn-default btn-switch" data-value="Comingsoon" id="switch-offline">Offline</button>
                    </div>
                </li>
                <li>
                	<!-- class="dropdown-toggle" data-toggle="dropdown" -->
                    <a href="" onClick="return showConfirmDialog('Conferma logout', 'Sei sicuro di voler abbandonare la sessione?', '', '', '', '', '', '', '<?php echo site_url('admin/logout')?>', 'false'); " title="<? echo lang("LABEL_LOGOUT"); ?>">
                        <i class="material-icons">person</i>
                        <p class="hidden-lg hidden-md">Profile</p>
                    </a>
                </li>
               <!-- <li class="separator hidden-lg hidden-md"></li>-->
            </ul>
           <!-- <form class="navbar-form navbar-right" role="search">
                <div class="form-group form-search is-empty">
                    <input type="text" class="form-control" placeholder="Search">
                    <span class="material-input"></span>
                </div>
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                    <i class="material-icons">search</i>
                    <div class="ripple-container"></div>
                </button>
            </form>-->
        </div>
    </div>
</nav>
<script type="text/javascript">
    $(document).ready(function() {
		var site_status = '<?php echo $this->router->routes['default_page']; ?>';
		$('.btn-switch[data-value="'+site_status+'"]').addClass('active');
		if(site_status == 'Comingsoon') 
			$('#admin-preview').show();
		else 
			$('#admin-preview').hide();
		
		$('.btn-switch').on('click', function(e){
		    e.preventDefault();
			if(!$(this).hasClass('active')) {
				var value = $(this).data('value');
				// modifica
				swal({
					title: 'Conferma modifica stato',
					text: 'Sei sicuro di voler cambiare lo stato del sito?',
					type: 'warning',
					showCancelButton: true,
					confirmButtonText: 'Si, procedi!',
					cancelButtonText: 'No, fermati',
					confirmButtonClass: "btn btn-success",
					cancelButtonClass: "btn btn-danger",
					buttonsStyling: false
				}).then(function() {
					$('.se-pre-con').show();
					var postData = {
					  'value' : value
					};
					$.ajax({
						url: '<? echo base_url();?>admin/updateSiteStatus',
					    type: 'POST',
						dataType: "HTML",
						async: true,
						data: postData,
						success: function(response) {
							$('.btn-switch').removeClass('active');
							if(value == 'Comingsoon') {
								$('#switch-offline').addClass('active');
								$('#admin-preview').show();
							} else {
								$('#switch-online').addClass('active');
								$('#admin-preview').hide();
							}
							$(".se-pre-con").delay(200).fadeOut("slow"); 
							swal({
								title: 'Operazione eseguita',
								text: 'Lo stato del sito adesso è ' + value,
								type: 'success',
								confirmButtonClass: "btn btn-success",
								buttonsStyling: false
							});
						},
						error: function () {
							$(".se-pre-con").delay(200).fadeOut("slow"); 
							swal({
								title: 'Errore!',
								text: 'Errore durante la modifica dello stato del sito.',
								type: 'error',
								confirmButtonClass: "btn btn-success",
								buttonsStyling: false
								});
						}
					});
				 
				}, function(dismiss) {
				  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
				  if (dismiss === 'cancel') {
					swal({
					  title: 'Operazione ignorata',
					  text: 'Non è stata effettuata nessuna operazione.',
					  type: 'error',
					  confirmButtonClass: "btn btn-info",
					  buttonsStyling: false
					});
				  }
				});
			}
		});
	});	
</script>