<!--   Core JS Files   -->
<?// if(isset($data['resourcetype']) && $data['resourcetype'] != 'CRUD') { ?>
	<!--<script src="<?// echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery-3.1.1.min.js" type="text/javascript"></script>
	<script src="<?// echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery-ui.min.js" type="text/javascript"></script>  --> 
<?// } ?>
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/bootstrap.min.js" type="text/javascript"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/material.min.js" type="text/javascript"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/moment.min.js"></script>
<!--  Charts Plugin 
<script src="<?// echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/chartist.min.js"></script>-->
<!--  Plugin for the Wizard -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    
<script src="<?// echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/bootstrap-notify.js"></script>-->
<!-- DateTimePicker Plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/nouislider.min.js"></script>
<!--  Google Maps Plugin   
<script src="https://maps.googleapis.com/maps/api/js"></script> -->
<!-- Select Plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/sweetalert2.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/cloudflare.promise.core.js"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery.tagsinput.js"></script>
<!-- Forms Validations Plugin -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/jquery.validate.min.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<!-- Material Dashboard javascript methods -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/material-dashboard.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/common_functions.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! 
<script src="<?// echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/demo.js"></script>-->
<script type="text/javascript">
    $(document).ready(function() {
		
	/*	$('.grocery-crud-table').delegate('.image-thumbnail', 'click', function(e){
			e.preventDefault();
			call_fancybox();
		});	
		
		$('.crud-form').delegate('.image-thumbnail', 'click', function(e){
			e.preventDefault();
			call_fancybox();
		});		*/

        // wait 2 sec to load all images in table
	    setTimeout(function(){  
          if ($('div#fancybox-frame:empty'))
     		console.log("TIMEOUT CALL fancybox");
     		call_fancybox();
        }, 2000);	
        
	});	

    var call_fancybox = function () {
		//If there is no thumbnail this means that the fancybox library doesn't exist
		if ($('.image-thumbnail').length > 0) {
			$('.image-thumbnail').fancybox({
				'transitionIn'	:	'elastic',
				'transitionOut'	:	'elastic',
				'speedIn'		:	600,
				'speedOut'		:	200,
				'overlayShow'	:	false
			});			
		}
	};
</script>		
    