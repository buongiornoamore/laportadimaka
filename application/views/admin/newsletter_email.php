<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo COMPANY_NAME . ' - ADMIN'; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
<!-- JS / CSS added to GROCERY CRUD template -->
<?php 
// import grocery and codigniter css
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<!-- END JS / CSS added to GROCERY CRUD template --> 
</head>
<body>
	<div class="wrapper">
        <? include('include/left_menu.php'); ?>
        <div class="main-panel">
            <? include('include/navbar_top.php'); ?>  
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
            			<div class="col-md-12">
                            <div class="card">
                            	<div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">send</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title"><?php echo $data['curr_function_title'];?>&nbsp;<a href="<?php echo site_url('admin/newsletter')?>" class="btn btn-danger btn-simple btn-little-icon" title="Ritorna alla newsletter">
                                            <i class="material-icons">arrow_back</i>
                                        </a>
                                    </h4>    
                                    <!-- mettere una dropdown per cambiare prodotto -->
	                                <div class="table-responsive">
                                    	<?php echo $output; ?>
                                    </div>
                                 </div>   
                        	</div>          
                        </div>    
                    </div>
                 </div>
            </div>        
        </div>
    </div>   
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" align="center" style="font-weight:800"></h4>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer text-center" align="center">
            <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
          </div>
        </div>
      </div>
    </div>
    
    <div id="listEmailModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg" style="overflow-y: initial !important">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" align="center" style="font-weight:800" id="listEmailModalTitle"></h4>
          </div>
          <div class="modal-body" style="height: 600px;overflow-y: auto;" align="center" id="listEmailModalBody"></div>
          <div class="modal-footer text-center" align="center">
            <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
          </div>
        </div>
      </div>
    </div>
    
    
</body>

<? include('include/footer_js_admin.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
		/*$('.header-tools > .floatL').append('<a id="freemail-btn" href="#" class="btn btn-default"><i class="fa fa-plus"></i> &nbsp;&nbsp;Email libera</a>');*/
		$('.header-tools > .floatL').append('&nbsp;<a id="emails-btn" href="#" class="btn btn-default"><i class="fa fa-users"></i> &nbsp;&nbsp;Lista indirizzi <span class="badge"><? echo count($data['curr_emails']); ?></span></a>');
		$(".header-tools > .floatL > .btn:first-child").hide();
		
		$('#emails-btn').on('click', function(e){
			e.preventDefault();
		    $('#listEmailModalTitle').html('Lista email per l\'invio [<? echo count($data['curr_emails']); ?>]');
		    $('#listEmailModalBody').html('<?
				foreach ($data['curr_emails'] as $address)
				{
					echo '<p style="background-color: #eee;">' . $address . '</p>';
				}
			?>'); 
			$('#listEmailModal').modal('show');
		});

		$('.grocery-crud-table').delegate('.btn-success', 'click', function(e){
		  e.preventDefault();
		  window.open($(this).attr('href'));
		  return false;
	/*	  $('.modal-title').html('Preview email HTML');
		  $('.modal-body').load( $(this).attr('href'), function() {
		  	$('#myModal').modal('show');
		  });*/
		});
		
		$('.grocery-crud-table').delegate('.btn-danger', 'click', function(e){	
		  e.preventDefault();
		  var href = $(this).attr('href');
		  console.log('href ' +href);
		  swal({
				title: 'Conferma invio email',
				text: 'Sei sicuro di voler inviare questa email?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Si, procedi!',
				cancelButtonText: 'No, fermati',
				confirmButtonClass: "btn btn-success",
				cancelButtonClass: "btn btn-danger",
				buttonsStyling: false
			}).then(function() {
			 	$('.se-pre-con').show();
				var postData = {
				  'contatti_list' : '<?php echo json_encode($data['curr_emails']); ?>',
				  'nomi_list' : '<?php echo json_encode($data['curr_names']); ?>',
				  'modulo' : '1'
				};
				$.ajax({
					url: href,
					type: 'POST',
					dataType: "HTML",
					async: true,
					data: postData,
					success: function(response) {
						$(".se-pre-con").delay(200).fadeOut("slow"); 
						swal({
							title: 'Email inviata',
							text: 'La tua email è stata inviata con successo !',
							type: 'success',
							confirmButtonClass: "btn btn-success",
							buttonsStyling: false
						});
					},
					error: function () {
						$(".se-pre-con").delay(200).fadeOut("slow"); 
						swal({
							title: 'Errore!',
							text: 'Errore durante l\'invio della email.',
							type: 'error',
							confirmButtonClass: "btn btn-success",
							buttonsStyling: false
							});
					}
				});
			 
			}, function(dismiss) {
			  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
			  if (dismiss === 'cancel') {
				swal({
				  title: 'Operazione ignorata',
				  text: 'Non è stata effettuata nessuna operazione.',
				  type: 'error',
				  confirmButtonClass: "btn btn-info",
				  buttonsStyling: false
				});
			  }
			});

			return false; // for good measure
		});
		
	
    });
</script>
</html>