<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><? echo lang('PAGE_HOME_TITLE') . ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="description" content="<? echo stripslashes(lang('PAGE_HOME_META_DESCRIPTION')); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
</head>
<body class="page-preloading">
  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->
  <!-- Page Wrapper -->
  <div class="page-wrapper">
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <? require_once('include/slider.php'); ?> <!-- Hero slider -->
    <?// require_once('include/filters.php'); ?> <!-- Filters Bar -->
     <!-- Features  space-top space-bottom padding-top-3x  -->
    <section class="container padding-bottom-3x">
      <div class="row">
        <!-- Feature -->
        <div class="col-md-3 col-sm-6">
          <div class="feature text-center">
            <div class="feature-icon">
              <i class="material-icons cake"></i>
            </div>
            <h4 class="feature-title"><? echo stripslashes(lang("HOME_FEATURE_1_TITLE")); ?></h4>
            <p class="feature-text"><? echo stripslashes(lang("HOME_FEATURE_1_DESC")); ?></p>
          </div>
        </div>
        <!-- Feature -->
        <div class="col-md-3 col-sm-6">
          <div class="feature text-center">
            <div class="feature-icon">
              <i class="material-icons party_mode"></i>
            </div>
            <h4 class="feature-title"><? echo stripslashes(lang("HOME_FEATURE_2_TITLE")); ?></h4>
            <p class="feature-text"><? echo stripslashes(lang("HOME_FEATURE_2_DESC")); ?></p>
          </div>
        </div>
        <!-- Feature -->
        <div class="col-md-3 col-sm-6">
          <div class="feature text-center">
            <div class="feature-icon">
              <i class="material-icons star"></i>
            </div>
            <h4 class="feature-title"><? echo stripslashes(lang("HOME_FEATURE_3_TITLE")); ?></h4>
            <p class="feature-text"><? echo stripslashes(lang("HOME_FEATURE_3_DESC")); ?></p>
          </div>
        </div>
        <!-- Feature -->
        <div class="col-md-3 col-sm-6">
          <div class="feature text-center">
            <div class="feature-icon">
              <i class="material-icons face"></i>
            </div>
            <h4 class="feature-title"><? echo stripslashes(lang("HOME_FEATURE_4_TITLE")); ?></h4>
            <p class="feature-text"><? echo stripslashes(lang("HOME_FEATURE_4_DESC")); ?></p>
          </div>
        </div>
      </div><!-- .row -->
    </section><!-- .container -->
    <!-- Shop / Categories -->
    <section class="<? echo $productsContainerClass; ?> padding-bottom-3x">
      <div class="row" id="products-div">
      </div><!-- .row -->
      <!-- Load More Btn -->
      <a href="<? echo site_url(lang('PAGE_SHOP_URL'));?>" class="load-more-btn space-top" id="load-more-btn"><? echo stripslashes(lang("HOME_SHOW_ALL_PRODUCTS")); ?></a>
    </section><!-- .container -->
    <!--<p align="center">In questo spazio possiamo inserire dei banner che pubblicizzano la spedizione gratuita o offerte speciali o ancora una lista di prodotti particolari, io consiglio dei banner con immagini e testo</p>-->
	<? require_once('include/footer.php'); ?> <!-- Footer -->
  </div><!-- .page-wrapper -->
  
 	<? require_once('include/common_header_js.php'); ?> <!-- Import js -->
	<script type="text/javascript">
		$(document).ready(function($){
		//	loadCartDropdown(true, false, false);
			// Defining a function to set size for #heroSlider 
			function fullscreen(){
				$('.singleSlide').css({
					width: $(window).width(),
					height: $(window).height()
				});
				$('.singleSlideImg').css({
					height: $(window).height() - $('#header-navbar').height()
				});
				$('.hero-slider').css({
					width: $(window).width(),
					height: $(window).height()
				});
				$('.hero-slider .slide').css({
					height: $(window).height() - $('#header-navbar').height()
				});
			}
			fullscreen();
			// Run the function in case of window resize
			$(window).resize(function() {
			   fullscreen();         
			});
		});
        $(window).load(function() {	
            //$(".se-pre-con").delay(200).fadeOut("slow"); 
            console.log('loadProductsCategories');
            loadProductsCategories();
        });
		function loadProductsCategories(){
			//$(".se-pre-con").show();
			return $.ajax({
				url: '<? echo base_url(); ?>frontend/Products/getProductsCategories',
				type: 'POST',
				dataType: "HTML",
				async: true,
				data: {},
				error: function(msg){
					console.log('error');
					//ShowPopupTime("Richiesta non inviata.<br/>Riprova.", 2000);
					return msg;
				},
				success: function(html){			
					$("#products-div").html(html);
					return true;
				}
			});
		} 
    </script>		
</body><!-- <body> -->

</html>
