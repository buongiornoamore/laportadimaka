<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><? echo lang('PAGE_ABOUT_TITLE') . ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="description" content="<? echo lang('PAGE_ABOUT_META_DESCRIPTION'); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
</head>
<body class="page-preloading">

  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->

  <!-- Page Wrapper -->
  <div class="page-wrapper">
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <!-- Container -->
    <? if(lang('PAGE_ABOUT_IMAGE') != "") { ?>
    <!-- Featured Image -->
    <div class="featured-image" style="background-image: url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/featured-image/<? echo lang('PAGE_ABOUT_IMAGE'); ?>);"></div>
    <? } ?> 
    <!-- Content -->
    <section class="container padding-top-3x padding-bottom-3x">
      <h1><? echo lang('PAGE_ABOUT_TITLE'); ?></h1>
      <div class="row">
        <div class="col-md-12 col-sm-12 padding-bottom">
          <p class="space-top" style="text-align:justify"><? echo lang('PAGE_ABOUT_DESCRIPTION'); ?></p>
        </div><!-- .col-md-5.col-sm-6 -->
      </div><!-- .row -->

        <!-- TEAM -->
    	<hr class="padding-bottom">
    	
          <h3 align="center"><?php echo lang('LABEL_TEAM'); ?></h3>
          <div class="row padding-top">
          <?php 
          foreach ($team as $t_item) {
		      ?>	  
              <!-- Teammate -->
              <div class="col-xs-6 col-md-3">
              <div class="teammate">
              <div class="teammate-thumb">
              <div class="social-bar text-center space-bottom">
              <? echo $t_item->staff_name; ?>
            <!--   <a href="#" class="sb-skype" data-toggle="tooltip" data-placement="top" title="Skype">
              <i class="socicon-skype"></i>
              </a>
              <a href="#" class="sb-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
              <i class="socicon-facebook"></i>
              </a>
              <a href="#" class="sb-google-plus" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google+">
              <i class="socicon-googleplus"></i>
              </a> -->
              </div><!-- .social-bar -->
              <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/team/<? echo $t_item->staff_image; ?>" alt="<? echo COMPANY_NAME; ?> Team - <? echo $t_item->staff_name; ?>">
              </div>
              <h4 class="teammate-name"><? echo $t_item->staff_name; ?></h4>
           <!--    <span class="teammate-position">Co-Founder, CEO</span> -->
              </div><!-- .teammate -->
              </div><!-- .col-xs-6.col-md-3 -->
              <?php 
		  }
		  ?>
          </div><!-- .row -->
        </section><!-- .container -->

    <!-- Video Popup
    <div class="fw-section padding-top-3x padding-bottom-3x" style="background-image: url(img/video_bg.jpg);">
      <div class="container padding-top-3x padding-bottom-3x text-center">
        <div class="space-top-3x space-bottom">

          <a href="https://player.vimeo.com/video/135832597?color=77cde3&title=0&byline=0&portrait=0" class="video-popup-btn">
            <i class="material-icons play_arrow"></i>
          </a>
        </div>
        <p class="space-bottom-2x">M-Store - your reliable partner.</p>
      </div>
    </div><!-- .fw-section --> 
	
    <? require_once('include/footer.php'); ?> <!-- Footer -->

  </div><!-- .page-wrapper -->

  <? require_once('include/common_header_js.php'); ?> <!-- Import js -->

</body><!-- <body> -->
<script type="text/javascript">
	$(window).load(function() {
    });
</script>
</html>
