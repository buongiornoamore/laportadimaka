<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><? echo lang('PAGE_CONTACTS_TITLE') . ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="description" content="<? echo lang('PAGE_CONTACTS_META_DESCRIPTION'); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
  <!--Google Maps API-->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByNmcHrKPDEd8MMUWG9qrpEXvF2Ligg4o"></script>
</head>

<!-- Body -->
<!-- Adding/Removing class ".page-preloading" is enabling/disabling background smooth page transition effect and spinner. Make sure you also added/removed link to page-preloading.js script in the <head> of the document. -->
<body class="page-preloading">

  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->

  <!-- Page Wrapper -->
  <div itemscope itemtype="http://schema.org/LocalBusiness" class="page-wrapper" >
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <!-- Container -->
   <!-- Google Map -->
    <!-- Data API:
         data-height="500" height of the map in pixels
         data-address="Your address" string
         data-zoom="number" to control map zoom when loaded
         data-disable-controls="false/true" enable/disable map controls like pan, zoom, etc.
         data-scrollwheel="true/false" enable/disable mouse scroll wheel zoom
         data-marker="path_to_your_image" path to custom marker image
         data-marker-title=" Your title" appears on marker hover
         data-styles="[array]" you can adjust the look and feel of your map. Recommend to use https://snazzymaps.com
     -->
     <div class="google-map"
          data-height="480"
          data-address="<? echo COMPANY_ADDRESS; ?>"
          data-zoom="8"
          data-disable-controls="false"
          data-scrollwheel="false"
          data-marker="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/map-marker.png"
          data-marker-title="<?= SITE_TITLE_NAME; ?>"
          data-styles='[{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#C6E2FF"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#C5E3BF"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#D1D1B8"}]}]'
     ></div><!-- .google-map -->

    <!-- Container -->
    <section class="container space-top-3x">
      <h1><? echo lang("MENU_CONTACTS");?> - <span itemprop="name"><?=COMPANY_NAME;?></span></h1>
      <div class="row padding-top">
        <div class="col-sm-5 padding-bottom-2x">
          <ul class="list-icon">
            <li>
              <i class="material-icons location_on"></i>
              	<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
              	<span itemprop="addressRegion">
			  	<?= COMPANY_ADDRESS; ?>
                </span>
                </span>
            </li>
            <li itemprop="telephone">
              <i class="socicon-whatsapp"></i><?= COMPANY_PHONE; ?>
            </li>
           <!-- <li>
              <i class="material-icons phone_iphone"></i>
              001 (800) 333-6578
            </li>-->
            <li>
              <i class="material-icons email"></i>
              <a href="mailto:<?= COMPANY_EMAIL; ?>"><span itemprop="email"><?= COMPANY_EMAIL; ?></span></a>
            </li>
          <!--  <li>
              <i class="socicon-skype"></i>
              <a href="#">skype_id</a>
            </li> -->
          </ul><!-- .list-icon -->
         <!-- <p>Orari: <span class="text-gray">10:00 / 19:00 - lun / sab</span></p>-->
          <span class="display-inline" style="margin-bottom: 6px;">Social: &nbsp;&nbsp;</span>
          <div class="social-bar display-inline">
            <a href="<?= FACEBOOK_LINK; ?>" class="sb-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
              <i class="socicon-facebook"></i>
            </a>

          <!--   <a href="#" class="sb-google-plus" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google+">
              <i class="socicon-googleplus"></i>
            </a>
           <a href="#" class="sb-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
              <i class="socicon-twitter"></i>
            </a> -->
            <a href="<?= INSTAGRAM_LINK; ?>" class="sb-instagram" data-toggle="tooltip" data-placement="top" title="" data-original-title="Instagram">
              <i class="socicon-instagram"></i>
            </a>
          </div><!-- .social-bar -->
        </div><!-- .col-sm-5 -->
        <div id="contacts-message-div" style="display:none;" class="col-sm-7 padding-bottom-2x">
          <div class="row padding-top">
               <div class="col-sm-12 padding-bottom" align="center" id="contacts-message" style="font-size:24px;min-height:80%x;padding-top:5px;">
                 </div>
            </div>
        </div>
        <div class="col-sm-7 padding-bottom-2x">
          <form method="post" class="ajax-form" id="contacts-form">
            <div class="contact-form container">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-element">
                    <input type="text" class="form-control" name="name" id="name" placeholder="<? echo lang("LABEL_NAME");?>" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-element">
                    <input type="email" class="form-control" name="email" id="email" placeholder="<? echo lang("LABEL_EMAIL");?>" required>
                  </div>
                </div>
              </div><!-- .row -->
              <div class="form-element">
                <textarea rows="6" class="form-control" style="resize:none" name="message" id="message" placeholder="<? echo lang("LABEL_MESSAGE");?>" required ></textarea>
              </div>
              <button type="button" id="submit-btn" class="btn btn-primary btn-block waves-effect waves-light space-top-none"><? echo lang("LABEL_SEND");?></button>
            </div>
            <div class="status-message"></div>
          </form>
        </div><!-- .col-sm-7 -->
      </div><!-- .row -->
    </section><!-- .container -->
    <!-- PAGE CONTENT -->

    <? require_once('include/footer.php'); ?> <!-- Footer -->

  </div><!-- .page-wrapper -->

  <? require_once('include/common_header_js.php'); ?> <!-- Import js -->
  <script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/gmap3.min.js"></script>

</body><!-- <body> -->
<script type="text/javascript">
	$(window).load(function() {
		loadCartDropdown(true, false, false);
    });

	$('#submit-btn').on('click', function(e) {
		e.preventDefault();
		$('#submit-btn').attr('disabled', 'disabled');
		if(
			validateText('name') &
			validateEmail('email') &
			validateText('message')
		)
		{
		  var formData = $('#contacts-form').serialize();
		  $.ajax({
				url: '<? echo base_url();?>mail/contactus',
				type: 'POST',
				dataType: "HTML",
				async: true,
				data: formData,
				error: function(msg){
					$('#submit-btn').removeAttr('disabled');
					swal({
					  position: 'center',
					  type: 'error',
					  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
					  showConfirmButton: false,
					  timer: 3000
					});
					return msg;
				},
				success: function(html){
					$('#submit-btn').removeAttr('disabled');
				//	$('#contacts-message').html(html);
					//$("#contacts-message-div").show();
					//$('#contacts-form').hide();
					$('#contacts-form')[0].reset();
					// messagge
					swal({
					  position: 'center',
					  type: 'success',
					  title: "<?php echo lang('MSG_SEND_CONTACT_US'); ?>",
					  showConfirmButton: false,
					  timer: 2000
					});
					return true;
				}
			});
		} else {
			$('#submit-btn').removeAttr('disabled');
		}
	}); 
  	$("#name").on("keyup blur", function(){validateText('name');});
	$("#message").on("keyup blur", function(){validateText('message');});
	$("#email").on("keyup blur", function(){validateEmail('email');});
</script>
</html>
