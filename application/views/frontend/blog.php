<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><? echo lang('PAGE_BLOG_TITLE') . ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="description" content="<? echo lang('PAGE_BLOG_META_DESCRIPTION'); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
</head>
<body class="page-preloading">
  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->
  <!-- Page Wrapper -->
  <div class="page-wrapper">
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <!-- Featured Image -->
    <? if(lang('PAGE_BLOG_IMAGE') != "") { ?>
    <!-- Featured Image -->
    <div class="featured-image" style="background-image: url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/featured-image/<? echo lang('PAGE_BLOG_IMAGE'); ?>);"></div>
    <? } ?> 
    <!-- Content -->
    <section class="container padding-top-3x padding-bottom" id="topSection">
      <div class="row">
      	<div class="col-sm-6">
      		<h1>Blog</h1>
      	</div>
      	<?//php echo print_r($categories); ?>
        <div class="col-sm-3">
          <div class="form-element form-select">
              <select class="form-control" name="categoriaSelect" id="categoriaSelect">
                <option value="0">Filtra per categoria</option>
                <?php foreach ($categories as $cat) {?>
          		<option value="<?php echo $cat->id_post_category; ?>" ><?php echo $cat->name_post_category; ?></option>
              	<?php } ?>	
              </select>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-element form-select">
            <select class="form-control" name="tagSelect" id="tagSelect">
                <option value="0">Filtra per tag</option>
          	   <?php foreach ($tags as $tag) {?>
          		<option value="<?php echo $tag->id_tag; ?>"><?php echo $tag->name_tag; ?></option>
          		<?php } ?>	
              </select>
          </div>
        </div>
      </div><!-- .row -->
      <!-- Post -->
      <div id="postList">
      </div>
	  <a href="#" class="load-more-btn space-top" id="load-more-btn"><? echo lang("LABEL_READ_MORE"); ?></a>
      </section>
    <!-- .container -->
    <? require_once('include/footer.php'); ?> <!-- Footer -->
  </div><!-- .page-wrapper -->
  <? require_once('include/common_header_js.php'); ?> <!-- Import js -->
</body><!-- <body> -->
<script type="text/javascript">
	$(window).load(function() {
		loadPosts(null, null, 0, false);
    });
    // load post from ajax call
	function loadPosts(category, tag, lastid, appendData){
		//console.log('loadPosts category: ' + category + " tag: " + tag + " lastid: " + lastid);
		$('.se-pre-con').show();
		return $.ajax({
			url: '<? echo base_url();?>frontend/Home/loadPosts',
			type: 'POST',
			dataType: "HTML",
			async: true,
			data: {'category': category, 'tag': tag, 'lastid': lastid},
			error: function(msg){
				console.log('error');
				$(".se-pre-con").delay(200).fadeOut("slow"); 
				return msg;
			},
			success: function(html){
				if(appendData) {
					$("#postList").append(html);
					scrollToAnchor('postClassLastAnchor'+lastid, $('#header-navbar').height(), event);
				} else {	
					$("#postList").html(html);
				}	
				// on change
				$('#categoriaSelect').on('change', function(event) {
					event.preventDefault();
					$('#tagSelect').val(0);
					loadPosts($(this).val(), null, 0, false);
// 					scrollToAnchor('topSection', $('#header-navbar').height(), event);
				});
				$('.catLink').on('click', function(event) {
					event.preventDefault();
					$('#tagSelect').val(0);
					$('#categoriaSelect').val($(this).attr('data-id'));
					loadPosts($(this).attr('data-id'), null, 0, false);
 					scrollToAnchor('topSection', $('#header-navbar').height(), event);
				});
				$('#tagSelect').on('change', function(event) {
					event.preventDefault();
					$('#categoriaSelect').val(0);
					loadPosts(null, $(this).val(), 0, false);
// 					scrollToAnchor('topSection', $('#header-navbar').height(), event);
				});
				$('.tagLink').on('click', function(event) {
					event.preventDefault();
					$('#categoriaSelect').val(0);
					$('#tagSelect').val($(this).attr('data-id'));
					loadPosts(null, $(this).attr('data-id'), 0, false);
 					scrollToAnchor('topSection', $('#header-navbar').height(), event);
				});
				$('#load-more-btn').on('click', function(event) {
					event.preventDefault();
			//		console.log('more btn from: ' + $(".postClass:last").attr("data-id"));
					loadPosts($('#categoriaSelect').val(), $('#tagSelect').val(), $(".postClass:last").attr("data-id"), true);
				});
				$(".se-pre-con").delay(200).fadeOut("slow"); 
				if($(".postClass:last").attr("data-moreload") <= 0)
					$('#load-more-btn').hide();
				else 
					$('#load-more-btn').show();
				return true;
			}
		});
	}
</script>
</html>