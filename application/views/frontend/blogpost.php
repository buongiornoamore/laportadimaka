<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><? echo lang('PAGE_BLOG_TITLE') . ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="description" content="<? echo lang('PAGE_BLOG_META_DESCRIPTION'); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
</head>
<body class="page-preloading">
  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->
  <!-- Page Wrapper -->
  <div class="page-wrapper">
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <!-- Featured Image -->
    <? if(lang('PAGE_BLOG_IMAGE') != "") { ?>
    <!-- Featured Image -->
    <div class="featured-image" style="background-image: url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/featured-image/<? echo lang('PAGE_BLOG_IMAGE'); ?>);"></div>
    <? } ?> 
    <!-- Content -->
    <div itemscope class="page-wrapper">
        <section class="fw-section bg-gray">
          <div class="container">
            <div class="product-info padding-top-2x text-center">   
        	    <div class="post-content">
                    <!-- Content should always be inside "article.single-post" -->
                    <article class="single-post">
                      <a href="<?php echo site_url(lang('PAGE_BLOG_URL')); ?>"><?php echo lang('LABEL_BACK'); ?></a>
                      <h2><b><? echo $post->titolo_post; ?></b></h2>
                      <div class="row">
                        <div class="col-xs-12">
                        	<?php 
                             if($post->immagine_post != null && $post->immagine_post != '') {
                                echo '<img src="'. ASSETS_ROOT_FOLDER_FRONTEND_IMG .'/blog/'.$post->immagine_post.'" class="space-bottom" alt="'. SITE_TITLE_NAME . ' | ' . $post->titolo_post .'">';
                            }
                            if($post->video_url_post != null && $post->video_url_post != '') {
                              echo '<div class="embed-responsive embed-responsive-16by9 space-bottom">
                                <iframe src="'. $post->video_url_post .'" allowfullscreen></iframe>
                              </div>';
                            }
                            ?>   
                        </div>
                      </div>
                      <?php echo $post->contenuto_post; ?>
                      <hr>
                      <div class="blog-post-meta">
                        <div class="column">
                        <!--   <span>by </span>
                          <a href="#">Admin</a>
                          <span class="divider"></span> -->
                          <a href="#"><?php echo formattaData($post->data_post, 'd/m/Y'); ?></a>
                          <span class="divider"></span>
                          <span><b><? echo lang("LABEL_CATEGORY"); ?></b></span>
                          <?    
            				  $catList = explode(",", $post->cats);
            				  foreach ($catList as $cat) {
            				      $catSplit = explode("|", $cat);
            				      //echo '<a href="'.site_url(lang('PAGE_BLOG_URL')).'" class="catLink" data-id="'.$catSplit[1].'">' . $catSplit[0] .'</a>&nbsp;';
            				      echo $catSplit[0].'&nbsp;';
            				  }
            				  if($post->tags != NULL && $post->tags != '') {
            				    echo '<span class="divider"></span>';
            				    $tagList = explode(",", $post->tags);
                            	foreach ($tagList as $tag) {
                            	    $tagSplit = explode("|", $tag);
                            	   // echo '<a href="'.site_url(lang('PAGE_BLOG_URL')).'" class="tagLink" data-id="'.$tagSplit[1].'">#' . $tagSplit[0] .'</a>&nbsp;';
                            	    echo '#'.$tagSplit[0].'&nbsp;';
                            	}
            				}
                          ?> 
                        </div>
                        <div class="column">
                          <a href="#">
                            <i class="material-icons comment"></i>
                            <?php echo count($comments); ?>
                          </a>
                        </div>
                      </div><!-- .blog-post-meta -->
                    </article><!-- .single-post -->
                </div>
                <br>
                <?
                if(count($comments) > 0) {  
                    echo '<h3><b>'.lang('LABEL_COMMENTS').'</b></h3>';
                    foreach ($comments as $comm) {
                        echo '<p align="left" style="margin:0 !important;"><b>' . $comm->post_comments_date . '</b> | ' . $comm->post_comments_username . '</p>';
                        echo '<p align="justify" style="margin:0 !important;">' . $comm->post_comments_text . '</p>';
                        echo '<hr>';
                    }
                }
                ?>
                <h4><b><?php echo lang('LABEL_COMMENTS_NEW'); ?></b></h4>
                <form method="post" class="ajax-form" id="comment-form">
                    <div class="contact-form container">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-element">
                            <input type="text" class="form-control" name="name" id="name" placeholder="<? echo lang("LABEL_NAME");?>" required>
                          </div>
                        </div>
                      </div><!-- .row -->
                      <div class="form-element">
                        <textarea rows="6" class="form-control" style="resize:none" name="message" id="message" placeholder="<? echo lang("LABEL_MESSAGE");?>" required ></textarea>
                      </div>
                      <button type="button" id="submit-btn" class="btn btn-primary btn-block waves-effect waves-light space-top-none"><? echo lang("LABEL_SEND");?></button>
                    </div>
                    <div class="status-message"></div>
                    <input type="hidden" name="postid" id="postid" value="<?php echo $post->id_post; ?>">
                  </form>      
                  <br>       
               </div>      
            </div><!-- .product-info -->
          </div><!-- .container -->
        </section><!-- .fw-section.bg-gray -->   
    </div>
    <!-- .container -->
    <? require_once('include/footer.php'); ?> <!-- Footer -->
  </div><!-- .page-wrapper -->

  <? require_once('include/common_header_js.php'); ?> <!-- Import js -->

</body><!-- <body> -->
<script type="text/javascript">
	$(window).load(function() {
    });

	$('#submit-btn').on('click', function(e) {
		e.preventDefault();
		$('#submit-btn').attr('disabled', 'disabled');
		if(
			validateText('name') &
			validateText('message')
		)
		{
		  var formData = $('#comment-form').serialize();
		  $.ajax({
				url: '<? echo base_url();?>frontend/Home/addcomment',
				type: 'POST',
				cache: false,
    			async: true,
				data: formData,
				error: function(msg){
					$('#submit-btn').removeAttr('disabled');
					swal({
					  position: 'center',
					  type: 'error',
					  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
					  showConfirmButton: false,
					  timer: 3000
					});
					return msg;
				},
				success: function(messJson){
    				var json = $.parseJSON(messJson);
    				swal({
      				  type: json['type'],
      				  title: json['title'],
      				  html: json['message'],
      				  confirmButtonClass: "btn btn-success",
      				  showCancelButton: false,
      				  buttonsStyling: false,
      				  showConfirmButton: false,
  					  timer: 3000,
  					  position: 'center'
      				}).then(function () {	
          			}).catch(swal.noop);

					// clean fields
      				$("#name").val('');
					$("#message").val('');
					$('#submit-btn').removeAttr('disabled');
      				return true;
				}
			});
		} else {
			$('#submit-btn').removeAttr('disabled');
		}
	}); 
  	$("#name").on("keyup blur", function(){validateText('name');});
	$("#message").on("keyup blur", function(){validateText('message');});
</script>
</html>