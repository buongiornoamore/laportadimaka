<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login<? echo ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
   <? require_once('include/common_header_js.php'); ?> <!-- Import js -->
</head>
<body class="page-preloading">
  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->
  <!-- Page Wrapper -->
  <div class="page-wrapper">
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <!-- Featured Image
    <div class="featured-image" style="background-image: url(< ? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/featured-image/faq.jpg);"></div>
     -->
    <!-- Content -->
    <section class="container padding-top-3x padding-bottom-2x">
      <!-- <h1>< ? echo lang('LABEL_USER_ACCOUNT'); ?></h1> -->

      <div class="row padding-top">
        <div class="col-md-5 padding-bottom" id="div-login-form">
          <!--  div id="infoMessage" class="text-danger">< ? php echo $this->session->flashdata('message'); ? ></div --> 
          <h3>Login</h3>
          <form method="post" id="login-form" action="<? echo site_url(lang('PAGE_LOGIN_URL'));?>" accept-charset="utf-8">
            <input type="text" class="form-control" id="identity" name="identity" placeholder="<? echo lang('login_identity_label'); ?>" required />
            <input type="password" class="form-control" id="password_login" name="password_login" placeholder="<? echo lang('login_password_label'); ?>" required />
            <div class="form-footer">
              <div class="rememberme" align="center">
                <label class="checkbox">
                  <input type="checkbox" checked id="remember" name="remember"> <? echo lang('login_remember_label'); ?>
                </label>
              </div>
              <div class="form-submit">
                <button type="button" id="btn_accedi" name="btn_accedi" class="btn btn-primary btn-block waves-effect waves-light"><?php echo lang('login_submit_btn');?></button>
              </div>
            </div>
          
       	  <p align="center"><a id="id_href_forgot_password" href="#"><?php echo lang('login_forgot_password');?></a></p>
       	  </form><!-- .login-form -->
        </div><!-- .col-md-4 -->
        <div class="col-md-5 col-md-offset-1"  id="div-register-form" >
          <!-- <div id="infoMessage" class="text-danger">< ?php echo $this->session->flashdata('register_message');? ></div> -->
          <h3><?php echo lang('LABEL_USER_REGISTER');?></h3>
          <!-- <div id="infoMessage" class="text-danger">< ?php echo $this->session->flashdata('message');?></div> -->
          <div class="inner">
            <form method="post" id="register-form" action="<? echo site_url('register');?>" accept-charset="utf-8"> <!-- < ? echo site_url();? >register -->
              <input type="text" id="first_name" name="first_name" class="form-control" placeholder="<?php echo lang('create_user_fname_label');?>" required>
              <input type="text" id="last_name" name="last_name" class="form-control" placeholder="<?php echo lang('create_user_lname_label');?>" required>
              <input type="email" id="email" name="email" class="form-control" placeholder="<?php echo lang('create_user_email_label');?>" required>
              <input type="password" id="password" name="password" class="form-control" placeholder="<?php echo lang('create_user_password_label');?>" required>
              <input type="password" id="password_confirm" name="password_confirm" class="form-control" placeholder="<?php echo lang('create_user_password_confirm_label');?>" required>
              <div class="form-footer">
                <div class="rememberme"></div>
                <div class="form-submit">
<!--                  <button type="submit" class="btn btn-primary btn-block waves-effect waves-light">SIGNAP - Submit</button>   -->
                  <button type="button" id="btn_registrati" name="btn_registrati" class="btn btn-primary btn-block waves-effect waves-light"><?php echo lang('LABEL_USER_SIGN_UP');?></button>
                </div>
              </div>
              <input type="hidden" id="fase_registrazione" name="fase_registrazione" value="0" />
              
            </form><!-- .login-form -->
          </div><!-- .inner -->
        </div><!-- .col-md-4.col-md-offset-1 -->
        <!--
        <div class="col-md-3 padding-top-2x">
          <a href="#" class="social-signup-btn ssb-facebook">
            <i class="socicon-facebook"></i>
            <span>< ?php// echo lang('LABEL_SIGN_UP_FACEBOOK');?></span>
          </a>
          <a href="#" class="social-signup-btn ssb-google">
            <i class="socicon-googleplus"></i>
            <span>< ?php// echo lang('LABEL_SIGN_UP_GOOGLE');?></span>
          </a>
          <a href="#" class="social-signup-btn ssb-twitter">
            <i class="socicon-twitter"></i>
            <span>< ?php// echo lang('LABEL_SIGN_UP_TWITTER');?></span>
          </a>
        </div>
        -->
 <!-- *********************** INDIRIZZO DI FATTURAZIONE *********************** -->
          <div id="div_indirizzo_fatt_form" class="col-md-8 col-md-offset-1 well-lg">
          <form method="post" id="indirizzo_fatturazione-form" action="<? echo site_url('salvaIndirizzoSpedizione');?>" accept-charset="utf-8" ">
                <div class="row">
                    <div class="form-element col-sm-12"><h3><? echo lang('LABEL_BILLING_ADDRESS'); ?></h3></div>
                </div>          			
                <div class="row">
                    <div class="form-element col-sm-9">
                      	<input type="text" id="indirizzo" name="indirizzo" class="form-control" placeholder="<? echo lang("create_user_address_label"); ?>" required />
                    </div>
                    <div class="form-element col-sm-3">
                    	 <input type="text" id="civico" name="civico" class="form-control" placeholder="<? echo lang("create_user_street_number_label"); ?>" required >
                     </div>
                </div>
                <div class="row">
	                <div class="col-sm-3">
		                  <div class="form-element">
		                     <input type="text" id="citta"  name="citta" class="form-control" placeholder="<? echo lang("create_user_city_label"); ?>" required />
	                    </div>
	                 </div>
	                  <div class="col-sm-3">
		                <div class="form-element">
		                	<input type="text" id="cap"  name="cap" class="form-control" placeholder="<? echo lang("create_user_cap_label"); ?>" required />
		                </div>
	                  </div>
	                 <div class="col-sm-6">
	                    <div class="form-element">
	                      <input type="text" id="nazione"  name="nazione" class="form-control" placeholder="<? echo lang("create_user_state_label"); ?>" required />
	                    </div>
	                </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 text-right mobile-center">
                    <input type="text" id="note" name="note" class="form-control" placeholder="<? echo lang("create_user_addressnotes_label"); ?>" >
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 mobile-center" >
	                <div class="form-element">
	                    <input type="text" id="riferimento" name="riferimento" class="form-control" placeholder="<? echo lang("LABEL_ADDRESS_REF"); ?>" />
	                     <input type="hidden" id="id_cliente_fatt" name="id_cliente_fatt" value="<? echo $this->session->userdata('id_cliente') ?>" />
	                </div>
                  </div>
                </div>
                <div class="col-sm-6 text-right mobile-center">
                	<button type="button" id="indirizzo_fatturazione_esci" class="btn btn-primary waves-effect waves-light">ESCI</button>
                   <button type="button" id="submit-indirizzo_fatturazione-form" class="btn btn-primary waves-effect waves-light" value=""><? echo lang("LABEL_UPDATE"); ?></button>
                </div>
		</form>
		</div>
<!-- *********************** INDIRIZZO DI FATTURAZIONE *********************** -->

      </div><!-- .row -->
    </section><!-- .container -->
	<? require_once('include/footer.php'); ?> <!-- Footer -->
  </div><!-- .page-wrapper -->
</body><!-- <body> -->	
    <script type="text/javascript">
    $(window).load(function() {
		loadCartDropdown(true, false, false);
    });

	$(document).ready(function() {

	  $("#div_indirizzo_fatt_form").hide();

	  $("#btn_registrati").click(function(e){
		 e.preventDefault();
		 resetLoginForm();
		 if(validateRegister()){
		     $.ajax({  
			   	  type: 'POST',
			   	  url: '<? echo site_url('register');?>',  
			   	  data: 'first_name=' + $("#first_name").val() + 
					   	  '&last_name=' + $("#last_name").val() + 
					   	  '&email=' + $("#email").val() + 
					   	  '&password=' + $("#password").val() + 
					   	  '&password_confirm=' + $("#password_confirm").val(),				   	
			   	  dataType: 'json',
			   	  async: true,
			   	  success: function(risposta) {  
			   		console.log("Siamo nella pagina di login : risposta " + risposta);
			   		//$('#register-form')[0].reset();
				 	swal({
			 			  position: 'center',
			    	  		  type: 'info',
			    	  		html:	risposta.messaggio, 
			    	    	  animation: false
			 	  	});
			 	  	if(risposta.return_id != ''){
			 	  		/*$("#div-login-form").hide();
			 	  		$("#div-register-form").hide();
			 	  		$("#div_indirizzo_fatt_form").show();
			 	  		$("#id_cliente_fatt").val(risposta.return_id)
			 	  		*/
			 	  		$("#identity").val( $("#email").val() )
			 	  		$("#password_login").val( $("#password").val() );
			 	  		$("#fase_registrazione").val('1');
			 	  		$("#btn_accedi").click();
				 	}
			   	  },
			   	  error: function(error){
			   		console.log("Siamo nella pagina di login : error "  );
				 	swal({
			 			  position: 'center',
			    	  		  type: 'error',
			    	  		html:	'----' , 
			    	    	  animation: false
			 	  	});
			   	  } 
		     });
		 }else{  }
		     		   
		  });

		  $("#btn_accedi").click(function(e){
			  e.preventDefault();
			  resetRegisterForm();
			    console.log("Siamo nella pagina di login : ");
			    if( validateText('identity') & validateText('password_login') ){
			     $.ajax({  
				   	  type: 'POST',
				   	  url: '<? echo site_url(lang('PAGE_LOGIN_URL'));?>',  
				   	  data: {"identity": $("#identity").val(), "password_login": $("#password_login").val(), "fase_registrazione" : $("#fase_registrazione").val() },
				   	  dataType: 'html',
				   	  // Provare con un ritorno json 
				   	  async: true,
				   	  success: function(risposta) {  
				   		console.log("Siamo nella pagina di login : risposta " + risposta);
				   		$('#login-form')[0].reset();
				   		if(risposta!='ok'){
				   			$('#login-form')[0].reset();
						 	swal({
					 			  position: 'center',
					    	  		  type: 'success',
					    	  		html:	risposta , 
					    	    	  animation: false
					 	  	});
						 	<?php unset($_SESSION['message']); ?>
				   		}else{
				   			$(window.location).attr('href', '<? echo site_url('it/account');?>');
				   		}
			   		
				   	  },
				   	  error: function(){
				   		console.log("Siamo nella pagina di login : error " );
				   		alert("Chiamata fallita!!!");
				   	  } 
			     });
			    }
			   
			  });

		     $("#indirizzo_fatturazione_esci").click(function(){
			 	swal({
		 			  position: 'center',
		    	  	  type: 'info',
		    	  	  html:	'<? echo lang('MSG_BILLING_ADDRESS_NECESSARY');?>', 
		    	      animation: false
		 	  	});
		 	  	
		     	$("#div_indirizzo_fatt_form").hide();
	 	  		$("#div-login-form").show();
	 	  		$("#div-register-form").show();
	 	  		resetRegisterForm();
	 	  		cleanFormIndirizzoFatt();
		 	  		
		     });
		     			
			function validateRegister() {
				if(
					validateText('first_name') &
					validateText('last_name') &
					validateText('email') &
					validateText('password') &
					validateText('password_confirm')
				)
				{ return true; } else { return false; }
			}

			function validateIndirizzoFatt() {
				if(
					validateText('indirizzo') &
					validateNumeric('civico') &
					validateNumeric('cap') &
					validateText('citta') &
					validateText('nazione')
				)
				{ return true; } else { return false; }
			}
		
		function resetLoginForm() {
			$("#password_login").val('');
			$("#password_login").removeClass('has-error');
			$("#identity").val('');
			$("#identity").removeClass('has-error');
		}
			
		function resetRegisterForm() {
			$('#first_name').val('');
			$("#first_name").removeClass('has-error');
			$('#last_name').val('');
			$("#last_name").removeClass('has-error');
			$('#email').val('');
			$("#email").removeClass('has-error');
			$('#password').val('');
			$("#password").removeClass('has-error');
			$('#password_confirm').val('');
			$("#password_confirm").removeClass('has-error');
		}


		function validateForgot() {
			if(
				validateText('identity')
			)
			{ return true;alert('1-true'); } else { return false;alert('1-false'); }
		}
		
		  $("#id_href_forgot_password").click(function(e){
				 e.preventDefault();
				 //resetLoginForm();
				 if(validateForgot()){
				     $.ajax({  
					   	  type: 'POST',
					   	  url: '<? echo site_url('forgot_password');?>',  
					   	  data: {"identity": $("#identity").val()},
					   	  dataType: 'json',
					   	  async: true,
					   	  success: function(risposta) {  
					   		console.log("Siamo nella pagina di login : risposta " + risposta);
					   		//$('#register-form')[0].reset();
						 	swal({
					 			  position: 'center',
					    	  		  type: 'info',
					    	  		html:	risposta.messaggio, 
					    	    	  animation: false
					 	  	});
					   	  },
					   	  error: function(error){
					   		console.log("Siamo nella pagina di login : error "  );
						 	swal({
					 			  position: 'center',
					    	  		  type: 'error',
					    	  		html:	error.messaggio , 
					    	    	  animation: false
					 	  	});
					   	  } 
				     });
				 }else{  }
				     		   
				  });
				  
	function validateIndirizzoFatt() {
		if(
			validateText('indirizzo') &
			validateText('civico') &
			validateText('cap') &
			validateText('citta') &
			validateText('nazione')
		)
		{ return true; } else { return false; }
	}


	function cleanFormIndirizzoFatt() {
		$('#indirizzo').val('');
		$("#indirizzo").removeClass('has-error');
		$('#civico').val('');
		$("#civico").removeClass('has-error');
		$('#cap').val('');
		$("#cap").removeClass('has-error');
		$('#citta').val('');
		$("#citta").removeClass('has-error');
		$('#nazione').val('');
		$("#nazione").removeClass('has-error');

	}
	
	  $("#submit-indirizzo_fatturazione-form").click(function(e){
			 e.preventDefault();
			 resetLoginForm();
			 if(validateRegister()){
			     $.ajax({  
				   	  type: 'POST',
				   	  url: '<? echo base_url();?>frontend/Account/salvaIndirizzoFatturazione',  
				   	  data: 'indirizzo=' + $("#indirizzo").val() + 
						   	  '&cap=' + $("#cap").val() + 
						   	  '&citta=' + $("#citta").val() + 
						   	  '&note=' + $("#note").val() + 
						   	  '&citta=' + $("#citta").val() +
						   	'&id_cliente_fatt=' + $("#id_cliente_fatt").val(),
				   	  dataType: 'json',
				   	  async: true,
						error: function(response){
							//$(".se-pre-con").delay(200).fadeOut("slow"); 
							swal({
							  position: 'center',
							  type: 'error',
							  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
							  showConfirmButton: false,
							  timer: 3000
							});
							return response;
						},
						success: function(response){
							console.log(response);
							//$(".se-pre-con").delay(200).fadeOut("slow");
				 	  		$("#div-login-form").show();
				 	  		$("#div-register-form").show();
				 	  		$("#div_indirizzo_fatt_form").hide();
				 	  		resetRegisterForm();
				 	  		cleanFormIndirizzoFatt();
							swal({
								title: response.title,
								text: response.text,
								type: response.type,
								showConfirmButton: false,
								timer: 3000
							}).catch(swal.noop);

							return true;
						} 
			     });
		 }else{ console.log("Siamo nella pagina di login : nell else " );  }
		     		   
		  });
		  		
	});
</script>
</html>
</html>