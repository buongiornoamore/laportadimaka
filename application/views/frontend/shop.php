<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><? echo lang('PAGE_SHOP_TITLE') . ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="description" content="<? echo lang('PAGE_SHOP_META_DESCRIPTION'); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
</head>
<body class="page-preloading">

  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->

  <!-- Page Wrapper -->
  <div class="page-wrapper">
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <? require_once('include/filters.php'); ?> <!-- Filters Bar -->
    <? require_once('include/footer.php'); ?> <!-- Footer -->
  </div><!-- .page-wrapper -->

 	<? require_once('include/common_header_js.php'); ?> <!-- Import js -->
	<script type="text/javascript">
        $(window).load(function() {	
       //     loadCartDropdown(true, false, false);
			console.log('url <? echo $category_url; ?>');
			loadFilterCategories('<? echo $category_url; ?>');
        });
		function loadProducts(htmlType, start_category_url){
			console.log('loadProducts category: ' + $('.filter-category-li.active a').data('category') + ' - load_from: ' + $('#load_from').val());
			if(!$('.se-pre-con').is(':visible')) {
				$('.se-pre-con').show();
			}
			var tags = '';
			var total = $(".tag-a.active").length;
			$(".tag-a.active").each(function( index ) {
				//console.log( index + ": " + $(this).data('id') );
			  	if (index === total - 1) {
        			// this is the last one
					tags += $(this).data('id');
    			} else {
					tags += $(this).data('id') + ',';
				}
			});

			var postData = {
			  'category_url' : (start_category_url != '') ? start_category_url : $('.filter-category-li.active a').data('category'),
			  'sort' : $('.widget-sorting-li.active a').data('sort'),
			  'range_min' : $('#ui-range-value-min').val(),
			  'range_max' : $('#ui-range-value-max').val(),
			  'tags' : tags,
			  'load_from' : $('#load_from').val()
			};

			console.log(postData);
			//$(".se-pre-con").show();
			return $.ajax({
				url: '<? echo base_url();?>frontend/Products/getProducts',
				type: 'POST',
				dataType: "HTML",
				async: true,
				data: postData,
				error: function(msg){
					console.log('error');
					$(".se-pre-con").delay(200).fadeOut("slow"); 
					return msg;
				},
				success: function(html){
					console.log('success ' + htmlType);
					if(htmlType == 'append') {
						$("#load_from").remove();
						$("#products-div").append(html);
						//console.log('success append');
					} else {
						$("#products-div").html(html);
						//console.log('success fresh');
					}
					if($("#load_from").val() > 0) {
						$("#load_from_sup").html($("#load_from").data('morecount'));
						$("#load-more-btn").show();
					} else {
						$("#load-more-btn").hide();
					}
					if($("#search-box").hasClass('open')) {
						$("#search_text").val('');
						$("#search-icon-btn").click();
					}
					// set current ative filter category
					if(start_category_url != '') {
						//$('.filter-category-li').removeClass('active');
						//$('*[data-category="'+start_category_id+'"]').closest("li").addClass('active');
						setCategoryActive(start_category_url);
					}
					$('.add-to-whishlist').on('click', function(event) {
						event.preventDefault();			
						addToWishList($(this).data('id'));
					});
					$(".se-pre-con").delay(200).fadeOut("slow");  
					return true;
				}
			});
		}
		function loadFilterCategories(category_url){
			console.log('loadFilterCategories ' + category_url);
			$('.se-pre-con').show();
			return $.ajax({
				url: '<? echo base_url();?>frontend/Products/getFilterCategories',
				type: 'POST',
				dataType: "HTML",
				async: true,
				data: {'category_url': category_url},
				error: function(msg){
					console.log('error');
					$(".se-pre-con").delay(200).fadeOut("slow"); 
					return msg;
				},
				success: function(html){
					//console.log('success ');
					$("#filter-categories").html(html);
					$("#current_filter_category").val(category_url);
				//	loadFilterTags();
					loadProducts('', '<? echo $category_url; ?>');
					$('#search-icon-btn').on('click', function(event) {
						event.preventDefault();
						scrollToAnchor('filter-categories', $('#header-navbar').height(), event);
					});
					// filter category action
					$('.filter-category').on('click', function(event) {
						event.preventDefault();
						// reload only if different category selection
						console.log($("#current_filter_category").val() + '  ' + $(this).data('category'))
						if($("#current_filter_category").val() != $(this).data('category') || $("#search-box").hasClass('open')) {
							//$('.filter-category-li').removeClass('active');
							//$(this).closest("li").addClass('active');
							setCategoryActive($(this).data('category'));
							console.log("Cat "+$(this).data('category'));
							// reset start category pagination
							$('#load_from').val(0);
							$("#current_filter_category").val($(this).data('category'));
							console.log("filter-category loadProducts()");
							loadProducts('', '');
							scrollToAnchor('filter-categories', $('#header-navbar').height(), event);
						}
					});
					$('.widget-sorting-link').on('click', function(event) {
						event.preventDefault();
						$('.widget-sorting-li').removeClass('active');
						$(this).closest("li").addClass('active');
						console.log("Order "+$('.widget-sorting-li.active a').data('sort'));
						$('#load_from').val(0);
						loadProducts('', '');
						scrollToAnchor('filter-categories', $('#header-navbar').height(), event);
					});
					$('#range-slider-btn').on('click', function(event) {
						//console.log("Range min: " + $('#ui-range-value-min').val() + ' max: ' + $('#ui-range-value-max').val());
						loadProducts('', '');
						scrollToAnchor('filter-categories', $('#header-navbar').height(), event);
					});
					$('#load-more-btn').on('click', function(event) {
						console.log("load-more-btn loadProducts()");
						loadProducts('append', '');
					});
					$('#search_text_btn').on('click', function(event) {
						console.log("search_text_btn searchProducts()");
						if($('#search_text').val() != '') {
							searchProducts();
						} else {
							$('#load_from').val(0);
							loadProducts('', '');
						}
						scrollToAnchor('filter-categories', $('#header-navbar').height(), event);
						// Clear filters
						clearFilters(false);
						setCategoryActive('');
					});
					$('#search_text').keyup(function(e){
						if(e.keyCode == 13)
						{
							console.log("search_text_btn searchProducts()");
							if($('#search_text').val() != '') {
							searchProducts();
							} else {
								$('#load_from').val(0);
								loadProducts('', '');
							}
							// Clear filters
							clearFilters(false);
							setCategoryActive('');
						}
					});
					$('.filters-toggle').on('click', function(event) {
						console.log($('.filters-toggle').hasClass('active'));
						if(!$('.filters-toggle').hasClass('active') || $("#search-msg").length) {
							// Clear filters
							clearFilters(true);
							$('#load_from').val(0);
							loadProducts('', '');
						}
					});
					return true;
				}
			});
		}
		function clearFilters(searchText) {
			// reset filters
			$(".tag-a.active").removeClass('active');
			resetSliderPrice();
			$(".widget-sorting-li").removeClass('active');
			$(".widget-sorting-li").first().addClass('active');
			if(searchText)
				$('#search_text').val('');

		}
		function setCategoryActive(category_url) {
			console.log('setCategoryActive: ' + category_url);
			$('.filter-category-li').removeClass('active');
			$('*[data-category="'+category_url+'"]').closest("li").addClass('active');
		}
		function loadFilterTags(){
			console.log('loadFilterTags');
			return $.ajax({
				url: '<? echo base_url();?>frontend/Products/getFilterTags',
				type: 'POST',
				dataType: "HTML",
				async: true,
				data: {},
				error: function(msg){
					console.log('error');
					//ShowPopupTime("Richiesta non inviata.<br/>Riprova.", 2000);
					return msg;
				},
				success: function(html){
					$("#filter-tags").html(html);
					$('.tag-a').on('click', function(event) {
						event.preventDefault();
						$('#load_from').val(0);
						console.log("current: " + $(this).data('id'));
						if($(this).hasClass('active'))
							$(this).removeClass('active');
						else
							$(this).addClass('active');
						loadProducts('', '');
						scrollToAnchor('filter-categories', $('#header-navbar').height(), event);
					});
					return true;
				}
			});
		}
		function searchProducts(){
			console.log('searchProducts text: ' + $('#search_text').val());
			if(!$('.se-pre-con').is(':visible')) {
				$('.se-pre-con').show();
			}
			return $.ajax({
				url: '<? echo base_url();?>frontend/Products/searchProducts',
				type: 'POST',
				dataType: "HTML",
				async: true,
				data: {"search_text": $('#search_text').val()},
				error: function(msg){
					console.log('error');
					$(".se-pre-con").delay(200).fadeOut("slow"); 
					return msg;
				},
				success: function(html){
					$("#products-div").html(html);
					$("#load-more-btn").hide();
					$('.add-to-whishlist').on('click', function(event) {
						event.preventDefault();			
						addToWishList($(this).data('id'));
						// la funzione sta in header
					});
					$(".se-pre-con").delay(200).fadeOut("slow"); 
					return true;
				}
			});
		}
    </script>
</body><!-- <body> -->
</html>
