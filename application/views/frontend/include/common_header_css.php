<!--Favicon
<link rel="shortcut icon" href="<//? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/favicon.ico" type="image/x-icon">-->
<link rel="shortcut icon" href="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/favicon.png" type="image/png">
<link rel="icon" href="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/favicon.png" type="image/png">
<!-- Google Material Icons -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/vendor/material-icons.min.css" rel="stylesheet" media="screen">
<!-- Brand Icons -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/vendor/socicon.min.css" rel="stylesheet" media="screen">
<!-- Bootstrap -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/vendor/bootstrap.min.css" rel="stylesheet" media="screen">
<!-- Theme Styles -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/theme.min.css" rel="stylesheet" media="screen">
<!-- Custom Styles -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/custom.css" rel="stylesheet" media="screen">
<!-- CSS per icona WhatsApp -->
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/flaticon-whatsapp/flaticon-whatsapp.css" rel="stylesheet" media="screen">
<link href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_CSS; ?>/vendor/sweetalert2.min.css" rel="stylesheet" media="screen"><!-- Sweetalert2 -->

<!-- GoogleAnalytics -->
<?php include_once("analyticstracking.php") ?>
<!-- Smartsupp -->
<?php include_once("smartsupp.php") ?>
<!-- Cookie law plugin -->
<?php include_once("cookielaw.php") ?>