 <section class="<? echo $productsContainerClass; ?>"> <!-- padding-top-3x -->
  <div class="filters-bar space-top-half">
    <div class="column">
      <!-- Nav Filters -->
      <ul class="nav-filters" id="filter-categories">
      </ul><!-- .nav-filters -->
      <input type="hidden" id="current_filter_category" value="" />
    </div><!-- .column -->
    <div class="column">
      <a href="#filters" class="filters-toggle" data-toggle="filters" style="visibility: hidden;">
        <i class="material-icons filter_list"></i>
        <? echo lang("LABEL_FILTER"); ?>
      </a>
      <a href="#search-box" class="search-btn" data-toggle="filters" id="search-icon-btn" style="visibility: hidden;">
        <i class="material-icons search"></i>
      </a>
    </div>
  </div><!-- .filters-bar -->
  <div class="row filters">
    <div class="col-xs-12 filters-pane" id="filters">
      <div class="row">
        <div class="col-md-3">

          <!-- Sorting Widget -->
          <div class="widget widget-sorting">
            <h3 class="widget-title"><? echo lang("LABEL_ORDER"); ?></h3>
            <ul>
              <li class="widget-sorting-li active"><a href="#" class="widget-sorting-link" data-sort="default">
                <i class="material-icons sort"></i>
                <? echo lang("LABEL_DEFAULT"); ?>
              </a></li>
              <li class="widget-sorting-li"><a href="#" class="widget-sorting-link" data-sort="discount">
                <i class="material-icons favorite_border"></i>
                <? echo lang("LABEL_SALE"); ?>
              </a></li>
              <li class="widget-sorting-li"><a href="#" class="widget-sorting-link" data-sort="new">
                <i class="material-icons vertical_align_top"></i>
                <? echo lang("LABEL_NEW"); ?>
              </a></li>
              <li class="widget-sorting-li"><a href="#" class="widget-sorting-link" data-sort="rate">
                <i class="material-icons star_border"></i>
                <? echo lang("LABEL_FEEDBACK"); ?>
              </a></li>
              <li class="widget-sorting-li"><a href="#" class="widget-sorting-link" data-sort="best">
                <i class="material-icons shopping_basket"></i>
                <? echo lang("LABEL_BEST"); ?>
              </a></li>
              <li class="widget-sorting-li"><a href="#" class="widget-sorting-link" data-sort="alpha">
                <i class="material-icons sort_by_alpha"></i>
                <? echo lang("LABEL_ALPHA"); ?>
              </a></li>
            </ul>
          </div><!-- .widget.widget-sorting -->
        </div>
        <!--<div class="col-md-2">
          <div class="widget widget-color">
            <h3 class="widget-title">Color Filter</h3>
            <ul>
              <li><a href="#">
                <span class="color" style="background-color: #93c4ef;"></span>
                Blue
              </a></li>
              <li><a href="#">
                <span class="color" style="background-color: #a7c04d;"></span>
                Green
              </a></li>
              <li><a href="#">
                <span class="color" style="background-color: #ef0568;"></span>
                Red
              </a></li>
              <li><a href="#">
                <span class="color" style="background-color: #ffce2b;"></span>
                Yellow
              </a></li>
            </ul>
          </div>
        </div><!-- .widget.widget-color -->
        <div class="col-md-3">

          <!-- Price Range Widget -->
          <!-- Please note: Only one Range Slider allowed on the page! -->
          <div class="widget widget-catesgories">
            <h3 class="widget-title"><? echo lang("LABEL_PRICE"); ?></h3>
            <form method="post" class="price-range-slider" id="price-range-slider" data-start-min="0" data-start-max="200" data-min="0" data-max="200" data-step="1">
              <div class="ui-range-slider"></div>
              <footer class="ui-range-slider-footer">
                <div class="column">
                  <button type="button" class="btn btn-ghost btn-sm btn-default" id="range-slider-btn"><? echo lang("LABEL_FILTER"); ?></button>
                </div>
                <div class="column">
                  <div class="ui-range-values">
                    <div class="ui-range-value-min">
                      €<span></span>
                      <input type="hidden" id="ui-range-value-min">
                    </div> -
                    <div class="ui-range-value-max">
                      €<span></span>
                      <input type="hidden" id="ui-range-value-max">
                    </div>
                  </div>
                </div>
              </footer>
            </form><!-- .price-range-slider -->
          </div><!-- .widget.widget-categories -->
        </div>
        <div class="col-md-5 col-md-offset-1">

          <!-- Tags Widget -->
          <div class="widget widget-tags" id="filter-tags">
            <!--<h3 class="widget-title">Tags</h3>
            <a href="#" class="tag-a" data-id="1">Senza glutine</a>
            <a href="#" class="tag-a" data-id="2">Senza lattosio</a>
            <a href="#" class="tag-a" data-id="3">Contorni</a>-->
          </div><!-- .widget.widget-price -->
        </div>
      </div><!-- .row -->
    </div><!-- .col-xs-12.filters-pane#filters -->
    <div class="col-xs-12 col-md-12 filters-pane" id="search-box">
    	<div class="col-md-10">
    		<input type="text" class="form-control" placeholder="<? echo lang("MSG_SEARCH_INSERT"); ?>" id="search_text">
    	</div>
        <div class="col-md-2">
      		<button type="button" class="btn btn-sm btn-primary" id="search_text_btn" style="height: 48px !important;margin: 0 !important;width: 100%;"><? echo lang("LABEL_SEARCH"); ?></button>
    	</div>
     </div><!-- .col-xs-12.filters-pane#search-box -->
  </div><!-- .row.filters -->
</section><!-- .container -->
<!-- Shop Catalog -->
<section class="<? echo $productsContainerClass; ?> padding-bottom-3x">
  <div class="row" id="products-div">	
  </div><!-- .row -->
  <!-- Load More Btn -->
  <a href="#" class="load-more-btn space-top" id="load-more-btn"><? echo lang("LABEL_SEE_MORE"); ?> <sup id="load_from_sup">25</sup></a>
</section><!-- .container -->