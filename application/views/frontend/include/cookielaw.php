<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script>
	// https://cookieconsent.insites.com/download/
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
		"popup": {
		  "background": "#252e39"
		},
		"button": {
		  "background": "#14a7d0"
		}
	  },
  	  "theme": "classic",
	  "content": {
		"message": "<?php echo lang('MSG_COOKIE_LAW') ?>",
		"dismiss": "<?php echo lang('MSG_COOKIE_LAW_BTN') ?>",
		"link": "<?php echo lang('MSG_COOKIE_LAW_INFO') ?>"
	  },
	  "position": "bottom-left"
	})});
</script>