<!-- Hero Slider -->
<!-- Data API:
  data-loop="true/false" enable/disable looping
  data-autoplay="true/false" enable/disable carousel autoplay
  data-interval="3000" autoplay interval timeout in miliseconds
  Simply add necessary data attribute to the ".hero-carousel" with appropriate value to adjust carousel functionality.
 -->
 <?  
 	if(count($homeSlider) == 1) { 
 		$slide = $homeSlider[0];
 ?>
 	<section class="singleSlide">
        <div class="singleSlideImg" style="background: url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/hero-slider/<? echo $slide->hs_image; ?>);">
        	<div class="container text-center padding-top-3x">
        		<span class="h1 from-bottom mobile-small-text shop-label-overlay" style="color: #ffffff;font-size:40px;top: 52%;"><? echo $slide->hs_title; ?></span><br>
            </div>
        </div>
    </section>
 <? } else { ?>
 <section class="hero-slider" data-loop="true" data-autoplay="true" data-interval="7000">
  <div class="inner">
    <?
		foreach ($homeSlider as $slide) {
		?>
			 <div class="slide" style="background-image: url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/hero-slider/<? echo $slide->hs_image; ?>);">
			  <div class="container text-center padding-top-3x">
				<? if($slide->hs_title != '') { ?>
				<span class="h1 from-bottom mobile-small-text shop-label-overlay" style="color: #ffffff;font-size:40px;top: 52%;"><? echo $slide->hs_title; ?></span><br>
				<? } ?>
				<!--<span class="h2 from-bottom"><span class="text-thin">o la si </span> <strong>indossa</strong></span>-->
			   <!-- <br><a href="shop" class="btn btn-primary btn-with-icon-right waves-effect waves-light scale-up hidden-xs">
				  IL CATALOGO
				  <i class="material-icons arrow_forward"></i>
				</a>-->
			  </div>
			</div><!-- .slide -->	
		<?
		}
	?>  
  </div><!-- .inner -->
</section><!-- .hero-slider -->
<? } ?>