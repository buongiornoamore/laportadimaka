<?php
session_start();
require_once('functions.php');
$logo_file = 'logo_small.png'; // logo_small.png
// check unique id for customer inside cookie
/*if(!isset($_COOKIE['uuid'])) {
	//echo "new uuid: ";
	$uuid = makeUnique(true);
	setcookie('uuid', $uuid, time() + (10 * 365 * 24 * 60 * 60), "/");
    $cookie= array(
	   'name'   => 'uuid',
	   'value'  => $uuid,                            
	   'expire' => time() + (10 * 365 * 24 * 60 * 60),   	
	   'secure' => TRUE
    );
	$this->input->set_cookie($cookie);
	//$_COOKIE['uuid'] = $uuid;
} */
?>
<!-- Navbar -->
<!-- Remove ".navbar-sticky" class to make navigation bar scrollable with the page. -->
<header class="navbar navbar-sticky" id="header-navbar">
  <!-- Site Logo -->
  <a href="<?php echo site_url(lang('PAGE_HOME_URL')); ?>" class="site-logo visible-desktop">
    <img itemprop="image" src="<? echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/<? echo $logo_file; ?>" title="<? echo lang('PAGE_HOME_TITLE') . ' | ' . SITE_TITLE_NAME; ?>" alt="<? echo lang('PAGE_HOME_TITLE') . ' | ' . SITE_TITLE_NAME; ?>" />
  </a><!-- site-logo.visible-desktop -->
  <a href="<?php echo site_url(lang('PAGE_HOME_URL')); ?>" class="site-logo visible-mobile">
  	<img src="<?php echo ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/<? echo $logo_file; ?>" title="<? echo lang('PAGE_HOME_TITLE') . ' | ' . SITE_TITLE_NAME; ?>" alt="<? echo lang('PAGE_HOME_TITLE') . ' | ' . SITE_TITLE_NAME; ?>" />
  </a><!-- site-logo.visible-mobile -->
  <!-- Language Switcher -->
  <div class="lang-switcher">
    <div class="lang-toggle">
      <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flags/<? echo $this->session->userdata('site_lang'); ?>.png" alt="<? echo $this->session->userdata('site_lang'); ?>">
      <i class="material-icons arrow_drop_down"></i>
      <ul class="lang-dropdown">
      	<?php 
			// load installed langs 
			foreach ($installedLangs as $lingua) {
		?>
        	<li class="lang-dropdown-li"><a href="<?php echo base_url(); ?>LangSwitch/switchLanguage/<? echo $lingua->codice_ci; ?>/<? echo $this->uri->segment(1).'/'.$this->uri->segment(2).($this->uri->segment(3) != '' ? '/'.$this->uri->segment(3) : '').($this->uri->segment(4) != '' ? '/'.$this->uri->segment(4) : ''); ?>"><img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/flags/<? echo $lingua->codice_ci; ?>.png" alt="<? echo $lingua->codice_ci; ?>"><? echo strtoupper(substr($lingua->codice_ci, 0, 2)); ?></a></li>
		<? } ?>   
      </ul>
      <a href="" style="display:none" id="hidden-lang" />
    </div>
  </div><!-- .lang-switcher -->
  <!-- Main Navigation -->
  <!-- Control the position of navigation via modifier classes: "text-left, text-center, text-right" -->
  <nav class="main-navigation text-center">
    <!-- menu custom -->
  	<ul class="menu">
    	<?php 
			// load pagine
			foreach ($menuPages as $pagina) {
		?>
  		<li><a href="<?php echo createUrlMenu($pagina->url_pagina); ?>"><? echo $pagina->nome_pagina; ?></a></li>
        <?php
			}
		?>
    </ul>
  </nav><!-- .main-navigation -->
  
  <!-- Toolbar -->
  <div class="toolbar">
    <div class="inner">
     
    </div><!-- .inner -->
  </div><!-- .toolbar -->
  <!-- COMMON HEADER SCRIPTS -->
  <script type="text/jscript">
  </script>
</header><!-- .navbar.navbar-sticky -->
