<!-- JavaScript (jQuery) libraries, plugins and custom scripts -->
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/jquery-2.1.4.min.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/bootstrap.min.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/smoothscroll.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/velocity.min.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/waves.min.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/icheck.min.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/owl.carousel.min.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/nouislider.min.js"></script>
<!--<script src="<?// echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/bootstrap-notify.min.js"></script>-->
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/sweetalert2.all.min.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/mailer/mailer.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/scripts.js"></script>
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/page-preloading.js"></script><!-- Page Preloading -->
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/modernizr.custom.js"></script><!-- Modernizr -->
<script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/cloudflare.promise.core.js"></script>
<script type="text/javascript">
	$(window).load(function() {
		/* set footer sticky min height page*/
		$('.maximized-container').css('min-height', $(window).height() - $('.footer').height() - $('.main-navigation').height() - 50);
		/* Newsletter subscribe*/
		$('#newsletter-btn').on('click', function(e) {
			e.preventDefault();
			console.log('newsletter-btn click');
			if(validateEmail('newsletter-email')){
				subscribeNewsletter();
			} 
		});
		$("#newsletter-email").on("keyup blur", function(){validateEmail('newsletter-email');});
    }); 
	$(window).resize(function(){
		$('.maximized-container').css('min-height', $(window).height() - $('.footer').height() - $('.main-navigation').height() - 50);
	});
	function subscribeNewsletter(){
		$(".se-pre-con").show();
		$.ajax({
			url: '<? echo base_url();?>mail/newsletter',
			type: "POST",
			data: {
				email: $('#newsletter-email').val()
			},
			cache: false,
			success: function(messJson) {
				var json = $.parseJSON(messJson);
				// Enable button & show success message
				$("#submit-news-btn").attr("disabled", false);
				//clear all fields
				$('#newsletterForm').trigger("reset");
				$(".se-pre-con").delay(200).fadeOut("slow");
				// sweet alert 2  
				swal({
				  position: 'center',
				  type: json['type'],
				  title: json['message'],
				  showConfirmButton: false,
				  timer: 3000
				});
			},
			error: function() {
				// Fail message
				$(".se-pre-con").delay(200).fadeOut("slow");
				swal({
				  position: 'center',
				  type: 'error',
				  title: "<?php echo lang('MSG_FAILURE_CONTACT'); ?>",
				  showConfirmButton: false,
				  timer: 3000
				});
				//clear all fields
				$('#newsletterForm').trigger("reset");
				$("#submit-news-btn").attr("disabled", false);
			}
		});
	}
</script>