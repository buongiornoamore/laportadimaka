<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><? echo $prod_page_title . ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="description" content="<? echo $prod_page_meta_desc; ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
</head>
<body class="page-preloading">
  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->
  <!-- Page Wrapper -->
  <div itemscope itemtype="http://schema.org/Product" class="page-wrapper">
	<? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <!-- Pager + Product Gallery -->
    <section class="fw-section bg-gray padding-top-3x" style="margin-top: 0 !important;">
      <!-- Page Navigation -->
      <? if (isset($prev) && $prev != NULL) { ?>
      <a href="<? echo base_url() . lang('PAGE_PRODUCTS_URL');?>/<? echo $prev->codice; ?>/<? echo cleanString($prev->descrizione_breve, true); ?>" class="page-nav page-prev">
        <span class="page-preview">
           <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG ."/shop/". $prev->url_immagine; ?>" alt="<? echo SITE_TITLE_NAME.' | '.$prev->nome; ?>" title="<? echo $prev->nome; ?>">
        </span>
        &mdash; Prev
      </a>
      <? } 
	  if (isset($next) && $next != NULL) {
	  ?>
      <a href="<? echo base_url() . lang('PAGE_PRODUCTS_URL');?>/<? echo $next->codice; ?>/<? echo cleanString($next->descrizione_breve, true); ?>" class="page-nav page-next">
        <span class="page-preview">
          <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG ."/shop/". $next->url_immagine; ?>" alt="<? echo SITE_TITLE_NAME.' | '.$next->nome; ?>" title="<? echo $next->nome; ?>">
        </span>
        Next &mdash;
      </a>
      <? }
	  	// TODO Impostare l'immagine della prima variante o di quella selezionata dal dettaglio
	  	$imm_grande = $prod->url_immagine;
		$imm_grande_retro = '';
	//	$imm_grande_retro = $prod->url_img_grande;
	  ?>		
      <div class="container padding-top">
        <!-- Product Gallery -->
        <div class="product-gallery">
          <!-- Preview -->
          <ul class="product-gallery-preview">
            <li id="preview01" class="current"><img style="border: 1px dotted #cccccc;" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/<? echo $imm_grande; ?>" alt="<? echo SITE_TITLE_NAME.' | '.$prod->nome.' - '.$prod->descrizione_breve.' '.lang('LABEL_FRONT'); ?>"></li>
            <? if (isset($imm_grande_retro) && $imm_grande_retro != "") { ?>
            <li id="preview02"><img style="border: 1px dotted #cccccc;" itemprop="image" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/<? echo $imm_grande_retro; ?>" alt="<? echo SITE_TITLE_NAME.' | '.$prod->nome.' - '.$prod->descrizione_breve.' '.lang('LABEL_BACK'); ?>"></li>
            <? } ?>
          </ul><!-- .product-gallery-preview -->
          <!-- Thumblist 
          <ul class="product-gallery-thumblist">
            <li class="active"><a href="#preview01" id="preview01-t">
              <img style="border: 1px dotted #cccccc;" src="<//? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/<//? echo $imm_grande; ?>" alt="<//? echo SITE_TITLE_NAME.' | '.$prod->nome; ?>">
            </a></li>
            <//? if (isset($imm_grande_retro) && $imm_grande_retro != "") { ?>
            <li><a href="#preview02" id="preview02-t">
              <img style="border: 1px dotted #cccccc;" src="<//? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/<//? echo $imm_grande_retro; ?>" alt="<//? echo SITE_TITLE_NAME.' | '.$prod->nome; ?> - Back">
            </a></li>
            <//? } ?>
          </ul><!-- .product-gallery-thumblist -->
        </div><!-- .product-gallery -->
      </div><!-- .container -->
      <div id="image-color-carousel" class="carousel slide"> 
	  <?
/*	  $currChild = NULL;
	  $currChildSizes = NULL;
	  if(!empty($child_list)) {
		$mobile_colors = '';
	
	    // colors thumbnail
	  	echo '<div class="list" id="image-color-carousel-list" '.(count($child_list) == 1 ? 'style="display:none"' : '').'><ul>';

	  	foreach ($child_list as &$childArray) {
			$active = '';
			$child = $childArray['child'];
			$sizes = $childArray['sizes'];
			$modelImgFront = $childArray['modelImgFront'];
			$modelImgBack = $childArray['modelImgBack'];
			
		//	var_dump($child);
			
			if($currChild == NULL) {
				$currChild = $child;
				$currChildSizes = $sizes;
				$active = 'active';
			}
			$childSizes = '';
			$countLastSize = 1;
			foreach ($sizes as $childSizeKey => $childSizeValue) {	
				//echo $child->colore . ' ' . $childSizeKey . ' ' . $childSizeValue . '<br>';
				$childSizes .= $childSizeValue . '-' . $childSizeKey . ($countLastSize < count($sizes) ? '|' : '');
				$countLastSize++;
			}
				
	    	echo '<li><a href="#" class="colors-img-thumb" id="'.$child->codice.'" data-id="'.$child->codice_prodotto.'" data-name="'.$child->colore.'" data-code="'.$child->colore_codice.'" data-imgf="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/'.$modelImgFront.'" data-imgb="'.($modelImgBack != '' ? ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/'.$modelImgBack : '').'" data-refcode="'.$child->codice.'" data-variantid="'.$child->id_variante.'" data-sizes="'.$childSizes.'"><img style="border: 1px dotted #cccccc;" class="t-img '.$active.'" src="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/cart/'.$child->url_img_piccola.'" alt="'.$child->colore.'" title="'.$child->colore.'" /></a></li>';	
			$mobile_colors .= '<li><a href="#" class="colors-img-thumb" id="'.$child->codice.'" data-id="'.$child->codice_prodotto.'" data-name="'.$child->colore.'" data-code="'.$child->colore_codice.'" data-imgf="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/'.$modelImgFront.'" data-imgb="'.($modelImgBack != '' ? ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/'.$modelImgBack : '').'" data-refcode="'.$child->codice.'" data-variantid="'.$child->id_variante.'" data-sizes="'.$childSizes.'"><i class="material-icons fiber_manual_record" style="color:#'.$child->colore_codice.'"></i></a></li>';
      	}
		
	    echo '<ul></div>';	
		echo '<div class="list-mobile" id="image-color-carousel-list-mobile" style="display:none"><ul>';
			echo $mobile_colors;
		echo '</ul></div>';	
	  } else {
			//echo 'count ' . count($child_list);
			//var_dump($child_list);
	  }*/
    ?>
    </div>
 </section><!-- .fw-section.bg-gray -->
	
    <!-- Product Info -->
    <section class="fw-section bg-gray">
      <div class="container">
        <div class="product-info padding-top-2x text-center">
          <h1 class="h2 space-bottom-half"><span itemprop="name"><b><? echo $prod->nome; ?></b></span></h1>
           <div class="product-meta">
            <div class="product-category">
            	<strong><? echo lang("LABEL_CATEGORY"); ?>: </strong>
				  <?    
				  	$counter_cat = 1;
                    foreach ($categories as &$cat) 
                    {
                      echo '<a href="'.site_url(lang('PAGE_SHOP_URL').'/'.$cat->url_categorie).'">'.$cat->nome_categoria_trad.'</a>' . (sizeof($categories) == $counter_cat ? '' : ' | ');	
					  $counter_cat++;
                    }
                  ?> 
            </div>
            <p itemprop="description" class="text-l" align="justify"><? echo $prod->descrizione; ?></p>
            Richiedi info -- bottone modulo contatti from prodotto
            <br/>
            ps: mettere multifoto per ogni prodotto
            <br/>
            caricare gallery foto sotto la foto principale
            <br/>
            sostituire next e prev con delle icone
          </div>      
		 
        </div><!-- .product-info -->
      </div><!-- .container -->
    </section><!-- .fw-section.bg-gray -->
    
    <!-- Include detail_tabs.php -->
    
    <!-- Related Products 
    <section class="container padding-top padding-bottom">
      <hr>
      <h3 class="padding-top"><?// echo lang("LABEL_ALSO_LIKE"); ?></h3>
      <div class="row padding-top">
        <div class="col-lg-3 col-sm-6">
          <div class="shop-item">
            <div class="shop-thumbnail">
              <span class="shop-label text-danger">Sale</span>
              <a href="#" class="item-link"></a>
              <img src="img/shop/th05.jpg" alt="Shop item">
              <div class="shop-item-tools">
                <a href="#" class="add-to-whishlist" data-toggle="tooltip" data-placement="top" title="Wishlist">
                  <i class="material-icons favorite_border"></i>
                </a>
                <a href="#" class="add-to-cart">
                  <em>Add to Cart</em>
                  <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                    <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
                  </svg>
                </a>
              </div>
            </div>
            <div class="shop-item-details">
              <h3 class="shop-item-title"><a href="#">Wall Clock</a></h3>
              <span class="shop-item-price">
                <span class="old-price">$69.00</span>
                $48.00
              </span>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6">
          <div class="shop-item">
            <div class="shop-thumbnail">
              <a href="#" class="item-link"></a>
              <img src="img/shop/th06.jpg" alt="Shop item">
              <div class="shop-item-tools">
                <a href="#" class="add-to-whishlist" data-toggle="tooltip" data-placement="top" title="Wishlist">
                  <i class="material-icons favorite_border"></i>
                </a>
                <a href="#" class="add-to-cart">
                  <em>Add to Cart</em>
                  <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                    <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
                  </svg>
                </a>
              </div>
            </div>
            <div class="shop-item-details">
              <h3 class="shop-item-title"><a href="#">LED Lighting</a></h3>
              <span class="shop-item-price">
                $130.00
              </span>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6">
          <div class="shop-item">
            <div class="shop-thumbnail">
              <a href="#" class="item-link"></a>
              <img src="img/shop/th04.jpg" alt="Shop item">
              <div class="shop-item-tools">
                <a href="#" class="add-to-whishlist" data-toggle="tooltip" data-placement="top" title="Wishlist">
                  <i class="material-icons favorite_border"></i>
                </a>
                <a href="#" class="add-to-cart">
                  <em>Add to Cart</em>
                  <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                    <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
                  </svg>
                </a>
              </div>
            </div>
            <div class="shop-item-details">
              <h3 class="shop-item-title"><a href="#">Alarm Clock</a></h3>
              <span class="shop-item-price">
                $178.00
              </span>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-sm-6">
          <div class="shop-item">
            <div class="shop-thumbnail">
              <a href="#" class="item-link"></a>
              <img src="img/shop/th08.jpg" alt="Shop item">
              <div class="shop-item-tools">
                <a href="#" class="add-to-whishlist" data-toggle="tooltip" data-placement="top" title="Wishlist">
                  <i class="material-icons favorite_border"></i>
                </a>
                <a href="#" class="add-to-cart">
                  <em>Add to Cart</em>
                  <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
                    <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
                  </svg>
                </a>
              </div>
            </div>
            <div class="shop-item-details">
              <h3 class="shop-item-title"><a href="#">Hook Basket</a></h3>
              <span class="shop-item-price">
                $112.35
              </span>
            </div>
          </div>
        </div>
      </div>
    </section><!-- .container -->

    <!-- Footer -->
	<? require_once('include/footer.php'); ?> <!-- Footer -->
  </div><!-- .page-wrapper -->
  <? require_once('include/common_header_js.php'); ?> <!-- Import js --> 	
  <script type="text/javascript">
        $(window).load(function() {	
            loadCartDropdown(true, false, false);
			
			$('.add-to-cart').on('click', function(event) {
				event.preventDefault();
				var variantInfo = $("#size").val().split(";");
				console.log("add-to-cart: " + $(this).data('id') + " qty: " + $(".quantity").val());
				console.log("hidden: " + $("#hidden-id").val());
				console.log("variant-id: " + variantInfo[1]);
				console.log("variant-code: " + variantInfo[0]);
				console.log("size: " + $("#size :selected").text() + ' ' + $("#size").val());
				addToCart($("#hidden-id").val(), $("#size :selected").text(), $("#size").val(),$(".quantity").val(), variantInfo[1]);
			});
			$(".incr-btn").on("click", function(e) {
				var $button = $(this);
				var oldValue = $button.parent().find('.quantity').val();
				$button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
				if ($button.data('action') == "increase") {
					var newVal = parseFloat(oldValue) + 1;
				} else {
				 // Don't allow decrementing below 1
					if (oldValue > 1) {
						var newVal = parseFloat(oldValue) - 1;
					} else {
						newVal = 1;
						$button.addClass('inactive');
					}
				}
				$button.parent().find('.quantity').val(newVal);
				e.preventDefault();
			});
			$('.add-to-wishlist-btn').on('click', function(event) {
				event.preventDefault();			
				//console.log('productcode ' + $(this).attr('data-productcode'));
				addToWishList($(this).attr('data-productcode'));
				// la funzione sta in header_navbar.php
			});
			$(".colors-img-thumb").on("click", function(e) {
				e.preventDefault();
				$(".t-img").removeClass('active');
				$(this).find("img").addClass('active');
				console.log("set: " + $(this).data("name") + ' ' + $(this).data("code"));
				$('#size').empty();
				var firstAvailableChildInfo = '';
				if($(this).data("sizes") != '') {
					var availableSizes = $(this).data("sizes").split("|");
    				for(var i = 0; i < availableSizes.length; i++){
						var sizeInfo = availableSizes[i].split("-");
						$('#size').append('<option value="'+sizeInfo[0]+'">'+sizeInfo[1]+'</option>');
						if(firstAvailableChildInfo == '')
							firstAvailableChildInfo = sizeInfo[0];
					}
					$('#btns-product-tools-msg').hide();
					$('#btns-product-tools').show();
				} else {
					// disable select and buy button
					$('#btns-product-tools-msg').html('<?php echo lang('MSG_NO_SIZE_FOR_PRODUCTS_COLOR'); ?>');
					$('#btns-product-tools').hide();
					$('#btns-product-tools-msg').show();
				}
				/*$("#color-span").css("color", "#"+$(this).data("code"));*/
				$(".add-to-cart").attr("data-id", $(this).data("id"));
				// set id on whislist button
				//console.log('refcode: ' + $(this).data("refcode"));
				//$("#hidden-id").val($(this).data("id"));
				if($(this).data("variantid") != '')
					$("#hidden-variant-id").val($(this).data("variantid"));
				else 
					$("#hidden-variant-id").val('');
				
				// hide color info and bullet if code is not set	
				if($(this).data("code") != '') {
					$("#color-span-bullet").css("color", "#"+$(this).data("code"));
					$("#color-span").html($(this).data("name"));
					$("#color-span-div").show();
				} else {
					$("#color-span-div").hide();
				}	
				
				console.log("img: " + $(this).data("imgf") + ' ' + $(this).data("imgb"))
				$("#preview01").find("img").attr("src", $(this).data("imgf"));
				$("#preview01-t").find("img").attr("src", $(this).data("imgf"));
				if($(this).data("imgb") != '') {
					$("#preview02").find("img").attr("src", $(this).data("imgb"));
					$("#preview02-t").find("img").attr("src", $(this).data("imgb"));
					$("#preview02").show();
					$("#preview02-t").show();
				} else {
					$("#preview02").hide();
					$("#preview02-t").hide();
				}
				if(firstAvailableChildInfo != '') {
					var variantInfo = firstAvailableChildInfo.split(";");
					$("#ref-code-span").html(variantInfo[0]);			
					$('.add-to-wishlist-btn').attr("data-productcode", variantInfo[0]);
				} else {
					$("#ref-code-span").html($(this).data("refcode"));			
					$('.add-to-wishlist-btn').attr("data-productcode", $(this).data("refcode"));
				}
			});
			$("#size").on("change", function(e) {
				console.log('change: ' + $(this).val());
				var variantInfo = $(this).val().split(";");
				$('.add-to-wishlist-btn').attr("data-productcode", variantInfo[0]);
				$("#hidden-variant-id").val(variantInfo[1]);
				$("#ref-code-span").html(variantInfo[0]);	
			});
			var curr_id = '<? echo $var_curr_id; ?>';
			console.log('curr_id: ' + '<? echo $var_curr_id; ?>');
			// set current variant selected
			if(curr_id != '') {
				$(".colors-img-thumb").each(function( index ) {
					if($(this).data('sizes').indexOf(curr_id) !== -1) {
						//console.log("Sizes: " + $(this).data('sizes') );
						$(this).trigger("click");
						return false;
					}
				});
				$("#"+curr_id).trigger("click");
			} else {
				$(".colors-img-thumb:first").trigger("click");
			}
		});
		function addToCart(product_id, size, size_id, qty, variant_id){
			console.log('add to cart prod id: ' + product_id + ' size: ' + size + ' variant_id: ' + variant_id);
			return $.ajax({
				url: '<? echo base_url();?>frontend/Cart/addToCart',
				type: 'POST',
				dataType: "HTML",
				async: true,
				data: {"product_id": product_id, "size": size, "size_id": size_id, "qty": qty, "variant_id": variant_id},
				error: function(msg){
					console.log('error');
					swal({
					  position: 'center',
					  type: 'error',
					  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
					  showConfirmButton: false,
					  timer: 3000
					}).catch(swal.noop);
					return msg;
				},
				success: function(html){
					loadCartDropdown(true, true, false);
					// messagge
					swal({
					  position: 'center',
					  type: 'success',
					  title: "<?php echo lang('MSG_CART_ADDED'); ?>",
					  showConfirmButton: false,
					  timer: 2000
					}).catch(swal.noop);
					return true;
				}
			});
		}
  </script>		
</body><!-- <body> -->

</html>
