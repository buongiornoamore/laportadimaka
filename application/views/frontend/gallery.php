<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><? echo lang('PAGE_GALLERY_TITLE') . ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="description" content="<? echo lang('PAGE_GALLERY_META_DESCRIPTION'); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
</head>
<body class="page-preloading">
	<? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->
    <!-- Page Wrapper -->
    <div class="page-wrapper">
		<? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
        
        <!-- Page Title -->
        <section class="<? echo $productsContainerClass; ?> padding-top-3x" align="center">
          <h1 class="space-top-half tablet-center"><? echo lang('PAGE_GALLERY_TITLE'); ?></h1>
        </section><!-- .container-fluid -->
    
        <!-- Filters Bar -->
        <section class="<? echo $productsContainerClass; ?>" align="center">
          <div class="filters-bar tablet-center space-top-half">
            <!-- Nav Filters -->
            <ul class="nav-filters">
              <!-- <sup><?// echo count($gallery_images); ?></sup> -->
              <li class="active"><a href="#" data-filter="*"><? echo lang('LABEL_ALL'); ?></a> <sup></sup></li>
              <?php 
			  // load immagini
		  	  foreach ($gallery_cats as $cat) {
				  echo '<li><a href="#" data-filter=".'.$cat->id_categoria_gallery.'-category">'.$cat->descrizione_categoria_gallery.'</a> <sup></sup></li>';
			  }
			  ?>
            </ul><!-- .nav-filters -->
          </div><!-- .filters-bar -->
        </section><!-- .container -->
    
        <!-- Gallery Grid Full Width With Gap-->
        <section class="<? echo $productsContainerClass; ?> padding-bottom-2x">
          <div class="isotope-grid col-3 filter-grid">
            <div class="grid-sizer"></div>
            <div class="gutter-sizer"></div>  		
            <?php 
			// load immagini
			foreach ($gallery_images as $image) {
			?>
            <!-- Gallery Item -->
            <div class="grid-item <? echo $image->id_categoria_ig; ?>-category">
              <a href="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/gallery/<? echo $image->immagine_ig; ?>" class="gallery-item">
                <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/gallery/<? echo $image->immagine_ig; ?>" alt="<? echo $image->titolo_ig; ?>">
                <span class="gallery-caption">
                  <h3><? echo $image->titolo_ig; ?></h3>
                  <p><? echo $image->testo_ig; ?></p>
                </span>
              </a>
            </div><!-- .grid-item -->
            <?	
			}
			?>
          </div><!-- .isotope-grid -->
        </section><!-- .container-fluid -->
        <? require_once('include/footer.php'); ?> <!-- Footer -->
    </div><!-- .page-wrapper -->
    <? require_once('include/common_header_js.php'); ?> <!-- Import js --> 
    <script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/isotope.pkgd.min.js"></script>
    <script src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_JS; ?>/vendor/magnific-popup.min.js"></script>
    <script type="text/javascript">
        $(window).load(function() {	
        });
    </script>		
</body><!-- <body> -->
</html>