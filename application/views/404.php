<?php
$show_lang_option = false;
$show_made_footer = false;
$motto_text = lang('LABEL_404_MESSAGE');
$notify_btn = lang('LABEL_404_BTN');

/* Coming soon OPTIONS */
$logo = COMNINGSOON_LOGO;
$back_image = ASSETS_ROOT_FOLDER_FRAMEWORK_IMG .'/'.COMNINGSOON_BACK_IMAGE; // default.jpg
$btn_color = '#'.COMNINGSOON_BTN_COLOR;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    
    <title><?=COMPANY_NAME;?></title>
    
    <link rel="shortcut icon" href="<? echo SITE_URL_PATH; ?>/assets/assets-framework/img/favicon.png" type="image/png">
    
    <link href="<? echo SITE_URL_PATH; ?>/assets/assets-comingsoon/css/bootstrap.css" rel="stylesheet" />
	<link href="<? echo SITE_URL_PATH; ?>/assets/assets-comingsoon/css/coming-sssoon.css" rel="stylesheet" />    
    
    <!--     Fonts     -->
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
  	
    <style type="text/css">
		.btn-color-custom:hover {
			background-color: <?= $btn_color;?> !important;
    		border-color: <?= $btn_color;?> !important;
			color: #fff !important;
		}
		
		.btn-color-custom {
			background-color: transparent !important;
    		border-color: <?= $btn_color;?> !important;
			color: <?= $btn_color;?> !important;
		}
    </style>
    
</head>
<body>
    <div class="main" style="background-image: url('<? echo SITE_URL_PATH; ?>/assets/assets-comingsoon/images/<? echo $back_image; ?>')">
        <div class="cover black" data-color="black"></div>
        <div class="container">
            <h1 class="logo cursive" style="padding-top: 50px;">
                <? 
                if($logo != '') {
                    echo '<p><img src="'.SITE_URL_PATH.'/assets/assets-framework/img/'.$logo.'"/></p>';
                } else {
                    echo $titolo_pagina;	
                }
                ?>
            </h1>
            <div class="content">
                <p class="motto"><?= $motto_text;?></p>
               <p align="center"><a href="<?php echo site_url(lang('PAGE_HOME_URL')); ?>" class="btn btn-danger btn-fill btn-color-custom btn-lg" id="btn-mail"><?=$notify_btn;?></a></p>
            </div>
        </div>
     </div>
 </body>
   <script src="<? echo SITE_URL_PATH; ?>/assets/assets-comingsoon/js/jquery-1.10.2.js" type="text/javascript"></script>
   <script src="<? echo SITE_URL_PATH; ?>/assets/assets-comingsoon/js/bootstrap.min.js" type="text/javascript"></script>
   <script type="text/javascript">
   		$(window).load(function() {	
  		});
	</script>	
</html>