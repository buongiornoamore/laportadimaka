<?
$show_lang_option = false;
$show_made_footer = false;
$motto_text = 'Presto saremo on line con il nostro nuovissimo SITO WEB';
$waiting_list_text = 'Unisciti alla mailing list e resta in contatto con noi.';
$notify_btn = 'Aggiornami';
$email_text = 'La tua email qui...';

/* Coming soon OPTIONS */
$logo = COMNINGSOON_LOGO;
$back_image = ASSETS_ROOT_FOLDER_FRAMEWORK_IMG .'/'.COMNINGSOON_BACK_IMAGE; // default.jpg
$btn_color = '#'.COMNINGSOON_BTN_COLOR;
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    
    <title><?=COMPANY_NAME;?></title>
    
    <link rel="shortcut icon" href="<? echo SITE_URL_PATH; ?>/assets/assets-framework/img/favicon.png" type="image/png">
    
    <link href="assets/assets-comingsoon/css/bootstrap.css" rel="stylesheet" />
	<link href="assets/assets-comingsoon/css/coming-sssoon.css" rel="stylesheet" />    
    
    <!--     Fonts     -->
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
  	
    <style type="text/css">
		.btn-color-custom {
			background-color: <?= $btn_color;?> !important;
    		border-color: <?= $btn_color;?> !important;
		}
    </style>
    
</head>

<body>
<nav class="navbar navbar-transparent navbar-fixed-top" role="navigation">  
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      	 <? if($show_lang_option) { ?>
         <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                <img src="<? echo $local_root_url; ?>/coming_soon/images/flags/US.png"/>
                English(US) 
                <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#"><img src="assets/assets-comingsoon/images/flags/DE.png"/> Deutsch</a></li>
                <li><a href="#"><img src="assets/assets-comingsoon/images/flags/GB.png"/> English(UK)</a></li>
                <li><a href="#"><img src="assets/assets-comingsoon/images/flags/FR.png"/> Français</a></li>
                <li><a href="#"><img src="assets/assets-comingsoon/images/flags/RO.png"/> Română</a></li>
                <li><a href="#"><img src="assets/assets-comingsoon/images/flags/IT.png"/> Italiano</a></li>
                
                <li class="divider"></li>
                <li><a href="#"><img src="assets/assets-comingsoon/images/flags/ES.png"/> Español <span class="label label-default">soon</span></a></li>
                <li><a href="#"><img src="assets/assets-comingsoon/images/flags/BR.png"/> Português <span class="label label-default">soon</span></a></li>
                <li><a href="#"><img src="assets/assets-comingsoon/images/flags/JP.png"/> 日本語 <span class="label label-default">soon</span></a></li>
                <li><a href="#"><img src="assets/assets-comingsoon/images/flags/TR.png"/> Türkçe <span class="label label-default">soon</span></a></li>
             
              </ul>
        </li>
		<? } ?>
      </ul>
      <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="<?=FACEBOOK_LINK;?>" target="_blank"> 
                    <i class="fa fa-facebook-square"></i>
                </a>
            </li>
          <!--   <li>
                <a href="<?//=$twitter_link;?>" target="_blank"> 
                    <i class="fa fa-twitter"></i>
                    Tweet
                </a>
            </li>-->
             <li>
                <a href="<?=INSTAGRAM_LINK;?>" target="_blank"> 
                    <i class="fa fa-instagram"></i>
                </a>
            </li>
             <li>
                <a href="mailto:<?=COMPANY_EMAIL;?>" target="_blank"> 
                    <i class="fa fa-envelope-o"></i>
                </a>
            </li>
       </ul>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container -->
</nav>
<div class="main" style="background-image: url('<? echo $back_image; ?>')">

<!--    Change the image source '/images/default.jpg' with your favourite image.     -->
    
    <div class="cover black" data-color="black"></div>
     
<!--   You can change the black color for the filter with those colors: blue, green, red, orange       -->

    <div class="container">
        <h1 class="logo cursive" style="padding-top: 50px;">
        	<? 
			if($logo != '') {
				echo '<p><img src="assets/assets-framework/img/'.$logo.'"/></p>';
			} else {
				echo $titolo_pagina;	
			}
			?>
        </h1>
<!--  H1 can have 2 designs: "logo" and "logo cursive"           -->
        
        <div class="content">
            <h4 class="motto"><?= $motto_text;?></h4>
            <div class="subscribe">
                <h5 class="info-text">
                    <?= $waiting_list_text;?> 
                </h5>
                <div class="row">
                    <!--<div class="col-md-4 col-md-offset-4 col-sm6-6 col-sm-offset-3 ">-->
                    <div class="col-md-6 col-md-offset-4 col-sm6-6 col-sm-offset-3 ">
                        <form class="form-inline" role="form">
                          <div class="form-group">
                            <label class="sr-only" for="exampleInputEmail2"><?=$email_text;?></label>
                            <input type="email" class="form-control transparent" placeholder="<?=$email_text;?>" id="input-email" />
                          </div>
                          <button type="button" class="btn btn-danger btn-fill btn-color-custom" id="btn-mail"><?=$notify_btn;?></button>
                        </form>
                    </div>
                </div>
                <h5 class="info-text" id="msg-text" style="color:green">
                </h5>
            </div>
        </div>
    </div>
    <div class="footer">
      <div class="container">
      <? if($show_made_footer) { ?>	
             Made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Creative Tim</a>. Free download <a href="http://www.creative-tim.com/product/coming-sssoon-page">here.</a>
      <? } ?>       
      </div>
    </div>
 </div>
 </body>
   <script src="assets/assets-comingsoon/js/jquery-1.10.2.js" type="text/javascript"></script>
   <script src="assets/assets-comingsoon/js/bootstrap.min.js" type="text/javascript"></script>
   <script type="text/javascript">
   		$(window).load(function() {	
			//$(".se-pre-con").delay(200).fadeOut("slow"); 
			//console.log('loaded');
			$('#btn-mail').on('click', function(event) {
				console.log('click');
				if(validateEmail('input-email'))
					sendInfoModule();
			});
			$("#input-email").on("keyup blur", function(){validateEmail('input-email');});	
  		});
		function sendInfoModule(){
			console.log('sendInfoModule');
			//$(".se-pre-con").show();
			return $.ajax({
				url: 'mail/send',
				type: 'POST',
				dataType: "HTML",
				async: true,
				data: {"to_mail": "<?=COMPANY_EMAIL;?>", "mail_type": "newsletter", "mail_text": "Nuovo contatto ricevuto da " + $("#input-email").val(), "subject": "Nuovo contatto newsletter"},
				error: function(msg){
					console.log('error');
					ShowPopupTime("Richiesta non inviata.<br/>Riprova.", 2000);
					return msg;
				},
				success: function(html){	
					console.log('success ' + html);
					$("#input-email").val('');
					ShowPopupTime("Grazie per esserti iscritto alla nostra newsletter.", 4000);
					return true;
				}
			});
		} 
		function ShowPopupTime(message, time) {
			$('#msg-text').html('<div id="popup-text">'+message+'</div>');
			$('#msg-text').show();
			setTimeout('HidePopup()', time);
		}
		function HidePopup() {
			$('#msg-text').hide();
		}
		function validateEmail(id_email){
			//testing regular expression
			var a = $("#"+id_email).val();
			var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+@[a-zA-Z0-9]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
			//if it's valid email
			if(filter.test(a)){
				$("#"+id_email).removeClass("error");
				return true;
			}else{
				$("#"+id_email).addClass("error");
				return false;
			}
		}
		</script>	
</html>