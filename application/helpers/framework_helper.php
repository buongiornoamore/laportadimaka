<?php defined('BASEPATH') OR exit('No direct script access allowed.');

if (!function_exists('stampaValutaHtml')) {

    /* stampa valuta da duoble con . a stringa con , in carratteri html */
    function stampaValutaHtml($val, $ico, $zero) {
		$val = number_format($val, 2);
		
		if($val > 0 || $val < 0) {
			$printVal = str_replace(".", ",", $val);
			if($ico)
				return "&euro;" . $printVal;
			else
				return $printVal;
		} else {
			if($zero)
				if($ico)
					return "&euro;0,00";
				else
					return "0,00";	 
			else 
				return "";
		}	
	}

}

if (!function_exists('formattaData')) {
	/* formatta data in formato 'd/m/Y' e altri H:m:s*/
	function formattaData($data, $formato) {
		return date_format(date_create($data), $formato);
	}
}

if (!function_exists('formatta_data_stringa')) {
	// formatta data per il db da string italiana a inglese
	function formatta_data_stringa($data) {
		$array = explode("/", $data); 
		$data_db = $array[2]."-".$array[1]."-".$array[0]; 
		return $data_db; 	
	}
}

if (!function_exists('stampaValuta')) {

    /* stampa valuta da duoble con . a stringa con , */
	function stampaValuta($val, $ico, $zero) {
		$val = number_format($val, 2);
		
		if($val > 0 || $val < 0) {
			$printVal = str_replace(".", ",", $val);
			if($ico)
				return "€" . $printVal;
			else
				return $printVal;
		} else {
			if($zero)
				if($ico)
					return "€0,00";
				else
					return "0,00";	 
			else 
				return "";
		}	
	}

}

if (!function_exists('cleanString')) {
	/* Elimina caratteri non ammessi nei permalink */
	function cleanString($string, $to_lower){
		$strResult = str_ireplace("à", "a", $string);
		$strResult  = str_ireplace("á", "a", $strResult);
		$strResult =  str_ireplace("è", "e", $strResult);
		$strResult =  str_ireplace("é", "e", $strResult);
		$strResult =  str_ireplace("ì", "i", $strResult);
		$strResult =  str_ireplace("í", "i", $strResult);
		$strResult =  str_ireplace("ò", "o", $strResult);
		$strResult =  str_ireplace("ó", "o", $strResult);
		$strResult =  str_ireplace("ù", "u", $strResult);
		$strResult =  str_ireplace("ú", "u", $strResult);
		$strResult =  str_ireplace("ç", "c", $strResult);
		$strResult =  str_ireplace("ö", "o", $strResult);
		$strResult =  str_ireplace("û", "u", $strResult);
		$strResult =  str_ireplace("ê", "e", $strResult);
		$strResult =  str_ireplace("ü", "u", $strResult);
		$strResult =  str_ireplace("ë", "e", $strResult);
		$strResult =  str_ireplace("ä", "a", $strResult);
		$strResult =  str_ireplace("'", " ", $strResult);
	 
		$strResult = preg_replace('/[^A-Za-z0-9 ]/', "", $strResult);
		$strResult = trim($strResult);
		$strResult =  preg_replace('/[ ]{2,}/', " ", $strResult);
	 
		$strResult = str_replace(" ", "-", $strResult);
	 
		return ($to_lower ? strtolower($strResult) : $strResult);
	}
}

// genera il file di lingua selezionato fronted | email | pages
if (!function_exists('update_lang_file')) {
	function update_lang_file($labels, $lang, $pages, $lingue_labels_type){
		
$langstr = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Language file ".$lingue_labels_type." - ".$lang->nome_lingue."
*/\n";

// frontend_lang
if($lingue_labels_type == 'frontend') {
	$langstr.= "/* LANG CONTENTS */\n";
	$langstr.= "\$lang['LANGUAGE_NAME'] = \"".addslashes($lang->nome_lingue)."\";"."\n";
	$langstr.= "\$lang['LANGUAGE_ABBR'] = \"".addslashes($lang->abbr_lingue)."\";"."\n";
	$langstr.= "\$lang['LANGUAGE_ID'] = \"".addslashes($lang->id_lingue)."\";"."\n";
	$langstr.= "\$lang['LANGUAGE_LOCALE_PAYPAL'] = \"".addslashes($lang->locale_paypal_lingue)."\";"."\n";
	$langstr.= "\$lang['LANGUAGE_CI'] = \"".addslashes($lang->codice_ci)."\";"."\n";
	
	$langstr.= "/* PAGE URL SHORTCUT */\n";
	foreach($pages as $page)
	{
		$langstr.= "\$lang['".$page->label_page_url."'] = \"".addslashes($page->url_pagina)."\";"."\n";
	}
}

// email_lang
if($lingue_labels_type == 'email') {
	$langstr.= "/* PAGE URL EMAIL SHORTCUT */\n";
	foreach($pages as $page)
	{
		$langstr.= "\$lang['".$page->label_page_url."_EMAIL'] = \"".addslashes($page->url_pagina)."\";"."\n";
	}
}

// pages_lang
if($lingue_labels_type == 'pages') {
	
	$langstr.= "/* MENU */\n";
	foreach($pages as $page)
	{
		$langstr.= "\$lang['".$page->nome_menu."'] = \"".addslashes($page->testo_menu)."\";"."\n";
	}

	foreach($labels as $page_labels)
	{
		$langstr.= "/* ".$page_labels->code." */\n";
		$langstr.= "\$lang['PAGE_".$page_labels->code."_CODE'] = \"".addslashes($page_labels->code)."\";"."\n";
		$langstr.= "\$lang['PAGE_".$page_labels->code."_TITLE'] = \"".addslashes($page_labels->title)."\";"."\n";
		$langstr.= "\$lang['PAGE_".$page_labels->code."_META_DESCRIPTION'] = \"".addslashes($page_labels->meta_description)."\";"."\n";
		$langstr.= "\$lang['PAGE_".$page_labels->code."_DESCRIPTION'] = \"".addslashes($page_labels->description)."\";"."\n";
		$langstr.= "\$lang['PAGE_".$page_labels->code."_IMAGE'] = \"".addslashes($page_labels->image)."\";"."\n";
	}
	
} else {
		
	// commons label no PAGES
	$langstr.= "/* LABELS ".$lingue_labels_type." */\n";
	foreach($labels as $label)
	{
		$langstr.= "\$lang['".$label->lingue_labels_lang_label."'] = \"".addslashes($label->lingue_labels_lang_value)."\";"."\n";
	}
	
}
		write_file('./application/language/'.$lang->codice_ci.'/'.$lingue_labels_type.'_lang.php', $langstr);

    }
}

// generate config file on the fly
if (!function_exists('updateconfigfile')) {
	function updateconfigfile($result){

$confstr = "<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');		
/* SITE_URL_ROOT è definita nella ROOT index.php */\n";
	  
foreach ($result as $row){
	$confstr .= "define('".$row->cf_name."', '".addslashes($row->cf_value)."');\n";
}

$confstr .= "// Genearl constants\n";
$confstr .= "require_once(APPPATH . 'config/constants_general.php');\n";
$confstr .= "/* End of file constants.php */\n";
$confstr .= "/* Location: ./application/config/constants.php */\n";

$confstr .= "?>";
        write_file('./application/config/production/constants.php', $confstr);

    }
}

// crete url from Menu option name
if (!function_exists('createUrlMenu')) {
	function createUrlMenu($my_string){
        return base_url() . str_replace(' ', '', strtolower(transliterateString($my_string)));
    }
}

if (!function_exists('transliterateString')) {	
	function transliterateString($txt) {
		$transliterationTable = array('á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A', 'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 'ą' => 'a', 'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'ae', 'Ä' => 'AE', 'æ' => 'ae', 'Æ' => 'AE', 'ḃ' => 'b', 'Ḃ' => 'B', 'ć' => 'c', 'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C', 'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ḋ' => 'd', 'Ḋ' => 'D', 'đ' => 'd', 'Đ' => 'D', 'ð' => 'dh', 'Ð' => 'Dh', 'é' => 'e', 'É' => 'E', 'è' => 'e', 'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E', 'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'ė' => 'e', 'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E', 'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 'ğ' => 'g', 'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G', 'ģ' => 'g', 'Ģ' => 'G', 'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h', 'Ħ' => 'H', 'í' => 'i', 'Í' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i', 'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 'ī' => 'i', 'Ī' => 'I', 'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k', 'Ķ' => 'K', 'ĺ' => 'l', 'Ĺ' => 'L', 'ľ' => 'l', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L', 'ł' => 'l', 'Ł' => 'L', 'ṁ' => 'm', 'Ṁ' => 'M', 'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N', 'ņ' => 'n', 'Ņ' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ò' => 'o', 'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'o', 'Ő' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ø' => 'oe', 'Ø' => 'OE', 'ō' => 'o', 'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'oe', 'Ö' => 'OE', 'ṗ' => 'p', 'Ṗ' => 'P', 'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r', 'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 'ś' => 's', 'Ś' => 'S', 'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's', 'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's', 'Ș' => 'S', 'ß' => 'SS', 'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T', 'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't', 'Ŧ' => 'T', 'ú' => 'u', 'Ú' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'ų' => 'u', 'Ų' => 'U', 'ū' => 'u', 'Ū' => 'U', 'ư' => 'u', 'Ư' => 'U', 'ü' => 'ue', 'Ü' => 'UE', 'ẃ' => 'w', 'Ẃ' => 'W', 'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w', 'Ẅ' => 'W', 'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y', 'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 'ź' => 'z', 'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z', 'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a', 'б' => 'b', 'Б' => 'b', 'в' => 'v', 'В' => 'v', 'г' => 'g', 'Г' => 'g', 'д' => 'd', 'Д' => 'd', 'е' => 'e', 'Е' => 'E', 'ё' => 'e', 'Ё' => 'E', 'ж' => 'zh', 'Ж' => 'zh', 'з' => 'z', 'З' => 'z', 'и' => 'i', 'И' => 'i', 'й' => 'j', 'Й' => 'j', 'к' => 'k', 'К' => 'k', 'л' => 'l', 'Л' => 'l', 'м' => 'm', 'М' => 'm', 'н' => 'n', 'Н' => 'n', 'о' => 'o', 'О' => 'o', 'п' => 'p', 'П' => 'p', 'р' => 'r', 'Р' => 'r', 'с' => 's', 'С' => 's', 'т' => 't', 'Т' => 't', 'у' => 'u', 'У' => 'u', 'ф' => 'f', 'Ф' => 'f', 'х' => 'h', 'Х' => 'h', 'ц' => 'c', 'Ц' => 'c', 'ч' => 'ch', 'Ч' => 'ch', 'ш' => 'sh', 'Ш' => 'sh', 'щ' => 'sch', 'Щ' => 'sch', 'ъ' => '', 'Ъ' => '', 'ы' => 'y', 'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 'e', 'Э' => 'e', 'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja');
		return str_replace(array_keys($transliterationTable), array_values($transliterationTable), $txt);
	}
}

if (!function_exists('formattaUrlCodice')) {
	// formatta Url o Codice senza spaci e con separatore
	function formattaUrlCodice($urlCodice, $separatore) {
		if($separatore == '')
			$separatore = '_';
		return strtolower(str_replace(' ', $separatore, $urlCodice));
	}
}

/* Controlla se esiste e rimuove file */
if (!function_exists('check_remove_image_file')) {
    // formatta Url o Codice senza spaci e con separatore
    function check_remove_image_file($file_path) {
        # delete file if exists
        if (file_exists($file_path)) {
            echo 'exist';
            unlink ($file_path);
        } else {
            echo 'not exist';
        }
    }
}

// Estrate dal testo una parte e mette i puntini
if (!function_exists('excerpt')) {
    function excerpt($title, $maxLength=100) {
      //  $title = strip_tags($title, '<p>');
        
        if (strlen($title) < $maxLength) {
            return $title;
        } else {
            
            $new = wordwrap($title, $maxLength - 2);
            $new = explode("\n", $new);
            
            $new = $new[0] . '...';
            
            return $new;
        }
    }
}
