<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Language file pages - ENGLISH
*/
/* MENU */
$lang['MENU_HOME'] = "Home";
$lang['MENU_SHOP'] = "MaKa World";
$lang['MENU_ABOUT'] = "About us";
$lang['MENU_GALLERY'] = "Gallery";
$lang['MENU_PRIVACY'] = "Privacy";
$lang['MENU_BLOG'] = "Blog";
$lang['MENU_CONTACTS'] = "Contacts";
/* HOME */
$lang['PAGE_HOME_CODE'] = "HOME";
$lang['PAGE_HOME_TITLE'] = "Original Party";
$lang['PAGE_HOME_META_DESCRIPTION'] = "";
$lang['PAGE_HOME_DESCRIPTION'] = "";
$lang['PAGE_HOME_IMAGE'] = "";
/* CONTACTS */
$lang['PAGE_CONTACTS_CODE'] = "CONTACTS";
$lang['PAGE_CONTACTS_TITLE'] = "Contacts and infos";
$lang['PAGE_CONTACTS_META_DESCRIPTION'] = "You can contact La porta di MaKa in many different ways: email, phone and socials.";
$lang['PAGE_CONTACTS_DESCRIPTION'] = "";
$lang['PAGE_CONTACTS_IMAGE'] = "";
/* PRIVACY */
$lang['PAGE_PRIVACY_CODE'] = "PRIVACY";
$lang['PAGE_PRIVACY_TITLE'] = "Privacy";
$lang['PAGE_PRIVACY_META_DESCRIPTION'] = "Find out how we treat your sensitive data, use cookies, and sessions data.";
$lang['PAGE_PRIVACY_DESCRIPTION'] = "<h3>
	Cookie policy</h3>
<div style=\"text-align: justify;\">
	This site uses some technical cookies that are used for navigation and to provide a service already requested by the user. They are not used for further purposes and are normally installed on most websites. A cookie is a small piece of information that is saved on the device of the user who visits a website. The cookie does not contain personal data and can not be used to identify you within other websites, including the analytics provider&#39;s website. Cookies can also be used to store the preferred settings, such as language and country, in order to make them immediately available on the next visit. We do not use IP addresses or cookies to personally identify users. We use the web analysis system in order to increase the efficiency of our portal.</div>
<div style=\"text-align: justify;\">
	&nbsp;</div>
<div style=\"text-align: justify;\">
	For more information on cookies we suggest you visit www.allaboutcookies.org which will provide you with information on how to manage according to your preferences, and possibly delete cookies depending on the browser you are using. On this website, we use the Google Analytics analysis system to measure and analyze visits to our site. We use IP addresses to collect data on Internet traffic, browser and users&#39; computers. This information is only examined for statistical purposes. The anonymity of the user is respected. Information on the operation of open source web analytics software Google Analytics.&nbsp;</div>
<div style=\"text-align: justify;\">
	&nbsp;</div>
<div style=\"text-align: justify;\">
	We reiterate that on the site are operating only technical cookies (such as those listed above) necessary to navigate and essential such as authentication, validation, management of a browsing session and prevention of fraud and allow for example: to identify if the user has had regularly access to areas of the site that require prior authentication or user validation and management of sessions related to the various services and applications or the storage of data for access in secure mode or the functions of control and prevention of fraud.</div>
<div style=\"text-align: justify;\">
	It is not mandatory to acquire the consent to the operation of only technical or third-party cookies or analytics similar to technical cookies. Their deactivation and / or denial to their operation will make it impossible to properly browse the site and / or the inability to use the services, pages, features or content available there.</div>
<div style=\"text-align: justify;\">
	&nbsp;</div>
<div style=\"text-align: justify;\">
	All data entered by our users within modules will be used exclusively to respond to their requests. For this reason they are obliged to provide the data necessary to fill in the required forms, otherwise it will not be possible to use our service.</div>
<div style=\"text-align: justify;\">
	In any case, we confirm that the data used and stored for the purpose of the service will never be transferred to third parties under any circumstances for any purpose or use.</div>
";
$lang['PAGE_PRIVACY_IMAGE'] = "";
/* SHOP */
$lang['PAGE_SHOP_CODE'] = "SHOP";
$lang['PAGE_SHOP_TITLE'] = "MaKa World";
$lang['PAGE_SHOP_META_DESCRIPTION'] = "";
$lang['PAGE_SHOP_DESCRIPTION'] = "";
$lang['PAGE_SHOP_IMAGE'] = "";
/* ABOUT */
$lang['PAGE_ABOUT_CODE'] = "ABOUT";
$lang['PAGE_ABOUT_TITLE'] = "About us";
$lang['PAGE_ABOUT_META_DESCRIPTION'] = "La porta di MaKa was born in Rome from ideas of Martina and Katy";
$lang['PAGE_ABOUT_DESCRIPTION'] = "<p>
	La porta di MaKa was born in Rome from ideas of Martina and Katy</p>
<h3>
	continua con il testo</h3>
";
$lang['PAGE_ABOUT_IMAGE'] = "about.jpg";
/* GALLERY */
$lang['PAGE_GALLERY_CODE'] = "GALLERY";
$lang['PAGE_GALLERY_TITLE'] = "Images gallery";
$lang['PAGE_GALLERY_META_DESCRIPTION'] = "This is our best images gallery.";
$lang['PAGE_GALLERY_DESCRIPTION'] = "";
$lang['PAGE_GALLERY_IMAGE'] = "";
