<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Language file email - ENGLISH
*/
/* PAGE URL EMAIL SHORTCUT */
$lang['PAGE_HOME_URL_EMAIL'] = "en/home";
$lang['PAGE_SHOP_URL_EMAIL'] = "en/shop";
$lang['PAGE_ABOUT_URL_EMAIL'] = "en/about";
$lang['PAGE_SHIPPING_URL_EMAIL'] = "en/shipping";
$lang['PAGE_CONTACTS_URL_EMAIL'] = "en/contacts";
$lang['PAGE_RULES_URL_EMAIL'] = "en/rules";
$lang['PAGE_ACCOUNT_URL_EMAIL'] = "en/account";
$lang['PAGE_LOGOUT_URL_EMAIL'] = "en/logout";
$lang['PAGE_LOGIN_URL_EMAIL'] = "en/login";
$lang['PAGE_CART_URL_EMAIL'] = "en/cart";
$lang['PAGE_CHECKOUT_URL_EMAIL'] = "en/checkout";
$lang['PAGE_PRIVACY_URL_EMAIL'] = "en/privacy";
$lang['PAGE_GALLERY_URL_EMAIL'] = "en/gallery";
$lang['PAGE_PRODUCTS_URL_EMAIL'] = "en/products";
/* LABELS email */
$lang['LABEL_MY_ACCOUNT_EMAIL'] = "My account";
$lang['LABEL_SEE_EMAIL_ONLINE'] = "See contents online";
$lang['LABEL_EMAIL_SALES_TITLE'] = "Check out our newest sales!";
$lang['LABEL_DETAIL_EMAIL'] = "See details";
$lang['TEXT_EMAIL_FOOTER_USUBSCRIBE'] = "unsubscribe";
$lang['TEXT_EMAIL_FOOTER_RESERVED'] = "All rights reserved";
$lang['TEXT_EMAIL_FOOTER_COPYRIGHT'] = "If you no longer wish to receive these emails please";
$lang['LABEL_EMAIL_SUBJECT_CONTACT'] = "Contact from";
$lang['LABEL_EMAIL_CONTACT_TITLE'] = "Thank you for contact us!";
$lang['LABEL_EMAIL_CONTACT_TEXT'] = "We have received your communication and will send you an answer as soon as possible. Thank you!";
$lang['LABEL_EMAIL_SUBJECT_NEWSLETTER'] = "Newsletter signup";
$lang['LABEL_EMAIL_NEWSLETTER_TITLE'] = "Thank you for signing up to our newsletter!";
$lang['LABEL_EMAIL_NEWSLETTER_TEXT'] = "You will receive updates, news and exclusive offers to stay in touch with our world.";
$lang['LABEL_EMAIL_SUBJECT_WELCOME'] = "Welcome to Ma Chlò!";
$lang['LABEL_EMAIL_WELCOME_TITLE'] = "Welcome to Ma Chlò!";
$lang['LABEL_EMAIL_WELCOME_TEXT'] = "Thank you for signing up! We hope you enjoy your time with us. Check out some of our newest offers below or the button to view your new account.";
$lang['LABEL_EMAIL_SUBJECT_NEW_ORDER'] = "New order";
$lang['LABEL_TITLE_INVOICE'] = "Order invoice";
$lang['LABEL_INVOICE_THANKS'] = "Thank you for your order!";
$lang['LABEL_INVOICE_THANKS_TEXT'] = "We\\\'ll let you know as soon as your items have shipped.<br>To change or view your order, please view your account by clicking the button below.";
$lang['LABEL_INVOICE_THANKS_TEXT_NOUSER'] = "We\'ll let you know as soon as your items have shipped.";
$lang['LABEL_SHIPPING_ADDRESS_EMAIL'] = "Shipping address";
$lang['LABEL_ORDER_EMAIL'] = "Order";
$lang['LABEL_ORDER_DATE_EMAIL'] = "Order date";
$lang['LABEL_ADDRESS_NOTES_EMAIL'] = "Address notes";
$lang['LABEL_ORDER_NOTES_EMAIL'] = "Order notes";
$lang['LABEL_DESCRIPTION_EMAIL'] = "Description";
$lang['LABEL_QTY_EMAIL'] = "Qty";
$lang['LABEL_SIZE_EMAIL'] = "Size";
$lang['LABEL_COLOR_EMAIL'] = "Color";
$lang['LABEL_SUBTOTAL_ORDER_EMAIL'] = "Subtotal";
$lang['LABEL_COUPON_APPLY_EMAIL'] = "Apply coupon";
$lang['LABEL_SHIPPING_EMAIL'] = "Shipping";
$lang['LABEL_TOTAL_EMAIL'] = "Total";
