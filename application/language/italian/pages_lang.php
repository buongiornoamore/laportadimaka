<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Language file pages - ITALIANO
*/
/* MENU */
$lang['MENU_HOME'] = "Home";
$lang['MENU_SHOP'] = "MaKa World";
$lang['MENU_ABOUT'] = "Chi siamo";
$lang['MENU_GALLERY'] = "Gallery";
$lang['MENU_PRIVACY'] = "Privacy";
$lang['MENU_BLOG'] = "Blog";
$lang['MENU_CONTACTS'] = "Contatti";
/* HOME */
$lang['PAGE_HOME_CODE'] = "HOME";
$lang['PAGE_HOME_TITLE'] = "Feste originali";
$lang['PAGE_HOME_META_DESCRIPTION'] = "Benvenuti nella porta di MaKa.";
$lang['PAGE_HOME_DESCRIPTION'] = "";
$lang['PAGE_HOME_IMAGE'] = "";
/* SHOP */
$lang['PAGE_SHOP_CODE'] = "SHOP";
$lang['PAGE_SHOP_TITLE'] = "MaKa World";
$lang['PAGE_SHOP_META_DESCRIPTION'] = "";
$lang['PAGE_SHOP_DESCRIPTION'] = "";
$lang['PAGE_SHOP_IMAGE'] = "";
/* ABOUT */
$lang['PAGE_ABOUT_CODE'] = "ABOUT";
$lang['PAGE_ABOUT_TITLE'] = "Chi siamo";
$lang['PAGE_ABOUT_META_DESCRIPTION'] = "La porta di MaKa nasce a Roma dalle idee di Martina e Katy";
$lang['PAGE_ABOUT_DESCRIPTION'] = "<p>
	La porta di MaKa nasce a Roma dalle idee di Martina e Katy...</p>
<p>
	Aggiungere il testo</p>
";
$lang['PAGE_ABOUT_IMAGE'] = "about.jpg";
/* GALLERY */
$lang['PAGE_GALLERY_CODE'] = "GALLERY";
$lang['PAGE_GALLERY_TITLE'] = "Galleria immagini";
$lang['PAGE_GALLERY_META_DESCRIPTION'] = "Ecco una galleria delle nostre immagini più belle.";
$lang['PAGE_GALLERY_DESCRIPTION'] = "";
$lang['PAGE_GALLERY_IMAGE'] = "";
/* CONTACTS */
$lang['PAGE_CONTACTS_CODE'] = "CONTACTS";
$lang['PAGE_CONTACTS_TITLE'] = "Contatti e recapiti";
$lang['PAGE_CONTACTS_META_DESCRIPTION'] = "Puoi contattare La Porta di MaKa in tantissimi modi diversi: email, telefono e socials.";
$lang['PAGE_CONTACTS_DESCRIPTION'] = "";
$lang['PAGE_CONTACTS_IMAGE'] = "";
/* PRIVACY */
$lang['PAGE_PRIVACY_CODE'] = "PRIVACY";
$lang['PAGE_PRIVACY_TITLE'] = "Privacy";
$lang['PAGE_PRIVACY_META_DESCRIPTION'] = "Scopri in che modo vengo trattati i tuoi dati sensibili, l\'utilizzo dei cookie e dei dati di sessione.";
$lang['PAGE_PRIVACY_DESCRIPTION'] = "<h3>
	Cookie policy</h3>
<p style=\"text-align: justify;\">
	In questo sito vengono utilizzati alcuni cookie tecnici che servono per la navigazione e per fornire un servizio gi&agrave; richiesto dall&#39;utente. Non vengono utilizzati per scopi ulteriori e sono normalmente installati nella maggior parte dei siti web. Un cookie &egrave; una piccola particella di informazioni che viene salvata sul dispositivo dell&#39;utente che visita un sito web. Il cookie non contiene dati personali e non pu&ograve; essere utilizzato per identificare l&#39;utente all&#39;interno di altri siti web, compreso il sito web del provider di analisi. I cookie possono inoltre essere utilizzati per memorizzare le impostazioni preferite, come lingua e paese, in modo da renderle immediatamente disponibili alla visita successiva. Non utilizziamo gli indirizzi IP o i cookie per identificare personalmente gli utenti. Utilizziamo il sistema di analisi web al fine di incrementare l&#39;efficienza del nostro portale.<br />
	<br />
	Per avere maggiori informazioni sui cookies vi suggeriamo di visitare il sito www.allaboutcookies.org che vi fornir&agrave; indicazioni su come gestire secondo le vostre preferenze, ed eventualmente cancellare i cookies in funzione del browser che state utilizzando.<br />
	In questo sito web utilizziamo il sistema di analisi Google Analytics per misurare e analizzare le visite al nostro sito. Utilizziamo gli indirizzi IP al fine di raccogliere dati sul traffico Internet, sul browser e sul computer degli utenti. Tali informazioni vengono esaminate unicamente per fini statistici. L&#39;anonimato dell&#39;utente viene rispettato. Informazioni sul funzionamento del software open source di analisi web Google Analytics.<br />
	<br />
	Ribadiamo che sul sito sono operativi esclusivamente cookies tecnici (come quelli sopra elencati) necessari per navigare e che essenziali quali autenticazione, validazione, gestione di una sessione di navigazione e prevenzione delle frodi e consentono ad esempio: di identificare se l&rsquo;utente ha avuto regolarmente accesso alle aree del sito che richiedono la preventiva autenticazione oppure la validazione dell&rsquo;utente e la gestione delle sessioni relative ai vari servizi e applicazioni oppure la conservazione dei dati per l&rsquo;accesso in modalit&agrave; sicura oppure le funzioni di controllo e prevenzione delle frodi.<br />
	Non &egrave; obbligatorio acquisire il consenso alla operativit&agrave; dei soli cookies tecnici o di terze parti o analitici assimilati ai cookies tecnici. La loro disattivazione e/o il diniego alla loro operativit&agrave; comporter&agrave; l&rsquo;impossibilit&agrave; di una corretta navigazione sul Sito e/o la impossibilit&agrave; di fruire dei servizi, delle pagine, delle funzionalit&agrave; o dei contenuti ivi disponibili.<br />
	<br />
	Tutti i dati inseriti dai nostri utenti all&#39;interno di moduli verranno utilizzati esclusivamente per rispondere alle loro richieste. Per questo motivo sono obbligati a fornire i dati necessari alla compilazione dei moduli richiesti, in caso contrario non sar&agrave; possibile utilizzare il nostro servizio.<br />
	In ogni caso confermiamo che i dati utilizzati e memorizzati al fine del funzionamento del servizio non verranno mai e in nessun caso ceduti a terzi per nessun tipo di finalit&agrave; od utilizzo.</p>
";
$lang['PAGE_PRIVACY_IMAGE'] = "";
/* BLOG */
$lang['PAGE_BLOG_CODE'] = "BLOG";
$lang['PAGE_BLOG_TITLE'] = "Blog";
$lang['PAGE_BLOG_META_DESCRIPTION'] = "";
$lang['PAGE_BLOG_DESCRIPTION'] = "";
$lang['PAGE_BLOG_IMAGE'] = "";
