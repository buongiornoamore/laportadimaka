<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Language file email - ITALIANO
*/
/* PAGE URL EMAIL SHORTCUT */
$lang['PAGE_HOME_URL_EMAIL'] = "it/home";
$lang['PAGE_SHOP_URL_EMAIL'] = "it/makaworld";
$lang['PAGE_ABOUT_URL_EMAIL'] = "it/chisiamo";
$lang['PAGE_PRODUCTS_URL_EMAIL'] = "it/dettaglio";
$lang['PAGE_CONTACTS_URL_EMAIL'] = "it/contatti";
$lang['PAGE_PRIVACY_URL_EMAIL'] = "it/privacy";
$lang['PAGE_GALLERY_URL_EMAIL'] = "it/gallery";
$lang['PAGE_BLOG_URL_EMAIL'] = "it/blog";
/* LABELS email */
$lang['LABEL_MY_ACCOUNT_EMAIL'] = "Il mio profilo";
$lang['LABEL_SEE_EMAIL_ONLINE'] = "Vedi email online";
$lang['LABEL_EMAIL_SALES_TITLE'] = "Controlla le nostre ultime offerte!";
$lang['LABEL_DETAIL_EMAIL'] = "Vedi i dettagli";
$lang['TEXT_EMAIL_FOOTER_USUBSCRIBE'] = "cancellati";
$lang['TEXT_EMAIL_FOOTER_RESERVED'] = "Tutti i diritti riservati";
$lang['TEXT_EMAIL_FOOTER_COPYRIGHT'] = "Se non vuoi più ricevere queste email per favore";
$lang['LABEL_EMAIL_SUBJECT_CONTACT'] = "Contatto dal sito";
$lang['LABEL_EMAIL_CONTACT_TITLE'] = "Ti ringraziamo per averci contattato!";
$lang['LABEL_EMAIL_CONTACT_TEXT'] = "Abbiamo ricevuto la tua comunicazione e ti invieremo una risposta nel minor tempo possibile. Grazie!";
$lang['LABEL_EMAIL_SUBJECT_NEWSLETTER'] = "Iscrizione alla Newsletter";
$lang['LABEL_EMAIL_NEWSLETTER_TITLE'] = "Grazie per esserti iscritto alla nostra newsletter!";
$lang['LABEL_EMAIL_NEWSLETTER_TEXT'] = "Riceverai gli aggiornamenti, le news e le nostre offerte esclusive per restare in contatto con il nostro mondo.";
$lang['LABEL_EMAIL_SUBJECT_WELCOME'] = "Benvenuto su Ma Chlò";
$lang['LABEL_EMAIL_WELCOME_TITLE'] = "Benvenuto su Ma Chlò!";
$lang['LABEL_EMAIL_WELCOME_TEXT'] = "Grazie per esserti registrato! Speriamo che ti piaccia il nostro lavoro. Consulta alcune delle nostre ultime offerte qui sotto o clicca sul pulsante per visualizzare il tuo nuovo account.";
$lang['LABEL_EMAIL_SUBJECT_NEW_ORDER'] = "Nuovo ordine";
$lang['LABEL_TITLE_INVOICE'] = "Riepilogo ordine";
$lang['LABEL_INVOICE_THANKS'] = "Ti ringraziamo per il tuo ordine!";
$lang['LABEL_INVOICE_THANKS_TEXT'] = "Ti faremo sapere non appena i tuoi articoli saranno spediti.<br>Per cambiare o visualizzare il tuo ordine, ti preghiamo di visualizzare il tuo account facendo clic sul pulsante qui sotto.";
$lang['LABEL_INVOICE_THANKS_TEXT_NOUSER'] = "Ti faremo sapere non appena i tuoi articoli saranno spediti.";
$lang['LABEL_SHIPPING_ADDRESS_EMAIL'] = "Indirizzo di spedizione";
$lang['LABEL_ORDER_EMAIL'] = "Ordine";
$lang['LABEL_ORDER_DATE_EMAIL'] = "Data ordine";
$lang['LABEL_ADDRESS_NOTES_EMAIL'] = "Note indirizzo";
$lang['LABEL_ORDER_NOTES_EMAIL'] = "Note ordine";
$lang['LABEL_DESCRIPTION_EMAIL'] = "Descrizione";
$lang['LABEL_QTY_EMAIL'] = "Quantità";
$lang['LABEL_SIZE_EMAIL'] = "Taglia";
$lang['LABEL_COLOR_EMAIL'] = "Colore";
$lang['LABEL_SUBTOTAL_ORDER_EMAIL'] = "Subtotale";
$lang['LABEL_COUPON_APPLY_EMAIL'] = "Applica coupon";
$lang['LABEL_SHIPPING_EMAIL'] = "Spedizione";
$lang['LABEL_TOTAL_EMAIL'] = "Totale";
