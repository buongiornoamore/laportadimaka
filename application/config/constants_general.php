<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/* Framework */
define('ASSETS_ROOT_FOLDER_FRAMEWORK',		    SITE_URL_PATH . '/assets/assets-framework');
define('ASSETS_ROOT_FOLDER_FRAMEWORK_CSS',		SITE_URL_PATH . '/assets/assets-framework/css');
define('ASSETS_ROOT_FOLDER_FRAMEWORK_JS',		SITE_URL_PATH . '/assets/assets-framework/js');
define('ASSETS_ROOT_FOLDER_FRAMEWORK_IMG',		SITE_URL_PATH . '/assets/assets-framework/img');
define('ASSETS_ROOT_FOLDER_FRAMEWORK_FONTS',	SITE_URL_PATH . '/assets/assets-framework/fonts');
/* Frontend assets global vars */
define('ASSETS_ROOT_FOLDER_FRONTEND',		    SITE_URL_PATH . '/assets/assets-frontend');
define('ASSETS_ROOT_FOLDER_FRONTEND_CSS',		SITE_URL_PATH . '/assets/assets-frontend/css');
define('ASSETS_ROOT_FOLDER_FRONTEND_JS',		SITE_URL_PATH . '/assets/assets-frontend/js');
define('ASSETS_ROOT_FOLDER_FRONTEND_IMG',		SITE_URL_PATH . '/assets/assets-frontend/img');
define('ASSETS_ROOT_FOLDER_FRONTEND_FONTS',		SITE_URL_PATH . '/assets/assets-frontend/fonts');
define('ASSETS_ROOT_FOLDER_FRONTEND_SASS',		SITE_URL_PATH . '/assets/assets-frontend/sass');
/* ADMIN */
define('ASSETS_ROOT_FOLDER_ADMIN',		        SITE_URL_PATH . '/assets/assets-admin');
define('ASSETS_ROOT_FOLDER_ADMIN_CSS',		    SITE_URL_PATH . '/assets/assets-admin/css');
define('ASSETS_ROOT_FOLDER_ADMIN_JS',		    SITE_URL_PATH . '/assets/assets-admin/js');
define('ASSETS_ROOT_FOLDER_ADMIN_IMG',		    SITE_URL_PATH . '/assets/assets-admin/img');
define('ASSETS_ROOT_FOLDER_ADMIN_FONTS',		SITE_URL_PATH . '/assets/assets-admin/fonts');
define('ASSETS_ROOT_FOLDER_ADMIN_SASS',		    SITE_URL_PATH . '/assets/assets-admin/sass');
/* End of file constants_general.php */
/* Location: ./application/config/constants_general.php */
