<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'defaultcontroller';
$route['404_override'] = 'defaultcontroller/redirect_404';

$route['test/(:any)'] = 'frontend/Home/test/$1';

/* Base url */
$route['comingsoon'] = 'comingsoon';
$route['mail'] = 'Mail';
$route['mail/newsletter'] = 'Mail/newsletter';
$route['mail/contactus'] = 'Mail/contactus';
$route['email/invoice/(:any)'] = 'Mail/invoice_viewer/$1';
$route['email/(:any)/(:any)'] = 'Mail/email_viewer/$1/$2';
$route['email_template/(:any)/(:any)'] = 'Mail/html_online_viewer/$1/$2';
$route['admin/send_email_template/(:any)'] = 'Mail/send_email_template/$1'; 
$route['admin/send_email_libera'] = 'Mail/send_email_libera';
$route['unsubscribe_confirm/(:any)/(:any)'] = 'admin/Users/unsubscribe_confirm/$1/$2';
$route['unsubscribe/(:any)/(:any)/(:any)'] = 'admin/Users/unsubscribe/$1/$2/$3';
$route['unsubscribe_execute/(:any)/(:any)/(:any)'] = 'admin/Users/unsubscribe_execute/$1/$2/$3';
$route['frontend/reset_password/(:any)'] = 'frontend/Account/reset_password/$1';
$route['forgot_password'] = 'frontend/Account/forgot_password';

/* Sitemap url */
$route['sitemap\.xml'] = "Sitemap";
$route['(:any)/sitemap_pages\.xml'] = "Sitemap/pages/$1";
$route['(:any)/sitemap_products\.xml'] = "Sitemap/products/$1";
$route['(:any)/sitemap_images\.xml'] = "Sitemap/images/$1";

/* Frontend url from tables */
// PAGES
require_once( BASEPATH .'database/DB'. EXT );
$db =& DB();
$query = $db->get('pagine');
$result = $query->result();
foreach( $result as $row )
{
	if($row->url_pagina != '' && $row->controller != '' && $row->url_pagina != 'default_controller') {
    	$route[ $row->url_pagina ]           = $row->controller;
    	//$route[ $row->url_pagina.'/:any' ]   = $row->controller;
    	//$route[ $row->controller ]           = 'error404';
    	//$route[ $row->controller.'/:any' ]   = 'error404';
	}
}

/* Admin url */
$route['admin'] = 'admin/Users';
$route['admin/login'] = 'admin/Users/login';
$route['admin/logout'] = 'admin/Users/logout';
$route['admin/forgot'] = 'admin/Users/forgot_password';
$route['admin/reset_password/(:any)'] = 'admin/Users/reset_password/$1';
//$route['admin/dashboard'] = 'admin/Dashboard';
$route['admin/register'] = 'admin/Users/register';

/* 
Si possono registrare più metodi/azioni per diversi CRUD nello stesso controller 
url              controller   metodo
'orders' => array('Orders', 'crud'),
'orders/:any' => array('Orders', 'crud/:any'),
*/
$admin_crud_routes = array(
    'utenti' => array('AdminUtenti', 'crud'),
	'products' => array('AdminProducts', 'crud'),
	'products/variants/(:any)/(:any)' => array('AdminProducts', 'variants/$1/$2'),
	'products/trad/(:any)/(:any)' => array('AdminProducts', 'traductions/$1/$2'),
	'categories' => array('AdminCategories', 'crud'),
	'categories/trad/(:any)' => array('AdminCategories', 'traductions/$1'),
	'gallerycategories' => array('AdminGalleryCategories', 'crud'),
	'gallerycategories/trad/(:any)/(:any)' => array('AdminGalleryCategories', 'traductions/$1/$2'),
	'galleryimages' => array('AdminGalleryImages', 'crud'),
	'galleryimages/trad/(:any)/(:any)' => array('AdminGalleryImages', 'traductions/$1/$2'),
	'emailtemplates' => array('AdminEmailTemplates', 'crud'),
	'emailtemplatesdefault' => array('AdminEmailTemplatesDefault', 'crud'),
	'contatti' => array('AdminContatti', 'crud'),
	'contatti/email' => array('AdminContatti', 'email'),
	'newsletter' => array('AdminNewsletter', 'crud'),
	'newsletter/email' => array('AdminNewsletter', 'email'),
	'configurazioni' => array('AdminConfigurazioni', 'crud'),
	'impostazioni' => array('AdminImpostazioni', 'crud'),
	'languages' => array('AdminLanguages', 'crud'),
	'languages/trad/(:any)' => array('AdminLanguages', 'traductions/$1'),
	'pages' => array('AdminPagine', 'crud'),
	'homeslider' => array('AdminHomeSlider', 'crud'),
	'homeslider/trad/(:any)/(:any)' => array('AdminHomeSlider', 'traductions/$1/$2'),
    'team' => array('AdminTeam', 'crud'),
    'blogpost' => array('AdminBlog', 'post'),
    'blogcategory' => array('AdminBlog', 'category'),
    'blogtag' => array('AdminBlog', 'tag'),
    'blogcomments' => array('AdminBlog', 'comments'),
    'blogcommentsbypost/(:any)' => array('AdminBlog', 'commentsbypost/$1')
 );
 
 foreach($admin_crud_routes as $key => $value){
	$controller_routes = $value[0];
	foreach($value as $keyAction => $valueAction){
		if($valueAction != $controller_routes)
			$route['admin/'.$key] = 'admin/'.$controller_routes.'/'.$valueAction;
			$route['admin/'.$key.'/print'] = 'admin/'.$controller_routes.'/'.$valueAction.'/print';
			$route['admin/'.$key.'/export'] = 'admin/'.$controller_routes.'/'.$valueAction.'/export';
			$route['admin/'.$key.'/delete_multiple'] = 'admin/'.$controller_routes.'/'.$valueAction.'/delete_multiple';
			$route['admin/'.$key.'/upload_file/(:any)'] = 'admin/'.$controller_routes.'/'.$valueAction.'/upload_file/$1';
			$route['admin/'.$key.'/delete_file/(:any)'] = 'admin/'.$controller_routes.'/'.$valueAction.'/delete_file/$1';
			$route['admin/'.$key.'/add'] = 'admin/'.$controller_routes.'/'.$valueAction.'/add';
			$route['admin/'.$key.'/insert'] = 'admin/'.$controller_routes.'/'.$valueAction.'/insert';
			$route['admin/'.$key.'/insert_validation'] = 'admin/'.$controller_routes.'/'.$valueAction.'/insert_validation';
			$route['admin/'.$key.'/success/:num'] = 'admin/'.$controller_routes.'/'.$valueAction.'/success';
			$route['admin/'.$key.'/delete/:num'] = 'admin/'.$controller_routes.'/'.$valueAction.'/delete';
			$route['admin/'.$key.'/edit/:num'] = 'admin/'.$controller_routes.'/'.$valueAction.'/edit';
			$route['admin/'.$key.'/update_validation/:num'] = 'admin/'.$controller_routes.'/'.$valueAction.'/update_validation';
			$route['admin/'.$key.'/update/:num'] = 'admin/'.$controller_routes.'/'.$valueAction.'/update';
			$route['admin/'.$key.'/ajax_list_info'] = 'admin/'.$controller_routes.'/'.$valueAction.'/ajax_list_info';
			$route['admin/'.$key.'/ajax_list'] = 'admin/'.$controller_routes.'/'.$valueAction.'/ajax_list';
			$route['admin/'.$key.'/read/:num'] = 'admin/'.$controller_routes.'/'.$valueAction.'/read';
	}
}

/* Admin CUSTOM routes */
// AdminConfigurazioni
$route['admin/updateSiteStatus'] = 'admin/AdminConfigurazioni/updateSiteStatus';
$route['generate_production_config'] = 'admin/AdminConfigurazioni/generateProductionConfigFile';
/* End of file routes.php */
/* Location: ./application/config/routes.php */

