<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Global frmework */
/* SITE_URL_ROOT è definita nella ROOT index.php */
define('SITE_URL_PATH',		               		'http://localhost/laportadimaka');
define('SITE_TITLE_NAME',		               	'La porta di MaKa');
/* Email smtp global vars */
define('SMTP_HOST_CUSTOM',		                'mail.laportadimaka.com');
define('SMTP_USER_CUSTOM',		                'info@laportadimaka.com');
define('SMTP_PASS_CUSTOM',		                'info18@Maka');
/* Database MySql global vars */
define('DB_HOSTNAME',	    	                'localhost');
define('DB_USERNAME',	    	                'root');
define('DB_PASSWORD',	    	                '');
define('DB_DATABASE',	    	                'laportad_framework');
/* Company global vars */
define('COMPANY_NAME',		                    'La porta di Maka');
define('COMPANY_EMAIL',		                    'info@laportadimaka.com');
define('COMPANY_PHONE',		                    ' +39 3286105475 | +39 3391037223');
define('COMPANY_ADDRESS',		                'Roma');
define('GPLUS_LINK',		                    '');
define('YOUTUBE_LINK',		                    '');
define('PINTEREST_LINK',		                '');
define('TWITTER_LINK',		                    '');
define('FACEBOOK_LINK',                         'https://www.facebook.com/laportadimaka/');
define('INSTAGRAM_LINK',                        'https://www.instagram.com/la_porta_di_maka/');
define('GOOGLE_ANALITYCS_ID',		            '');
define('SMARTSUPP_ID',		                    '370f8baf5d6ce1e020e8de6e8f803908056dbfe1');
/* Coming soon OPTIONS */
define('COMNINGSOON_LOGO',		                'logo_medium.png');
define('COMNINGSOON_BACK_IMAGE',		        'default.jpg');
define('COMNINGSOON_BTN_COLOR',		            'EE2D20');
/* Payments info */

// Genearl constants
require_once(APPPATH . 'config/constants_general.php');
/* End of file constants.php */
/* Location: ./application/config/constants.php */
?>