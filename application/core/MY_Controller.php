<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('framework_helper');
		$this->load->library('form_validation');
		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth');
		$this->lang->load('auth', 'italian');
		$this->lang->load('ion_auth');
		$this->load->helper('cookie'); 
		$this->load->library('email');
	}
		
	public function send_email_text($subject, $testo_email, $to_email, $lingua_id) {
	    
	    // recupera la lingua dal db
	    $this->db->where('id_lingue', $lingua_id);
	    $this->db->from('lingue');
	    $query = $this->db->get();
	    $lingua = $query->row();
	    
	    // setta il file di lingua corrente per le email
	    $this->lang->load('email', $lingua->codice_ci);
	    
	    $mail_reply = COMPANY_EMAIL;
	    
	/*    $data = array(
	        'template_name' => $subject,
	        'show_unsubscribe_link' => false,
	        'unsubscribe_link' => '',
	        'show_online_link' => false,
	        'online_link' => '',
	        'template_title' => $subject,
	        'template_text' => $testo_email,
	        'products' => null
	    );*/
	    
	//    $body = $this->load->view('admin/html_templates/template.php', $data, TRUE);
	    
	    $this->email->clear(TRUE);
	    $this->email
	    ->from($mail_reply, COMPANY_NAME)
	    ->reply_to($mail_reply)    // Optional, an account where a human being reads.
	    ->to($to_email)
	    ->subject($subject)
	    ->message($testo_email)
	    ->send();
	    
	}
	
	/* 
		Invia una email con il riepilogo del carrello in html e le informazioni dell'ordine
		$email_to: indirizzo email a cui inviare
		$lingua_traduzione_id: lingua della email
		$data:  array contenente i dati ordine per la costruzione del body html
		$send_to_company: true false se inviare la mail in copia alal company
		$show_only: se true mostra la pagina html con la mail altrimenti al invia tramite email senza mostrarla
	*/
	public function send_email_order_invoice($id_ordine, $send_to_company, $show_only) {		
		
		// carica dati ordine e invia nella lingua del cliente
		$this->db->from('ordini');
		$this->db->join('clienti', 'clienti.id_cliente = ordini.id_cliente');
		$this->db->join('lingue', 'lingue.id_lingue = clienti.id_lingua');
		$this->db->join('indirizzo_spedizione', 'indirizzo_spedizione.id_indirizzo_spedizione = ordini.id_indirizzo_spedizione', 'LEFT');
		$this->db->join('coupon', 'coupon.codice_coupon = ordini.coupon_code', 'LEFT');
		$this->db->join('indirizzo_fatturazione', 'indirizzo_fatturazione.id_indirizzo_fatturazione = ordini.id_indirizzo_fatturazione_spedizione', 'LEFT');
		$this->db->where('ordini.id_ordine', $id_ordine);
		$query_ordine = $this->db->get();
		
		if($query_ordine->num_rows()) {
			$ordine = $query_ordine->row();
			// carica lingua del cliente
			$this->lang->load('email', $ordine->codice_ci);
			
			// carica carrello da storico
			// prodotti
			$this->db->select('storico_carrello.*, prodotti.*');
			$this->db->from('storico_carrello');
			$this->db->join('prodotti', 'prodotti.id_prodotti = storico_carrello.id_prodotto');
		//	$this->db->join('taglie', 'taglie.id_taglia = storico_carrello.taglia');
		//	$this->db->join('colori_prodotti', 'colori_prodotti.id_colori_prodotti = storico_carrello.colore_prodotto_id', 'left');
			$this->db->where('storico_carrello.id_ordine', $id_ordine);
			$this->db->where('storico_carrello.id_variante', 0);
			$query_products = $this->db->get();
			
			// varianti
			$this->db->select('storico_carrello.*, prodotti.*, varianti_prodotti.*');
			$this->db->from('storico_carrello');
			$this->db->join('prodotti', 'prodotti.id_prodotti = storico_carrello.id_prodotto');
			$this->db->join('varianti_prodotti', 'varianti_prodotti.id_variante = storico_carrello.id_variante', 'left');
			//$this->db->join('taglie', 'taglie.id_taglia = storico_carrello.taglia');
			//$this->db->join('colori_prodotti', 'colori_prodotti.id_colori_prodotti = storico_carrello.colore_prodotto_id', 'left');
			$this->db->where('storico_carrello.id_ordine', $id_ordine);
			$this->db->where('storico_carrello.id_variante >', 0);
			$query_variants = $this->db->get();
			
			$customer = $ordine->cognome . ' ' . $ordine->nome;
			$address = ($ordine->id_indirizzo_spedizione > 0 ? ($ordine->riferimento_sped != '' ? $ordine->riferimento_sped : $customer ) . '<br>' . $ordine->indirizzo_sped . ', ' . $ordine->civico_sped .'<br>'. $ordine->cap_sped . ' - ' . $ordine->citta_sped . '<br>' . $ordine->nazione_sped : ($ordine->riferimento_fatt != '' ? $ordine->riferimento_fatt : $customer) . '<br>' . $ordine->indirizzo_fatt . ', ' . $ordine->civico_fatt .'<br>'. $ordine->cap_fatt . ' - ' . $ordine->citta_fatt . '<br>' . $ordine->nazione_fatt);
			$address_notes = ($ordine->id_indirizzo_spedizione > 0 ? $ordine->note_sped : $ordine->note_fatt);
			
			// recupera email del cliente
			$email_to = '';
			if($ordine->user_id > 0) {
				$this->db->from('users');
				$this->db->where('id', $ordine->user_id);
				$query_user = $this->db->get();
				$user = $query_user->row();
				$email_to = $user->email; 
			} else {
				$email_to = $ordine->email; 
			}
			
			$shipping_cost = 0;
			// il totale ordine è già decurtato del coupon ed è la somma effettivamente pagata dal cliente
			$subtotal = $ordine->totale_ordine + $ordine->coupon_value; 
			
			$order_data = array(
				'template_name' => lang('LABEL_TITLE_INVOICE'),
				'language_ci' =>  $ordine->codice_ci,
				'customer_name' => $customer,
				'customer_user_id' => $ordine->user_id,
				'order_number' => $id_ordine,
				'shipping_address' => $address,
				'shipping_address_notes' => $address_notes,
				'order_date' => $ordine->data_ordine,
				'order_total' => $ordine->totale_ordine,
				'order_subtotal' => $subtotal,
				'order_notes' => $ordine->note_ordine,
				'order_shipping_total' => $shipping_cost,
				'order_coupon' => $ordine->codice_coupon,
				'order_coupon_total' => $ordine->coupon_value,
				'order_items_products' => $query_products->result(),
				'order_items_variants' => $query_variants->result(),
			    'order_payment_type' => $ordine->tipo_pagamento,
			    'order_payment_status' => $ordine->stato_pagamento
			);
	
			if($show_only) {
				$this->load->view('admin/html_templates/invoice', $order_data);	
			} else {
				$body = $this->load->view('admin/html_templates/invoice.php', $order_data, TRUE);
			
				$this->email->clear(TRUE);
				$mail_reply = COMPANY_EMAIL;
				$subject = lang('LABEL_TITLE_INVOICE') . ' #' . $id_ordine . ' - ' . $customer;
				 
				$this->email
					->from($mail_reply, COMPANY_NAME)
					->reply_to($mail_reply)    // Optional, an account where a human being reads.
					->to($email_to)
					->subject($subject)
					->message($body)
					->send();
					
				// invia anche all'indirizzo COMPANY_EMAIL	
				if($send_to_company) {
					$this->email
					->from($mail_reply, COMPANY_NAME)
					->reply_to($mail_reply)    // Optional, an account where a human being reads.
					->to($mail_reply)
					->subject($subject. ' | ' . $email_to)
					->message($body)
					->send();
				}
			}
	
		} else {
			if($show_only)
				echo 'L\'ordine selezionato è inesistente';
		}	
	}
	
	/* 
		Invia una email con il template di DEFAULT selezionato
		$online_link_show: se mostrare il link di visualizzazione online in alto
		$template_name: nome del tempalte
		$email: indirizzo email a cui inviare (decodificato)
		$send_to_company: true false se inviare la mail in copia alla company
		$show_only: se true mostra la pagina html con la mail altrimenti al invia tramite email senza mostrarla
	*/
	public function send_email_default_template($online_link_show, $template_name, $email, $send_to_company, $show_html, $send_emails) {		
		// WELCOME
		if($template_name == 'welcome') {
			// carica la lingua dalla email del cliente
			$this->db->from('clienti');
			$this->db->join('lingue', 'lingue.id_lingue = clienti.id_lingua');
			$this->db->join('users', 'users.id = clienti.user_id');
			$this->db->where('users.email', $email);
			$query_user = $this->db->get();
			$cliente = $query_user->row();
			// setta il file di lingua corrente per le email
			$this->lang->load('email', $cliente->codice_ci);
			
			// recupera 2 prodotti dallo shop con traduzione
			$this->db->select('*');
			$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti');
			$this->db->from('prodotti');
			$this->db->where('prodotti.stato', 1);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $cliente->id_lingua);
			$this->db->order_by("prodotti.ordine", 'desc');	
			$this->db->order_by("prodotti.id_tipo_prodotto", 'desc');	
			$this->db->limit(2);
			$query_prods = $this->db->get();
			
			$data = array(
				'template_name' => lang('LABEL_EMAIL_WELCOME_TITLE'),
				'show_unsubscribe_link' => false,
				'unsubscribe_link' => '',
				'show_online_link' => $online_link_show,
				'online_link' => site_url('email/'.$template_name.'/'.urlencode($email)),
				'products' => $query_prods->result()
			);	
			$subject = lang('LABEL_EMAIL_SUBJECT_WELCOME');	
		}
		
		// NEWSLETTER
		if($template_name == 'newsletter') {
			// aggiungi dati degli utlimi prodotti disponibili
			$this->db->from('contatti_newsletter');
			$this->db->join('lingue', 'lingue.id_lingue = contatti_newsletter.lingua_traduzione_id');
			$this->db->where('contatti_newsletter.email_contatto', urldecode($email));
			$query_email = $this->db->get();
			$news_item = $query_email->row();
			
			// setta il file di lingua corrente per le email
			$this->lang->load('email', $news_item->codice_ci);
			
			// recupera 2 prodotti dallo shop con traduzione
			$this->db->select('*');
			$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti');
			$this->db->from('prodotti');
			$this->db->where('prodotti.stato', 1);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $news_item->lingua_traduzione_id);
			$this->db->order_by("prodotti.ordine", 'desc');	
			$this->db->order_by("prodotti.id_tipo_prodotto", 'desc');	
			$this->db->limit(2);
			$query_prods = $this->db->get();
	
			$data = array(
				'template_name' => lang('LABEL_EMAIL_SUBJECT_NEWSLETTER'),
				'show_unsubscribe_link' => true,
				'unsubscribe_link' => site_url('unsubscribe/'.$news_item->codice_ci.'/2/'.urlencode($email)),
				'show_online_link' => $online_link_show,
				'online_link' => site_url('email/'.$template_name.'/'.urlencode($email)),
				'products' => $query_prods->result()
			);	
			$subject = lang('LABEL_EMAIL_SUBJECT_NEWSLETTER');	
		}
		
		// CONTACT
		if($template_name == 'contact') {
			// aggiungi dati degli utlimi prodotti disponibili
			$this->db->from('contatti_moduli');
			$this->db->join('lingue', 'lingue.id_lingue = contatti_moduli.id_lingua');
			$this->db->where('contatti_moduli.email', urldecode($email));
			$query_email = $this->db->get();
			$contatto = $query_email->row();
			
			// setta il file di lingua corrente
			$this->lang->load('email', $contatto->codice_ci);
			
			// recupera 2 prodotti dallo shop con traduzione
			$this->db->select('*');
			$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti');
			$this->db->from('prodotti');
			$this->db->where('prodotti.stato', 1);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $contatto->id_lingua);
			$this->db->order_by("prodotti.ordine", 'desc');	
			$this->db->order_by("prodotti.id_tipo_prodotto", 'desc');	
			$this->db->limit(2);
			$query_prods = $this->db->get();
	
			$data = array(
				'template_name' => lang('LABEL_EMAIL_SUBJECT_CONTACT'),
				'show_unsubscribe_link' => true,
				'unsubscribe_link' => site_url('unsubscribe/'.$contatto->codice_ci.'/1/'.urlencode($email)),
				'show_online_link' => $online_link_show,
				'online_link' => site_url('email/'.$template_name.'/'.urlencode($email)),
				'products' => $query_prods->result()
			);	
			$subject = lang('LABEL_EMAIL_SUBJECT_CONTACT');	
		}
		
		if($show_html) {
			$this->load->view('admin/html_templates/'.$template_name, $data);
		} 
		if($send_emails) {
			$email_to = $email;
			$body = $this->load->view('admin/html_templates/'.$template_name.'.php', $data, TRUE);
		
			$this->email->clear(TRUE);
			$mail_reply = COMPANY_EMAIL;
			 
			$this->email
				->from($mail_reply, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($email_to)
				->subject($subject)
				->message($body)
				->send();
				
			// invia anche all'indirizzo COMPANY_EMAIL	
			if($send_to_company) {
				$this->email
				->from($mail_reply, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($mail_reply)
				->subject($subject. ' - ' . $email_to)
				->message($body)
				->send();
			}
		}
		
	}
	
	/* 
		Invia una email con il template selezionato
		$online_link_show: se mostrare il link di visualizzazione online in alto
		$template_id: id del tempalte sul db
		$email: indirizzo email a cui inviare (decodificato)
		$send_to_company: true false se inviare la mail in copia alla company
		$show_only: se true mostra la pagina html con la mail altrimenti al invia tramite email senza mostrarla
	*/
	public function send_email_custom_template($online_link_show, $template_id, $email, $send_to_company, $show_html, $send_emails) {		
		// decode della email e poi ancora encode se necessario
		$email = urldecode($email);
		
		$this->db->from('email_templates');
		$this->db->join('lingue', 'lingue.id_lingue = email_templates.lingua_traduzione_id');
		$this->db->where('email_templates.id_template', $template_id);
		$query = $this->db->get();
		$temp = $query->row();
		
		// setta il file di lingua corrente per le email
		$this->lang->load('email', $temp->codice_ci);
		
		// recupera 2 prodotti dallo shop con traduzione
		$this->db->select('*');
		$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti');
		$this->db->from('prodotti');
		$this->db->where('prodotti.stato', 1);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $temp->lingua_traduzione_id);
		$this->db->order_by("prodotti.ordine", 'desc');	
		$this->db->order_by("prodotti.id_tipo_prodotto", 'desc');	
		$this->db->limit(2);
		$query_prods = $this->db->get();
			
		$data = array(
			'template_name' => $temp->nome_template,
			'show_unsubscribe_link' => true,
			'unsubscribe_link' => site_url('unsubscribe/'.$temp->codice_ci.'/'.$temp->id_tipo_template.'/'.urlencode($email)),
			'show_online_link' => $online_link_show,
			'online_link' => site_url('email_template/'.$template_id.'/'.urlencode($email)),
			'template_title' => $temp->titolo_template,
			'template_text' => $temp->testo_template,
			'products' => $query_prods->result()
		);	
		
		$subject = $temp->subject_template;
		
		if($show_html) {
			$this->load->view('admin/html_templates/template', $data);
		} 
		if($send_emails) {
			$email_to = $email;
			$body = $this->load->view('admin/html_templates/template.php', $data, TRUE);
		
			$this->email->clear(TRUE);
			$mail_reply = COMPANY_EMAIL;
			 
			$this->email
				->from($mail_reply, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($email_to)
				->subject($subject)
				->message($body)
				->send();
				
			// invia anche all'indirizzo COMPANY_EMAIL	
			if($send_to_company) {
				$this->email
				->from($mail_reply, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($mail_reply)
				->subject($subject. ' - ' . $email_to)
				->message($body)
				->send();
			}
		}
		
	}	
}

class Frontend_Controller extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	function show_view_with_menu($view_name, $data) {
		$this->default_controller_mode();
		$this->checkLanguagePermalink();
		
	    // pagine del menù
		$this->db->select('*');
		$this->db->from('pagine');
		$this->db->where('ordine_menu >', 0);
		$this->db->where('id_lingua', lang('LANGUAGE_ID'));
		$this->db->order_by('ordine_menu', 'asc');
		$query_pages = $this->db->get();
		$data['menuPages'] = $query_pages->result();
		
		// lingue
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->where('stato_lingua >', 0);
		$query_lang = $this->db->get();
		$data['installedLangs'] = $query_lang->result();
		
		// se pagina index carica slider
		if($view_name == 'frontend/index') {
			$this->db->select('*');
			$this->db->from('home_slider');
			$this->db->where('id_lingua', lang('LANGUAGE_ID'));
			$query_slider = $this->db->get();
			$data['homeSlider'] = $query_slider->result();
		}
//  		if(!isset($data)){
//  			log_message('info','>>>>>>>>>> Frontend_Controller > show_view_with_menu() > isset($data)  ' );
//  			redirect($view_name); 
//  		} else {
//  			log_message('info','>>>>>>>>>> Frontend_Controller > show_view_with_menu() > !isset($data)  ' );
// 			// pagine attive o disattive dal menu
// 		    $this->load->view($view_name, $data); // the actual view you wanna load
//  		}

		
		// pagine attive o disattive dal menu
		$this->load->view($view_name, $data); // the actual view you wanna load
	}
	
	function default_controller_mode() {
		// se DEFAULT_CONTROLLER è COMINGSOON redireziona sempre li tranne che per utente admin loggato
		if (!$this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
		{
			if($this->router->routes['default_page'] == 'Comingsoon')
				redirect('comingsoon');	
		}
	}
	
	function checkLanguagePermalink() {
	//	echo $this->uri->uri_string();
		if($this->uri->segment(1) != strtolower(lang('LANGUAGE_ABBR'))) {
			$this->db->select('codice_ci');
			$this->db->from('lingue');
			$this->db->where('abbr_lingue', strtoupper($this->uri->segment(1)));
			$query_lang = $this->db->get();
			$curr_lang = $query_lang->row();
			$this->session->set_userdata('site_lang', $curr_lang->codice_ci);
			$this->config->set_item('language', $curr_lang->codice_ci);
			redirect(base_url().$this->uri->uri_string());
		}
	}
	
}

class Admin_Controller extends MY_Controller {
	
	public $site_status = 'offline';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('file_helper');
		$this->load->library('grocery_CRUD');
	}
	
	function checkUserPermissions() 
	{
		if (!$this->ion_auth->logged_in())
		{
			log_message('info','********************* LOGGED IN ** index() - NOT LOGGED ');
			// redirect them to the login page
			redirect('admin/login', 'refresh');
		}
	/*	elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			log_message('info','********************* LOGGED IN ** index() index() - NOT ADMIN ');
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}*/
		log_message('info','********************* LOGGED IN ** index() index() - ADMIN ');
	}
	
}