<?php
class LangSwitch extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function switchLanguage($language = "", $route = "it/home", $param = "", $param2 = "", $param3 = "") {
        $language = ($language != "") ? $language : "italian";
        $this->session->set_userdata('site_lang', $language);
		$this->config->set_item('language', $language);
		
		// load language from db
		$this->db->where('codice_ci', $language);
		$this->db->from('lingue');
		$query = $this->db->get();
		$lingua = $query->row();
		
		// load current 
		$this->db->where('url_pagina', $route.'/'.$param.($param2 != '' ? '/(:any)' : '') . ($param3 != '' ? '/(:any)' : ''));
		$this->db->from('pagine');
		$query_old = $this->db->get();
		$old_page = $query_old->row();
		
		// load language pagine
		$this->db->where('id_lingua', $lingua->id_lingue);
		$this->db->where('controller', $old_page->controller);
		$this->db->where('url_pagina !=', "default_page");
		$this->db->from('pagine');
		$query_new = $this->db->get();
		$new_page = $query_new->row();

		$route_to = '';
		$urls = explode("/", $new_page->url_pagina);
		if($param != "")
			$route_to .= $urls[0] . '/' . $urls[1] . ($param2 != '' ? '/'.$param2 : '') . ($param3 != '' ? '/'.$param3 : '');

       redirect(base_url().$route_to);
	}
}
?>