<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index() {
	}
	
	public function send() {
		$to_mail = $this->input->post('to_mail', true);
		$mail_type = $this->input->post('mail_type', true);
		$mail_text = $this->input->post('mail_text', true);
		$subject = $this->input->post('subject', true);
		$mail_reply = $this->input->post('mail_reply', true);
		echo $to_mail . ' ' . $mail_type;
	
		$message = '<p>'.$mail_text.'</p>';
		
		// Get full html:
		$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
			<title>' . html_escape($subject) . '</title>
			<style type="text/css">
				body {
					font-family: Arial, Verdana, Helvetica, sans-serif;
					font-size: 16px;
				}
			</style>
		</head>
		<body>
		' . $message . '
		</body>
		</html>';
		// Also, for getting full html you may use the following internal method:
		//$body = $this->email->full_html($subject, $message);
		
		$result = $this->email
				->from($to_mail, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($to_mail)
				->subject($subject)
				->message($body)
				->send();
		
		var_dump($result);
		echo '<br />';
		echo $this->email->print_debugger();
	}
	
	/* Richiesta di contatto */
	public function contactus() {

		$name = $this->input->post('name', true);			
		$phone = $this->input->post('phone', true);		
		$email = $this->input->post('email', true);			
		$message_post = $this->input->post('message', true);	
		
		// salva sul DB la richiesta contatto
		$data = array(
		   'id_contatto' => NULL,
		   'nome' => $name,
		   'email' => $email,
		   'telefono' => $phone,
		   'messaggio' => $message_post,
		   'data' => date('Y-m-d H:i:s'),
		   'stato_contatto' => 1,
		   'id_lingua' => lang('LANGUAGE_ID')
		);

		$this->db->insert('contatti_moduli', $data);
		$this->data['message'] = lang('MSG_SUCCESS_CONTACT');
		$this->data['type'] = 'success';
		
		// invio email template newsletter al cliente
		$this->send_email_default_template(true, 'contact', $email, true, false, true);
		
		echo json_encode($this->data);
	}
	
	/* Registrazione email newsletter frontend */
	public function newsletter() {
		
		$email = $this->input->post('email', true);	
		
		// controlla se esiste già in newsletter oppure se è sospeso e inserisce oppure aggiorna lo stato
		$this->db->where('email_contatto', $email);
		$query_exist = $this->db->get('contatti_newsletter');
		if ($query_exist->num_rows() > 0)
		{
			if($query_exist->row()->stato_contatto == 1) {
				// error
				$this->data['message'] = lang('MSG_UNIQUE_NEWSLETTER');
				$this->data['type'] = 'error';
			} else {
				$data = array(
				   'stato_contatto' => 1
				);	
				$this->db->where('email_contatto', $email);
				$this->db->update('contatti_newsletter', $data); 	
				
				// invio email template newsletter al cliente
				$this->send_email_default_template(true, 'newsletter', $email, true, false, true);
				
				$this->data['message'] = lang('MSG_SUCCESS_NEWSLETTER');
				$this->data['type'] = 'success';
			}
		}
		else
		{
			// salva sul DB la richiesta contatto
			$data_insert = array(
			   'id_contatto_newsletter' => NULL,
			   'email_contatto' => $email,
			   'data_contatto' => date('Y-m-d H:i:s'),
			   'stato_contatto' => 1,
			   'data_unsubscribe' => NULL,
		  	   'lingua_traduzione_id' => lang('LANGUAGE_ID')
			);
	
			$this->db->insert('contatti_newsletter', $data_insert);
		
			// invio email template newsletter al cliente
			$this->send_email_default_template(true, 'newsletter', $email, true, false, true);
			
			$this->data['message'] = lang('MSG_SUCCESS_NEWSLETTER');
			$this->data['type'] = 'success';
		}
		
		echo json_encode($this->data);
		
	}
	
	/* Invia template email a una lsita di contatti */
	public function send_email_template($template_id) {
							
		$contatti_list = json_decode($this->input->post('contatti_list', true));	
		$nomi_list = json_decode($this->input->post('nomi_list', true));
		$tipo_template = $this->input->post('modulo', true); // 1 contatti - 2 newsletter - 3 custom
		
		// email template dal db
		$this->db->from('email_templates');
		$this->db->join('lingue', 'lingue.id_lingue = email_templates.lingua_traduzione_id');
		$this->db->where('email_templates.id_template', $template_id);
		$query = $this->db->get();
		$temp = $query->row();
		
		// setta il file di lingua corrente per le email
		$this->lang->load('email', $temp->codice_ci);
		
		// recupera 2 prodotti dallo shop con traduzione
		$this->db->select('*');
		$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti');
		$this->db->from('prodotti');
		$this->db->where('prodotti.stato', 1);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $temp->lingua_traduzione_id);
		$this->db->order_by("prodotti.ordine", 'desc');	
		$this->db->order_by("prodotti.id_tipo_prodotto", 'desc');	
		$this->db->limit(2);
		$query_prods = $this->db->get();
		
		$subject = $temp->subject_template;
		$mail_reply = COMPANY_EMAIL;
		
		// bulk newsletter
		for($i = 0; $i < count($contatti_list); ++$i) {	
			
			$data = array(
				'template_name' => $temp->nome_template,
				'show_unsubscribe_link' => true,
				'unsubscribe_link' => site_url('unsubscribe/'.$temp->codice_ci.'/'.$temp->id_tipo_template.'/'.urlencode($contatti_list[$i])),
				'show_online_link' => true,
				'online_link' => site_url('email_template/'.$template_id.'/'.urlencode($contatti_list[$i])),
				'template_title' => $temp->titolo_template,
				'template_text' => $temp->testo_template,
				'products' => $query_prods->result()
			);	
			
			$body = $this->load->view('admin/html_templates/template.php', $data, TRUE);
		
			$this->email->clear(TRUE);	 
			$this->email
				->from($mail_reply, COMPANY_NAME)
				->reply_to($mail_reply)    // Optional, an account where a human being reads.
				->to($contatti_list[$i])
				->subject($subject)
				->message($body)
				->send();
				
		}
		
	}
	
	public function send_email_libera() {
		
		$subject = $this->input->post('subject_email', true);		
		$testo_email = $this->input->post('testo_email', true);				
		$to_email = $this->input->post('to_email', true);		
		$lingua_id = $this->input->post('lingua_id', true);
		
		// recupera la lingua dal db
		$this->db->where('id_lingue', $lingua_id);
		$this->db->from('lingue');
		$query = $this->db->get();
		$lingua = $query->row();
		
		// setta il file di lingua corrente per le email
		$this->lang->load('email', $lingua->codice_ci);
		
		// recupera 2 prodotti dallo shop con traduzione
		$this->db->select('*');
		$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti');
		$this->db->from('prodotti');
		$this->db->where('prodotti.stato', 1);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', $lingua_id);
		$this->db->order_by("prodotti.ordine", 'desc');	
		$this->db->order_by("prodotti.id_tipo_prodotto", 'desc');	
		$this->db->limit(2);
		$query_prods = $this->db->get();
		
		$mail_reply = COMPANY_EMAIL;
			
		$data = array(
			'template_name' => $subject,
			'show_unsubscribe_link' => false,
			'unsubscribe_link' => '',
			'show_online_link' => false,
			'online_link' => '',
			'template_title' => $subject,
			'template_text' => $testo_email,
			'products' => $query_prods->result()
		);	

		$body = $this->load->view('admin/html_templates/template.php', $data, TRUE);

		$this->email->clear(TRUE);	 
		$this->email
			->from($mail_reply, COMPANY_NAME)
			->reply_to($mail_reply)    // Optional, an account where a human being reads.
			->to($to_email)
			->subject($subject)
			->message($body)
			->send();	
			
	}
	
	// Visualizza email custom online
	public function html_online_viewer($template_id, $email) {
		$this->send_email_custom_template(false, $template_id, $email, false, true, false);
	}
	
	// Visualizza email default online
	public function email_viewer($template_name, $email) {
		$this->send_email_default_template(false, $template_name, urldecode($email), false, true, false);		
	}
	
	// Visualizza email invoice order online
	public function invoice_viewer($id_ordine) {
		$this->send_email_order_invoice($id_ordine, true, true);
	}
	
}