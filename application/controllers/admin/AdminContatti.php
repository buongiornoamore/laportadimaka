<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminContatti extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('contatti_moduli');
			// nome in tabella
			$crud->display_as('id_lingua', 'Lingua');
			$crud->display_as('stato_contatto', 'Stato');
			$crud->display_as('data', 'Data contatto');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
			$crud->set_relation('stato_contatto', 'stato_descrizione', 'testo_stato_descrizione');
			// colonne da mostrare
			//$crud->change_field_type('data','date');
			$crud->columns('nome', 'email', 'data', 'stato_contatto', 'id_lingua');
			// unset delete action
			//$crud->unset_add();
			$crud->unset_texteditor('messaggio');
			$crud->required_fields('nome', 'email', 'stato_contatto', 'messaggio', 'id_lingua');

			$crud->change_field_type('data_unsubscribe', 'date');
			$crud->change_field_type('data', 'date');
			$crud->add_action('Invia email', '', '', 'fa-envelope', array($this, 'load_email_templates'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-CONTATTI';
			$data['curr_page_title'] = 'Email';
			$data['collapseParentMenu'] = 'email';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/contatti',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function load_email_templates($primary_key, $row)
	{
		if($row->stato_contatto == 1)
			return site_url('admin/contatti/email/'.$row->id_contatto);
		else
			return 'disabled-action';
	}
	
	public function email($cont_id) {
		$this->checkUserPermissions();
		//CRUD contatti email
		try {
			// load contatto
			$this->db->from('contatti_moduli');
			$this->db->join('lingue', 'lingue.id_lingue = contatti_moduli.id_lingua');
			$this->db->where('id_contatto', $cont_id);
			$query = $this->db->get();
			$cont = $query->row();
			
			$this->cont_id = $cont->id_contatto;
		    $this->cont_email = $cont->email;
			$this->cont_nome = $cont->nome;
			$this->cont_lingua = $cont->id_lingua;
			
			$crud = new grocery_CRUD();

			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('email_templates');
			$crud->where('lingua_traduzione_id', $cont->id_lingua);
			// nome in tabella
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			//$crud->set_relation('id_benefici', 'benefici', 'titolo_benefici');
			// campi obbligatori
			//$crud->required_fields('testo_benefici');
			//$crud->edit_fields('testo_benefici');
			//$crud->add_fields('testo_benefici');
			// colonne da mostrare
			$crud->columns('nome_template', 'lingua_traduzione_id');
			// unset delete action
			$crud->unset_delete();
			$crud->unset_edit();
		//	$crud->unset_add();
			$crud->unset_read();
			$crud->add_action('Invia email', '', '', 'fa-envelope', array($this, 'send_email_templates'));
			$crud->add_action('Preview email', '', '', 'fa-html5', array($this, 'preview_email_templates'));
			//$crud->unset_texteditor('testo_benefici');
			$crud->field_type('id_contatto', 'hidden', $cont_id);
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-CONTATTI';
			$data['curr_page_title'] = 'Email';
			$data['collapseParentMenu'] = 'email';
			$data['curr_function_title'] = 'Email per <b>' . $cont->email . '</b> - ' . $cont->nome_lingue;
			$data['curr_customer_email'] = $cont->email;
			$data['curr_customer_id'] = $cont_id;
			$data['curr_customer_name'] = $cont->nome;
			$data['curr_customer_lang'] = $cont->id_lingua;
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/contatti_email',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function send_email_templates($primary_key, $row)
	{
		return site_url('admin/send_email_template/'.$row->id_template);
	}
	
	function preview_email_templates($primary_key, $row)
	{
		return site_url('email_template/'.$row->id_template.'/testmail');
	}
	
}
