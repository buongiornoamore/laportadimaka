<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper(array('url','language'));
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->library('session');
	}

	public function index()
	{
		//$this->lang->load('auth', $this->session->userdata('site_lang'));
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			// redirect('admin/login', 'refresh');
			log_message('info','********************* NOT LOGGED IN ');
			$this->load->view('admin/login');

		}
	/*	elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			// return show_error('You must be an administrator to view this page.');
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->load->view('admin/login');*/
	//	}
		else
		{
			// // set the flash data error message if there is one
			// $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//
			// //list the users
			// $this->data['users'] = $this->ion_auth->users()->result();
			// foreach ($this->data['users'] as $k => $user)
			// {
			// 	$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			// }
			//
			// $this->_render_page('auth/index', $this->data);

			// $this->_render_page('admin/dashboard');
			log_message('info','********************* LOGGED IN ');
			redirect('admin/products', 'refresh');
		}
	}

	// log the user in
	public function login()
	{

		$this->config->load('config');
 		//echo $this->config->item('language');
		//$this->lang->load('auth', $this->session->userdata('site_lang'));
		$this->data['title'] = $this->lang->line('login_heading');

		//validate form input
		$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
		$this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

		if ($this->form_validation->run() == true)
		{
			// check to see if the user is logging in
			// check for "remember me"
			$remember = (bool) $this->input->post('remember');

			log_message('info','********************* login() - $this->input->post(identity) : ' .$this->input->post('identity'));
			log_message('info','********************* login() - $this->input->post(password) : ' .$this->input->post('password'));

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());

				log_message('info','********************* LOGGED IN ** login()');

				//redirect('admin', 'refresh');
				//$this->load->view('admin/dashboard');
				redirect('/admin/products');
			}
			else
			{
				log_message('info','********************* NOT LOGGED IN ** login()');
				// if the login was un-successful
				// redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('admin/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			// the user is not logging in so display the login page
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = array('name' => 'identity',
				'id'    => 'identity',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id'   => 'password',
				'type' => 'password',
			);

			$this->load->view('admin/login', $this->data);
		}
	}

	// log the user out
	public function logout()
	{
		$this->data['title'] = "Logout";

		// log the user out
		$logout = $this->ion_auth->logout();

		// redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('admin/login', 'refresh');
	}


	// forgot password
	public function forgot_password()
	{

		// setting validation rules by checking whether identity is username or email
		if($this->config->item('identity', 'ion_auth') != 'email' )
		{
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
		}
		else
		{
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() == false)
		{
			$this->data['type'] = $this->config->item('identity','ion_auth');
			// setup the input
			$this->data['identity'] = array('name' => 'identity', 'id' => 'identity',
			);

			if ( $this->config->item('identity', 'ion_auth') != 'email' ){

				$this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
			}
			else
			{

				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			// set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			// $this->_render_page('auth/forgot_password', $this->data);
			$this->load->view('admin/login', $this->data);
		}
		else
		{
			$identity_column = $this->config->item('identity','ion_auth');
			$identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

			if(empty($identity)) {

	            		if($this->config->item('identity', 'ion_auth') != 'email')
		            	{

		            		$this->ion_auth->set_error('forgot_password_identity_not_found');
		            	}
		            	else
		            	{

		            	   $this->ion_auth->set_error('forgot_password_email_not_found');
		            	}

		                $this->session->set_flashdata('message', $this->ion_auth->errors());
                		redirect("auth/login", 'refresh');
            		}

			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{

				// if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("admin/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{

				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("admin/login", 'refresh');
			}
		}
	}



		// reset password - final step for forgotten password
		public function reset_password($code = NULL)
		{
			if (!$code)
			{
				show_404();
			}

			$user = $this->ion_auth->forgotten_password_check($code);

			if ($user)
			{

				log_message('info','********************* reset_password ** 1 ');
				// if the code is valid then display the password reset form
				$this->form_validation->set_rules('new_password', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_password_confirm]');
				$this->form_validation->set_rules('new_password_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

				if ($this->form_validation->run() == false)
				{
					log_message('info','********************* reset_password ** 2 ');
					// display the form

					// set the flash data error message if there is one
					$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

					$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
					$this->data['new_password'] = array(
						'name' => 'new_password',
						'id'   => 'new_password',
						'type' => 'password',
						'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
					);
					$this->data['new_password_confirm'] = array(
						'name'    => 'new_password_confirm',
						'id'      => 'new_password_confirm',
						'type'    => 'password',
						'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
					);
					$this->data['user_id'] = array(
						'name'  => 'user_id',
						'id'    => 'user_id',
						'type'  => 'hidden',
						'value' => $user->id,
					);
					$this->data['csrf'] = $this->_get_csrf_nonce();
					// $this->data['csrf'] = array('csrf' => $this->_get_csrf_nonce());
					$this->data['code'] = $code;

					// render
					$this->load->view('admin/resetPassword', $this->data);
				}
				else
				{
					log_message('info','********************* reset_password ** 3 ');
					// do we have a valid request?
					if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
					{

						// something fishy might be up
						$this->ion_auth->clear_forgotten_password_code($code);

						show_error($this->lang->line('error_csrf'));

					}
					else
					{
						log_message('info','********************* reset_password ** 3 ');
						// finally change the password
						$identity = $user->{$this->config->item('identity', 'ion_auth')};

						$change = $this->ion_auth->reset_password($identity, $this->input->post('new_password'));

						if ($change)
						{
							// if the password was successfully changed
							$this->session->set_flashdata('message', $this->ion_auth->messages());
							// redirect("admin/login", 'refresh');
							$this->load->view('admin/login', $this->data);
						}
						else
						{
							$this->session->set_flashdata('message', $this->ion_auth->errors());
							// redirect('admin/resetPassword' . $code, 'refresh');
							$this->load->view('admin/login', $this->data);
						}
					}
				}
			}
			else
			{
				// if the code is invalid then send them back to the forgot password page
				$this->session->set_flashdata('message', $this->ion_auth->errors());

				// redirect("admin/login", 'refresh');
				$this->load->view('admin/login');

			}
		}


		public function _get_csrf_nonce()
		{
			$this->load->helper('string');
			$key   = random_string('alnum', 8);
			$value = random_string('alnum', 20);
			$this->session->set_flashdata('csrfkey', $key);
			$this->session->set_flashdata('csrfvalue', $value);

			return array($key => $value);
		}


		public function _valid_csrf_nonce()
		{
			$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
			if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue'))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}

	/* UNSUBSCRIBE */
	public function unsubscribe($language, $from_newsletter, $contact_mail) {
		// switch to user language
		$this->session->set_userdata('site_lang', $language);
		$this->config->set_item('language', $language);
		//var_dump($data);
		redirect('unsubscribe_execute/'.$language.'/'.$from_newsletter.'/'.$contact_mail.'', 'refresh');
	}
	
	public function unsubscribe_execute($language, $from_newsletter, $contact_mail) {
		// switch to user language
		$this->session->set_userdata('site_lang', $language);
		$this->config->set_item('language', $language);
		//$this->lang->load('frontend', $language);
	
		if($from_newsletter == 2) {
			$this->db->where('email_contatto', urldecode($contact_mail));
			$this->db->where('stato_contatto', 1);
			$this->db->from('contatti_newsletter');
		} else if($from_newsletter == 1) {
			$this->db->where('email', urldecode($contact_mail));
			$this->db->where('stato_contatto', 1);
			$this->db->from('contatti_moduli');
		}
		$query = $this->db->get();
		$user = $query->row();
		
		if (isset($user) && $user != NULL) {
			// redirect to message page
			$data = array(
				'from_newsletter' => $from_newsletter,
				'contact_mail' => $contact_mail,
				'message' => '',
				'label' => $this->lang->line('LABEL_UNSUBSCRIBE')
			);	
		} else {
			$data = array(
				'from_newsletter' => '',
				'contact_mail' => '',
				'message' => lang('MSG_UNSUBSCRIBE_NOTFOUND'),
				'label' => $this->lang->line('LABEL_UNSUBSCRIBE')
			);	
		}
		//var_dump($data);
		$this->load->view('admin/unsubscribe', $data);
	}
	
	public function unsubscribe_confirm($from_newsletter, $contact_mail) {
		// check user exist
		if($from_newsletter == 2) {
			$this->db->where('email_contatto', urldecode($contact_mail));
			$this->db->where('stato_contatto', 1);
			$this->db->from('contatti_newsletter');
		} else if ($from_newsletter == 1){
			$this->db->where('email', urldecode($contact_mail));
			$this->db->where('stato_contatto', 1);
			$this->db->from('contatti_moduli');
		} 
		$query = $this->db->get();
		$user = $query->row();
		
		if (isset($user) && $user != NULL) {
			$data = array(
			   'data_unsubscribe' => date('Y-m-d H:i:s'),
			   'stato_contatto' => 2
			);	
			if($from_newsletter > 0) {
				$this->db->where('email_contatto', urldecode($contact_mail));
				$this->db->update('contatti_newsletter', $data); 
			} else {
				$this->db->where('email', urldecode($contact_mail));
				$this->db->update('contatti_moduli', $data); 
			}
			
			$data = array(
				'from_newsletter' => '',
				'contact_mail' => '',
				'message' => lang('MSG_UNSUBSCRIBE_DONE')
			);	
		} else {
			$data = array(
				'from_newsletter' => '',
				'contact_mail' => '',
				'message' => lang('MSG_UNSUBSCRIBE_NOTFOUND')
			);	
		}
		$this->load->view('admin/unsubscribe', $data);
	}
	
}
