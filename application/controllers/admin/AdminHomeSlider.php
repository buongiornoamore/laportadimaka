<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminHomeSlider extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// subject
			$crud->set_subject('Slide');
			// tabella
			$crud->set_table('home_slider');
			$crud->order_by('hs_order', 'asc');
			$crud->order_by('id_lingua', 'asc');
			// nome in tabella
			$crud->display_as('hs_order', 'Ordine');
			$crud->display_as('hs_image', 'Immagine');
			$crud->display_as('hs_title', 'Titolo');
			$crud->display_as('id_lingua', 'Lingua');
			// file upload
			$crud->set_field_upload('hs_image', 'assets/assets-frontend/img/hero-slider');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
	    	// campi obbligatori
			$crud->required_fields('hs_image', 'hs_order', 'id_lingua');
			// callbacks
			$crud->callback_after_upload(array($this, '_callback_after_upload_img'));
			$crud->callback_before_delete(array($this, '_callback_before_delete_slide'));
			// colonne da mostrare
			$crud->columns('hs_image', 'hs_title', 'id_lingua', 'hs_order');
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-HOMESLIDER';
			$data['curr_page_title'] = 'Home slider';
			$data['collapseParentMenu'] = 'homeslider';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/homeslider',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
    
	// Verifica se la categoria contiene prodotti
	function _callback_before_delete_slide($primary_key)
	{
	   $this->db->where('hs_id', $primary_key);
	   $slide = $this->db->get('home_slider')->row();
	    
	   check_remove_image_file('./assets/assets-frontend/img/hero-slider/'.$slide->hs_image);
	   return true; 
	}
	
	// Resize dell'immagine caricata
	function _callback_after_upload_img($uploader_response, $field_info, $files_to_upload) {
	    $this->load->library('image_moo');
	    
	    // Sovrascrivo immagine con stesso nome am nuove dimensioni
	    $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
	    $this->image_moo->load($file_uploaded)->resize_crop(1920, 790)->save($file_uploaded, true);
	    
	    return true;
	}
	
}
