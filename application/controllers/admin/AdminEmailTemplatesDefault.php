<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminEmailTemplatesDefault extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('email_templates_default');
			//$crud->where('default_template_id', 0);
			// nome in tabella
			$crud->display_as('etd_nome_operazione', 'Nome template');
			$crud->display_as('etd_nome_operazione', 'Subject email');
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			// colonne da mostrare
			$crud->columns('etd_nome_operazione', 'etd_subject', 'lingua_traduzione_id');
			$crud->edit_fields('etd_nome_operazione', 'etd_subject', 'etd_testo', 'lingua_traduzione_id');
			//$crud->required_fields('etd_nome_operazione', 'etd_subject', 'etd_testo', 'lingua_traduzione_id');
			$crud->required_fields('etd_testo');
			$crud->unset_texteditor('etd_testo');
			$crud->unset_add();
			$crud->unset_read();
			$crud->unset_delete();
			$crud->add_action('Html preview', '', '', 'fa-html5', array($this, 'load_preview'));
			
			// set update only after update 
			//if ($crud->getState() == 'edit') {
			$crud->change_field_type('etd_nome_operazione', 'readonly');
			$crud->change_field_type('etd_subject', 'readonly');
			$crud->change_field_type('lingua_traduzione_id', 'readonly');
			//} 
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-EMAILTEMPLATESDEFAULT';
			$data['curr_page_title'] = 'Email';
			$data['collapseParentMenu'] = 'email';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/emailtemplatesdefault',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function load_preview($primary_key , $row)
	{
		return site_url('email_template/'.$row->id_template.'/testmail');
	}
		
}
