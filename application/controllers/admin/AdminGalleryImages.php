<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminGalleryImages extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			$crud->set_subject('Immagine');
			// tabella
			$crud->set_table('immagini_gallery');
			$crud->order_by('ordine_ig', 'desc');
			// nome in tabella
			$crud->display_as('immagine_ig', 'Immagine');
			$crud->display_as('nome_ig', 'Nome immagine');
			$crud->display_as('stato_ig', 'Stato');
			$crud->display_as('id_categoria_ig', 'Categoria');
			$crud->display_as('ordine_ig', 'Ordine');
			// file upload
			$crud->set_field_upload('immagine_ig', 'assets/assets-frontend/img/gallery');
			// realazioni join
			$crud->set_relation('stato_ig', 'stato_descrizione', 'testo_stato_descrizione');
			$crud->set_relation('id_categoria_ig', 'categorie_gallery', 'nome_categoria_gallery');
	    	// campi obbligatori
			$crud->required_fields('nome_ig', 'id_categoria_ig', 'immagine_ig', 'stato_ig', 'ordine_ig');
			// colonne da mostrare
			$crud->columns('immagine_ig', 'nome_ig', 'id_categoria_ig', 'stato_ig', 'ordine_ig');
			// unset delete action
			// custom action
			$crud->add_action('Traduzioni immagine', '', '', 'fa-flag', array($this, 'load_traduzioni'));
			
			$crud->callback_after_upload(array($this, '_callback_after_upload_img'));
			$crud->callback_before_delete(array($this, '_callback_before_delete_img'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-GALLERY-IMAGES';
			$data['curr_page_title'] = 'Gallery';
			$data['collapseParentMenu'] = 'gallery';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/galleryimages',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	// Resize dell'immagine caricata
	function _callback_after_upload_img($uploader_response, $field_info, $files_to_upload) {
	    $this->load->library('image_moo');
	    
	    // Sovrascrivo immagine con stesso nome am nuove dimensioni
	    $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
	    $this->image_moo->load($file_uploaded)->resize_crop(960, 693)->save($file_uploaded, true);
	    
	    return true;
	}
	
	function _callback_before_delete_img($primary_key)
	{	    
	    $this->db->where('id_ig', $primary_key);
	    $img = $this->db->get('immagini_gallery')->row();
	    
	    // elimina traduzioni e immagine
	    check_remove_image_file('./assets/assets-frontend/img/gallery/'.$img->immagine_ig);
	    $this->db->delete('immagini_gallery_traduzioni', array('id_ig' => $primary_key));
	    return true;    
	}
	
	function load_traduzioni($primary_key , $row)
	{
		return site_url('admin/galleryimages/trad/'.$row->id_ig.'/'.$row->nome_ig);
	}

	public function traductions($ig_id, $ig_name)
	{
		// CRUD traduzioni
		try {
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('immagini_gallery_traduzioni');
			$crud->where('id_ig', $ig_id);
			// nome in tabella
			$crud->display_as('titolo_ig', 'Titolo immagine');
			$crud->display_as('testo_ig', 'Testo immagine');
			$crud->display_as('id_lingua', 'Lingua');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
			// campi obbligatori
			$crud->required_fields('titolo_ig', 'testo_ig', 'id_lingua');
			$crud->edit_fields('titolo_ig', 'testo_ig', 'id_lingua');
			$crud->add_fields('id_ig', 'titolo_ig', 'testo_ig', 'id_lingua');
			// colonne da mostrare
			$crud->columns('titolo_ig', 'testo_ig', 'id_lingua');
			// hidden fileds
			$crud->field_type('id_ig', 'hidden', $ig_id);
			$output = $crud->render();
			
			$data['curr_page'] = 'ADMIN-GALLERY-IMAGES';
			$data['curr_page_title'] = 'Gallery';
			$data['collapseParentMenu'] = 'gallery';
			$data['curr_function_title'] = 'Traduzioni per immagine <b>' . urldecode($ig_name) . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/galleryimages_trad',(array)$output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}
