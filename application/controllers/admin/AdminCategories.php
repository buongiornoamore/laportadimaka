<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminCategories extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD Categorie
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// subject
			$crud->set_subject('Categoria');
			// tabella
			$crud->set_table('categorie');
			// nome in tabella
			$crud->display_as('url_categorie', 'Url');
			$crud->display_as('nome', 'Nome');
			$crud->display_as('descrizione', 'Descrizione');
			$crud->display_as('immagine', 'Immagine');
			$crud->display_as('stato', 'Stato');
			$crud->display_as('prodotti', 'Prodotti');
			$crud->display_as('label_color_class', 'Colore');
			// realazioni join
			$crud->set_relation('stato', 'stato_prodotti', 'stato_prodotti_desc');
			$crud->set_relation('label_color_class', 'colori_classi', 'colore_classe_nome');
			$crud->set_relation_n_n('prodotti', 'prodotti_categorie', 'prodotti', 'id_categoria', 'id_prodotto', 'stato', 'id_prodotto', array('stato'=>1));
	
			// file upload
			$crud->set_field_upload('immagine', 'assets/assets-frontend/img/categories');
			// campi obbligatori
			$crud->required_fields('url_categorie', 'nome', 'descrizione', 'stato', 'ordine', 'label_color_class');
			// campi per add
			$crud->add_fields('url_categorie', 'nome', 'descrizione', 'stato', 'ordine', 'label_color_class');
			// campi per edit
			$crud->edit_fields('url_categorie', 'nome', 'descrizione', 'stato', 'ordine', 'label_color_class', 'immagine');
			// colonne da mostrare
			$crud->columns('immagine', 'url_categorie', 'nome', 'stato', 'prodotti');
			$crud->add_action('Traduzioni categoria', '', '', 'fa-flag', array($this, 'load_traduzioni'));
			// callbacks
			$crud->callback_after_upload(array($this, '_callback_after_upload_img'));
			$crud->callback_before_delete(array($this, '_callback_before_delete_category'));
			$crud->set_lang_string('delete_error_message', 'Si &egrave verificato un errore oppure hai tentato di cancellare una categoria che contienet prodotti.<br><br>Elimina prima i prodotti e riprova!');
			$crud->set_lang_string('insert_error', 'Si &egrave verificato un errore in inserimento oppure hai cercato di utilizzare un URL CODE già presente.<br><br>Sostituisci e riprova!');
			$crud->callback_before_insert(array($this,'formatCategoryUrl'));
			$crud->callback_before_update(array($this,'formatCategoryUrl'));
			$crud->callback_column('prodotti', function($value, $row) {
				$counter_active = 0;
				$counter_disabled = 0;
				if(strlen($value)) {
					$arr_values = explode(',', $value);
					foreach ($arr_values as $stato) {
						if($stato == 1)
							$counter_active++;
						else 		
							$counter_disabled++;
					}
				} 
				
				return '<b>Attivi</b> ' . $counter_active . '<br><b>Sospesi</b> ' . $counter_disabled;
			});
			    
			// unset delete action
			//$crud->unset_delete();
			$crud->unset_read();
			    
			$crud->order_by('stato','desc');
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-CATEGORIES';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/categories',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function load_traduzioni($primary_key , $row)
	{
		return site_url('admin/categories/trad/'.$row->id_categorie);
	}
	
	function formatCategoryUrl($post_array) {	    
	    $category_new = formattaUrlCodice($post_array['url_categorie'], '-');
	    // check if exist
	    $this->db->where('url_categorie', $category_new);
	    $cat = $this->db->get('categorie')->row();

	    if (isset($cat) && $cat != NULL) {
	        
		    return false;
	    } else {
	        $post_array['url_categorie'] = $category_new;
	        return $post_array;
	    }
	}
	       
	// Resize dell'immagine caricata
	function _callback_after_upload_img($uploader_response, $field_info, $files_to_upload) {
	    $this->load->library('image_moo');
	    
	    // Sovrascrivo immagine con stesso nome am nuove dimensioni
	    $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
	    $this->image_moo->load($file_uploaded)->resize_crop(500, 455)->save($file_uploaded, true);
	    
	    return true;
	}
	
	// Verifica se la categoria contiene prodotti
	function _callback_before_delete_category($primary_key)
	{
	    
	    // check if is last admin
	    $this->db->where('id_categoria', $primary_key);
	    $prodList = $this->db->get('prodotti_categorie')->result();
	    
	    // property_manager
	    if(count($prodList) > 0) {
	        
	        return false;
	        
	    } else {
	        
	        // get category
	        $this->db->where('id_categorie', $primary_key);
	        $cat = $this->db->get('categorie')->row();
	        
	        // remove avatar image from upload folder
            check_remove_image_file('./assets/assets-frontend/img/categories/'.$cat->immagine);
            $this->db->delete('categorie_traduzioni', array('id_categoria' => $primary_key));
            return true;
	                
	    }
	    
	}
	
	public function traductions($category_id)
	{
	    // get category
	    $this->db->where('id_categorie', $category_id);
	    $cat = $this->db->get('categorie')->row();
	    
		//CRUD varianti
		try {
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// subject
			$crud->set_subject('Traduzioni');
			// tabella
			$crud->set_table('categorie_traduzioni');
			$crud->where('id_categoria', $category_id);
			// nome in tabella
			$crud->display_as('nome_categoria_trad', 'Nome');
			$crud->display_as('id_lingua', 'Lingua');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
			// campi obbligatori
			$crud->required_fields('nome_categoria_trad', 'id_lingua');
			$crud->edit_fields('id_categoria', 'nome_categoria_trad', 'id_lingua');
			$crud->add_fields('id_categoria', 'nome_categoria_trad', 'id_lingua');
			// colonne da mostrare
			$crud->columns('nome_categoria_trad', 'id_lingua');
			// unset delete action
			//$crud->unset_delete();
			$crud->field_type('id_categoria', 'hidden', $category_id);
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-CATEGORIES';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['curr_function_title'] = 'Traduzioni per categoria <b>' . $cat->nome . '</b> ['.$category_id.']';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/categories_trad',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

}
