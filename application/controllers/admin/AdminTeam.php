<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminTeam extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}

	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// subject
			$crud->set_subject('Team');
			// tabella
			$crud->set_table('staff'); 
			$crud->order_by('staff_id', 'desc');
			// nome in tabella
			$crud->display_as('staff_name', 'Nome');
			$crud->display_as('staff_image', 'Immagine');
			// file upload
			$crud->set_field_upload('staff_image', 'assets/assets-frontend/img/team');
			// campi obbligatori
			$crud->add_fields('staff_image', 'staff_name');
			// campi per edit
			$crud->edit_fields('staff_image', 'staff_name');
			$crud->required_fields('staff_image', 'staff_name');
			// callbacks
			$crud->callback_after_upload(array($this, '_callback_after_upload_img'));
			$crud->callback_before_delete(array($this, '_callback_before_delete_staff'));
			// colonne da mostrare
			$crud->columns('staff_image', 'staff_name');
	
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-TEAM';
			$data['curr_page_title'] = 'Team';
			$data['collapseParentMenu'] = 'team';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/team',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	// Resize dell'immagine caricata
	function _callback_after_upload_img($uploader_response, $field_info, $files_to_upload) {
	    $this->load->library('image_moo');
	    
	    // Sovrascrivo immagine con stesso nome am nuove dimensioni
	    $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
	    $this->image_moo->load($file_uploaded)->resize_crop(380, 380)->save($file_uploaded, true);
	    
	    return true;
	}
	
	function _callback_before_delete_staff($primary_key)
	{
	    
	    $this->db->where('staff_id', $primary_key);
	    $staff = $this->db->get('staff')->row();
	    
	    // elimina traduzioni e immagine
	    check_remove_image_file('./assets/assets-frontend/img/team/'.$staff->staff_image);
	    return true;
	    
	}
	
}
