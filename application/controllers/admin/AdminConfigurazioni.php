<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminConfigurazioni extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		
	}
	
	// genera il file di produzione con le costanti presenti nella tabella constants_framework
	public function generateProductionConfigFile()
	{
		$query = $this->db->get('constants_framework');
		updateconfigfile($query->result());
		echo "File ./application/config/production/constants.php was generated";
	}
	
	// gestisce il passaggio del sito da comingsoon a online
	public function updateSiteStatus()
	{
		$value = $this->input->post('value');
		$data = array(
		   'controller' => $value
		);	
		$this->db->where('url_pagina', 'default_page');
		$this->db->update('pagine', $data); 
	}
}
