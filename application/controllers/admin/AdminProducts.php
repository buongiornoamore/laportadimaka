<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminProducts extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}

	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// subject
			$crud->set_subject('Prodotto');
			// tabella
			$crud->set_table('prodotti'); // tornare a prodotti
			$crud->order_by('ordine', 'desc');
			// nome in tabella
			$crud->display_as('id_tipo_prodotto', 'Tipo prodotto');
			$crud->display_as('stato', 'Stato');
			$crud->display_as('ordine', 'Posizione');
			$crud->display_as('url_immagine', 'Immagine');
			// file upload
			$crud->set_field_upload('url_immagine', 'assets/assets-frontend/img/shop');
			// realazioni join
			$crud->set_relation('id_tipo_prodotto', 'tipo_prodotto', 'descrizione_tipo_prodotto');
			$crud->set_relation('stato', 'stato_prodotti', 'stato_prodotti_desc');
			$crud->set_relation_n_n('categorie', 'prodotti_categorie', 'categorie', 'id_prodotto', 'id_categoria', 'nome');
			// campi obbligatori
			$crud->add_fields('id_tipo_prodotto', 'codice', 'nome', 'stato', 'ordine', 'categorie');
			// campi per edit
			$crud->edit_fields('id_tipo_prodotto', 'codice', 'nome', 'stato', 'ordine', 'categorie', 'url_immagine');
			$crud->required_fields('id_tipo_prodotto', 'codice', 'nome', 'stato', 'ordine', 'categorie');

			// callbacks
			$crud->callback_after_upload(array($this, '_callback_after_upload_img'));
			$crud->callback_before_delete(array($this, '_callback_before_delete_product'));
			$crud->callback_before_insert(array($this,'formatCodeUrl'));
			$crud->callback_before_update(array($this,'formatCodeUrl'));
			$crud->set_lang_string('insert_error', 'Si &egrave verificato un errore in inserimento oppure hai cercato di utilizzare un URL CODE già presente.<br><br>Sostituisci e riprova!');
			
			// colonne da mostrare
			$crud->columns('url_immagine', 'codice', 'nome', 'categorie', 'stato', 'ordine');
			// unset delete action
		   // custom action
			$crud->add_action('Traduzioni prodotto', '', '', 'fa-flag', array($this, 'load_traduzioni'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PRODUCTS';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/products',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function load_traduzioni($primary_key , $row)
	{
		return site_url('admin/products/trad/'.$row->id_prodotti.'/'.$row->codice);
	}
	
	// Resize dell'immagine caricata
	function _callback_after_upload_img($uploader_response, $field_info, $files_to_upload) {
	    $this->load->library('image_moo');
	    
	    // Sovrascrivo immagine con stesso nome am nuove dimensioni
	    $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
	    $this->image_moo->load($file_uploaded)->resize_crop(660, 660)->save($file_uploaded, true);
	    
	    return true;
	}
	
	function _callback_before_delete_product($primary_key)
	{
	    
	    $this->db->where('id_prodotti', $primary_key);
	    $prod = $this->db->get('prodotti')->row();
	    
	    // elimina traduzioni e immagine
	    check_remove_image_file('./assets/assets-frontend/img/shop/'.$prod->url_immagine);
	    $this->db->delete('prodotti_traduzioni', array('id_prodotti' => $primary_key));
	    return true;
	    
	}
	
	function formatCodeUrl($post_array) {
	    $code_new = formattaUrlCodice($post_array['codice'], '-');
	    // check if exist
	    $this->db->where('codice', $code_new);
	    $prod = $this->db->get('prodotti')->row();
	    
	    if (isset($prod) && $prod != NULL) {
	        return false;
	    } else {
	        $post_array['codice'] = $code_new;
	        return $post_array;
	    }
	}
	
	public function traductions($prod_id, $prod_code)
	{
	    // get category
	    $this->db->where('id_prodotti', $prod_id);
	    $prod = $this->db->get('prodotti')->row();
	    
		//CRUD varianti
		try {
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// subject
			$crud->set_subject('Traduzioni');
			// tabella
			$crud->set_table('prodotti_traduzioni');
			$crud->where('id_prodotti', $prod_id);
			// nome in tabella
			$crud->display_as('nome_prodotto_trad', 'Nome');
			$crud->display_as('descrizione', 'Descrizione');
			$crud->display_as('descrizione_breve', 'Descrizione breve');
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			// campi obbligatori
			$crud->required_fields('descrizione', 'nome_prodotto_trad', 'descrizione_breve', 'lingua_traduzione_id');
			$crud->edit_fields('nome_prodotto_trad', 'descrizione', 'descrizione_breve', 'lingua_traduzione_id');
			$crud->add_fields('id_prodotti', 'nome_prodotto_trad', 'descrizione', 'descrizione_breve', 'lingua_traduzione_id');
			// colonne da mostrare
			$crud->columns('descrizione', 'descrizione_breve', 'lingua_traduzione_id');
			// unset delete action
			//$crud->unset_delete();
			$crud->unset_texteditor('descrizione');
			$crud->field_type('id_prodotti', 'hidden', $prod_id);
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PRODUCTS';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['curr_function_title'] = 'Traduzioni per <b>' . $prod->nome . '</b> ['.$prod_id.']';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/products_trad',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}
