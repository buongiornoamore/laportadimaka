<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminUtenti extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
	     
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('users');
			$crud->order_by('id', 'desc');
			if( $this->ion_auth->user()->row()->role == 0 )
    			$crud->where('role >=', 0); // no admin | super_admin
           else
                $crud->where('role >=', 1); // admin
                    
			$crud->set_subject('Utente');
			
			$crud->display_as('first_name', 'Nome');
			$crud->display_as('last_name', 'Cognome');
			$crud->display_as('role', 'Ruolo');
			$crud->display_as('username', 'Username');
			$crud->display_as('user_avatar_url', 'Avatar');
			$crud->display_as('active', 'Stato');
			
			$crud->set_field_upload('user_avatar_url', 'assets/assets-admin/img/avatar');

			// azioni
			
			// colonne da mostrare
			$crud->columns('user_avatar_url', 'first_name', 'last_name', 'email', 'username', 'role');

			$crud->fields('first_name', 'id', 'last_name', 'username', 'email', 'password', 'role', 'user_avatar_url', 'active');
			
			// unset action
			$crud->unset_read();

			
			$crud->field_type('password', 'password');
            
			$crud->callback_column('user_avatar_url', array($this, '_callback_user_avatar_url'));
			$crud->callback_before_insert(array($this,'_callback_before_insert_user'));
	    	$crud->callback_before_update(array($this,'_callback_before_update_user'));
			$crud->callback_after_upload(array($this, '_callback_after_upload_img'));
			$crud->callback_before_delete(array($this, '_callback_before_delete_user'));
			$crud->set_lang_string('delete_error_message', 'Si &egrave verificato un errore oppure hai tentato di cancellare un utente protetto.<br><br>Verifica i permessi e riprova!');
			$crud->set_lang_string('alert_delete', 'Sei sicuro di voler eliminare questo utente?<br><br>Se si tratta di un admin potresti non avere i peremssi per rimuoverlo!');
			
			if($crud->getState() == 'add') {
			    
			    $crud->required_fields('first_name', 'last_name', 'email', 'password', 'role');
			    
			    $crud->change_field_type('user_id', 'hidden');
			    if( $this->ion_auth->user()->row()->role == 0 )
    			    $crud->set_relation('role', 'groups', '{name} ({description})', array('id >=' => 0));
	            else    
	                $crud->set_relation('role', 'groups', '{name} ({description})', array('id >' => 1));
    			
	            $crud->change_field_type('id', 'hidden');
			    
			} else if($crud->getState() == 'edit') {

			    $crud->required_fields('first_name', 'last_name', 'email', 'password');
			    
			    $crud->change_field_type('user_id', 'readonly');
			    if( $this->ion_auth->user()->row()->role == 0 ) {
			        $crud->set_relation('role', 'groups', '{name} ({description})', array('id >=' => 0));
			    } else if ($this->ion_auth->user()->row()->role == 1) {
		            $crud->set_relation('role', 'groups', '{name} ({description})', array('id >=' => 1));
			    } else {
			        $crud->change_field_type('role', 'readonly');
			        $crud->set_relation('role', 'groups', '{name} ({description})');
			    }
			    
			    $crud->change_field_type('id', 'hidden');
		  
			} else {
			    
			    $crud->set_relation('role', 'groups', '{name}');
			    
			}
			
			$output = $crud->render();
			
			$data['curr_page'] = 'ADMIN-UTENTI';
			$data['curr_page_title'] = 'Configurazioni';
			$data['collapseParentMenu'] = 'configurazioni';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/utenti',(array)$output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function _callback_user_avatar_url($value, $row)
	{   
	    if($value != '') {
	        return '<a href="'.ASSETS_ROOT_FOLDER_ADMIN_IMG.'/avatar/'.$value.'" class="image-thumbnail"><img src="'.ASSETS_ROOT_FOLDER_ADMIN_IMG.'/avatar/'.$value.'" height="50px" /></a>';
	    } else {
	        return '<a href="'.ASSETS_ROOT_FOLDER_ADMIN_IMG.'/avatar/default_avatar.png" class="image-thumbnail"><img src="'.ASSETS_ROOT_FOLDER_ADMIN_IMG.'/avatar/default_avatar.png" height="50px" /></a>';
	    }
	}
	
	// set encrypt before save pwd
	function _callback_before_insert_user($post_array) {
	    
	    $post_array['password'] = $this->ion_auth->hash_password($post_array['password'], FALSE, FALSE);          
	    return $post_array;
	    
	}
	
	// check if role was changed and insert new values by new role
	function _callback_before_update_user($post_array) {
	    
       // encrypt pwd if is new one (clear)
        if($user->password != $post_array['password'])
            $post_array['password'] = $this->ion_auth->hash_password($post_array['password'], FALSE, FALSE);  
        
       return $post_array;
	    
	}
	
	// Resize dell'immagine caricata e creazione avatar
	function _callback_after_upload_img($uploader_response, $field_info, $files_to_upload) {
	    $this->load->library('image_moo');
	    
	    // Sovrascrivo immagine con stesso nome am nuove dimensioni
	    $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
	    $this->image_moo->load($file_uploaded)->resize_crop(200, 200)->save($file_uploaded, true);
	    
	    return true;
	}

	function _callback_before_delete_user($primary_key)
	{
	    
        // property_manager
	    if($primary_key == 0) {
	        
	       return false;
	       
	    } else {
	       
	        // check if is last admin
	        $this->db->where('role', 1);
	        $userFound = $this->db->get('users')->result();
	        
	        if(count($userFound) > 1)
	            return true;
	        else
	            return false;
	        
	    }
	    
	    // remove avatar image from upload folder
	    check_remove_image_file('./assets/assets-admin/img/avatar/'.$user->user_avatar_url);
	    
	}
	
}
