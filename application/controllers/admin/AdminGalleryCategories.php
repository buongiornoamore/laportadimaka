<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminGalleryCategories extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			$crud->set_subject('Categoria');
			// tabella
			$crud->set_table('categorie_gallery');
			// nome in tabella
			$crud->display_as('nome_categoria_gallery', 'Nome categoria');
			$crud->display_as('stato_categoria_gallery', 'Stato');
			// file upload
			// realazioni join
			$crud->set_relation('stato_categoria_gallery', 'stato_descrizione', 'testo_stato_descrizione');
	    	// campi obbligatori
			$crud->required_fields('nome_categoria_gallery', 'stato_categoria_gallery');
			// regole validazione campi
		//	$crud->set_rules('prezzo', 'Prezzo', 'required|numeric');
		//	$crud->set_rules('prezzo_scontato', 'Prezzo scontato', 'numeric');
			// campi per add
		//	$crud->add_fields('categorie', 'id_tipo_prodotto', 'codice', 'nome', 'prezzo', 'prezzo_scontato', 'stato', 'id_colori_prodotti', 'taglie', 'tags', 'ordine');
		//	$crud->edit_fields('categorie', 'id_tipo_prodotto', 'codice', 'nome', 'prezzo', 'prezzo_scontato', 'stato', 'id_colori_prodotti', 'taglie', 'tags', 'ordine', 'url_img_piccola', 'url_img_grande', 'url_img_grande_retro');
			//if($crud->getState() == 'add')
    	//	{
		//		$crud->field_type('stato', 'dropdown', 3); // stato sospeso
		//	} 
			// callbacks
		//	$crud->callback_before_insert(array($this,'formatProdottoCodice'));
		//	$crud->callback_before_update(array($this,'formatProdottoCodice'));
			$crud->callback_after_delete(array($this, '_callback_delete_category'));
			// colonne da mostrare
			$crud->columns('nome_categoria_gallery', 'stato_categoria_gallery');
			// unset delete action
			//$crud->unset_delete();
			// custom action
			$crud->add_action('Traduzioni categorie', '', '', 'fa-flag', array($this, 'load_traduzioni'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-GALLERY-CATEGORIES';
			$data['curr_page_title'] = 'Gallery';
			$data['collapseParentMenu'] = 'gallery';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/gallerycategories',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function _callback_delete_category($primary_key) {
		// cancella la categoria gallery e a cascata tutte le immagini in essa contenute compresi i file fisici
		$query_category = $this->db->where('id_categoria_ig', $primary_key)->get('immagini_gallery');
		foreach ($query_category->result() as $image)
		{
		unlink('assets/assets-frontend/img/gallery/'.$image->immagine_ig); 
		unlink('assets/assets-frontend/img/gallery/thumbs/'.$image->immagine_thumb_ig);
		$this->db->delete('immagini_gallery_traduzioni', array('id_ig' => $image->id_ig));  
		}
		$this->db->delete('immagini_gallery', array('id_categoria_ig' => $primary_key)); 
		// cancella le traduzioni per la categoria
		$this->db->delete('categorie_gallery_traduzioni', array('id_categoria_gallery' => $primary_key)); 
		return true;	
	}
	
	function load_traduzioni($primary_key , $row)
	{
		return site_url('admin/gallerycategories/trad/'.$row->id_categoria_gallery.'/'.$row->nome_categoria_gallery);
	}

	public function traductions($cat_id, $cat_name)
	{
		// CRUD traduzioni
		try {
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('categorie_gallery_traduzioni');
			$crud->where('id_categoria_gallery', $cat_id);
			// nome in tabella
			$crud->display_as('descrizione_categoria_gallery', 'Descrizione categoria');
			$crud->display_as('id_lingua', 'Lingua');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
			// campi obbligatori
			$crud->required_fields('descrizione_categoria_gallery', 'id_lingua');
			$crud->edit_fields('descrizione_categoria_gallery', 'id_lingua');
			$crud->add_fields('id_categoria_gallery', 'descrizione_categoria_gallery', 'id_lingua');
			// colonne da mostrare
			$crud->columns('descrizione_categoria_gallery', 'id_lingua');
			// unset delete action
			//$crud->unset_delete();
			//$crud->unset_texteditor('descrizione_categoria_gallery');
			$crud->field_type('id_categoria_gallery', 'hidden', $cat_id);
			$output = $crud->render();
			
			$data['curr_page'] = 'ADMIN-GALLERY-CATEGORIES';
			$data['curr_page_title'] = 'Gallery';
			$data['collapseParentMenu'] = 'gallery';
			$data['curr_function_title'] = 'Traduzioni per categoria <b>' . urldecode($cat_name) . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/gallerycategories_trad',(array)$output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}
