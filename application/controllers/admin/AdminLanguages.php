<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminLanguages extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('lingue');
			// nome in tabella
			$crud->display_as('nome_lingue', 'Lingua');
			$crud->display_as('abbr_lingue', 'Abbreviazione');
			$crud->display_as('stato_lingua', 'Stato');
			// realazioni join
			$crud->set_relation('stato_lingua', 'stato_descrizione', 'testo_stato_descrizione');
			// colonne da mostrare
			$crud->columns('nome_lingue', 'abbr_lingue', 'stato_lingua');
			// unset action
			$crud->unset_delete();
			$crud->unset_add();
		//	$crud->required_fields('cf_value');
			// custom action
			$crud->add_action('Traduzioni', '', '', 'fa-flag', array($this, 'load_traduzioni'));
			//$crud->add_action('Traduzioni email', '', '', 'fa-envelope', array($this, 'load_traduzioni'));
			//callbacks
			
			// set update only after update 
			if ($crud->getState() == 'edit') {
				$crud->change_field_type('nome_lingue', 'readonly');
				$crud->change_field_type('abbr_lingue', 'readonly');
				$crud->change_field_type('locale_paypal_lingue', 'readonly');
				$crud->change_field_type('codice_ci', 'readonly');
			} 
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-LANGUAGES';
			$data['curr_page_title'] = 'Configurazioni';
			$data['collapseParentMenu'] = 'configurazioni';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/languages',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}	
	}
	
	function load_traduzioni($primary_key , $row)
	{
		return site_url('admin/languages/trad/'.$row->id_lingue);
	}

	public function traductions($lang_id)
	{
		//CRUD varianti
		try {
			$this->current_lang_id = $lang_id;
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('lingue_labels_lang');
			$crud->where('id_lingua', $lang_id);
			// nome in tabella
			$crud->display_as('lingue_labels_lang_label', 'Label');
			$crud->display_as('lingue_labels_lang_value', 'Valore');
			$crud->display_as('lingue_labels_lang_type', 'Tipo');
			$crud->display_as('lingue_labels_lang_desc', 'Descrizione');
			$crud->display_as('id_lingua', 'Lingua');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
			// campi obbligatori
			$crud->required_fields('lingue_labels_lang_value');
			$crud->edit_fields('lingue_labels_lang_label', 'lingue_labels_lang_value', 'lingue_labels_lang_desc', 'lingue_labels_lang_type', 'id_lingua');
			// colonne da mostrare
			$crud->columns('lingue_labels_lang_label', 'lingue_labels_lang_value', 'lingue_labels_lang_desc', 'lingue_labels_lang_type', 'id_lingua');
			// unset action
			$crud->unset_delete();
			$crud->unset_add();
			// callbacks
			$crud->callback_after_update(array($this, 'update_lang_callback'));
			
			// set update only after update 
			if ($crud->getState() == 'edit') {
				$crud->change_field_type('lingue_labels_lang_label', 'readonly');
				$crud->change_field_type('lingue_labels_lang_desc', 'readonly');
				$crud->change_field_type('id_lingua', 'readonly');
				$crud->change_field_type('lingue_labels_lang_type', 'readonly');
			} 
			$crud->unset_texteditor('lingue_labels_lang_value');
			
			//$crud->field_type('id_lingua', 'hidden', $lang_id);
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-LANGUAGES';
			$data['curr_page_title'] = 'Configurazioni';
			$data['collapseParentMenu'] = 'configurazioni';
			$data['curr_function_title'] = 'Traduzioni <b>labels</b> sito';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/lingue_trad',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
		
	public function update_lang_callback($post_array, $primary_key)
	{
		// current row
		$this->db->select('lingue_labels_lang_type');
		$this->db->from('lingue_labels_lang');
		$this->db->where('id_lingue_labels_lang', $primary_key);
		$curr_row = $this->db->get()->row();
		$lingue_labels_lang_type = $curr_row->lingue_labels_lang_type;
		// load lingua		
		$lang_id = $this->current_lang_id;
		$this->db->select('*');
		$this->db->from('lingue');
		$this->db->where('id_lingue', $lang_id);
		$query_lang = $this->db->get();
		// load lingue_frontend_lang		
		$this->db->select('*');
		$this->db->from('lingue_labels_lang');
		$this->db->where('id_lingua', $lang_id);
		$this->db->where('lingue_labels_lang_type', $lingue_labels_lang_type);
		$query_fe = $this->db->get();
		// load pagine 		
		$this->db->select('*');
		$this->db->from('pagine');
		$this->db->where('id_lingua', $lang_id);
		$this->db->where('label_page_url !=', "");
		$query_pages = $this->db->get();
		// chiama una procedura parametrica passando i dati di interesse e il tipo di labels da generare
		update_lang_file($query_fe->result(), $query_lang->row(), $query_pages->result(), $lingue_labels_lang_type);
		return true;
	}

}
