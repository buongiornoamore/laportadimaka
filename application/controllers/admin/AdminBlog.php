<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminBlog extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function post()
	{
		$this->checkUserPermissions();
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			$crud->set_subject('Post');
			// tabella
			$crud->set_table('post'); // tornare a prodotti
			$crud->order_by('data_post', 'desc');
			// nome in tabella
			$crud->display_as('titolo_post', 'Titolo');
			$crud->display_as('data_post', 'Data');
			$crud->display_as('stato_post', 'Stato');
			$crud->display_as('excerpt_post', 'Descrizione');
			$crud->display_as('contenuto_post', 'Testo');
			$crud->display_as('video_url_post', 'Link video');
			
			// file upload
			$crud->set_field_upload('immagine_post', 'assets/assets-frontend/img/blog');
			// realazioni join
			$crud->set_relation('stato_post', 'stato_prodotti', 'stato_prodotti_desc');
			$crud->set_relation_n_n('categorie', 'post_category_posts', 'post_category', 'id_post_posts', 'id_post_category_posts', 'name_post_category');
			$crud->set_relation_n_n('tags', 'post_tag_posts', 'post_tag', 'post_id_tag', 'post_tag_id', 'name_tag');
			
			$crud->field_type('data_post', 'date');
			//$crud->field_type('data_creazione_post', 'hidden');
			
			if($crud->getState() == 'add'){
			    $crud->field_type('data_creazione_post', 'hidden', date('Y-m-d H:i:s'));
			} else {
			    $crud->field_type('data_creazione_post', 'hidden');
			}
			
			// campi obbligatori
			$crud->required_fields('data_post', 'categorie', 'titolo_post', 'contenuto_post', 'stato_post', 'excerpt_post');
			$crud->add_fields('data_post', 'categorie', 'titolo_post', 'excerpt_post', 'contenuto_post', 'tags', 'stato_post', 'data_creazione_post', 'immagine_post', 'video_url_post');

			// colonne da mostrare
			$crud->columns('data_post', 'titolo_post', 'stato_post');
			// unset delete action
		//	$crud->unset_delete();
			//$crud->unset_add(); nascondi nella pagina
			// custom action
			$crud->add_action('Commenti sul post', '', '', 'fa-comment', array($this, 'load_comments'));
	//		$crud->add_action('Varianti prodotto', '', '', 'fa-shopping-bag', array($this, 'load_varianti'));
			// callbacks
			$crud->callback_after_upload(array($this, '_callback_after_upload_img'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-BLOG-POST';
			$data['curr_page_title'] = 'Post';
			$data['collapseParentMenu'] = 'blog';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/blogpost',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	// Resize dell'immagine caricata
	function _callback_after_upload_img($uploader_response, $field_info, $files_to_upload) {
	    $this->load->library('image_moo');
	    // Sovrascrivo immagine con stesso nome a nuove dimensioni
	    $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
	    $this->image_moo->load($file_uploaded)->resize_crop(800, 420)->save($file_uploaded, true);
	    
	    return true;
	}
	
	function load_comments($primary_key , $row)
	{
	    return site_url('admin/blogcommentsbypost/'.$primary_key);
	}
	
	public function category()
	{
	    $this->checkUserPermissions();
	    //CRUD
	    try{
	        $crud = new grocery_CRUD();
	        // tema
	        $crud->set_theme('bootstrap');
	        $crud->set_subject('Categorie');
	        // tabella
	        $crud->set_table('post_category');
	        // nome in tabella
	        $crud->display_as('id_post_category', 'ID');
	        $crud->display_as('name_post_category', 'Nome');
	        // callbacks
	        $crud->callback_before_delete(array($this, '_callback_before_delete_category'));
	        $crud->set_lang_string('delete_error_message', 'Si &egrave verificato un errore oppure hai tentato di cancellare una categoria collegata a dei post.<br><br>Elimina prima i legami con i post e riprova!');
	        // campi obbligatori
	        $crud->required_fields('name_post_category');
	        $crud->add_fields('name_post_category');
	        
	        // colonne da mostrare
	        $crud->columns('name_post_category');

	        // Controllo per la cancellazione prima se ci sono post collegati
	        
	        $output = $crud->render();
	        
	        $data['curr_page'] = 'ADMIN-BLOG-CATEGORY';
	        $data['curr_page_title'] = 'Categorie';
	        $data['collapseParentMenu'] = 'blog';
	        $data['resourcetype'] = 'CRUD';
	        $output->data = $data;
	        $this->load->view('admin/blogcategory',(array)$output);
	    }catch(Exception $e){
	        show_error($e->getMessage().' --- '.$e->getTraceAsString());
	    }
	}
	
	// Verifica se la categoria contiene prodotti
	function _callback_before_delete_category($primary_key)
	{
	    
	    // check if is last admin
	    $this->db->where('id_post_category_posts', $primary_key);
	    $prodList = $this->db->get('post_category_posts')->result();
	    
	    // property_manager
	    if(count($prodList) > 0) {
	        
	        return false;
	        
	    } else {
	        
	        // get category
	      //  $this->db->where('id_categorie', $primary_key);
	      //  $cat = $this->db->get('categorie')->row();
	        
	        // remove avatar image from upload folder
	       // check_remove_image_file('./assets/assets-frontend/img/categories/'.$cat->immagine);
	      //  $this->db->delete('categorie_traduzioni', array('id_categoria' => $primary_key));
	        return true;
	       
	    }
	    
	}
	
	public function tag()
	{
	    $this->checkUserPermissions();
	    //CRUD
	    try{
	        $crud = new grocery_CRUD();
	        // tema
	        $crud->set_theme('bootstrap');
	        $crud->set_subject('Tag');
	        // tabella
	        $crud->set_table('post_tag');
	        // nome in tabella
	        $crud->display_as('id_tag', 'ID');
	        $crud->display_as('name_tag', 'Nome');
	        // realazioni join
	        //  $crud->set_relation('stato_post', 'stato_prodotti', 'stato_prodotti_desc');
	        //  $crud->set_relation_n_n('categorie', 'post_category_posts', 'post_category', 'id_post', 'id_post_category', 'name_post_category');
	        // campi obbligatori
	        $crud->required_fields('name_tag');
	        $crud->add_fields('name_tag');
	        
	        // colonne da mostrare
	        $crud->columns('name_tag');
	        
	        // Controllo per la cancellazione prima se ci sono post collegati
	        $crud->callback_before_delete(array($this, '_callback_before_delete_tag'));
	        $output = $crud->render();
	        
	        $data['curr_page'] = 'ADMIN-BLOG-TAGS';
	        $data['curr_page_title'] = 'Tags';
	        $data['collapseParentMenu'] = 'blog';
	        $data['resourcetype'] = 'CRUD';
	        $output->data = $data;
	        $this->load->view('admin/blogtag',(array)$output);
	    }catch(Exception $e){
	        show_error($e->getMessage().' --- '.$e->getTraceAsString());
	    }
	}
	
	function _callback_before_delete_tag($primary_key)
	{
	    // elimina le relazioni tra tag e post
	    $this->db->delete('post_tag_posts', array('post_tag_id' => $primary_key));
	    return true;
	    
	}
	
	public function comments()
	{
	    $this->checkUserPermissions();
	    //CRUD
	    try{
	        $crud = new grocery_CRUD();
	        // tema
	        $crud->set_theme('bootstrap');
	        $crud->set_subject('Commenti');
	        // tabella
	        $crud->set_table('post_comments');
	        // nome in tabella
	        $crud->display_as('post_comments_id', 'ID');
	        $crud->display_as('post_comments_username', 'Utente');
	        $crud->display_as('post_comments_status', 'Stato');
	        $crud->display_as('post_comments_date', 'Data');
	        $crud->display_as('post_comments_text', 'Testo');
	        $crud->display_as('post_comments_post_id', 'Post');
	        // realazioni join
	        $crud->set_relation('post_comments_status', 'stato_prodotti', 'stato_prodotti_desc');
	        $crud->set_relation('post_comments_post_id', 'post', 'titolo_post');
	        // campi obbligatori
	        $crud->required_fields('post_comments_text', 'post_comments_username', 'post_comments_status', 'post_comments_post_id');
	        $crud->unset_texteditor('post_comments_text');
	        $crud->add_fields('post_comments_text', 'post_comments_username', 'post_comments_status', 'post_comments_post_id');
	        // colonne da mostrare
	        $crud->columns('post_comments_date', 'post_comments_username', 'post_comments_status');
	        
	        // Controllo per la cancellazione prima se ci sono post collegati
	        $crud->callback_before_delete(array($this, '_callback_before_delete_tag'));
	        $output = $crud->render();
	        
	        $data['curr_page'] = 'ADMIN-BLOG-COMMENTS';
	        $data['curr_page_title'] = 'Commenti';
	        $data['collapseParentMenu'] = 'blog';
	        $data['resourcetype'] = 'CRUD';
	        $output->data = $data;
	        $this->load->view('admin/blogcomments',(array)$output);
	    }catch(Exception $e){
	        show_error($e->getMessage().' --- '.$e->getTraceAsString());
	    }
	}
	
	public function commentsbypost($post_id)
	{
	    $this->checkUserPermissions();
	    
	    // get post
	    $this->db->where('id_post', $post_id);
	    $post = $this->db->get('post')->row();
	    
	    //CRUD
	    try{
	        $crud = new grocery_CRUD();
	        $crud->where('post_comments_post_id', $post_id);
	        // tema
	        $crud->set_theme('bootstrap');
	        $crud->set_subject('Commenti');
	        // tabella
	        $crud->set_table('post_comments');
	        // nome in tabella
	        $crud->display_as('post_comments_id', 'ID');
	        $crud->display_as('post_comments_username', 'Utente');
	        $crud->display_as('post_comments_status', 'Stato');
	        $crud->display_as('post_comments_date', 'Data');
	        $crud->display_as('post_comments_text', 'Testo');
	        $crud->display_as('post_comments_post_id', 'Post');
	        // realazioni join
	        $crud->set_relation('post_comments_status', 'stato_prodotti', 'stato_prodotti_desc');
	        $crud->set_relation('post_comments_post_id', 'post', 'titolo_post');
	        // campi obbligatori
	        $crud->required_fields('post_comments_text', 'post_comments_date', 'post_comments_username', 'post_comments_status', 'post_comments_post_id');
	        $crud->unset_texteditor('post_comments_text');
	        // colonne da mostrare
	        $crud->columns('post_comments_date', 'post_comments_username', 'post_comments_status');
	        $crud->unset_add();
	        // Controllo per la cancellazione prima se ci sono post collegati
	        $crud->callback_before_delete(array($this, '_callback_before_delete_tag'));
	        $output = $crud->render();
	        
	        $data['curr_page'] = 'ADMIN-BLOG-COMMENTS';
	        $data['curr_page_title'] = 'Commenti';
	        $data['collapseParentMenu'] = 'blog';
	        $data['curr_function_title'] = 'Commenti per il post <b>' . $post->titolo_post . '</b> ['.$post_id.']';
	      $data['resourcetype'] = 'CRUD';
	        $output->data = $data;
	        $this->load->view('admin/blogcomments_bypost',(array)$output);
	    }catch(Exception $e){
	        show_error($e->getMessage().' --- '.$e->getTraceAsString());
	    }
	}
	
}
