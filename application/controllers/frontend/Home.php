<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Frontend_Controller {

	public function __construct() 
	{
        parent::__construct();   
	}
	
	public function test($order_number) {
	    log_message('info','**** Send email invoice ***');
	    $this->send_email_order_invoice($order_number, true, false);
	    echo '**** Send email invoice ***';
	}
	
	public function paynow($order_number) {
	  //  echo '**** Paynow #' . $order_number;
	    $data = array();
	    $data['order_number'] = $order_number;
	    $this->load->view('frontend/paynow', $data);
	}
	
	public function index()
	{
		$data = array('productsContainerClass' => 'container-fluid');	
		// fullwidth: container-fluid | standard: container	
		$this->show_view_with_menu('frontend/index', $data);
	}
	
	public function about()
	{
		$data = array();
		// caricare categorie gallery
		$team = $this->db->get('staff')->result();
		$data['team'] = $team;
		$this->show_view_with_menu('frontend/about', $data);	
	}

	public function contacts()
	{
		$data = array();
		$this->show_view_with_menu('frontend/contacts', $data);	
	}
	
	public function privacy()
	{
		$data = array();
		$this->show_view_with_menu('frontend/privacy', $data);
	}
	
	public function shipping()
	{
		$data = array();
		$this->show_view_with_menu('frontend/shipping', $data);
	}
	
	public function rules()
	{
		$data = array();
		$this->show_view_with_menu('frontend/rules', $data);
	}
	
	public function shop($category_url = '')
	{
		// fullwidth: container-fluid | standard: container	
		$data = array('productsContainerClass' => 'container-fluid', 'category_url' => $category_url);		
		$this->show_view_with_menu('frontend/shop', $data);
	}
	
	public function gallery()
	{
		// fullwidth: container-fluid | standard: container	
		$data = array();
		$data = $this->loadGallery($data);
		$data['productsContainerClass'] = 'container-fluid';
		$this->show_view_with_menu('frontend/gallery', $data);
	}
	
	public function loadGallery($data)
	{
		// caricare categorie gallery
		$this->db->select('*');
		$this->db->from('categorie_gallery');
		$this->db->join('categorie_gallery_traduzioni', 'categorie_gallery_traduzioni.id_categoria_gallery = categorie_gallery.id_categoria_gallery', 'left');
		$this->db->where('categorie_gallery.stato_categoria_gallery', 1);
		$this->db->where('categorie_gallery_traduzioni.id_lingua', lang('LANGUAGE_ID'));
		$query_cats = $this->db->get();
		$data['gallery_cats'] = $query_cats->result();
		
		// caricare immagini galleria
		$this->db->select('*');
		$this->db->from('immagini_gallery');
		$this->db->join('immagini_gallery_traduzioni', 'immagini_gallery_traduzioni.id_ig = immagini_gallery.id_ig', 'left');
		$this->db->join('stato_descrizione', 'stato_descrizione.id_stato_descrizione = immagini_gallery.stato_ig');
		$this->db->join('categorie_gallery', 'categorie_gallery.id_categoria_gallery = immagini_gallery.id_categoria_ig');
		$this->db->join('categorie_gallery_traduzioni', 'categorie_gallery_traduzioni.id_categoria_gallery = categorie_gallery.id_categoria_gallery');
		$this->db->where('immagini_gallery.stato_ig', 1);
		$this->db->where('immagini_gallery_traduzioni.id_lingua', lang('LANGUAGE_ID'));
		$this->db->where('categorie_gallery_traduzioni.id_lingua', lang('LANGUAGE_ID'));
		$this->db->order_by("immagini_gallery.ordine_ig", "DESC");		
		$query_immagini = $this->db->get();
		$data['gallery_images'] = $query_immagini->result();

		return $data;
	}
	
	public function blog()
	{    
	    // fullwidth: container-fluid | standard: container
	    $data = array();
	    $data['productsContainerClass'] = 'container-fluid';
	      
        // load post categories
	    $this->db->select('post_category.*, count(post_category_posts.id_pcp) as posts');
	    $this->db->join('post_category_posts', 'post_category_posts.id_post_category_posts = post_category.id_post_category');
	    $this->db->from('post_category');
	    $query_cat = $this->db->get();
	    $data['categories'] = $query_cat->result();
	
	    // carica lista tags
	    $this->db->select('post_tag.*, count(post_tag_posts.post_tag_posts_id) as posts');
	    $this->db->join('post_tag_posts', 'post_tag_posts.post_tag_id = post_tag.id_tag');
	    $this->db->from('post_tag');
	    $query_tags = $this->db->get();
	    $data['tags'] = $query_tags->result();
	
	    //    print_r($this->db->last_query());
	    $this->show_view_with_menu('frontend/blog', $data);
	}
	
	public function loadPosts() {
	    $showLimit = 4;
	    $category = $_POST['category'];
	    $tag = $_POST['tag'];
	    $lastid = $_POST['lastid'];
	    /* Count all */
	    $this->db->select('post.*, GROUP_CONCAT(DISTINCT post_category.name_post_category) as cats, GROUP_CONCAT(DISTINCT post_tag.name_tag) as tags, COUNT(post_comments.post_comments_id) AS comments');
	    $this->db->from('post');
	    $this->db->join('post_category_posts', 'post_category_posts.id_post_posts = post.id_post');
	    $this->db->join('post_category', 'post_category.id_post_category = post_category_posts.id_post_category_posts');
	    $this->db->join('post_tag_posts', 'post_tag_posts.post_id_tag = post.id_post', 'left');
	    $this->db->join('post_tag', 'post_tag.id_tag = post_tag_posts.post_tag_id', 'left');
	    $this->db->join('post_comments', 'post_comments.post_comments_post_id = post.id_post', 'left');
	    /*  $this->db->join('categorie_gallery_traduzioni', 'categorie_gallery_traduzioni.id_categoria_gallery = categorie_gallery.id_categoria_gallery');
	     $this->db->where('immagini_gallery.stato_ig', 1);
	     $this->db->where('immagini_gallery_traduzioni.id_lingua', lang('LANGUAGE_ID'));*/
	    if($category != null && $category != "" && $category != "0")
	        $this->db->where('post_category_posts.id_post_category_posts', $category);
        if($tag != null && $tag != "" && $tag != "0")
            $this->db->where('post_tag_posts.post_tag_id', $tag);
        $this->db->where('post.stato_post', 1);
        if($lastid > 0)
            $this->db->where('post.id_post <', intval($lastid));
        $this->db->group_by("post.id_post");
        $this->db->order_by("post.data_post", "DESC");
        $query_posts_all = $this->db->get();
        $count_all = count($query_posts_all->result());
	    /* and count all */
        //print_r($this->db->last_query());
        
	    // caricare posts del blog
	    $this->db->select('post.*, GROUP_CONCAT(DISTINCT CONCAT(post_category.name_post_category, "|", post_category.id_post_category)) as cats, GROUP_CONCAT(DISTINCT CONCAT(post_tag.name_tag, "|", post_tag.id_tag)) as tags, (SELECT COUNT(*) FROM post_comments WHERE post_comments.post_comments_post_id = post.id_post AND post_comments.post_comments_status = 1) AS comments', FALSE);
	    $this->db->from('post');
	    $this->db->join('post_category_posts', 'post_category_posts.id_post_posts = post.id_post');
	    $this->db->join('post_category', 'post_category.id_post_category = post_category_posts.id_post_category_posts');
	    $this->db->join('post_tag_posts', 'post_tag_posts.post_id_tag = post.id_post', 'left');
	    $this->db->join('post_tag', 'post_tag.id_tag = post_tag_posts.post_tag_id', 'left');
	//    $this->db->join('post_comments', 'post_comments.post_comments_post_id = post.id_post', 'left');
	    /*  $this->db->join('categorie_gallery_traduzioni', 'categorie_gallery_traduzioni.id_categoria_gallery = categorie_gallery.id_categoria_gallery');
	     $this->db->where('immagini_gallery.stato_ig', 1);
	     $this->db->where('immagini_gallery_traduzioni.id_lingua', lang('LANGUAGE_ID'));*/
	    if($category != null && $category != "" && $category != "0")
	        $this->db->where('post_category_posts.id_post_category_posts', $category);
	    if($tag != null && $tag != "" && $tag != "0")
            $this->db->where('post_tag_posts.post_tag_id', $tag);
        $this->db->where('post.stato_post', 1);
        if($lastid > 0)
            $this->db->where('post.id_post <', intval($lastid));
        $this->db->group_by("post.id_post");
        $this->db->order_by("post.data_post", "DESC");
        $this->db->limit($showLimit);
        $query_posts = $this->db->get();
        
     //    print_r($this->db->last_query());
        
        // print posts
        $posts = $query_posts->result();
        $moreToLoad = ($count_all > $showLimit ? $count_all : 0);
        foreach ($posts as $post) {
            echo '<div class="row padding-top paddin-bottom postClass" data-id="'.$post->id_post.'" data-moreload="'.$moreToLoad.'">
            <div class="col-md-3 col-sm-4">
              <div class="blog-post-meta">
                <div class="column">
                  <!-- <span>by </span>
                  <a href="#">Rokaux</a>
                  <span class="divider"></span> -->
                  <a href="blog/'.$post->id_post.'">'.formattaData($post->data_post, 'd/m/Y') .'</a>
                </div>
                <div class="column">
                  <a href="blog/'.$post->id_post.'">
                    <i class="material-icons chat"></i>
                    '.$post->comments .'
                  </a>
                </div>
              </div>
              <h2 class="blog-post-title"><a href="blog/'.$post->id_post.'" class="ajax-post-link">'.$post->titolo_post.'</a></h2>
            </div>
            <div class="col-md-offset-1 col-sm-8">';
            if($post->immagine_post != null && $post->immagine_post != '') {
                echo '<img src="'. ASSETS_ROOT_FOLDER_FRONTEND_IMG .'/blog/'.$post->immagine_post.'" class="space-bottom" alt="'. SITE_TITLE_NAME . ' | ' . $post->titolo_post .'">';
            }
            if($post->video_url_post != null && $post->video_url_post != '') {
              echo '<div class="embed-responsive embed-responsive-16by9 space-bottom">
                <iframe src="'. $post->video_url_post .'" allowfullscreen></iframe>
              </div>';
            }
            echo '<div align="justify">'. excerpt($post->excerpt_post, 250) .'</div>
              <div class="blog-post-meta space-top">
                <div class="column"><b>Categoria:</b> ';
                  $catList = explode(",", $post->cats);
                  foreach ($catList as $cat) {
                      $catSplit = explode("|", $cat);
                      echo '<a href="#" class="catLink" data-id="'.$catSplit[1].'">' . $catSplit[0] .'</a>&nbsp;';
                  }
                  // tags
                  if($post->tags != '') {
                      echo '<br><b>Tag:</b> ';
                      $tagList = explode(",", $post->tags);
                      foreach ($tagList as $tag) {
                          $tagSplit = explode("|", $tag);
                          echo '<a href="#" class="tagLink" data-id="'.$tagSplit[1].'">#' . $tagSplit[0] .'</a>&nbsp;';
                      }
                  }
                echo '</div>
                <div class="column" id="postClassLastAnchor'.$post->id_post.'">
                  <a href="blog/'.$post->id_post.'" class="read-more ajax-post-link">'.lang("LABEL_READ_MORE") .'</a>
                </div>
              </div>
            </div>
          </div><!-- .row -->
          <hr>';
        }
	}
	
	// blog detail
	public function blogpost($post_id)
	{
	    // fullwidth: container-fluid | standard: container
	    $data = array();
	    $data['productsContainerClass'] = 'container-fluid';
	    
	    // caricare posts del blog
	    $this->db->select('post.*, GROUP_CONCAT(DISTINCT CONCAT(post_category.name_post_category, "|", post_category.id_post_category)) as cats, GROUP_CONCAT(DISTINCT CONCAT(post_tag.name_tag, "|", post_tag.id_tag)) as tags', FALSE);
	   // $this->db->select('post.*, GROUP_CONCAT(DISTINCT post_category.name_post_category) as cats, GROUP_CONCAT(DISTINCT post_tag.name_tag) as tags');
	    $this->db->from('post');
	    $this->db->join('post_category_posts', 'post_category_posts.id_post_posts = post.id_post');
	    $this->db->join('post_category', 'post_category.id_post_category = post_category_posts.id_post_category_posts');
	    $this->db->join('post_tag_posts', 'post_tag_posts.post_id_tag = post.id_post', 'left');
	    $this->db->join('post_tag', 'post_tag.id_tag = post_tag_posts.post_tag_id', 'left');
	    
	    //$this->db->where('post.stato_post', 1);
	    $this->db->where('post.id_post', $post_id);
	    $this->db->group_by("post.id_post");
	    $this->db->order_by("post.data_post", "DESC");
	    $query_posts = $this->db->get();
	    
	    //print_r($this->db->last_query());
	    
	    $data['post'] = $query_posts->row();
	    
	    // Commenti
	    $this->db->where('post_comments_post_id', $post_id);	    
	    $this->db->where('post_comments_status', 1);
	    $this->db->order_by("post_comments_date", "DESC");
	    $data['comments'] = $this->db->get('post_comments')->result();
	    
	    $this->show_view_with_menu('frontend/blogpost', $data);
	}
	
	// insert comment to approve and send email
	public function addcomment() {
	    
	    // load current
	    //$this->lang->load('frontend', 'it');
	    
	    $messageJson = array();
	    $name = $this->input->post('name');
	    $message = $this->input->post('message');
	    $postid = $this->input->post('postid');
	   
	    try {
	        
    	    $comment = array(
    	        'post_comments_id' => NULL,
    	        'post_comments_text' => $message,
    	        'post_comments_username' => $name,
    	        'post_comments_status' => 2,
    	        'post_comments_date' => date('Y-m-d H:i:s'),
    	        'post_comments_post_id' => $postid
    	    );
	   
    	    // insert
    	    $this->db->insert('post_comments', $comment); 
    	    
    	    // invia email a maka per commento nuovo inserito
    	    $email_body = 'Nuovo commento inserito approva su admin<br/>' .
                            'Post: #'.$postid .'<br/>' .
                            'Nome: '.$name .'<br/>' .
                            'Messaggio: '.$message;
    	    $this->send_email_text('Nuovo commento post #'.$postid, $email_body, COMPANY_EMAIL, 1);
    	    
	        $messageJson['message'] = lang('MSG_POST_ADDED');
	        $messageJson['title'] = lang('MSG_OPERATION_SUCCESS');
	        $messageJson['type'] = 'success';
	        
	    } catch (Exception $e) {
	        
	        $messageJson['title'] = lang('MSG_OPERATION_FAILURE');
	        $messageJson['message'] = lang('MSG_SERVICE_FAILURE');
	        $messageJson['type'] = 'error';
	        
	    }
	    
	    echo json_encode($messageJson);
	
	}
	
}