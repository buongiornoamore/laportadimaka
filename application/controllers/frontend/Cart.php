<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//require_once(APPPATH . 'libraries/Stripe/lib/Stripe.php');

class Cart extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data = array();
		$this->show_view_with_menu('frontend/shopping-cart', $data);	
	}

	public function checkout()
	{
		log_message( 'info', '>>>> Cart >>> checkout ' );
		// @TODO verificare che non si acceda al checkout da link con carrello vuoto
		$this->db->select('COUNT(*) as count');
		$this->db->from('carrello');
		$this->db->where('carrello.id_sessione_utente', $this->input->cookie('uuid', TRUE));
		$query = $this->db->get();
		$row = $query->row();
		if($row->count > 0){
			log_message( 'info', '>>>> Cart >>> checkout >> $row->count > 0 ' );
		//	if ($this->ion_auth->logged_in() && !$this->ion_auth->is_admin()){
			if ($this->ion_auth->logged_in()){	
				log_message( 'info', '>>>> Cart >>> checkout >> logged_in ' );
				$this->load->model('Cliente'); // users (plural) is the table-level model
				$clienteObj = $this->Cliente->get_Cliente_by_User_id($this->ion_auth->user()->row()->id);
				
				log_message( 'info', '>>>> Cart >>> checkout >> $clienteObj->user_id >>> ' . $this->ion_auth->user()->row()->id );
				log_message( 'info', '>>>> Cart >>> checkout >> $clienteObj->id_cliente >>> ' . $clienteObj->id_cliente );
				// carica lista indirizzi spedizione del cliente
				$this->db->select('*');
				$this->db->from('indirizzo_spedizione');
				$this->db->where('indirizzo_spedizione.id_cliente', $clienteObj->id_cliente);
				$this->db->order_by('indirizzo_spedizione.flag_predefinito_sped', 'DESC');
				$query_sped = $this->db->get();
				// carica lista countries
				$this->db->select('*');
				$this->db->from('countries');
				$this->db->order_by('country_name', 'ASC');
				$query_country = $this->db->get();
				
				$data = array('cliente' => $clienteObj, 'indirizzi_spedizione' => $query_sped->result(), 'countries' => $query_country->result());
			}else{
				log_message( 'info', '>>>> Cart >>> checkout >> !logged_in ' );
				// carica lista countries
				$this->db->select('*');
				$this->db->from('countries');
				$this->db->order_by('country_name', 'ASC');
				$query_country = $this->db->get();
				$data = array('countries' => $query_country->result());
			}
			//var_dump($data);
			$this->show_view_with_menu('frontend/checkout', $data);	
		}
		else{
			$data = array();
		//	$this->show_view_with_menu('frontend/shopping-cart', $data);	
			redirect(lang('PAGE_CART_URL'), 'refresh');
		}
	}

	private function checkCartItems() {
		// elimina dal carrello se variante o prodotto non esiste
		$this->db->select('carrello.*, varianti_prodotti.id_variante AS var_check, prodotti.id_prodotti AS prod_check');
		$this->db->join('varianti_prodotti', 'varianti_prodotti.id_variante = carrello.id_variante', 'LEFT');
		$this->db->join('prodotti', 'prodotti.id_prodotti = carrello.id_prodotto', 'LEFT');
		$this->db->from('carrello');
		if(!$this->ion_auth->logged_in())
			$this->db->where('carrello.id_sessione_utente', $this->input->cookie('uuid', TRUE));
		else 
			$this->db->where('carrello.id_cliente', $this->session->userdata('id_cliente'));
		
		$query_check= $this->db->get();
		foreach ($query_check->result() as $check)
		{
			//echo $check->var_check . ' ' . $check->prod_check . '<br>';
			if($check->var_check == NULL || $check->prod_check == NULL) {
				$this->db->delete('carrello', array('id_carrello' => $check->id_carrello)); 
			}
		}
	}
	
	public function loadCart()
	{	
		// carerllo check prodotti
		$this->checkCartItems();
		// mostra solo i prodotti acquistabili e non quelli sospesi o disattivati
		$this->db->select('carrello.*,
							prodotti_traduzioni.descrizione_breve AS descrizione_breve,
							prodotti.nome AS nome,
							varianti_prodotti.colore AS colore,
							varianti_prodotti.colore_codice AS colore_codice,
							varianti_prodotti.codice AS codice_variante,
							varianti_prodotti.codice_prodotto AS codice_prodotto,
							varianti_prodotti.url_img_piccola AS url_img_piccola_variante,
							varianti_prodotti.url_img_grande AS url_img_grande_variante,
							varianti_prodotti.prezzo AS prezzo_variante,
							varianti_prodotti.prezzo_scontato AS prezzo_scontato_variante');
		
		$this->db->from('carrello');
		$this->db->join('varianti_prodotti', 'varianti_prodotti.id_variante = carrello.id_variante');
		$this->db->join('prodotti', 'prodotti.id_prodotti = carrello.id_prodotto');
		$this->db->join('prodotti_traduzioni', 'carrello.id_prodotto = prodotti_traduzioni.id_prodotti', "LEFT");
		
		if(!$this->ion_auth->logged_in())
			$this->db->where('carrello.id_sessione_utente', $this->input->cookie('uuid', TRUE));
		else 
			$this->db->where('carrello.id_cliente', $this->session->userdata('id_cliente'));
				
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->where('varianti_prodotti.in_stock >', 0);
		$this->db->where('varianti_prodotti.stato', 1);
		$this->db->where('prodotti.stato', 1);
		
		$query = $this->db->get();
		$carrello_list = $query->result();
		$carrello_list_count = $query->num_rows();

		echo '<!-- Cart -->
        <div class="col-sm-8 padding-bottom-2x">
          <p class="text-sm">
            <span class="text-gray">'. lang("LABEL_CART_INFO_ACTUALLY_1") .'</span> '. $carrello_list_count . ' ' . lang("LABEL_PRODUCTS").'
            <span class="text-gray"> '. lang("LABEL_CART_INFO_ACTUALLY_2") .'</span>
          </p>
          <div class="shopping-cart">';

		$cart_total = 0;
		if(!isset($carrello_list)) {
			echo '<p>'.lang("LABEL_CART_EMPTY").'</p>';
		} else {
			foreach($carrello_list as $item) {

				$detail_link = '';
				$img_piccola = '';
				if($item->id_tipo_prodotto = 1 && $item->prezzo_scontato_variante > 0){
					$prezzo_reale = ($item->prezzo_scontato_variante);
				}else {
					$prezzo_reale = ($item->prezzo_variante);
				}
				$detail_link = 	base_url() . lang('PAGE_PRODUCTS_URL') . '/' . $item->codice_prodotto . '/' . $item->codice_variante . '/' . cleanString($item->descrizione_breve . ' ' . $item->colore, true);
				$img_piccola = $item->url_img_piccola_variante;
				echo '<!-- Item -->
				<div class="item">
				  <a href="'.$detail_link.'" class="item-thumb" title="'.lang('LABEL_DETAIL').'" alt="'.$item->nome.'">
					<img src="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/cart/'.$img_piccola.'" alt="'.SITE_TITLE_NAME.' | '.$item->nome.(isset($item->colore) && $item->colore != '' ? ' - ' . $item->colore : '').'">
				  </a>
				  <div class="item-details">
					<h3 class="item-title"><a href="'.$detail_link.'" title="'.lang('LABEL_DETAIL').'">'.$item->nome.(isset($item->colore) && $item->colore != '' ? ' - ' . $item->colore . '<i class="material-icons fiber_manual_record color-span-bullet" style="color:#'.$item->colore_codice.'"></i>' : '').'</a></h3>
					<h4 class="item-price">'. lang("LABEL_SIZE") . ': ' . $item->taglia . ' | ' . stampaValutaHtml($prezzo_reale, true, true) .'</h4>
					<div class="count-input">
					  <a class="incr-btn" data-action="decrease" href="#" data-id="'.$item->id_carrello.'">–</a>
					  <input class="quantity" type="text" value="'.$item->qty.'" data-old="'.$item->qty.'" id="qty_'.$item->id_carrello.'" readonly>
					  <a class="incr-btn" data-action="increase" href="#" data-id="'.$item->id_carrello.'">+</a>
					</div>
				  </div>
				  <a href="#" class="item-remove" data-toggle="tooltip" data-placement="top" title="'.lang("LABEL_REMOVE").'" data-id="'.$item->id_carrello.'">
					<i class="material-icons remove_shopping_cart"></i>
				  </a>
				</div><!-- .item -->';

				$cart_total += ($prezzo_reale * $item->qty);
			}
		}

        echo '</div><!-- .shopping-cart -->';
        if($cart_total > 0) {
		  echo '<!-- Coupon -->
          <div class="">
            <p class="text-gray text-sm"><b>'.lang('LABEL_COUPON_HAVE').'</b></p>
            <form method="post" class="row" id="coupon-form">
              <div class="col-md-8 col-sm-7">
                <div class="form-element '.($this->session->userdata('validated_coupon') != NULL ? 'valid' : '').'" id="coupon-element-div" placeholder="'.lang('LABEL_COUPON_INSERT').'">
                  <input type="text" class="form-control" id="coupon-element-input" value="'.($this->session->userdata('validated_coupon') != NULL ? $this->session->userdata('validated_coupon') : '').'" />    
				</div>
              </div>
              <div class="col-md-4 col-sm-5">
                <button type="button" id="submit-coupon-btn" class="btn btn-primary btn-block space-top-none space-bottom" data-text="'.lang('LABEL_COUPON_APPLY').'">'.lang('LABEL_COUPON_APPLY').'</button>
				<span id="coupon-element-loader" style="display:none;"><img style="width: 45px;" src="'. ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/field_loader.gif" /></span>
              </div>
            </form>
          </div>';
		}
		echo '</div><!-- .col-sm-8 -->';
		$cart_total_nocoupon = $cart_total;
		if($this->session->userdata('validated_coupon') != NULL) {
			$valore_sconto_coupon = 0;
			if($this->session->userdata('validated_coupon_perc') > 0) {
				$valore_sconto_coupon = ($cart_total * $this->session->userdata('validated_coupon_perc')) / 100;
			} else {
				$valore_sconto_coupon = $this->session->userdata('validated_coupon_value');
			}
			$this->session->set_userdata('validated_coupon_value', $valore_sconto_coupon);
			$cart_total -= $valore_sconto_coupon;
		 } 
		
		 echo '<!-- Sidebar -->
			<div class="col-md-3 col-md-offset-1 col-sm-4 padding-bottom-2x">
			  <aside>';
		if($cart_total > 0) {
			echo '<h3 class="toolbar-title">'. lang("LABEL_TOTAL_CART") .'</h3>
				<h4 class="amount">'. stampaValutaHtml($cart_total, true, true) .'</h4>';
			if($this->session->userdata('validated_coupon') != NULL) {
				echo '<p><b>'.lang('LABEL_CART').':</b> ' . stampaValutaHtml($cart_total_nocoupon, true, true)
				.'<br><span style="color:red">'.lang('LABEL_DISCOUNT').($this->session->userdata('validated_coupon_perc') > 0 ? ' '.$this->session->userdata('validated_coupon_perc').'%' : '').':</span> '.stampaValutaHtml($valore_sconto_coupon, true, true).'<br><b>'.lang('LABEL_COUPON').':</b> ' . $this->session->userdata('validated_coupon')
				.'</p>';
			} 
		  echo '<p class="text-sm text-gray">'. lang("LABEL_TOTAL_ORDER_NOTES") .'</p>
			   <!-- <a href="cart" class="btn btn-default btn-block waves-effect waves-light">
					 <i class="material-icons loop"></i>
					 <?// echo lang("LABEL_UPDATE_CART"); ?>
				</a>-->
				<a href="'.site_url(lang('PAGE_SHOP_URL')).'" class="btn btn-default btn-ghost icon-left btn-block">
				  <i class="material-icons arrow_back"></i>
				  '. lang("LABEL_BACK_TO_SHOP") .'
				</a>';
				if($carrello_list_count > 0)
					echo '<a href="checkout" class="btn btn-primary btn-block waves-effect waves-light space-top-none">'. lang("LABEL_CONFIRM") .'</a>';
		} else {
			  echo '<a href="'.site_url(lang('PAGE_SHOP_URL')).'" class="btn btn-default btn-ghost icon-left btn-block">
				  <i class="material-icons arrow_back"></i>
				  '. lang("LABEL_BACK_TO_SHOP") .'
				</a>';
				// invalidate coupons
				$this->session->unset_userdata('validated_coupon');
				$this->session->unset_userdata('validated_coupon_value');
				$this->session->unset_userdata('validated_coupon_perc');
				$this->session->unset_userdata('validated_coupon_type');
		}
		  echo '</aside>
			</div><!-- .col-md-3.col-sm-4 -->';
	}

	// add product to cart
	public function addToCart()
	{
		$product_id = $_POST['product_id'];
		$variant_id = (isset($_POST['variant_id']) && $_POST['variant_id'] != '' ? $_POST['variant_id'] : 0);
		$size = $_POST['size'];
		//$size_id = $_POST['size_id'];
		$size_id = 0;
		$qty = (isset($_POST['qty']) && $_POST['qty'] > 0 ? $_POST['qty'] : 1);
		
		// Recupera uuid dal cookie
		$user_uuid = $this->input->cookie('uuid', TRUE);
		
		// recupera id_cliente se loggato
		$id_cliente = 0;
		if($this->ion_auth->logged_in()) {
			// Recupero ID CLIENTE DALLA SESSIONE
			$id_cliente = $this->session->userdata('id_cliente');
		}
		
		// check if product exist in cart update qty (same id_prodotto and id_sessione_utente and scaduto = 0)
		$this->db->select('*');
		$this->db->from('carrello');
		$this->db->where('id_prodotto', $product_id);
		$this->db->where('id_variante', $variant_id);
		if($id_cliente > 0)
			$this->db->where('id_cliente', $id_cliente);
		else
			$this->db->where('id_sessione_utente', $user_uuid);
		$this->db->where('taglia', $size);
		$this->db->where('taglia_id', $size_id);

		$query = $this->db->get();
		$count = $query->num_rows();
		
		if ($count === 0)
		{
			// insert new product
			$data = array(
			   'id_carrello' => NULL,
			   'id_prodotto' => $product_id,
			   'id_variante' => $variant_id,
			   'id_sessione_utente' => $user_uuid,
			   'id_cliente' => $id_cliente,
			   'data_creazione' => date('Y-m-d H:i:s'),
			   'qty' => $qty,
			   'taglia' => $size,
			   'taglia_id' => $size_id
			);

			$this->db->insert('carrello', $data);
		}
		else
		{
			$row = $query->row();
			// update qty of existent product
			$qty = $row->qty + $qty;
			$data = array(
               'qty' => $qty
            );

			$this->db->where('id_carrello', $row->id_carrello);
			$this->db->update('carrello', $data);
		}
	}

	// remove product from cart
	public function removeFromCart()
	{
		$cart_id = $_POST['cart_id'];
		$this->db->where('id_carrello', $cart_id);
		$this->db->delete('carrello');
	}

	// update product to cart
	public function updateToCart()
	{
		$cart_id = $_POST['cart_id'];
		$qty = $_POST['qty'];
		$data = array(
		   'qty' => $qty
		);

		$this->db->where('id_carrello', $cart_id);
		$this->db->update('carrello', $data);
	}

	// load/refresh cart-dropdown
	public function loadCartDropDown()
	{
		// carerllo check prodotti
		$this->checkCartItems();
		
		// Recupera uuid dal cookie
		$user_uuid = $this->input->cookie('uuid', TRUE);
		
		// recupera id_cliente se loggato
		$id_cliente = 0;
		if($this->ion_auth->logged_in()) {
			// Recupero ID CLIENTE DALLA SESSIONE
			$id_cliente = $this->session->userdata('id_cliente');
		}
		
		$this->db->select('carrello.*,
							prodotti_traduzioni.descrizione_breve AS descrizione_breve,
							prodotti.nome AS nome,
							varianti_prodotti.colore AS colore,
							varianti_prodotti.codice AS codice_variante,
							varianti_prodotti.codice_prodotto AS codice_prodotto,
							varianti_prodotti.url_img_piccola AS url_img_piccola_variante,
							varianti_prodotti.url_img_grande AS url_img_grande_variante,
							varianti_prodotti.prezzo AS prezzo_variante,
							varianti_prodotti.prezzo_scontato AS prezzo_scontato_variante');
		
		$this->db->from('carrello');
		$this->db->join('varianti_prodotti', 'varianti_prodotti.id_variante = carrello.id_variante');
		$this->db->join('prodotti', 'prodotti.id_prodotti = carrello.id_prodotto');
		$this->db->join('prodotti_traduzioni', 'carrello.id_prodotto = prodotti_traduzioni.id_prodotti', "LEFT");
		if($id_cliente > 0)
			$this->db->where('carrello.id_cliente', $id_cliente);
		else
			$this->db->where('carrello.id_sessione_utente', $user_uuid);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));	
		$this->db->where('varianti_prodotti.in_stock >', 0);
		$this->db->where('varianti_prodotti.stato', 1);
		$this->db->where('prodotti.stato', 1);
		
		$query = $this->db->get();
		$carrello_list = $query->result();
		$carrello_list_count = $query->num_rows();

		echo '<a href="'. ($carrello_list_count > 0 ? site_url(lang('PAGE_CART_URL')) : '#') .'"><!--cart-->
          <i>
            <span class="material-icons shopping_basket"></span>
            <span class="count">'. $carrello_list_count .'</span>
          </i>
        </a>';
		// visibility:hidden per il carrello piccolo
		echo'<!-- Cart Dropdown -->
		<div class="cart-dropdown" id="cart-dropdown" style="'.($_POST['loadDropDownDiv'] == 'false' ? 'visibility:hidden;' : '').'">';
		$cart_total = 0;
		if(!isset($carrello_list) || $carrello_list_count == 0) {
			echo '<p align="center" style="margin:0">'. lang("LABEL_CART_EMPTY") .'</p>';
		} else {
			foreach($carrello_list as $item) {

				$detail_link = '';
				$img_piccola = '';
				if($item->id_variante > 0){
					if($item->id_tipo_prodotto = 1 && $item->prezzo_scontato_variante > 0){
						$prezzo_reale = ($item->prezzo_scontato_variante);
					}else {
						$prezzo_reale = ($item->prezzo_variante);
					}
					$detail_link = base_url() . lang('PAGE_PRODUCTS_URL') .  '/' . $item->codice_prodotto . '/' . $item->codice_variante . '/' . cleanString($item->descrizione_breve . (isset($item->colore) ? ' ' . $item->colore : ''), true);
					$img_piccola = $item->url_img_piccola_variante;
				}
				else
				{
					if($item->id_tipo_prodotto = 1 && $item->prezzo_scontato > 0){
						$prezzo_reale = ($item->prezzo_scontato);
					}else {
						$prezzo_reale = ($item->prezzo);
					}
					$detail_link = 	base_url() . lang('PAGE_PRODUCTS_URL') . '/' . $item->codice_prodotto . '/' . cleanString($item->descrizione_breve . (isset($item->colore) && $item->colore != '' ? ' - ' . $item->colore : ''), true);
					$img_piccola = $item->url_img_piccola;
				}

				echo '<div class="cart-item">
						<a href="'.$detail_link.'" class="item-thumb" title="'.lang('LABEL_DETAIL').'" alt="'.$item->nome.'">
						  <img src="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/cart/'.$img_piccola.'" alt="'.SITE_TITLE_NAME.' | '.$item->nome.(isset($item->colore) && $item->colore != '' ? ' - ' . $item->colore : '').'">
						</a>
						<div class="item-details">
						  <h3 class="item-title"><a href="'.$detail_link.'" title="'.lang('LABEL_DETAIL').'">'.$item->nome.'</a></h3>

						  <h4 class="item-price">'. $item->qty .' x ' . stampaValutaHtml($prezzo_reale, true, true) . '</h4>
						  <h4 class="item-price">'. lang("LABEL_SIZE") . ': ' . $item->taglia .(isset($item->colore) && $item->colore != '' ? ' - ' . $item->colore : ''). '</h4>
						</div>
						<a href="#" class="close-btn item-remove-drop" data-toggle="tooltip" data-placement="top" title="'.lang("LABEL_REMOVE").'" data-id="'.$item->id_carrello.'">
						  <i class="material-icons close"></i>
						</a>
					  </div><!-- .cart-item -->';

				$cart_total += ($prezzo_reale * $item->qty);

			}
			
			$cart_total_coupon = $cart_total;
			
			  echo '<div class="cart-subtotal"><div class="column">
					  <span>'. lang("LABEL_CART") .':</span>
					</div>
					<div class="column">
					  <span class="amount">'. stampaValutaHtml($cart_total, true, true) .'</span>
					</div>
				  </div>';
				  
				  // COUPON
					if($cart_total > 0 && $this->session->userdata('validated_coupon') != NULL) {
						$valore_sconto_coupon = 0;
						if($this->session->userdata('validated_coupon_perc') > 0) {
							$valore_sconto_coupon = ($cart_total * $this->session->userdata('validated_coupon_perc')) / 100;
						} else {
							$valore_sconto_coupon = $this->session->userdata('validated_coupon_value');
						}
						
						// verifica se il coupon vale più del carrello
						if($cart_total > 0 && $valore_sconto_coupon <= $cart_total) {
							$this->session->set_userdata('validated_coupon_value', $valore_sconto_coupon);
							$cart_total_coupon = $cart_total - $valore_sconto_coupon;
							echo '<div class="cart-subtotal"><div class="column">
							  <span>'.lang('LABEL_COUPON').($this->session->userdata('validated_coupon_perc') > 0 ? ' '.$this->session->userdata('validated_coupon_perc').'%' : '').':</span>
							  </div>
							  <div class="column">
								<span class="amount" style="color:red">'. stampaValutaHtml($valore_sconto_coupon, true, true) .'</span></div></div>';
							echo '<div class="cart-subtotal" style="border-top: 1px dotted #ccc;"><div class="column">
							  <span>'.lang('LABEL_TOTAL_ORDER').':</span>
							  </div>
							  <div class="column">
								<span class="amount">'. stampaValutaHtml($cart_total_coupon, true, true) .'</span></div></div>';	
							// hidden per pagina carrello	 
							$cart_total_hidden_div = '<h4 id="total-order-text">'. stampaValutaHtml($cart_total_coupon, true, true) .'</h4>'.
							'<p><b>'.lang('LABEL_CART').':</b> ' . stampaValutaHtml($cart_total, true, true)
						.'<br><span style="color:red">'.lang('LABEL_DISCOUNT').($this->session->userdata('validated_coupon_perc') > 0 ? ' '.$this->session->userdata('validated_coupon_perc').'%' : '').':</span> '.stampaValutaHtml($valore_sconto_coupon, true, true).'<br><b>'.lang('LABEL_COUPON').':</b> ' . $this->session->userdata('validated_coupon').'</p>';	
						} else {
							// invalida il coupon e non lo calcolare
							$this->session->unset_userdata('validated_coupon');
							$this->session->unset_userdata('validated_coupon_value');
							$this->session->unset_userdata('validated_coupon_perc');
							$this->session->unset_userdata('validated_coupon_type');
							$cart_total_hidden_div = '<h4 id="total-order-text">'. stampaValutaHtml($cart_total, true, true) .'</h4>';
						}
					 } else {
						// invalida il coupon e non lo calcolare
						$this->session->unset_userdata('validated_coupon');
						$this->session->unset_userdata('validated_coupon_value');
						$this->session->unset_userdata('validated_coupon_perc');
						$this->session->unset_userdata('validated_coupon_type');
						$cart_total_hidden_div = '<h4 id="total-order-text">'. stampaValutaHtml($cart_total, true, true) .'</h4>';	
					 }
				  
				  echo '<p align="center" style="padding-top:30px;margin:0;"><a href="'. site_url(lang('PAGE_CART_URL')) .'" class="btn btn-primary btn-block waves-effect waves-light space-top-none">'. lang("LABEL_CART") .'</a></p>
				</div><!-- .cart-dropdown -->';
	  
			// Hiiden fields
			echo '<input type="hidden" id="cart-total-hidden" value="'. stampaValutaHtml($cart_total, true, true) .'" />
				<input type="hidden" id="cart-total-float-hidden" value="'. $cart_total_coupon .'" />
				<input type="hidden" id="cart-total-float-nocoupon-hidden" value="'. $cart_total .'" />
				<div id="cart-total-hidden-div" style="display:none">'.$cart_total_hidden_div.'</div>';
		
		}
	}

	// checkout
	public function checkoutOrder()
	{
		$co_f_name = $_POST['co_f_name'];
		$co_email = $_POST['co_email'];
		$co_l_name = $_POST['co_l_name'];
		$co_phone = $_POST['co_phone'];
		$co_country = $_POST['co_country'];
		$co_city = $_POST['co_city'];
		$co_ref = $_POST['co_ref'];
		$co_address = $_POST['co_address'];
		$co_civico = $_POST['co_civico'];
		$co_postalcode = $_POST['co_postalcode'];
		$co_note_order = (isset($_POST['co_note_order']) ? $_POST['co_note_order'] : '');
		$co_payment = $_POST['co_payment'];
		$co_payment_status = $_POST['co_payment_status'];
		$co_total_order = $_POST['co_total_order'];
		$token_id_hidden = $_POST['token_id_hidden'];
		$coupon_code_hidden = $_POST['coupon_code_hidden'];
		$coupon_value_hidden = $_POST['coupon_value_hidden'];
		
		//*** indirizzo alternativo di spedizione se presente
		// se l'utente non è loggato l'indirizzo current va inserito sia in indirizzo_fatturazione che in indirizzo_spedizione
		$curr_address_choice = (isset($_POST['curr_address_choice']) ? $_POST['curr_address_choice'] : 'current'); 
		// id_indirizzo_spedizione
		$co_address_default_id = (isset($_POST['co_address_default_id']) ? $_POST['co_address_default_id'] : 0); 
		$co_address_registered = (isset($_POST['co_address_registered']) ? $_POST['co_address_registered'] : 0); 
		$co_alt_ref = (isset($_POST['co_alt_ref']) ? $_POST['co_alt_ref'] : '');
		$co_alt_country = (isset($_POST['co_alt_country']) ? $_POST['co_alt_country'] : '');
		$co_alt_city = (isset($_POST['co_alt_city']) ? $_POST['co_alt_city'] : '');
		$co_alt_address = (isset($_POST['co_alt_address']) ? $_POST['co_alt_address'] : '');
		$co_alt_postalcode = (isset($_POST['co_alt_postalcode']) ? $_POST['co_alt_postalcode'] : '');
		$co_alt_civico = (isset($_POST['co_alt_civico']) ? $_POST['co_alt_civico'] : '');
		$co_alt_note_address = (isset($_POST['co_alt_note_address']) ? $_POST['co_alt_note_address'] : '');

		$user_uuid = $this->input->cookie('uuid', TRUE);
		
		$id_cliente = 0;
		
		log_message('info','**** Start chechout_order ***');
		//*** inserisci nuovo cliente se non loggato
		if($this->ion_auth->logged_in()) {
			// Recupero ID CLIENTE DALLA SESSIONE
			$id_cliente = $this->session->userdata('id_cliente');
		} else {
			// @TODO verificare se email cliente è già esistente e proporre registrazione o recupero password o inserimento password
			$data_cliente = array(
			   'id_cliente' =>NULL,
			   'nome' => $co_f_name,
			   'cognome' => $co_l_name,
			   'email' => $co_email,
			   'telefono' => $co_phone,
			   'partita_iva' => '',
			   'codice_fiscale' => '',
			   'newsletter' => 1,
			   'punti' => 0,
			   'id_lingua' => lang('LANGUAGE_ID'),
			   'id_indirizzo_fatturazione' => 0
			);
			$this->db->insert('clienti', $data_cliente);

			// id cliente
			$id_cliente = $this->db->insert_id();
		}
		
		log_message('info','**** Set customer id: ' .$id_cliente. ' ***');
		
		$id_indirizzo_fatturazione = $_POST['id_fatturazione_hidden'];
		//*** inserisci nuovo indirizzo_fatturazione se non esistente altrimenti update
		if($id_indirizzo_fatturazione == 0) {
			$data_fatt = array(
			   'id_indirizzo_fatturazione' =>NULL,
			   'indirizzo_fatt' => $co_address,
			   'civico_fatt' => $co_civico,
			   'cap_fatt' => $co_postalcode,
			   'nazione_fatt' => $co_country,
			   'citta_fatt' => $co_city,
			   'riferimento_fatt' => $co_ref,
			   'note_fatt' => '',
			   'id_cliente' => $id_cliente
			);
			$this->db->insert('indirizzo_fatturazione', $data_fatt);
			// id_indirizzo_spedizione
			$id_indirizzo_fatturazione = $this->db->insert_id();
		}
		
		log_message('info','**** Set indirizzo_fatturazione id: ' .$id_indirizzo_fatturazione. ' ***');
		
		// update indirizzo fatturazione appena creato in clienti
		if($id_indirizzo_fatturazione > 0) {
			$this->db->set('id_indirizzo_fatturazione', $id_indirizzo_fatturazione, FALSE);
			$this->db->where('id_cliente', $id_cliente);
			$this->db->update('clienti');
		}
		
		$id_indirizzo_fatturazione_spedizione = 0;
		$indirizzo_spedizione_storico = '';
		$note_spedizione_storico = '';
		if($curr_address_choice == 'other') {
			// SPEDISCI A UN ALTRO INDIRIZZO
			if($co_address_registered == 0) {
				// inserisci nuovo in indirizzo_spedizione
				$data_sped = array(
				   'id_indirizzo_spedizione' =>NULL,
				   'id_cliente' => $id_cliente,
				   'indirizzo_sped' => $co_alt_address,
				   'civico_sped' => $co_alt_civico,
				   'cap_sped' => $co_alt_postalcode,
				   'nazione_sped' => $co_alt_country,
				   'citta_sped' => $co_alt_city,
				   'riferimento_sped' => $co_alt_ref,
				   'note_sped' => $co_alt_note_address,
				   'flag_predefinito_sped' => 0
				);
				$this->db->insert('indirizzo_spedizione', $data_sped);
				// id_indirizzo_spedizione
				$co_address_registered = $this->db->insert_id();
			} else {
				// $co_address_registered contiene l'id dell'indirizzo selezionato nella combo
			}
			// setto indirizzo corrente come spedizione per lo storico
			$indirizzo_spedizione_storico = $co_alt_ref . ' | ' . $co_alt_address . ', ' . $co_alt_civico . ' - ' . $co_alt_postalcode . ' ' . $co_alt_city. ' (' . $co_alt_country . ')';
			$note_spedizione_storico = $co_alt_note_address;
		} else {
			// SPEDISCI A QUESTO INDIRIZZO
			// lega indirizzo di fatturazione alla spedizione nell'ordine
			$id_indirizzo_fatturazione_spedizione = $id_indirizzo_fatturazione;
			$co_address_registered = 0;
		}
		
		// calcola punti per il cliente e metti anche in ordine
		if(POINTS_ENABLED)
			$points = round($co_total_order * PERC_PUNTI_RETURN / 100);
		
		//*** inserisci nuovo ordine
		$data = array(
		   'id_ordine' => NULL,
		   'id_cliente' => $id_cliente,
		   'data_ordine' => date('Y-m-d H:i:s'),
		   'totale_ordine' => round($co_total_order, 2),
		   'note_ordine' => $co_note_order,
		   'tipo_pagamento' => ($co_payment == 'paypal' ? 1 : 2),
		   'stato_pagamento' => 2, //$co_payment_status
		   'token_pagamento' => $token_id_hidden,
		   'stato_ordine' => 1,
		   'id_indirizzo_spedizione' => $co_address_registered,
		   'id_indirizzo_fatturazione_spedizione' => $id_indirizzo_fatturazione_spedizione,
		   'punti' => (POINTS_ENABLED ? $points : 0),
		   'coupon_code' => $coupon_code_hidden,
		   'coupon_value' => $coupon_value_hidden
		);

		$this->db->insert('ordini', $data);

		// id_ordine
		$id_ordine_new = $this->db->insert_id();
		
		log_message('info','**** Set order id: ' .$id_ordine_new. ' ***');
		
		// disattiva coupon
		if($coupon_code_hidden != '') {
			log_message('info','**** Start Set coupon code: ' .$coupon_code_hidden. ' ***');
			if($this->session->userdata('validated_coupon_type') <= 2) {
				$data_update_coupon = array(
				   'stato_coupon' => 3,
				   'utilizzatore_coupon' => $co_email
				);
				$this->db->where('codice_coupon', $coupon_code_hidden);
				$this->db->update('coupon', $data_update_coupon);
				$this->session->unset_userdata('validated_coupon');
			}
			$this->session->unset_userdata('validated_coupon');
			$this->session->unset_userdata('validated_coupon_value');
			$this->session->unset_userdata('validated_coupon_perc');
			$this->session->unset_userdata('validated_coupon_type');
			log_message('info','**** End Set coupon code: ' .$coupon_code_hidden. ' ***');
		}
		
		if(POINTS_ENABLED) {
			// *** aggiungi punti all'utente se loggato
			if($this->ion_auth->logged_in()) {
				$this->db->set('punti', 'punti + ' . (int) $points, FALSE);
				$this->db->where('id_cliente', $id_cliente);
				$this->db->update('clienti'); 
			}
		}
		
		//*** inserisci informazione cliente in storico_clienti
		$this->db->select('*');
		$this->db->from('clienti');
		$this->db->join('indirizzo_fatturazione', 'clienti.id_indirizzo_fatturazione = indirizzo_fatturazione.id_indirizzo_fatturazione');
		$this->db->where('clienti.id_cliente', $id_cliente);
		$query_cliente = $this->db->get();
		$cliente = $query_cliente->row();
		
		$indirizzo_fatt_storico = $cliente->riferimento_fatt . ' | ' . $cliente->indirizzo_fatt . ', ' . $cliente->civico_fatt . ' - ' . $cliente->cap_fatt . ' ' . $cliente->citta_fatt. ' (' . $cliente->nazione_fatt . ')';
		
		$data_cliente_storico = array(
		   'id_storico_clienti' => NULL,
		   'id_ordine' => $id_ordine_new,
		   'nome' => $cliente->nome,
		   'cognome' => $cliente->cognome,
		   'email' => $cliente->email,
		   'telefono' => $cliente->telefono,
		   'indirizzo_fatturazione' => $indirizzo_fatt_storico ,
		   'indirizzo_spedizione' => ($indirizzo_spedizione_storico != '' ? $indirizzo_spedizione_storico : $indirizzo_fatt_storico),
		   'note_indirizzo_spedizione' => ($note_spedizione_storico != '' ? $note_spedizione_storico : $cliente->note_fatt),
		   'note_indirizzo_fatturazione' => $cliente->note_fatt,
		   'partita_iva' => $cliente->partita_iva,
		   'codice_fiscale' => $cliente->codice_fiscale,
		   'id_lingua' => $cliente->id_lingua
		);
		
		$this->db->insert('storico_clienti', $data_cliente_storico);
		
		//*** inserisci in storico_carrello da carrello per utente loggato oppure no
		$this->db->select('*');
		$this->db->from('carrello');
		if(!$this->ion_auth->logged_in()) {
			$this->db->where('carrello.id_sessione_utente', $this->input->cookie('uuid', TRUE));
		} else {
			$this->db->where('carrello.id_cliente', $id_cliente);
		}
		
		$query_carrello = $this->db->get();
		$carrello = $query_carrello->result();
		
		foreach($carrello as $item) {
			
			$this->db->select('varianti_prodotti.*, prodotti.nome AS nome, tipo_prodotto.descrizione_tipo_prodotto AS descrizione_tipo_prodotto');
			$this->db->from('varianti_prodotti');
			$this->db->join('prodotti', 'prodotti.codice = varianti_prodotti.codice_prodotto');
			$this->db->join('tipo_prodotto', 'prodotti.id_tipo_prodotto = tipo_prodotto.id_tipo_prodotto');	
			$this->db->where('varianti_prodotti.id_variante', $item->id_variante);
			$query_carrello_storico = $this->db->get();
			$prodotto_storico = $query_carrello_storico->row();
			
			// storico_carrello
			$data = array(
			   'id_storico_carrello' => NULL,
			   'id_ordine' => $id_ordine_new,
			   'id_variante' => $item->id_variante,
			   'id_prodotto' => $item->id_prodotto,
			   'data_storicizzazione' => date('Y-m-d H:i:s'),
			   'qty' => $item->qty,
			   'taglia' => $item->taglia,
			   'tipo_prodotto' => $prodotto_storico->descrizione_tipo_prodotto,
			   'codice_prodotto' => $prodotto_storico->codice_prodotto,
			   'codice_variante' => $prodotto_storico->codice,
			   'nome' => $prodotto_storico->nome,
			   'prezzo' => $prodotto_storico->prezzo,
			   'prezzo_scontato' => $prodotto_storico->prezzo_scontato,
			   'url_immagine' => $prodotto_storico->url_img_grande,
			   'colore_prodotto' => $prodotto_storico->colore,
			   'colore_prodotto_codice' => $prodotto_storico->colore_codice
			);
	
			$this->db->insert('storico_carrello', $data);
			
			// copia immagine del prodotto in folder storico ASSETS_ROOT_FOLDER_FRONTEND_IMG
			copy('assets/assets-frontend/img/shop/'.$prodotto_storico->url_img_grande, 'assets/assets-frontend/img/shop/storico/'.$prodotto_storico->url_img_grande);
			
		}
		
		//*** cancella da carrello temporaneo
		if(!$this->ion_auth->logged_in()) {
			$this->db->where('id_sessione_utente', $user_uuid);
		} else {
			$this->db->where('id_cliente', $id_cliente);
		}
		$this->db->delete('carrello');
		
		//*** pagamento Stripe
		/*
		$error_add_payment = '';
		if($token_id_hidden != '')
		{
			if($token_id_hidden != 'paypal')
			{
				log_message('info','**** Start STRIPE paymnet ***');
				try {
					Stripe::setApiKey(STRIPE_SK);
					$charge = Stripe_Charge::create(array(
						"amount" => $co_total_order * 100,
						"currency" => "EUR",
						"card" => $token_id_hidden,
						"description" => "Ordine #".$id_ordine_new." - ".$co_l_name." ".$co_f_name
					));
					$co_payment_status = 1;
					// update ordine
					$data_update = array(
					   'stato_pagamento' => 1
					);
					$this->db->where('id_ordine', $id_ordine_new);
					$this->db->update('ordini', $data_update);
				} catch (Exception $e) {
					// Something else happened, completely unrelated to Stripe
					//echo json_encode(array('status' => 500, 'error' => STRIPE_FAILED));
				//	echo lang('MSG_ORDER_PAYMENT_ERROR') . ' #'.$id_ordine_new.' - '.$co_l_name. ' ' .$co_f_name;
					//*** messaggio success per la pagina 
					//$this->data['title'] = lang('LABEL_ORDER').' #'.$id_ordine_new;
					//$this->data['message'] =  lang('MSG_ORDER_PAYMENT_ERROR') . ' #'.$id_ordine_new.' - '.$co_l_name. ' ' .$co_f_name;
				    log_message('error', lang('MSG_ORDER_PAYMENT_ERROR') . ' | ' . $e);
					$error_add_payment = lang('MSG_ORDER_PAYMENT_ERROR');
					//$this->data['type'] = 'error';
					//echo json_encode($this->data);
				//	exit();
				}
				log_message('info','**** End STRIPE Payment ***');
			}
			else
			{
				// PAYPAL payment was made
				$co_payment_status = 1;
				// update ordine
				$data_update = array(
				   'stato_pagamento' => 1
				);
				$this->db->where('id_ordine', $id_ordine_new);
				$this->db->update('ordini', $data_update);
				log_message('info','**** Start set PAYPAL Payment ***');
			}
		}
		*/
		
		// Invia email di riepilogo
		$this->send_email_order_invoice($id_ordine_new, true, false);
		log_message('info','**** Send email invoice ***');
		
		//*** messaggio success per la pagina 
		$this->data['title'] = lang('LABEL_ORDER').' #'.$id_ordine_new;
		$this->data['message'] = lang('MSG_ORDER_SUCCESS').($this->ion_auth->logged_in() ? '<br><a href="'.site_url(lang('PAGE_ACCOUNT_URL')).'">'.lang('LABEL_YOUR_ACCOUNT').'</a>' : ''); // @TODO SE UTENTE LOGGATO MOSTRA LINK PER ANDARE NEI TUOI ORDINI A VEDERLO
		$this->data['type'] = 'success';
		echo json_encode($this->data);
				
	}
	
	// valida e verifica il coupon sconto sul db e lo inserisce in sessione
	public function validateCoupon() {
		$coupon_code = $_POST['coupon_code'];
		$total_cart = $_POST['total_cart'];
		
		if($coupon_code == '' || $total_cart <= 0) {
			$this->session->unset_userdata('validated_coupon');
			$this->session->unset_userdata('validated_coupon_value');
			$this->session->unset_userdata('validated_coupon_perc');
			$this->session->unset_userdata('validated_coupon_type');
			$this->data['type'] = 'cleared';
		} else {
			$this->db->select('*');
			$this->db->from('coupon');		
			$this->db->where('codice_coupon', strtoupper($coupon_code));
			$this->db->where('stato_coupon', 1);
			$this->db->where('data_scadenza_coupon >=', date('Y-m-d'));
			$query_coupon = $this->db->get();
			$coupon = $query_coupon->row();
			//print_r($this->db->last_query());
			$this->data['title'] = lang('LABEL_COUPON').' '.$coupon_code;
			if(isset($coupon) && $coupon != NULL) {	
				// valida il coupon per importo non superiore al carrello
				if($total_cart < $coupon->importo_coupon) {
					$this->session->unset_userdata('validated_coupon');
					$this->session->unset_userdata('validated_coupon_value');
					$this->session->unset_userdata('validated_coupon_perc');
					$this->session->unset_userdata('validated_coupon_type');
					$this->data['type'] = 'error';
					$this->data['message'] =  lang('MSG_COUPON_INVALID_OVER') . ' ('.$coupon_code.')'; 
				} else { 
					$this->session->set_userdata('validated_coupon', $coupon->codice_coupon);
					$this->session->set_userdata('validated_coupon_value', $coupon->importo_coupon);
					$this->session->set_userdata('validated_coupon_perc', $coupon->percentuale_coupon);
					$this->session->set_userdata('validated_coupon_type', $coupon->tipo_coupon);
					$this->data['type'] = 'success';
					$this->data['message'] =  '';
				}
			} else {
				$this->session->unset_userdata('validated_coupon');
				$this->session->unset_userdata('validated_coupon_value');
				$this->session->unset_userdata('validated_coupon_perc');
				$this->session->unset_userdata('validated_coupon_type');
				$this->data['type'] = 'error';
				$this->data['message'] =  lang('MSG_COUPON_INVALID') . ' ('.$coupon_code.')';
			}
		}
		echo json_encode($this->data);
	}
	
}
