<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends Frontend_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}

	// print ajax list of available products
	public function getProducts()
	{
		$category_url = $_POST['category_url'];

		$load_step = 6;
		//$load_from = (isset($_POST['load_from']) ? (int)$_POST['load_from'] + $load_step : 0);
		$load_from = (isset($_POST['load_from']) ? $_POST['load_from'] : 0);
		$load_to = $load_from + $load_step;

		$num_results = $this->countAllRows($category_url);
		$more_count = $num_results - $load_from - $load_step;
		
	//	echo $num_results . ' more: ' . $more_count;
		
		// retrun load from step
		echo '<input type="hidden" id="load_from" data-morecount="'.$more_count.'" value="'. (($num_results - $load_from - $load_step) > 0 ? $load_from + $load_step : 0) . '" />';

		$this->db->select('prodotti.*, tipo_prodotto.*, prodotti_traduzioni.*');

		if($category_url != '')
			$this->db->where('categorie.url_categorie', $category_url);
			
		$this->db->where('prodotti.stato', 1);
		$this->db->where('categorie.stato', 1);
		$this->db->where('categorie_traduzioni.id_lingua', lang('LANGUAGE_ID'));
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->from('prodotti');
		$this->db->join('tipo_prodotto', 'prodotti.id_tipo_prodotto = tipo_prodotto.id_tipo_prodotto');
		$this->db->join('prodotti_categorie', 'prodotti_categorie.id_prodotto = prodotti.id_prodotti');
		$this->db->join('categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie');
		$this->db->join('categorie_traduzioni', 'categorie_traduzioni.id_categoria = categorie.id_categorie', 'LEFT');
		$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
		$this->db->group_by("prodotti.id_prodotti");
		$this->db->order_by("prodotti.ordine", 'desc');

		// limit load more
		if($load_from > 0)
			$this->db->limit($load_step, $load_from);
		else
			$this->db->limit($load_step);

		$query = $this->db->get();

		// print query
		//print_r($this->db->last_query());
	
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$this->getItemDiv($row);
			}
		} else {
			echo '<div class="col-md-12 col-sm-12" align="center"><b>'.lang('MSG_NO_RESULT_FILTER') .'</b></div>';
		}
	}

	public function countAllRows($category_url)
	{
		if($category_url != '')
			$this->db->where('categorie.url_categorie', $category_url);

		$this->db->where('prodotti.stato', 1);
		$this->db->where('categorie.stato', 1);
		$this->db->where('categorie_traduzioni.id_lingua', lang('LANGUAGE_ID'));
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->from('prodotti');
		$this->db->join('tipo_prodotto', 'prodotti.id_tipo_prodotto = tipo_prodotto.id_tipo_prodotto');
		$this->db->join('prodotti_categorie', 'prodotti_categorie.id_prodotto = prodotti.id_prodotti');
		$this->db->join('categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie');
		$this->db->join('categorie_traduzioni', 'categorie_traduzioni.id_categoria = categorie.id_categorie', 'LEFT');
		$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
		$this->db->group_by("prodotti.id_prodotti");
		$this->db->order_by("prodotti.ordine", 'desc');

		$query = $this->db->get();
		return $query->num_rows();
	}

	// print ajax list of available products
	public function getFilterCategories()
	{
		$category_url = $_POST['category_url'];

		// default TUTTI distinct sulla tabella delle categorie_prodotti
		$this->db->select('COUNT(DISTINCT prodotti.id_prodotti) AS numrows', false);
		$this->db->where('prodotti.stato', 1);
		$this->db->where('categorie.stato', 1);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->where('categorie_traduzioni.id_lingua', lang('LANGUAGE_ID'));
		$this->db->from('prodotti');
		$this->db->join('prodotti_categorie', 'prodotti_categorie.id_prodotto = prodotti.id_prodotti');
		$this->db->join('categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie');
		$this->db->join('categorie_traduzioni', 'categorie_traduzioni.id_categoria = categorie.id_categorie', 'LEFT');
		$this->db->join('prodotti_traduzioni', 'prodotti_traduzioni.id_prodotti = prodotti.id_prodotti');
		$queryCount = $this->db->get();
		
		echo '<li class="filter-category-li active"><a href="#" class="filter-category" data-category="">'.lang("LABEL_ALL").'</a> <sup>'.$queryCount->row('numrows').'</sup></li>';
		//print_r($this->db->last_query());
		// other categories
		$this->db->select('categorie.*, COUNT(prodotti.id_prodotti) AS num_prod, categorie_traduzioni.nome_categoria_trad AS nome_categoria_trad');
		$this->db->from('categorie');
		$this->db->join('categorie_traduzioni', 'categorie_traduzioni.id_categoria = categorie.id_categorie', 'LEFT');
		$this->db->join('prodotti_categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie');
		$this->db->join('prodotti', 'prodotti.id_prodotti = prodotti_categorie.id_prodotto');
		$this->db->join('prodotti_traduzioni', 'prodotti_traduzioni.id_prodotti = prodotti.id_prodotti');
		$this->db->where('categorie.stato', 1);
		$this->db->where('prodotti.stato', 1);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->where('categorie_traduzioni.id_lingua', lang('LANGUAGE_ID'));
		$this->db->group_by("categorie.id_categorie");
		$this->db->order_by("categorie.ordine", "ASC");
		$this->db->having('num_prod >', 0);
		
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			if($row->num_prod > 0)
				echo '<li class="filter-category-li"><a href="#" class="filter-category" data-category="'.$row->url_categorie.'">'.$row->nome_categoria_trad.'</a> <sup>'.$row->num_prod.'</sup></li>';	
		}
	}
	
	// print ajax list of available products organized in categories for index page
	public function getProductsCategories()
	{
		$this->db->select('categorie.*, colori_classi.*, COUNT(prodotti.id_prodotti) AS num_prod, COUNT(prodotti_traduzioni.id_prodotti_traduzioni) AS num_trad, categorie_traduzioni.nome_categoria_trad AS nome_categoria_trad');
		$this->db->from('categorie');
		$this->db->join('categorie_traduzioni', 'categorie_traduzioni.id_categoria = categorie.id_categorie', 'LEFT');
		$this->db->join('prodotti_categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie', 'LEFT');
		$this->db->join('prodotti', 'prodotti.id_prodotti = prodotti_categorie.id_prodotto', 'LEFT');
		$this->db->join('colori_classi', 'colori_classi.id_colore_classe = categorie.label_color_class', 'LEFT');
		$this->db->join('prodotti_traduzioni', 'prodotti_traduzioni.id_prodotti = prodotti.id_prodotti', 'LEFT');
		$this->db->where('categorie.stato', 1);
		$this->db->where('prodotti.stato', 1);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->where('categorie_traduzioni.id_lingua', lang('LANGUAGE_ID'));
		$this->db->group_by("categorie.id_categorie");
		$this->db->order_by("categorie.ordine", "ASC");
		$this->db->having('num_prod >', 0);
		$this->db->having('num_trad >', 0);

		$query = $this->db->get();
		$num_rows = $query->num_rows();
		
		// default classes
		$cat_class_sm = "col-sm-6";
		$cat_class_md = "col-md-4";

		// calculate bootstrap class dinamically
		if($num_rows == 2) {
			$cat_class_sm = "col-sm-6";
			$cat_class_md = "col-md-6";
		} else if($num_rows == 3) {
			$cat_class_sm = "col-sm-4";
			$cat_class_md = "col-md-4";
		} else if($num_rows == 4) {
			$cat_class_sm = "col-sm-6";
			$cat_class_md = "col-md-3";
		} else if($num_rows == 6) {
			$cat_class_sm = "col-sm-6";
			$cat_class_md = "col-md-4";
		} else if($num_rows == 8) {
			$cat_class_sm = "col-sm-6";
			$cat_class_md = "col-md-3";
		}

		// print query
		//print_r($this->db->last_query());
		echo '<h3 align="center">'.stripslashes(lang("HOME_CATEGORY_TITLE")).'</h3>';
		foreach ($query->result() as $row)
		{
			//echo $row->nome . ' - ' . $row->immagine;
		    // '.$row->colore_classe.'
			echo '<!-- Item -->
			<div class="'.$cat_class_md.' '.$cat_class_sm.'">
			  <div class="shop-item">
				<div class="shop-thumbnail">
					<span class="shop-label-overlay">'.$row->nome_categoria_trad.'</span>
					<a href="'.base_url().lang('PAGE_SHOP_URL').'/'.$row->url_categorie.'" class="item-link"></a>
					<img src="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/categories/'.$row->immagine.'" alt="'.SITE_TITLE_NAME.' | '.$row->nome.'">
				</div>
			  </div><!-- .shop-item -->
			</div><!-- .'.$cat_class_md.'.'.$cat_class_sm.' -->';
		}

	}
	// print ajax list of available tags
	public function getFilterTags()
	{
		// header
		echo '<h3 class="widget-title">'. lang("LABEL_TAGS") .'</h3>';
		$this->db->select('*');
		$this->db->from('tags');
		$this->db->join('tags_prodotti', 'tags_prodotti.id_tag = tags.id_tag');
		$this->db->group_by("tags.id_tag");
		$query = $this->db->get();

		foreach ($query->result() as $row)
		{
			echo '<a href="#" class="tag-a" data-id="'.$row->id_tag.'">'.$row->nome_tag.'</a>';
		}
	}
	/*
		Restituisce la stampa HTML di un prodotto
		css_class: top-rated | text-danger | text-warning | empty ''
	*/
	private function getItemDiv($product)
	{	
		echo '<!-- Item -->
			<div class="col-md-4 col-sm-6">
			  <div class="shop-item">
				<div class="shop-thumbnail preloadimg" title="'.$product->descrizione.'" >';

		if($product->css_class != '') {
			if($product->css_class != 'top-rated') {
			    echo '<span class="shop-label-overlay '.$product->css_class.'">'.$product->nome.'</span>';
			} else {
				// TODO gestire il rating system in base ai commenti degli utenti ?
				echo '<span class="item-rating text-top-rated">
						<i class="material-icons star"></i>
						<i class="material-icons star"></i>
						<i class="material-icons star"></i>
						<i class="material-icons star"></i>
						<i class="material-icons star_border"></i>
					  </span>';
			}
		}
			
		// retrieve available colors
	/*	$this->db->select('colore, colore_codice, prezzo, prezzo_scontato');
		$this->db->from('varianti_prodotti');
		$this->db->where('codice_prodotto', $product->codice);
		$this->db->where('varianti_prodotti.stato', 1);
		$this->db->order_by('prezzo', "ASC"); 
		$query_colors = $this->db->get();
		
		$num_colors = $query_colors->num_rows();
		$found_colors = array();
		$min_price = 0;
		if($num_colors > 0) {
			foreach ($query_colors->result() as $color) {
				// verifica prezzo min
				$var_price_tomin = ($color->prezzo_scontato > 0 ? $color->prezzo_scontato : $color->prezzo);
				$min_price = (($var_price_tomin < $min_price || $min_price == 0) ? $var_price_tomin : $min_price);
				if(!in_array($color->colore_codice, $found_colors)) {
					array_push($found_colors, $color->colore_codice);
				}
			}
		}
		
		if(count($found_colors) > 1) {
			echo '<span class="colors-label"><ul>';
				foreach ($found_colors as $colorCode){
					echo '<li><i class="material-icons fiber_manual_record" title="'.$colorCode.'" style="color:#'.$colorCode.';"></i></li>';
				}
			echo '</ul></span>';
		}
		
		/*
			<a href="#" class="add-to-whishlist" data-toggle="tooltip" data-id="'.$product->id_prodotti.'" data-placement="top" title="'.lang("LABEL_ADD_TO_WHISH").'">
				  <i class="material-icons favorite_border"></i>
				</a>
		*/
		$detail_link = 	base_url() . lang('PAGE_PRODUCTS_URL') . '/' . $product->codice . '/' . cleanString($product->descrizione_breve . (isset($product->colore) ? ' ' . $product->colore : ''), true);
		echo '<a href="'.$detail_link.'" class="item-link"></a>
			  <img src="'.ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/shop/'.$product->url_immagine.'" alt="'.SITE_TITLE_NAME.' | '.$product->nome.' - '.$product->descrizione_breve.'">
			  <div class="shop-item-tools">
				<a href="'.$detail_link.'" class="add-to-cart show-detail mobile-btn-sm" data-id="'.$product->id_prodotti.'" title="'.lang("LABEL_DETAIL").'">
				  <em><i class="material-icons zoom_in" style="font-size: 22px;"></i> <span class="mobile-hide">'.lang("LABEL_DETAIL").'</span></em>
				  <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
					<path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
				  </svg>
				</a>
			  </div>
			</div>
		  </div><!-- .shop-item -->
		</div><!-- .col-md-4.col-sm-6 -->';
	}
	
	// print detail of single product from SEO URL by prodotti->codice
	public function detailcode($prod_code, $variant_code = NULL)
	{
		/* Recupero il prodotto e le sue varianti */
		$this->db->select('p.*, tipo_prodotto.*, prodotti_traduzioni.*');
		$this->db->where('p.stato', 1);
		$this->db->where('p.codice', $prod_code);
		$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
		$this->db->join('tipo_prodotto', 'p.id_tipo_prodotto = tipo_prodotto.id_tipo_prodotto');
		$this->db->join('prodotti_traduzioni', 'p.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
		$this->db->from('prodotti as p');
		$query = $this->db->get();
		
		$product = $query->row();
		
		if (isset($product)) {
			
			// prodotti figli
	/*		$this->db->select('*');
			$this->db->from('varianti_prodotti');
			$this->db->where('codice_prodotto', $product->codice);
			$this->db->where('stato', 1);
			$this->db->order_by('varianti_prodotti.id_variante, varianti_prodotti.colore');  
			$query_child = $this->db->get();
			//print_r($this->db->last_query());			
			$models = array();
			$def_curr_id = 0;
			$curr_model = '';
			$curr_child = NULL;
			$sizesModel = array();
			$childCountTotal = count($query_child->result());
			$countChild = 1;
			$modelImgFront = '';
			$modelImgBack = '';
			
			foreach ($query_child->result() as $rowChild) {
				
				// impsota immagini comuni per il modello
				if($modelImgFront == '' && $rowChild->url_img_grande != '')
					$modelImgFront = $rowChild->url_img_grande;
				if($modelImgBack == '' && $rowChild->url_img_grande_retro != '')
					$modelImgBack = $rowChild->url_img_grande_retro;	
				
				// imposta id default corrente
				if($def_curr_id == 0)
					$def_curr_id = $rowChild->codice;
					
				if($curr_model == '') {
					// found single product
					if($countChild == $childCountTotal){
						$curr_model = $rowChild->colore;
						$curr_child = $rowChild;
						$sizesModel[$rowChild->taglia] = $rowChild->codice.';'.$rowChild->id_variante;
						$arrayC = array(
							"child" => $curr_child,
							"sizes" => $sizesModel,
							"modelImgFront" => $modelImgFront,
							"modelImgBack" => $modelImgBack
						);
						$models[$curr_model] = $arrayC;
					} else {
						$curr_model = $rowChild->colore;
						$curr_child = $rowChild;
						$sizesModel[$rowChild->taglia] = $rowChild->codice.';'.$rowChild->id_variante;
						//array_push($sizesModel, $rowChild->taglia);	
					}
				} else if($curr_model != $rowChild->colore) {
					// aggiungi a models e azzera
					$arrayC = array(
						"child" => $curr_child,
						"sizes" => $sizesModel,
						"modelImgFront" => $modelImgFront,
						"modelImgBack" => $modelImgBack
					);
					$models[$curr_model] = $arrayC;
					$sizesModel = array();
					$modelImgFront = '';
					$modelImgBack = '';
					$curr_model = $rowChild->colore;
					$curr_child = $rowChild;
					$sizesModel[$rowChild->taglia] = $rowChild->codice.';'.$rowChild->id_variante;
					//array_push($sizesModel, $rowChild->taglia);	
				} else if($countChild == $childCountTotal) { 
					$curr_child = $rowChild;
					$sizesModel[$rowChild->taglia] = $rowChild->codice.';'.$rowChild->id_variante;
					$arrayC = array(
						"child" => $curr_child,
						"sizes" => $sizesModel,
						"modelImgFront" => $modelImgFront,
						"modelImgBack" => $modelImgBack
					);
					$models[$curr_model] = $arrayC;
				} else {
					$curr_child = $rowChild;
					$sizesModel[$rowChild->taglia] = $rowChild->codice.';'.$rowChild->id_variante;
				}
				$countChild++;
			}
			
			$curr_id = (isset($variant_code) && $variant_code != NULL ? $variant_code : '');
			$data['child_list'] = $models;*/
			
			// prev
			$this->db->select('*');
			$this->db->from('prodotti');
			$this->db->where('prodotti.id_prodotti <', $product->id_prodotti);
			$this->db->where('stato', 1);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
			$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
			$this->db->order_by('prodotti.id_prodotti', "DESC"); 
			$this->db->limit(1);
			$query_prev = $this->db->get();
			$prev = $query_prev->row();
			$data['prev'] = $prev;
			
			// next
			$this->db->select('*');
			$this->db->from('prodotti');
			$this->db->where('prodotti.id_prodotti >', $product->id_prodotti);
			$this->db->where('stato', 1);
			$this->db->where('prodotti_traduzioni.lingua_traduzione_id', lang('LANGUAGE_ID'));
			$this->db->join('prodotti_traduzioni', 'prodotti.id_prodotti = prodotti_traduzioni.id_prodotti', "LEFT");
			$this->db->order_by('prodotti.id_prodotti', "ASC"); 
			$this->db->limit(1);
			$query_next = $this->db->get();
			$next = $query_next->row();
			$data['next'] = $next;
			
			// categorie
			$this->db->select('*');
			$this->db->from('prodotti_categorie');
			$this->db->join('categorie', 'prodotti_categorie.id_categoria = categorie.id_categorie');
			$this->db->join('categorie_traduzioni', 'categorie_traduzioni.id_categoria = categorie.id_categorie', 'LEFT');
			$this->db->where('categorie.stato', 1);
			$this->db->where('prodotti_categorie.id_prodotto', $product->id_prodotti);
			$this->db->where('categorie_traduzioni.id_lingua', lang('LANGUAGE_ID'));
			$query_cat = $this->db->get();
			$data['categories'] = $query_cat->result();
			
			// tags
		//	$this->db->select('*');
		//	$this->db->from('tags');
		//	$this->db->join('tags_prodotti', 'tags_prodotti.id_tag = tags.id_tag');
		//	$this->db->where('tags_prodotti.id_prodotto =', $product->id_prodotti);
		//	$query_tags = $this->db->get();
			
			// set view data
		//	$data['tags_count'] = $query_tags->num_rows();
		//	$data['tags'] = $query_tags;
			
			$data['prod'] = $product;
	//		$data['var_curr_id'] = $curr_id;
			$data['prod_curr_code'] = $product->codice;
			$data['prod_page_title'] = $product->nome;
			$data['prod_page_meta_desc'] = $product->descrizione_breve;
			$this->show_view_with_menu('frontend/detail', $data);
			
		} else {
			// se il prodotto richiesto in dettaglio non esiste
			redirect(site_url(lang('PAGE_SHOP_URL')), 'refresh');
		}	
			
		
	}
}

/* End of file Products.php */
/* Location: ./application/controllers/frontend/fe_home.php */
