function showConfirmDialog(titleCofirm, textConfirm, confirmButtonText, cancelButtonText, titleSuccess, textSuccess, titleDismiss, textDismiss, redirectSuccessUrl, redirectErrorUrl, showSuccess) {
	swal({
		title: (titleCofirm != '' ? titleCofirm : 'Conferma eliminazione'),
		text: (textConfirm != '' ? textConfirm : 'Sei sicuro di voler eliminare questo elemento?'),
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: (confirmButtonText != '' ? confirmButtonText : 'Si, procedi!'),
		cancelButtonText: (cancelButtonText != '' ? cancelButtonText : 'No, fermati'),
		confirmButtonClass: "btn btn-success",
		cancelButtonClass: "btn btn-danger",
		buttonsStyling: false
	}).then(function() {
		if(showSuccess != '' && showSuccess == 'true') {
		  swal({
			title: (titleSuccess != '' ? titleSuccess : 'Eliminati!'),
			text: (textSuccess != '' ? textSuccess : 'I tuoi dati sono stati cancellati con successo.'),
			type: 'success',
			confirmButtonClass: "btn btn-success",
			buttonsStyling: false
			});
		}
		if(redirectSuccessUrl != '') 
			window.location = redirectSuccessUrl;
	}, function(dismiss) {
	  // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
	  if (dismiss === 'cancel') {
		swal({
		  title: (titleDismiss != '' ? titleDismiss : 'Operazione ignorata'),
		  text: (textDismiss != '' ? textDismiss : 'Non è stata effettuata nessuna operazione.'),
		  type: 'error',
		  confirmButtonClass: "btn btn-info",
		  buttonsStyling: false
		});
	  }
	});
	return false;
}