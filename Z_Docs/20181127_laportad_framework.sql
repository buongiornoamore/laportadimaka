-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Nov 27, 2018 alle 15:53
-- Versione del server: 10.1.33-MariaDB
-- Versione PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laportad_framework`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie`
--

CREATE TABLE `categorie` (
  `id_categorie` int(11) NOT NULL,
  `url_categorie` varchar(25) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `descrizione` varchar(100) DEFAULT NULL,
  `immagine` varchar(150) NOT NULL,
  `stato` int(11) NOT NULL,
  `ordine` int(11) NOT NULL DEFAULT '1',
  `label_color_class` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `url_categorie`, `nome`, `descrizione`, `immagine`, `stato`, `ordine`, `label_color_class`) VALUES
(1, 'laboratori', 'Laboratori', 'I nostri laboratori', 'aa142-laboratori.png', 1, 1, 3),
(2, 'face-painting', 'Face painting', 'Il mondo del face painting', '7cd72-fb_img_1540297100037.jpg', 1, 2, 2),
(3, 'feste-a-tema', 'Feste a tema', 'Feste a tema', '270ee-img_20181117_162658.jpg', 1, 3, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_gallery`
--

CREATE TABLE `categorie_gallery` (
  `id_categoria_gallery` int(11) NOT NULL,
  `nome_categoria_gallery` varchar(250) NOT NULL,
  `stato_categoria_gallery` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_gallery`
--

INSERT INTO `categorie_gallery` (`id_categoria_gallery`, `nome_categoria_gallery`, `stato_categoria_gallery`) VALUES
(1, 'MaKa world', 1),
(2, 'Su di noi', 1),
(3, 'Coming soon', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_gallery_traduzioni`
--

CREATE TABLE `categorie_gallery_traduzioni` (
  `id_categorie_gallery_traduzioni` int(11) NOT NULL,
  `id_categoria_gallery` int(11) NOT NULL,
  `descrizione_categoria_gallery` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_gallery_traduzioni`
--

INSERT INTO `categorie_gallery_traduzioni` (`id_categorie_gallery_traduzioni`, `id_categoria_gallery`, `descrizione_categoria_gallery`, `id_lingua`) VALUES
(1, 1, 'MaKa world', 2),
(2, 1, 'MaKa world', 1),
(3, 2, 'Su di noi', 1),
(4, 2, 'About us', 2),
(5, 3, 'Novità', 1),
(6, 3, 'Coming soon', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_traduzioni`
--

CREATE TABLE `categorie_traduzioni` (
  `id_categoria_traduzioni` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `nome_categoria_trad` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_traduzioni`
--

INSERT INTO `categorie_traduzioni` (`id_categoria_traduzioni`, `id_categoria`, `nome_categoria_trad`, `id_lingua`) VALUES
(1, 1, 'Laboratori', 1),
(2, 1, 'Labs', 2),
(3, 2, 'Face painting', 1),
(4, 2, 'Face painting', 2),
(5, 3, 'Feste a tema', 1),
(6, 3, 'Theme party', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `colori_classi`
--

CREATE TABLE `colori_classi` (
  `id_colore_classe` int(11) NOT NULL,
  `colore_classe` varchar(100) NOT NULL,
  `colore_classe_nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `colori_classi`
--

INSERT INTO `colori_classi` (`id_colore_classe`, `colore_classe`, `colore_classe_nome`) VALUES
(1, 'text-danger', 'Rosso'),
(2, 'text-success', 'Verde'),
(3, 'text-warning', 'Arancio');

-- --------------------------------------------------------

--
-- Struttura della tabella `constants_framework`
--

CREATE TABLE `constants_framework` (
  `id_cf` int(11) NOT NULL,
  `cf_name` varchar(100) NOT NULL,
  `cf_value` varchar(250) NOT NULL,
  `cf_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `constants_framework`
--

INSERT INTO `constants_framework` (`id_cf`, `cf_name`, `cf_value`, `cf_description`) VALUES
(1, 'SITE_URL_PATH', 'https://www.laportadimaka.com', 'Indirizzo online del sito'),
(2, 'SITE_TITLE_NAME', 'La porta di MaKa', 'Titolo del sito online'),
(3, 'SMTP_HOST_CUSTOM', 'mail.laportadimaka.com', 'Indirizzo del server SMTP'),
(4, 'SMTP_USER_CUSTOM', 'info@laportadimaka.com', 'Utente del server SMTP'),
(5, 'SMTP_PASS_CUSTOM', 'info18@Maka', 'Password del server SMTP'),
(6, 'DB_HOSTNAME', '37.60.232.114', 'Indirizzo del DB'),
(7, 'DB_USERNAME', 'laportad', 'Utente del DB'),
(8, 'DB_PASSWORD', 'maka18@Cpanel', 'Password del DB'),
(9, 'DB_DATABASE', 'laportad_framework', 'Nome del DB'),
(10, 'COMPANY_NAME', 'La porta di MaKa', 'Nome della società/ditta'),
(11, 'COMPANY_EMAIL', 'info@laportadimaka.com', 'Email della società/ditta'),
(13, 'COMPANY_PHONE', ' +39 3286105475 | +39 3391037223', 'Telefono società/ditta'),
(14, 'COMPANY_ADDRESS', 'Roma', 'Indirizzo società/ditta'),
(15, 'GPLUS_LINK', '', 'Link social G+'),
(16, 'YOUTUBE_LINK', '', 'Link social YOUTUBE'),
(17, 'PINTEREST_LINK', '', 'Link social PINTEREST'),
(18, 'TWITTER_LINK', '', 'Link social TWITTER'),
(19, 'FACEBOOK_LINK', 'https://www.facebook.com/laportadimaka/', 'Link social FACEBOOK'),
(20, 'INSTAGRAM_LINK', 'https://www.instagram.com/la_porta_di_maka/', 'Link social INSTAGRAM'),
(21, 'GOOGLE_ANALITYCS_ID', '', 'UID Google Analitycs'),
(23, 'COMNINGSOON_LOGO', 'logo_medium.png', 'File logo del COMING SOON'),
(24, 'COMNINGSOON_BACK_IMAGE', 'default.jpg', 'File immagine di sfondo Coming soon'),
(25, 'COMNINGSOON_BTN_COLOR', 'EE2D20', 'Colore del pulsante Coming soon'),
(34, 'SMARTSUPP_ID', '370f8baf5d6ce1e020e8de6e8f803908056dbfe1', 'Chiave (ID KEY) per il collegamento a Smartsupp Chat');

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_moduli`
--

CREATE TABLE `contatti_moduli` (
  `id_contatto` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `messaggio` text NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_moduli`
--

INSERT INTO `contatti_moduli` (`id_contatto`, `nome`, `email`, `telefono`, `messaggio`, `data`, `stato_contatto`, `data_unsubscribe`, `id_lingua`) VALUES
(1, 'Roberto Rossi', 'roberto.rossi77@gmail.com', '0', 'Ciao prova di un contatto', '2018-11-26 14:03:15', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_newsletter`
--

CREATE TABLE `contatti_newsletter` (
  `id_contatto_newsletter` int(11) NOT NULL,
  `email_contatto` varchar(250) NOT NULL,
  `data_contatto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_newsletter`
--

INSERT INTO `contatti_newsletter` (`id_contatto_newsletter`, `email_contatto`, `data_contatto`, `stato_contatto`, `data_unsubscribe`, `lingua_traduzione_id`) VALUES
(1, 'roberto.rossi77@gmail.com', '2018-11-26 14:04:11', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `email_templates`
--

CREATE TABLE `email_templates` (
  `id_template` int(11) NOT NULL,
  `nome_template` varchar(250) NOT NULL,
  `subject_template` varchar(250) NOT NULL,
  `titolo_template` varchar(250) NOT NULL,
  `testo_template` text NOT NULL,
  `id_tipo_template` int(11) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `stato` varchar(15) NOT NULL DEFAULT 'ATTIVO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `stato`) VALUES
(0, 'super_admin', 'Super Amministratore', 'HIDDEN'),
(1, 'admin', 'Amministratore del portale', 'ATTIVO'),
(2, 'members', 'Utente ospite (GUEST)', 'ATTIVO');

-- --------------------------------------------------------

--
-- Struttura della tabella `home_slider`
--

CREATE TABLE `home_slider` (
  `hs_id` int(11) NOT NULL,
  `hs_image` varchar(250) NOT NULL,
  `hs_order` int(11) NOT NULL,
  `hs_title` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `home_slider`
--

INSERT INTO `home_slider` (`hs_id`, `hs_image`, `hs_order`, `hs_title`, `id_lingua`) VALUES
(1, '01.jpg', 1, 'Apri la porta<br> ed entra nel mondo di<br><span style=\"color:lightgreen\">Ma</span><span style=\"color:red\">Ka</span>', 1),
(2, '01_en.jpg', 1, 'Open the door and<br>enter the world of<br><span style=\"color:lightgreen\">Ma</span><span style=\"color:red\">Ka</span>', 2),
(3, '02.jpg', 2, 'PIGNATTE', 1),
(4, '02_en.jpg', 2, 'PARTY POTS', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `immagini_gallery`
--

CREATE TABLE `immagini_gallery` (
  `id_ig` int(11) NOT NULL,
  `id_categoria_ig` int(11) NOT NULL,
  `nome_ig` varchar(250) NOT NULL,
  `stato_ig` tinyint(4) NOT NULL,
  `immagine_ig` varchar(250) NOT NULL,
  `ordine_ig` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `immagini_gallery`
--

INSERT INTO `immagini_gallery` (`id_ig`, `id_categoria_ig`, `nome_ig`, `stato_ig`, `immagine_ig`, `ordine_ig`) VALUES
(1, 3, 'La maga Nana', 1, '2ef0d-gallery_maga.jpg', 1),
(2, 1, 'Animatori in azione', 1, 'ac75c-img-20180609-wa0028.jpg', 2),
(3, 1, 'Set face painting', 1, '3b922-fb_img_1540296954324.jpg', 4),
(4, 2, 'Marty e Katy', 1, '5ac02-about.jpeg', 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `immagini_gallery_traduzioni`
--

CREATE TABLE `immagini_gallery_traduzioni` (
  `id_immagini_gallery_traduzioni` int(11) NOT NULL,
  `titolo_ig` varchar(250) NOT NULL,
  `testo_ig` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `id_ig` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `immagini_gallery_traduzioni`
--

INSERT INTO `immagini_gallery_traduzioni` (`id_immagini_gallery_traduzioni`, `titolo_ig`, `testo_ig`, `id_lingua`, `id_ig`) VALUES
(1, 'La Maga Nana', 'Lo spettacolo di magia', 1, 1),
(2, 'The Nana magician', 'The our special magician show', 2, 1),
(3, 'Set face painting', 'Set face painting', 1, 3),
(4, 'Face painting set', 'Face painting set', 2, 3),
(5, 'Animatori in azione', 'Animatori in azione', 1, 2),
(6, 'Team in action', 'Team in action', 2, 2),
(7, 'Marty e Katy', 'Marty e Katy', 1, 4),
(8, 'Marty and Katy', 'Marty and Katy', 2, 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue`
--

CREATE TABLE `lingue` (
  `id_lingue` int(11) NOT NULL,
  `nome_lingue` varchar(50) NOT NULL,
  `abbr_lingue` varchar(10) NOT NULL,
  `locale_paypal_lingue` varchar(10) NOT NULL,
  `codice_ci` varchar(150) NOT NULL,
  `stato_lingua` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue`
--

INSERT INTO `lingue` (`id_lingue`, `nome_lingue`, `abbr_lingue`, `locale_paypal_lingue`, `codice_ci`, `stato_lingua`) VALUES
(1, 'ITALIANO', 'IT', 'it_IT', 'italian', 1),
(2, 'ENGLISH', 'EN', 'en_GB', 'english', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue_labels_lang`
--

CREATE TABLE `lingue_labels_lang` (
  `id_lingue_labels_lang` int(11) NOT NULL,
  `lingue_labels_lang_label` varchar(250) NOT NULL,
  `lingue_labels_lang_value` text NOT NULL,
  `lingue_labels_lang_desc` varchar(250) NOT NULL,
  `lingue_labels_lang_type` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue_labels_lang`
--

INSERT INTO `lingue_labels_lang` (`id_lingue_labels_lang`, `lingue_labels_lang_label`, `lingue_labels_lang_value`, `lingue_labels_lang_desc`, `lingue_labels_lang_type`, `id_lingua`) VALUES
(1, 'HOME_CATEGORY_TITLE', 'Esplora il mondo di MaKa', 'Intestazione sezione categorie home page', 'frontend', 1),
(2, 'HOME_SHOW_ALL_PRODUCTS', 'Entra nel nostro mondo', 'Testo pulsante collegamento ai prodotti Home Page', 'frontend', 1),
(3, 'HOME_FEATURE_1_TITLE', 'La tua festa originale', 'Titolo index nr 1', 'frontend', 1),
(4, 'HOME_FEATURE_1_DESC', 'Organizziamo la tua festa speciale', 'Descrizione index nr 1', 'frontend', 1),
(5, 'HOME_FEATURE_2_TITLE', 'Laboratori creativi', 'Titolo index nr 2', 'frontend', 1),
(6, 'HOME_FEATURE_2_DESC', 'Giochiamo e sperimentiamo', 'Descrizione index nr 2', 'frontend', 1),
(7, 'HOME_FEATURE_3_TITLE', 'Feste a tema', 'Titolo index nr 3', 'frontend', 1),
(8, 'HOME_FEATURE_3_DESC', 'Scegli il tuo tema preferito', 'Descrizione index nr 3', 'frontend', 1),
(9, 'HOME_FEATURE_4_TITLE', 'Team professionale', 'Titolo index nr 4', 'frontend', 1),
(10, 'HOME_FEATURE_4_DESC', 'Staff giovane e preparato', 'Descrizione index nr 4', 'frontend', 1),
(11, 'FOOTER_HELP', 'Hai bisogno di aiuto? Contattaci', 'Testo footer help sinistra', 'frontend', 1),
(12, 'FOOTER_NEWSLTTER_TITLE', 'NEWSLETTER', 'NEWSLETTER', 'frontend', 1),
(13, 'FOOTER_NEWSLTTER_DESC', 'Ricevi la nosta newsletter', 'Testo footer newsletter', 'frontend', 1),
(14, 'FOOTER_NEWSLTTER_INPUT', 'Inserisci la tua e-mail', 'Etichetta campo newsletter Footer', 'frontend', 1),
(17, 'LABEL_FILTER', 'Filtra', 'Filtra', 'frontend', 1),
(18, 'LABEL_SEARCH', 'Cerca', 'Cerca', 'frontend', 1),
(19, 'LABEL_ORDER', 'Ordina per', 'Ordina per', 'frontend', 1),
(20, 'LABEL_DEFAULT', 'Default', 'Default', 'frontend', 1),
(21, 'LABEL_SALE', 'Offerta', 'Offerta', 'frontend', 1),
(22, 'LABEL_NEW', 'Novità', 'Novità', 'frontend', 1),
(23, 'LABEL_FEEDBACK', 'Voto medio', 'Voto medio', 'frontend', 1),
(24, 'LABEL_BEST', 'Best seller', 'Best seller', 'frontend', 1),
(25, 'LABEL_ALPHA', 'Alfabetico', 'Alfabetico', 'frontend', 1),
(26, 'LABEL_PRICE', 'Prezzo', 'Prezzo', 'frontend', 1),
(27, 'LABEL_TAGS', 'Tags', 'Tags', 'frontend', 1),
(28, 'LABEL_SEE_MORE', 'Vedi altri', 'Vedi altri', 'frontend', 1),
(29, 'LABEL_ADD_TO_CART', 'Aggiungi', 'Aggiungi', 'frontend', 1),
(30, 'LABEL_ADD_TO_WHISH', 'Aggiungi alla Whishlist', 'Aggiungi alla Whishlist', 'frontend', 1),
(31, 'LABEL_ALL', 'Tutti', 'Tutti', 'frontend', 1),
(32, 'LABEL_MY_ACCOUNT', 'Il mio profilo', 'Il mio profilo', 'frontend', 1),
(33, 'LABEL_USER_ACCOUNT', 'Account utente', 'Account utente', 'frontend', 1),
(34, 'LABEL_USER_REGISTER', 'Registrazione', 'Registrazione', 'frontend', 1),
(35, 'LABEL_USER_SIGN_UP', 'Iscriviti', 'Iscriviti', 'frontend', 1),
(36, 'LABEL_USER_LOGIN', 'Effettua il Login', 'Effettua il Login', 'frontend', 1),
(37, 'LABEL_UPDATE', 'Salva', 'Salva', 'frontend', 1),
(38, 'LABEL_GOT', 'Hai', 'Hai', 'frontend', 1),
(39, 'LABEL_POINTS', 'punti', 'punti', 'frontend', 1),
(40, 'LABEL_POINTS_DESC', 'Puoi utilizzare i tuoi punti per acquistare prodotti dal nostro shop o accedere alle promozioni speciali.', 'Puoi utilizzare i tuoi punti per acquistare prodotti dal nostro shop o accedere alle promozioni speciali.', 'frontend', 1),
(41, 'LABEL_MANAGE_POINTS', 'Gestisci i tuoi punti', 'Gestisci i tuoi punti', 'frontend', 1),
(42, 'LABEL_BACK_SHOP', 'Torna allo Shop', 'Torna allo Shop', 'frontend', 1),
(43, 'LABEL_LOGOUT', 'Logout', 'Logout', 'frontend', 1),
(44, 'LABEL_PROFILE', 'Profilo', 'Profilo', 'frontend', 1),
(45, 'LABEL_ORDERS', 'Ordini', 'Ordini', 'frontend', 1),
(46, 'LABEL_ADDRESSES', 'Indirizzi di spedizione', 'Indirizzi di spedizione', 'frontend', 1),
(47, 'LABEL_WHISHLIST', 'Whishlist', 'Whishlist', 'frontend', 1),
(48, 'LABEL_NAME', 'Nome', 'Nome', 'frontend', 1),
(49, 'LABEL_SURNAME', 'Cognome', 'Cognome', 'frontend', 1),
(50, 'LABEL_EMAIL', 'Email', 'Email', 'frontend', 1),
(51, 'LABEL_PHONE', 'Telefono', 'Telefono', 'frontend', 1),
(52, 'LABEL_COUNTRY', 'Stato/Paese', 'Stato/Paese', 'frontend', 1),
(53, 'LABEL_CITY', 'Città', 'Città', 'frontend', 1),
(54, 'LABEL_ADDRESS', 'Indirizzo', 'Indirizzo', 'frontend', 1),
(55, 'LABEL_ADDRESS_REF', 'Riferimento spedizione c/o (es Mario Rossi)', 'Riferimento spedizione c/o (es Mario Rossi)', 'frontend', 1),
(56, 'LABEL_ADDRESS_REF_FATT', 'Riferimento fatturazione (persona o azienda)', 'Riferimento fatturazione (persona o azienda)', 'frontend', 1),
(57, 'LABEL_CIVICO', 'Civico', 'Civico', 'frontend', 1),
(58, 'LABEL_POSTAL_CODE', 'CAP', 'CAP', 'frontend', 1),
(59, 'LABEL_ADDRESS_NOTES', 'Note indirizzo', 'Note indirizzo', 'frontend', 1),
(60, 'LABEL_ORDER_NOTES', 'Note ordine', 'Note ordine', 'frontend', 1),
(61, 'LABEL_TOTAL_ORDER', 'Totale ordine', 'Totale ordine', 'frontend', 1),
(62, 'LABEL_TOTAL', 'Totale', 'Totale', 'frontend', 1),
(63, 'LABEL_QTY', 'Quantità', 'Quantità', 'frontend', 1),
(64, 'LABEL_SUBTOTAL_ORDER', 'Subtotale', 'Subtotale', 'frontend', 1),
(65, 'LABEL_TOTAL_CART', 'Totale carrello', 'Totale carrello', 'frontend', 1),
(66, 'LABEL_TOTAL_ORDER_NOTES', '* Note: il totale include i costi di spedizione', '* Note: il totale include i costi di spedizione', 'frontend', 1),
(67, 'LABEL_TOTAL_CART_NOTES', '* Note: il totale non include eventuali spese di spedizione. I costi di spedizione o consegna verranno calcolati nel checkout successivo', '* Note: il totale non include eventuali spese di spedizione. I costi di spedizione o consegna verranno calcolati nel checkout successivo', 'frontend', 1),
(68, 'LABEL_CONFIRM', 'CONFERMA', 'CONFERMA', 'frontend', 1),
(69, 'LABEL_REMOVE', 'ELIMINA', 'ELIMINA', 'frontend', 1),
(70, 'LABEL_BACK_TO_CART', 'RITORNA AL CARRELLO', 'RITORNA AL CARRELLO', 'frontend', 1),
(71, 'LABEL_UPDATE_CART', 'AGGIORNA IL CARRELLO', 'AGGIORNA IL CARRELLO', 'frontend', 1),
(72, 'LABEL_BACK_TO_SHOP', 'TORNA ALLO SHOP', 'TORNA ALLO SHOP', 'frontend', 1),
(73, 'LABEL_CART', 'Carrello', 'Carrello', 'frontend', 1),
(74, 'LABEL_CART_EMPTY', 'Il tuo carrello è vuoto !', 'Il tuo carrello è vuoto !', 'frontend', 1),
(75, 'LABEL_CHECKOUT', 'Checkout', 'Checkout', 'frontend', 1),
(76, 'LABEL_PAYMENT_METHOD', 'Modalità di pagamento', 'Modalità di pagamento', 'frontend', 1),
(77, 'LABEL_PAYMENT_METHOD_PAYPAL', 'Paypal', 'Paypal', 'frontend', 1),
(78, 'LABEL_PAYMENT_METHOD_CC', 'Carta di credito / Prepagata', 'Carta di credito / Prepagata', 'frontend', 1),
(79, 'LABEL_MESSAGE', 'Messaggio', 'Messaggio', 'frontend', 1),
(80, 'LABEL_SEND', 'INVIA', 'INVIA', 'frontend', 1),
(81, 'LABEL_PRODUCTS', 'prodotti', 'prodotti', 'frontend', 1),
(82, 'LABEL_CART_INFO_ACTUALLY_1', 'Attualmente ci sono', 'Attualmente ci sono', 'frontend', 1),
(83, 'LABEL_CART_INFO_ACTUALLY_2', 'nel carrello', 'nel carrello', 'frontend', 1),
(84, 'LABEL_SIZE', 'Tg', 'Tg', 'frontend', 1),
(85, 'LABEL_COLOR', 'Colore', 'Colore', 'frontend', 1),
(86, 'LABEL_CATEGORY', 'Categoria', 'Categoria', 'frontend', 1),
(87, 'LABEL_DESCRIPTION', 'Descrizione', 'Descrizione', 'frontend', 1),
(88, 'LABEL_REVIEWS', 'Commenti', 'Commenti', 'frontend', 1),
(89, 'LABEL_ALSO_LIKE', 'Ti potrebbero piacere', 'Ti potrebbero piacere', 'frontend', 1),
(90, 'LABEL_REFCODE', 'Rif', 'Rif', 'frontend', 1),
(91, 'LABEL_DETAIL', 'Dettaglio', 'Dettaglio', 'frontend', 1),
(92, 'LABEL_SHOPPING_CART', 'Carrello', 'Carrello', 'frontend', 1),
(93, 'LABEL_FRONT', 'FRONTE', 'FRONTE', 'frontend', 1),
(94, 'LABEL_BACK', 'RETRO', 'RETRO', 'frontend', 1),
(95, 'LABEL_AVAILABILITY', 'Disponibilità', 'Disponibilità', 'frontend', 1),
(96, 'LABEL_AVAILABILITY_HIGH', 'Alta', 'Alta', 'frontend', 1),
(97, 'LABEL_AVAILABILITY_LOW', 'Bassa', 'Bassa', 'frontend', 1),
(98, 'LABEL_404_MESSAGE', 'Oops.... la pagina richiesta non esiste !', 'Oops.... la pagina richiesta non esiste !', 'frontend', 1),
(99, 'LABEL_404_BTN', 'TORNA ALLA HOME', 'TORNA ALLA HOME', 'frontend', 1),
(100, 'LABEL_ORDER', 'Ordine', 'Ordine', 'frontend', 1),
(101, 'LABEL_YOUR_ACCOUNT', 'Il tuo account', 'Il tuo account', 'frontend', 1),
(102, 'LABEL_USER_ALREADY_REGISTERED', 'Sei già registrato ?', 'Sei già registrato ?', 'frontend', 1),
(103, 'LABEL_USER_NOTREGISTERED', 'Altrimenti puoi ordinare compilando i dati', 'Altrimenti puoi ordinare compilando i dati', 'frontend', 1),
(104, 'LABEL_USER_NOTREGISTERED_POINTS', 'non accumulerai punti e bonus riservati ai clienti registrati', 'non accumulerai punti e bonus riservati ai clienti registrati', 'frontend', 1),
(105, 'LABEL_COUPON', 'COUPON', 'COUPON', 'frontend', 1),
(106, 'LABEL_COUPON_HAVE', 'Hai un codice sconto?', 'Hai un codice sconto?', 'frontend', 1),
(107, 'LABEL_COUPON_INSERT', 'Inserisci il codice coupon', 'Inserisci il codice coupon', 'frontend', 1),
(108, 'LABEL_COUPON_APPLY', 'Applica coupon', 'Applica coupon', 'frontend', 1),
(109, 'LABEL_DISCOUNT', 'Sconto', 'Sconto', 'frontend', 1),
(110, 'LABEL_SHIPPING_THIS', 'Spedisci a questo indirizzo', 'Spedisci a questo indirizzo', 'frontend', 1),
(111, 'LABEL_SHIPPING_OTHER', 'Spedisci ad un altro indirizzo', 'Spedisci ad un altro indirizzo', 'frontend', 1),
(112, 'LABEL_NEW_ADDRESS', 'Nuovo indirizzo', 'Nuovo indirizzo', 'frontend', 1),
(113, 'LABEL_BILLING_ADDRESS', 'Indirizzo di fatturazione', 'Indirizzo di fatturazione', 'frontend', 1),
(114, 'LABEL_SHIPPING_ADDRESS', 'Indirizzo di spedizione', 'Indirizzo di spedizione', 'frontend', 1),
(115, 'LABEL_SHIPPING', 'Spedizione', 'Spedizione', 'frontend', 1),
(116, 'LABEL_CHANGE_PASSWORD', 'Cambio Password', 'Cambio Password', 'frontend', 1),
(117, 'LABEL_ORDER_DATE', 'Data ordine', 'Data ordine', 'frontend', 1),
(118, 'LABEL_STRIPE_DESC', 'Paga in tutta sicurezza con Stripe', 'Paga in tutta sicurezza con Stripe', 'frontend', 1),
(119, 'MSG_SEARCH_INSERT', 'Inserisci il testo da ricercare', 'Inserisci il testo da ricercare', 'frontend', 1),
(120, 'MSG_SAVE_NEWSLETTER', 'Iscrivimi alla newsletter', 'Iscrivimi alla newsletter', 'frontend', 1),
(121, 'MSG_SEND_CONTACT_US', 'Richiesta di contatto inviata con successo!<br/>Grazie.', 'Richiesta di contatto inviata con successo!<br/>Grazie.', 'frontend', 1),
(122, 'MSG_PAYPAL_NOTETOPAYER', 'L\'indirizzo di spedizione resterà quello inserito nel modulo di checkout e non quello indicato nel pagamento PayPal !', 'L\'indirizzo di spedizione resterà quello inserito nel modulo di checkout e non quello indicato nel pagamento PayPal !', 'frontend', 1),
(123, 'MSG_PAYPAL_CANCEL', 'Riprova ad effettuare il pagamemto !', 'Riprova ad effettuare il pagamemto !', 'frontend', 1),
(124, 'MSG_PAYPAL_ERROR', 'Errore durante il pagamento:', 'Errore durante il pagamento:', 'frontend', 1),
(125, 'MSG_NO_RESULT', 'Nessun risultato per ', 'Nessun risultato per ', 'frontend', 1),
(126, 'MSG_NO_RESULT_FILTER', 'Nessun risultato presente per i filtri selezionati.', 'Nessun risultato presente per i filtri selezionati.', 'frontend', 1),
(127, 'MSG_NO_SIZE_FOR_PRODUCTS_COLOR', 'Al momento non ci sono taglie disponibili per questo prodotto/colore', 'Al momento non ci sono taglie disponibili per questo prodotto/colore', 'frontend', 1),
(128, 'MSG_ORDER_SUCCESS', 'inserito con successo ! Grazie.', 'inserito con successo ! Grazie.', 'frontend', 1),
(129, 'MSG_ORDER_PAYMENT_ERROR', 'Errore durante il pagamento ordine. Per favore verifica il tuo ordine e riprova.', 'Errore durante il pagamento ordine. Per favore verifica il tuo ordine e riprova.', 'frontend', 1),
(130, 'MSG_SUCCESS_CONTACT', 'Il tuo messaggio è stato inviato correttamente. Grazie !', 'Il tuo messaggio è stato inviato correttamente. Grazie !', 'frontend', 1),
(131, 'MSG_FAILURE_CONTACT', 'Abbiamo riscontrato un problema nell\'invio del mesaggio. Riprova!', 'Abbiamo riscontrato un problema nell\'invio del mesaggio. Riprova!', 'frontend', 1),
(132, 'MSG_SUCCESS_NEWSLETTER', 'Adesso sei iscritto alla newsletter. Grazie !', 'Adesso sei iscritto alla newsletter. Grazie !', 'frontend', 1),
(133, 'MSG_UNIQUE_NEWSLETTER', 'L\'indirizzo email è già iscritto alla newsletter', 'L\'indirizzo email è già iscritto alla newsletter', 'frontend', 1),
(134, 'MSG_UNSUBSCRIBE_DONE', 'La tua email/iscrizione è stata rimossa. Grazie!', 'La tua email/iscrizione è stata rimossa. Grazie!', 'frontend', 1),
(135, 'MSG_UNSUBSCRIBE_NOTFOUND', 'Il contatto richiesto non è attualmente registrato', 'Il contatto richiesto non è attualmente registrato', 'frontend', 1),
(136, 'MSG_CART_REMOVED', 'Prodotto rimosso dal carrello', 'Prodotto rimosso dal carrello', 'frontend', 1),
(137, 'MSG_CART_ADDED', 'Prodotto inserito nel carrello', 'Prodotto inserito nel carrello', 'frontend', 1),
(138, 'MSG_CART_UPDATED', 'Prodotto aggiornato nel carrello', 'Prodotto aggiornato nel carrello', 'frontend', 1),
(139, 'MSG_SERVICE_FAILURE', 'Si è verificato un errore. Riprova!', 'Si è verificato un errore. Riprova!', 'frontend', 1),
(140, 'MSG_BILLING_ADDRESS_NECESSARY', 'L\'indirizzo di fatturazione è obbligatorio ai fini del processo di acquisto! <br/>Sei sicuro di voler uscire?<br/>Ti sarà comunque chiesto nella fase di acquisto!', 'L\'indirizzo di fatturazione è obbligatorio ai fini del processo di acquisto! <br/>Sei sicuro di voler uscire?<br/>Ti sarà comunque chiesto nella fase di acquisto!', 'frontend', 1),
(141, 'MSG_ALTERNATE_ADDRESS_LOGGED', 'inserisci un nuovo indirizzo o selezionane uno già presente nella lista', 'inserisci un nuovo indirizzo o selezionane uno già presente nella lista', 'frontend', 1),
(142, 'MSG_ALTERNATE_ADDRESS_NOTLOGGED', 'inserisci un nuovo indirizzo per la spedizione', 'inserisci un nuovo indirizzo per la spedizione', 'frontend', 1),
(143, 'MSG_COUPON_INVALID', 'Coupon non valido o scaduto', 'Coupon non valido o scaduto', 'frontend', 1),
(144, 'MSG_COUPON_INVALID_OVER', 'Il valore del Coupon inserito è maggiore del carrello!', 'Il valore del Coupon inserito è maggiore del carrello!', 'frontend', 1),
(145, 'LABEL_TP_SALE', 'Offerta', 'Offerta', 'frontend', 1),
(146, 'LABEL_TP_BESTSELLER', 'Più venduti', 'Più venduti', 'frontend', 1),
(147, 'LABEL_TP_TOPRATED', 'Più votati', 'Più votati', 'frontend', 1),
(148, 'LABEL_TP_STANDARD', 'Standard', 'Standard', 'frontend', 1),
(149, 'Standard', 'Nuovo', 'Nuovo', 'frontend', 1),
(150, 'SEND_AREYOUSURE_BTN', 'SEI SICURO ?', 'SEI SICURO ?', 'frontend', 1),
(151, 'LABEL_UNSUBSCRIBE', 'Cancellati', 'Cancellati', 'frontend', 1),
(152, 'LABEL_MY_ACCOUNT_EMAIL', 'Il mio profilo', 'Il mio profilo', 'email', 1),
(153, 'LABEL_SEE_EMAIL_ONLINE', 'Vedi email online', 'Vedi email online', 'email', 1),
(154, 'LABEL_EMAIL_SALES_TITLE', 'Controlla le nostre ultime offerte!', 'Ultime offerte', 'email', 1),
(155, 'LABEL_DETAIL_EMAIL', 'Vedi i dettagli', 'Vedi i dettagli', 'email', 1),
(156, 'TEXT_EMAIL_FOOTER_USUBSCRIBE', 'cancellati', 'Cancellati footer', 'email', 1),
(157, 'TEXT_EMAIL_FOOTER_RESERVED', 'Tutti i diritti riservati', 'Tutti i diritti riservati', 'email', 1),
(158, 'TEXT_EMAIL_FOOTER_COPYRIGHT', 'Se non vuoi più ricevere queste email per favore', 'Non ricevere più email', 'email', 1),
(159, 'LABEL_EMAIL_SUBJECT_CONTACT', 'Contatto dal sito', 'Contatto dal sito', 'email', 1),
(160, 'LABEL_EMAIL_CONTACT_TITLE', 'Ti ringraziamo per averci contattato!', 'Ti ringraziamo per il contatto', 'email', 1),
(161, 'LABEL_EMAIL_CONTACT_TEXT', 'Abbiamo ricevuto la tua comunicazione e ti invieremo una risposta nel minor tempo possibile. Grazie!', 'Comunicazione ricevuta', 'email', 1),
(162, 'LABEL_EMAIL_SUBJECT_NEWSLETTER', 'Iscrizione alla Newsletter', 'Iscrizione alla Newsletter', 'email', 1),
(163, 'LABEL_EMAIL_NEWSLETTER_TITLE', 'Grazie per esserti iscritto alla nostra newsletter!', 'Grazie newsletter', 'email', 1),
(164, 'LABEL_EMAIL_NEWSLETTER_TEXT', 'Riceverai gli aggiornamenti, le news e le nostre offerte esclusive per restare in contatto con il nostro mondo.', 'Riceverai gli aggiornamenti', 'email', 1),
(165, 'LABEL_EMAIL_SUBJECT_WELCOME', 'Benvenuto su Ma Chlò', 'Benvenuto su', 'email', 1),
(166, 'LABEL_EMAIL_WELCOME_TITLE', 'Benvenuto su Ma Chlò!', 'Benvenuto su TITOLO', 'email', 1),
(167, 'LABEL_EMAIL_WELCOME_TEXT', 'Grazie per esserti registrato! Speriamo che ti piaccia il nostro lavoro. Consulta alcune delle nostre ultime offerte qui sotto o clicca sul pulsante per visualizzare il tuo nuovo account.', 'Grazie per esserti registrato', 'email', 1),
(168, 'LABEL_EMAIL_SUBJECT_NEW_ORDER', 'Nuovo ordine', 'Nuovo ordine', 'email', 1),
(169, 'LABEL_TITLE_INVOICE', 'Riepilogo ordine', 'Riepilogo ordine', 'email', 1),
(170, 'LABEL_INVOICE_THANKS', 'Ti ringraziamo per il tuo ordine!', 'Ti ringraziamo per il tuo ordine!', 'email', 1),
(171, 'LABEL_INVOICE_THANKS_TEXT', 'Ti faremo sapere non appena i tuoi articoli saranno spediti.<br>Per cambiare o visualizzare il tuo ordine, ti preghiamo di visualizzare il tuo account facendo clic sul pulsante qui sotto.', 'Ti faremo sapere USER', 'email', 1),
(172, 'LABEL_INVOICE_THANKS_TEXT_NOUSER', 'Ti faremo sapere non appena i tuoi articoli saranno spediti.', 'Ti faremo sapere NO USER', 'email', 1),
(173, 'LABEL_SHIPPING_ADDRESS_EMAIL', 'Indirizzo di spedizione', 'Indirizzo di spedizione', 'email', 1),
(174, 'LABEL_ORDER_EMAIL', 'Ordine', 'Ordine', 'email', 1),
(175, 'LABEL_ORDER_DATE_EMAIL', 'Data ordine', 'Data ordine', 'email', 1),
(176, 'LABEL_ADDRESS_NOTES_EMAIL', 'Note indirizzo', 'Note indirizzo', 'email', 1),
(177, 'LABEL_ORDER_NOTES_EMAIL', 'Note ordine', 'Note ordine', 'email', 1),
(178, 'LABEL_DESCRIPTION_EMAIL', 'Descrizione', 'Descrizione', 'email', 1),
(179, 'LABEL_QTY_EMAIL', 'Quantità', 'Quantità', 'email', 1),
(180, 'LABEL_SIZE_EMAIL', 'Taglia', 'Taglia', 'email', 1),
(181, 'LABEL_COLOR_EMAIL', 'Colore', 'Colore', 'email', 1),
(182, 'LABEL_SUBTOTAL_ORDER_EMAIL', 'Subtotale', 'Subtotale', 'email', 1),
(183, 'LABEL_COUPON_APPLY_EMAIL', 'Applica coupon', 'Applica coupon', 'email', 1),
(184, 'LABEL_SHIPPING_EMAIL', 'Spedizione', 'Spedizione', 'email', 1),
(185, 'LABEL_TOTAL_EMAIL', 'Totale', 'Totale', 'email', 1),
(186, 'HOME_CATEGORY_TITLE', 'Discover MaKa World', 'Intestazione sezione categorie home page', 'frontend', 2),
(187, 'HOME_SHOW_ALL_PRODUCTS', 'Enter inside our world', 'Testo pulsante collegamento ai prodotti Home Page', 'frontend', 2),
(188, 'HOME_FEATURE_1_TITLE', 'You best party', 'Titolo index nr 1', 'frontend', 2),
(189, 'HOME_FEATURE_1_DESC', 'We make your special party', 'Descrizione index nr 1', 'frontend', 2),
(190, 'HOME_FEATURE_2_TITLE', 'Creative workshops', 'Titolo index nr 2', 'frontend', 2),
(191, 'HOME_FEATURE_2_DESC', 'Let\'s play, create and experiment', 'Descrizione index nr 2', 'frontend', 2),
(192, 'HOME_FEATURE_3_TITLE', 'Theme party', 'Titolo index nr 3', 'frontend', 2),
(193, 'HOME_FEATURE_3_DESC', 'Choose your favorite theme', 'Descrizione index nr 3', 'frontend', 2),
(194, 'HOME_FEATURE_4_TITLE', 'Team skilled', 'Titolo index nr 4', 'frontend', 2),
(195, 'HOME_FEATURE_4_DESC', 'Young and prepared staff', 'Descrizione index nr 4', 'frontend', 2),
(196, 'FOOTER_HELP', 'Do you need help? Contact us', 'Testo footer help sinistra', 'frontend', 2),
(197, 'FOOTER_NEWSLTTER_TITLE', 'NEWSLETTER', 'NEWSLETTER', 'frontend', 2),
(198, 'FOOTER_NEWSLTTER_DESC', 'Subscribe to our newsletter', 'Testo footer newsletter', 'frontend', 2),
(199, 'FOOTER_NEWSLTTER_INPUT', 'Your e-mail', 'Etichetta campo newsletter Footer', 'frontend', 2),
(202, 'LABEL_FILTER', 'Filter', 'Filtra', 'frontend', 2),
(203, 'LABEL_SEARCH', 'Search', 'Cerca', 'frontend', 2),
(204, 'LABEL_ORDER', 'Order by', 'Ordina per', 'frontend', 2),
(205, 'LABEL_DEFAULT', 'Default', 'Default', 'frontend', 2),
(206, 'LABEL_SALE', 'Sale', 'Offerta', 'frontend', 2),
(207, 'LABEL_NEW', 'New', 'Novità', 'frontend', 2),
(208, 'LABEL_FEEDBACK', 'Feedback', 'Voto medio', 'frontend', 2),
(209, 'LABEL_BEST', 'Best seller', 'Best seller', 'frontend', 2),
(210, 'LABEL_ALPHA', 'Alphabetical', 'Alfabetico', 'frontend', 2),
(211, 'LABEL_PRICE', 'Price', 'Prezzo', 'frontend', 2),
(212, 'LABEL_TAGS', 'Tags', 'Tags', 'frontend', 2),
(213, 'LABEL_SEE_MORE', 'See more', 'Vedi altri', 'frontend', 2),
(214, 'LABEL_ADD_TO_CART', 'Add to cart', 'Aggiungi', 'frontend', 2),
(215, 'LABEL_ADD_TO_WHISH', 'Add to Whishlist', 'Aggiungi alla Whishlist', 'frontend', 2),
(216, 'LABEL_ALL', 'All', 'Tutti', 'frontend', 2),
(217, 'LABEL_MY_ACCOUNT', 'My account', 'Il mio profilo', 'frontend', 2),
(218, 'LABEL_USER_ACCOUNT', 'User account', 'Account utente', 'frontend', 2),
(219, 'LABEL_USER_REGISTER', 'Register', 'Registrazione', 'frontend', 2),
(220, 'LABEL_USER_SIGN_UP', 'Sign up', 'Iscriviti', 'frontend', 2),
(221, 'LABEL_USER_LOGIN', 'Please Login', 'Effettua il Login', 'frontend', 2),
(222, 'LABEL_UPDATE', 'Update', 'Salva', 'frontend', 2),
(223, 'LABEL_GOT', 'You have', 'Hai', 'frontend', 2),
(224, 'LABEL_POINTS', 'points', 'punti', 'frontend', 2),
(225, 'LABEL_POINTS_DESC', 'You can use your points to receive special offers and have dedicated discounts.', 'Puoi utilizzare i tuoi punti per acquistare prodotti dal nostro shop o accedere alle promozioni speciali.', 'frontend', 2),
(226, 'LABEL_MANAGE_POINTS', 'Manage your points', 'Gestisci i tuoi punti', 'frontend', 2),
(227, 'LABEL_BACK_SHOP', 'Back to Shop', 'Torna allo Shop', 'frontend', 2),
(228, 'LABEL_LOGOUT', 'Logout', 'Logout', 'frontend', 2),
(229, 'LABEL_PROFILE', 'Profile', 'Profilo', 'frontend', 2),
(230, 'LABEL_ORDERS', 'Orders', 'Ordini', 'frontend', 2),
(231, 'LABEL_ADDRESSES', 'Shipping addresses', 'Indirizzi di spedizione', 'frontend', 2),
(232, 'LABEL_WHISHLIST', 'Whishlist', 'Whishlist', 'frontend', 2),
(233, 'LABEL_NAME', 'Name', 'Nome', 'frontend', 2),
(234, 'LABEL_SURNAME', 'Surname', 'Cognome', 'frontend', 2),
(235, 'LABEL_EMAIL', 'Email', 'Email', 'frontend', 2),
(236, 'LABEL_PHONE', 'Phone', 'Telefono', 'frontend', 2),
(237, 'LABEL_COUNTRY', 'Country/State', 'Stato/Paese', 'frontend', 2),
(238, 'LABEL_CITY', 'City', 'Città', 'frontend', 2),
(239, 'LABEL_ADDRESS', 'Address', 'Indirizzo', 'frontend', 2),
(240, 'LABEL_ADDRESS_REF', 'Reference shipping c/o (ex Tom Brad)', 'Riferimento spedizione c/o (es Mario Rossi)', 'frontend', 2),
(241, 'LABEL_ADDRESS_REF_FATT', 'Reference invoice (person or company)', 'Riferimento fatturazione (persona o azienda)', 'frontend', 2),
(242, 'LABEL_CIVICO', 'Number', 'Civico', 'frontend', 2),
(243, 'LABEL_POSTAL_CODE', 'Postal code', 'CAP', 'frontend', 2),
(244, 'LABEL_ADDRESS_NOTES', 'Address notes', 'Note indirizzo', 'frontend', 2),
(245, 'LABEL_ORDER_NOTES', 'Order notes', 'Note ordine', 'frontend', 2),
(246, 'LABEL_TOTAL_ORDER', 'Total order', 'Totale ordine', 'frontend', 2),
(247, 'LABEL_TOTAL', 'Total', 'Totale', 'frontend', 2),
(248, 'LABEL_QTY', 'Qty', 'Quantità', 'frontend', 2),
(249, 'LABEL_SUBTOTAL_ORDER', 'Subtotal', 'Subtotale', 'frontend', 2),
(250, 'LABEL_TOTAL_CART', 'Total cart', 'Totale carrello', 'frontend', 2),
(251, 'LABEL_TOTAL_ORDER_NOTES', '* Notes: this total include the shipping cost', '* Note: il totale include i costi di spedizione', 'frontend', 2),
(252, 'LABEL_TOTAL_CART_NOTES', '* Notes: the total does not include any shipping charges. Shipping or delivery costs will be calculated in the next checkout', '* Note: il totale non include eventuali spese di spedizione. I costi di spedizione o consegna verranno calcolati nel checkout successivo', 'frontend', 2),
(253, 'LABEL_CONFIRM', 'CONFIRM', 'CONFERMA', 'frontend', 2),
(254, 'LABEL_REMOVE', 'REMOVE', 'ELIMINA', 'frontend', 2),
(255, 'LABEL_BACK_TO_CART', 'BACK TO CART', 'RITORNA AL CARRELLO', 'frontend', 2),
(256, 'LABEL_UPDATE_CART', 'UPDATE CART', 'AGGIORNA IL CARRELLO', 'frontend', 2),
(257, 'LABEL_BACK_TO_SHOP', 'BACK TO SHOP', 'TORNA ALLO SHOP', 'frontend', 2),
(258, 'LABEL_CART', 'Cart', 'Carrello', 'frontend', 2),
(259, 'LABEL_CART_EMPTY', 'Your cart is empty !', 'Il tuo carrello è vuoto !', 'frontend', 2),
(260, 'LABEL_CHECKOUT', 'Checkout', 'Checkout', 'frontend', 2),
(261, 'LABEL_PAYMENT_METHOD', 'Payment method', 'Modalità di pagamento', 'frontend', 2),
(262, 'LABEL_PAYMENT_METHOD_PAYPAL', 'Paypal', 'Paypal', 'frontend', 2),
(263, 'LABEL_PAYMENT_METHOD_CC', 'Credit card / Prepaid card', 'Carta di credito / Prepagata', 'frontend', 2),
(264, 'LABEL_MESSAGE', 'Message', 'Messaggio', 'frontend', 2),
(265, 'LABEL_SEND', 'SEND', 'INVIA', 'frontend', 2),
(266, 'LABEL_PRODUCTS', 'products', 'prodotti', 'frontend', 2),
(267, 'LABEL_CART_INFO_ACTUALLY_1', 'There are currently', 'Attualmente ci sono', 'frontend', 2),
(268, 'LABEL_CART_INFO_ACTUALLY_2', 'in your shopping cart', 'nel carrello', 'frontend', 2),
(269, 'LABEL_SIZE', 'Size', 'Tg', 'frontend', 2),
(270, 'LABEL_COLOR', 'Color', 'Colore', 'frontend', 2),
(271, 'LABEL_CATEGORY', 'Category', 'Categoria', 'frontend', 2),
(272, 'LABEL_DESCRIPTION', 'Description', 'Descrizione', 'frontend', 2),
(273, 'LABEL_REVIEWS', 'Reviews', 'Commenti', 'frontend', 2),
(274, 'LABEL_ALSO_LIKE', 'You May Also Like', 'Ti potrebbero piacere', 'frontend', 2),
(275, 'LABEL_REFCODE', 'Ref', 'Rif', 'frontend', 2),
(276, 'LABEL_DETAIL', 'Detail', 'Dettaglio', 'frontend', 2),
(277, 'LABEL_SHOPPING_CART', 'Shopping cart', 'Carrello', 'frontend', 2),
(278, 'LABEL_FRONT', 'FRONT', 'FRONTE', 'frontend', 2),
(279, 'LABEL_BACK', 'BACK', 'RETRO', 'frontend', 2),
(280, 'LABEL_AVAILABILITY', 'Availability', 'Disponibilità', 'frontend', 2),
(281, 'LABEL_AVAILABILITY_HIGH', 'High', 'Alta', 'frontend', 2),
(282, 'LABEL_AVAILABILITY_LOW', 'Low', 'Bassa', 'frontend', 2),
(283, 'LABEL_404_MESSAGE', 'Oops.... the page requested not exist !', 'Oops.... la pagina richiesta non esiste !', 'frontend', 2),
(284, 'LABEL_404_BTN', 'BACK TO HOME', 'TORNA ALLA HOME', 'frontend', 2),
(285, 'LABEL_ORDER', 'Order', 'Ordine', 'frontend', 2),
(286, 'LABEL_YOUR_ACCOUNT', 'Your account', 'Il tuo account', 'frontend', 2),
(287, 'LABEL_USER_ALREADY_REGISTERED', 'Are you already registered?', 'Sei già registrato ?', 'frontend', 2),
(288, 'LABEL_USER_NOTREGISTERED', 'Otherwise you can order filling in the data', 'Altrimenti puoi ordinare compilando i dati', 'frontend', 2),
(289, 'LABEL_USER_NOTREGISTERED_POINTS', 'you will not accumulate points and bonuses reserved for registered customers', 'non accumulerai punti e bonus riservati ai clienti registrati', 'frontend', 2),
(290, 'LABEL_COUPON', 'COUPON', 'COUPON', 'frontend', 2),
(291, 'LABEL_COUPON_HAVE', 'Do you have a discount code?', 'Hai un codice sconto?', 'frontend', 2),
(292, 'LABEL_COUPON_INSERT', 'Enter the coupon code', 'Inserisci il codice coupon', 'frontend', 2),
(293, 'LABEL_COUPON_APPLY', 'Apply coupons', 'Applica coupon', 'frontend', 2),
(294, 'LABEL_DISCOUNT', 'Discount', 'Sconto', 'frontend', 2),
(295, 'LABEL_SHIPPING_THIS', 'Send to this address', 'Spedisci a questo indirizzo', 'frontend', 2),
(296, 'LABEL_SHIPPING_OTHER', 'Send to another address', 'Spedisci ad un altro indirizzo', 'frontend', 2),
(297, 'LABEL_NEW_ADDRESS', 'New address', 'Nuovo indirizzo', 'frontend', 2),
(298, 'LABEL_BILLING_ADDRESS', 'Billing address', 'Indirizzo di fatturazione', 'frontend', 2),
(299, 'LABEL_SHIPPING_ADDRESS', 'Shipping address', 'Indirizzo di spedizione', 'frontend', 2),
(300, 'LABEL_SHIPPING', 'Shipping', 'Spedizione', 'frontend', 2),
(301, 'LABEL_CHANGE_PASSWORD', 'Change Password', 'Cambio Password', 'frontend', 2),
(302, 'LABEL_ORDER_DATE', 'Order date', 'Data ordine', 'frontend', 2),
(303, 'LABEL_STRIPE_DESC', 'Make secure payments with Stripe', 'Paga in tutta sicurezza con Stripe', 'frontend', 2),
(304, 'MSG_SEARCH_INSERT', 'Enter text to search', 'Inserisci il testo da ricercare', 'frontend', 2),
(305, 'MSG_SAVE_NEWSLETTER', 'Sign up to newsletter', 'Iscrivimi alla newsletter', 'frontend', 2),
(306, 'MSG_SEND_CONTACT_US', 'Contact requests send!<br/>Thank you.', 'Richiesta di contatto inviata con successo!<br/>Grazie.', 'frontend', 2),
(307, 'MSG_PAYPAL_NOTETOPAYER', 'The shipping address will remain in the checkout form and not the one indicated in the PayPal payment!', 'L\'indirizzo di spedizione resterà quello inserito nel modulo di checkout e non quello indicato nel pagamento PayPal !', 'frontend', 2),
(308, 'MSG_PAYPAL_CANCEL', 'Retry with payment to complete the order !', 'Riprova ad effettuare il pagamemto !', 'frontend', 2),
(309, 'MSG_PAYPAL_ERROR', 'Error during payment operation:', 'Errore durante il pagamento:', 'frontend', 2),
(310, 'MSG_NO_RESULT', 'No results found for  ', 'Nessun risultato per ', 'frontend', 2),
(311, 'MSG_NO_RESULT_FILTER', 'No results found with filters selected.', 'Nessun risultato presente per i filtri selezionati.', 'frontend', 2),
(312, 'MSG_NO_SIZE_FOR_PRODUCTS_COLOR', 'No availables sizes at the moment for this product/color', 'Al momento non ci sono taglie disponibili per questo prodotto/colore', 'frontend', 2),
(313, 'MSG_ORDER_SUCCESS', 'saved successfully! Thank you.', 'inserito con successo ! Grazie.', 'frontend', 2),
(314, 'MSG_ORDER_PAYMENT_ERROR', 'Error during order payment. Please che your order and retry.', 'Errore durante il pagamento ordine. Per favore verifica il tuo ordine e riprova.', 'frontend', 2),
(315, 'MSG_SUCCESS_CONTACT', 'Your message has been sent correctly. Thank you !', 'Il tuo messaggio è stato inviato correttamente. Grazie !', 'frontend', 2),
(316, 'MSG_FAILURE_CONTACT', 'Sorry it seems that our mail server is not responding. Please try again later!', 'Abbiamo riscontrato un problema nell\'invio del mesaggio. Riprova!', 'frontend', 2),
(317, 'MSG_SUCCESS_NEWSLETTER', 'Now you are subscribed to the newsletter. Thank you !', 'Adesso sei iscritto alla newsletter. Grazie !', 'frontend', 2),
(318, 'MSG_UNIQUE_NEWSLETTER', 'The email address is already subscribed to the newsletter.', 'L\'indirizzo email è già iscritto alla newsletter', 'frontend', 2),
(319, 'MSG_UNSUBSCRIBE_DONE', 'Your email/subscription was removed. Thank you!', 'La tua email/iscrizione è stata rimossa. Grazie!', 'frontend', 2),
(320, 'MSG_UNSUBSCRIBE_NOTFOUND', 'The required contact is not currently registered', 'Il contatto richiesto non è attualmente registrato', 'frontend', 2),
(321, 'MSG_CART_REMOVED', 'Product removed from cart', 'Prodotto rimosso dal carrello', 'frontend', 2),
(322, 'MSG_CART_ADDED', 'Product added to cart', 'Prodotto inserito nel carrello', 'frontend', 2),
(323, 'MSG_CART_UPDATED', 'Product updated into cart', 'Prodotto aggiornato nel carrello', 'frontend', 2),
(324, 'MSG_SERVICE_FAILURE', 'There was an error. Please retry!', 'Si è verificato un errore. Riprova!', 'frontend', 2),
(325, 'MSG_BILLING_ADDRESS_NECESSARY', 'The billing address is required for the purchase process!<br/>Are you sure you want to leave?<br/>You will still be asked for the purchase phase!', 'L\'indirizzo di fatturazione è obbligatorio ai fini del processo di acquisto! <br/>Sei sicuro di voler uscire?<br/>Ti sarà comunque chiesto nella fase di acquisto!', 'frontend', 2),
(326, 'MSG_ALTERNATE_ADDRESS_LOGGED', 'enter a new address or select one already in the list', 'inserisci un nuovo indirizzo o selezionane uno già presente nella lista', 'frontend', 2),
(327, 'MSG_ALTERNATE_ADDRESS_NOTLOGGED', 'enter a new shipping address', 'inserisci un nuovo indirizzo per la spedizione', 'frontend', 2),
(328, 'MSG_COUPON_INVALID', 'Coupon invalid or expired', 'Coupon non valido o scaduto', 'frontend', 2),
(329, 'MSG_COUPON_INVALID_OVER', 'The value of the inserted Coupon is greater than the cart!', 'Il valore del Coupon inserito è maggiore del carrello!', 'frontend', 2),
(330, 'LABEL_TP_SALE', 'Sale', 'Offerta', 'frontend', 2),
(331, 'LABEL_TP_BESTSELLER', 'Best seller', 'Più venduti', 'frontend', 2),
(332, 'LABEL_TP_TOPRATED', 'Top rated', 'Più votati', 'frontend', 2),
(333, 'LABEL_TP_STANDARD', 'Standard', 'Standard', 'frontend', 2),
(334, 'Standard', 'New', 'Nuovo', 'frontend', 2),
(335, 'SEND_AREYOUSURE_BTN', 'ARE YOU SURE ?', 'SEI SICURO ?', 'frontend', 2),
(336, 'LABEL_UNSUBSCRIBE', 'Unsubscribe', 'Cancellati', 'frontend', 2),
(337, 'LABEL_MY_ACCOUNT_EMAIL', 'My account', 'Il mio profilo', 'email', 2),
(338, 'LABEL_SEE_EMAIL_ONLINE', 'See contents online', 'Vedi email online', 'email', 2),
(339, 'LABEL_EMAIL_SALES_TITLE', 'Check out our newest sales!', 'Ultime offerte', 'email', 2),
(340, 'LABEL_DETAIL_EMAIL', 'See details', 'Vedi i dettagli', 'email', 2),
(341, 'TEXT_EMAIL_FOOTER_USUBSCRIBE', 'unsubscribe', 'Cancellati footer', 'email', 2),
(342, 'TEXT_EMAIL_FOOTER_RESERVED', 'All rights reserved', 'Tutti i diritti riservati', 'email', 2),
(343, 'TEXT_EMAIL_FOOTER_COPYRIGHT', 'If you no longer wish to receive these emails please', 'Non ricevere più email', 'email', 2),
(344, 'LABEL_EMAIL_SUBJECT_CONTACT', 'Contact from', 'Contatto dal sito', 'email', 2),
(345, 'LABEL_EMAIL_CONTACT_TITLE', 'Thank you for contact us!', 'Ti ringraziamo per il contatto', 'email', 2),
(346, 'LABEL_EMAIL_CONTACT_TEXT', 'We have received your communication and will send you an answer as soon as possible. Thank you!', 'Comunicazione ricevuta', 'email', 2),
(347, 'LABEL_EMAIL_SUBJECT_NEWSLETTER', 'Newsletter signup', 'Iscrizione alla Newsletter', 'email', 2),
(348, 'LABEL_EMAIL_NEWSLETTER_TITLE', 'Thank you for signing up to our newsletter!', 'Grazie newsletter', 'email', 2),
(349, 'LABEL_EMAIL_NEWSLETTER_TEXT', 'You will receive updates, news and exclusive offers to stay in touch with our world.', 'Riceverai gli aggiornamenti', 'email', 2),
(350, 'LABEL_EMAIL_SUBJECT_WELCOME', 'Welcome to Ma Chlò!', 'Benvenuto su', 'email', 2),
(351, 'LABEL_EMAIL_WELCOME_TITLE', 'Welcome to Ma Chlò!', 'Benvenuto su TITOLO', 'email', 2),
(352, 'LABEL_EMAIL_WELCOME_TEXT', 'Thank you for signing up! We hope you enjoy your time with us. Check out some of our newest offers below or the button to view your new account.', 'Grazie per esserti registrato', 'email', 2),
(353, 'LABEL_EMAIL_SUBJECT_NEW_ORDER', 'New order', 'Nuovo ordine', 'email', 2),
(354, 'LABEL_TITLE_INVOICE', 'Order invoice', 'Riepilogo ordine', 'email', 2),
(355, 'LABEL_INVOICE_THANKS', 'Thank you for your order!', 'Ti ringraziamo per il tuo ordine!', 'email', 2),
(356, 'LABEL_INVOICE_THANKS_TEXT', 'We\\\'ll let you know as soon as your items have shipped.<br>To change or view your order, please view your account by clicking the button below.', 'Ti faremo sapere USER', 'email', 2),
(357, 'LABEL_INVOICE_THANKS_TEXT_NOUSER', 'We\'ll let you know as soon as your items have shipped.', 'Ti faremo sapere NO USER', 'email', 2),
(358, 'LABEL_SHIPPING_ADDRESS_EMAIL', 'Shipping address', 'Indirizzo di spedizione', 'email', 2),
(359, 'LABEL_ORDER_EMAIL', 'Order', 'Ordine', 'email', 2),
(360, 'LABEL_ORDER_DATE_EMAIL', 'Order date', 'Data ordine', 'email', 2),
(361, 'LABEL_ADDRESS_NOTES_EMAIL', 'Address notes', 'Note indirizzo', 'email', 2),
(362, 'LABEL_ORDER_NOTES_EMAIL', 'Order notes', 'Note ordine', 'email', 2),
(363, 'LABEL_DESCRIPTION_EMAIL', 'Description', 'Descrizione', 'email', 2),
(364, 'LABEL_QTY_EMAIL', 'Qty', 'Quantità', 'email', 2),
(365, 'LABEL_SIZE_EMAIL', 'Size', 'Taglia', 'email', 2),
(366, 'LABEL_COLOR_EMAIL', 'Color', 'Colore', 'email', 2),
(367, 'LABEL_SUBTOTAL_ORDER_EMAIL', 'Subtotal', 'Subtotale', 'email', 2),
(368, 'LABEL_COUPON_APPLY_EMAIL', 'Apply coupon', 'Applica coupon', 'email', 2),
(369, 'LABEL_SHIPPING_EMAIL', 'Shipping', 'Spedizione', 'email', 2),
(370, 'LABEL_TOTAL_EMAIL', 'Total', 'Totale', 'email', 2),
(371, 'LABEL_TP_NEW', 'Nuovo', 'Nuovo prodotto', 'frontend', 1),
(372, 'LABEL_TP_NEW', 'New', 'New product', 'frontend', 2),
(373, 'LABEL_FROM', 'from', 'From', 'frontend', 2),
(374, 'LABEL_FROM', 'da', 'Da', 'frontend', 1),
(375, 'MSG_WISHLIST_INSERTED', 'Prodotto inserito nella Wishlist !!!', 'Prodotto inserito', 'frontend', 1),
(376, 'MSG_WISHLIST_INSERTED', 'Product inserted in the Wishlist !!!', 'Product inserted', 'frontend', 2),
(377, 'MSG_WISHLIST_PRESENT', 'Prodotto già presente nella Wishlist !!!', 'Prodotto già presente', 'frontend', 1),
(378, 'MSG_WISHLIST_PRESENT', 'Product already present in the Wishlist !!!', 'Product already present', 'frontend', 2),
(379, 'MSG_WISHLIST_NOTALLOWED', 'Effettua la login/regitrazione per utilizzare la Wishlist !!!', 'Utente non abilitato alla Wishlist', 'frontend', 1),
(380, 'MSG_WISHLIST_NOTALLOWED', 'Please login / register to use the Wishlist !!!', 'User not allowed to use wishlist', 'frontend', 2),
(381, 'MSG_OPERATION_SUCCESS', 'Operazione eseguita', 'Operazione eseguita', 'frontend', 1),
(382, 'MSG_OPERATION_SUCCESS', 'Operation performed', 'Operation performed', 'frontend', 2),
(383, 'MSG_OPERATION_FAILURE', 'Operazione non eseguita', 'Operazione non eseguita', 'frontend', 1),
(384, 'MSG_OPERATION_FAILURE', 'Operation not performed', 'Operation not performed', 'frontend', 2),
(385, 'LABEL_CANCEL', 'Cancel', 'Cancel operation', 'frontend', 2),
(386, 'LABEL_CANCEL', 'Cancella', 'Cancella operazione', 'frontend', 1),
(387, 'LABEL_CLOSE', 'Close', 'Close', 'frontend', 2),
(388, 'LABEL_CLOSE', 'Chiudi', 'Chiudi', 'frontend', 1),
(389, 'LABEL_DEFAULT_ADDRESS', 'Indirizzo predefinito', 'Indirizzo predefinito', 'frontend', 1),
(390, 'LABEL_DEFAULT_ADDRESS', 'Default address', 'Default address', 'frontend', 2),
(391, 'MSG_OPERATION_SAVED', 'salvato correttamente', 'salvato correttamente', 'frontend', 1),
(392, 'MSG_OPERATION_SAVED', 'saved succesfully', 'saved succesfully', 'frontend', 2),
(393, 'MSG_OPERATION_DELETE', 'deleted succesfully', 'deleted succesfully', 'frontend', 2),
(394, 'MSG_OPERATION_DELETE', 'eliminato correttamente', 'eliminato correttamente', 'frontend', 1),
(395, 'MSG_NO_ELEMENTS', 'Nessun elemento presente', 'Nessun elemento presente', 'frontend', 1),
(396, 'MSG_NO_ELEMENTS', 'No elements found', 'No elements found', 'frontend', 2),
(397, 'LABEL_TRACKING', 'Tracking', 'Tracking', 'frontend', 1),
(398, 'LABEL_TRACKING', 'Tracking', 'Tracking', 'frontend', 2),
(399, 'MSG_ORDER_CONTACT', 'Per informazioni o modifiche al tuo ordine CONTATTACI', 'Per informazioni o modifiche al tuo ordine CONTATTACI', 'frontend', 1),
(400, 'MSG_ORDER_CONTACT', 'For information or changes to your order CONTACT US', 'For information or changes to your order CONTACT US', 'frontend', 2),
(401, 'MSG_COOKIE_LAW', 'Questo sito utilizza i cookie per migliorare la tua esperienza di navigazione.', 'Questo sito utilizza i cookie per migliorare la tua esperienza di navigazione.', 'frontend', 1),
(402, 'MSG_COOKIE_LAW', 'This website uses cookies to ensure you get the best experience on our website.', 'Questo sito utilizza i cookie per migliorare la tua esperienza di navigazione.', 'frontend', 2),
(403, 'MSG_COOKIE_LAW_BTN', 'Ho capito!', 'Pulsante ok (ho capito)', 'frontend', 1),
(404, 'MSG_COOKIE_LAW_BTN', 'Got it!', 'Pulsante ok (ho capito)', 'frontend', 2),
(405, 'MSG_COOKIE_LAW_INFO', 'leggi di più', 'Per saperne di più', 'frontend', 1),
(406, 'MSG_COOKIE_LAW_INFO', 'learn more', 'Per saperne di più', 'frontend', 2),
(407, 'LABEL_INVOICE_CC_PAYMENT_FAIL', 'Il pagamento relativo al tuo ordine non risulta effettuato. Procedi al pagamento utilizzando il pulsante di pagamento.', 'Messaggio pagamento non effettuato', 'frontend', 1),
(408, 'LABEL_INVOICE_CC_PAYMENT_FAIL', 'The payment for your order is not made. Proceed to payment using the payment button.', 'Messaggio pagamento non effettuato', 'frontend', 2),
(409, 'LABEL_PAY_NOW', 'Paga adesso', 'Etichetta bottone paga adesso', 'frontend', 1),
(410, 'LABEL_PAY_NOW', 'Pay now', 'Etichetta bottone paga adesso', 'frontend', 2),
(411, 'LABEL_TEAM', 'Il nostro Team', 'Etichetta Team', 'frontend', 1),
(412, 'LABEL_TEAM', 'Our Team', 'Etichetta Team', 'frontend', 2),
(413, 'LABEL_READ_MORE', 'Leggi di più', 'Etichetta leggi di più', 'frontend', 1),
(414, 'LABEL_READ_MORE', 'Read more', 'Etichetta leggi di più', 'frontend', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine`
--

CREATE TABLE `pagine` (
  `id_pagina` int(11) NOT NULL,
  `nome_pagina` varchar(50) NOT NULL,
  `url_pagina` varchar(50) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `controller` varchar(250) NOT NULL,
  `tipo_pagina` varchar(25) NOT NULL,
  `trad_code` varchar(25) NOT NULL,
  `label_page_url` varchar(250) NOT NULL,
  `ordine_menu` int(5) NOT NULL,
  `nome_menu` varchar(255) NOT NULL,
  `testo_menu` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine`
--

INSERT INTO `pagine` (`id_pagina`, `nome_pagina`, `url_pagina`, `id_lingua`, `controller`, `tipo_pagina`, `trad_code`, `label_page_url`, `ordine_menu`, `nome_menu`, `testo_menu`) VALUES
(1, 'default page', 'default_page', 1, 'frontend/Home', 'statica', '', '', 0, '', ''),
(2, 'Home', 'it/home', 1, 'frontend/Home', 'statica', '', 'PAGE_HOME_URL', 1, 'MENU_HOME', 'Home'),
(3, 'MaKa World', 'it/makaworld', 1, 'frontend/Home/shop', 'dinamica', 'makaworld', 'PAGE_SHOP_URL', 2, 'MENU_SHOP', 'MaKa World'),
(4, 'MaKa World', 'it/makaworld/(:any)', 1, 'frontend/Home/shop/$1', 'dinamica', 'makaworld', '', 0, '', ''),
(5, 'Chi siamo', 'it/chisiamo', 1, 'frontend/Home/about', 'statica', '', 'PAGE_ABOUT_URL', 3, 'MENU_ABOUT', 'Chi siamo'),
(6, 'Dettaglio', 'it/dettaglio', 1, 'frontend/Products', 'dinamica', 'dettaglio', 'PAGE_PRODUCTS_URL', 0, '', ''),
(7, 'Contatti', 'it/contatti', 1, 'frontend/Home/contacts', 'statica', '', 'PAGE_CONTACTS_URL', 9, 'MENU_CONTACTS', 'Contatti'),
(9, 'Dettaglio', 'it/dettaglio/(:any)/(:any)', 1, 'frontend/Products/detailcode/$1', 'dinamica', 'dettaglio', '', 0, '', ''),
(10, 'Privacy', 'it/privacy', 1, 'frontend/Home/privacy', 'statica', '', 'PAGE_PRIVACY_URL', 7, 'MENU_PRIVACY', 'Privacy'),
(11, 'Home', 'en/home', 2, 'frontend/Home', 'statica', '', 'PAGE_HOME_URL', 1, 'MENU_HOME', 'Home'),
(12, 'MaKa World', 'en/makaworld', 2, 'frontend/Home/shop', 'dinamica', 'makaworld', 'PAGE_SHOP_URL', 2, 'MENU_SHOP', 'MaKa World'),
(13, 'MaKa World', 'en/makaworld/(:any)', 2, 'frontend/Home/shop/$1', 'dinamica', 'makaworld', '', 0, '', ''),
(14, 'About', 'en/about', 2, 'frontend/Home/about', 'statica', '', 'PAGE_ABOUT_URL', 3, 'MENU_ABOUT', 'About us'),
(15, 'Contacts', 'en/contacts', 2, 'frontend/Home/contacts', 'statica', '', 'PAGE_CONTACTS_URL', 9, 'MENU_CONTACTS', 'Contacts'),
(16, 'Detail', 'en/detail/(:any)/(:any)', 2, 'frontend/Products/detailcode/$1', 'dinamica', 'dettaglio', '', 0, '', ''),
(17, 'Privacy', 'en/privacy', 2, 'frontend/Home/privacy', 'statica', '', 'PAGE_PRIVACY_URL', 7, 'MENU_PRIVACY', 'Privacy'),
(18, 'Gallery', 'it/gallery', 1, 'frontend/Home/gallery', 'statica', '', 'PAGE_GALLERY_URL', 6, 'MENU_GALLERY', 'Gallery'),
(19, 'Gallery', 'en/gallery', 2, 'frontend/Home/gallery', 'statica', '', 'PAGE_GALLERY_URL', 6, 'MENU_GALLERY', 'Gallery'),
(20, 'Detail', 'en/detail', 2, 'frontend/Products', 'dinamica', 'dettaglio', 'PAGE_PRODUCTS_URL', 0, '', ''),
(21, 'Blog', 'it/blog', 1, 'frontend/Home/blog', 'statica', '', 'PAGE_BLOG_URL', 8, 'MENU_BLOG', 'Blog'),
(22, 'Blog', 'en/blog', 2, 'frontend/Home/blog', 'statica', '', 'PAGE_BLOG_URL', 8, 'MENU_BLOG', 'Blog');

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine_contenuti`
--

CREATE TABLE `pagine_contenuti` (
  `id_pc` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `meta_description` text NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine_contenuti`
--

INSERT INTO `pagine_contenuti` (`id_pc`, `code`, `title`, `meta_description`, `description`, `image`, `id_lingua`) VALUES
(1, 'HOME', 'Feste originali', 'Benvenuti nella porta di MaKa.', '', '', 1),
(2, 'SHOP', 'MaKa World', '', '', '', 1),
(3, 'ABOUT', 'Chi siamo', 'La porta di MaKa nasce a Roma dalle idee di Martina e Katy', '<p>\n	La porta di MaKa nasce a Roma dalle idee di Martina e Katy...</p>\n<p>\n	Aggiungere il testo</p>\n', 'about.jpg', 1),
(4, 'GALLERY', 'Galleria immagini', 'Ecco una galleria delle nostre immagini più belle.', '', '', 1),
(5, 'CONTACTS', 'Contatti e recapiti', 'Puoi contattare La Porta di MaKa in tantissimi modi diversi: email, telefono e socials.', '', '', 1),
(6, 'PRIVACY', 'Privacy', 'Scopri in che modo vengo trattati i tuoi dati sensibili, l\'utilizzo dei cookie e dei dati di sessione.', '<h3>\n	Cookie policy</h3>\n<p style=\"text-align: justify;\">\n	In questo sito vengono utilizzati alcuni cookie tecnici che servono per la navigazione e per fornire un servizio gi&agrave; richiesto dall&#39;utente. Non vengono utilizzati per scopi ulteriori e sono normalmente installati nella maggior parte dei siti web. Un cookie &egrave; una piccola particella di informazioni che viene salvata sul dispositivo dell&#39;utente che visita un sito web. Il cookie non contiene dati personali e non pu&ograve; essere utilizzato per identificare l&#39;utente all&#39;interno di altri siti web, compreso il sito web del provider di analisi. I cookie possono inoltre essere utilizzati per memorizzare le impostazioni preferite, come lingua e paese, in modo da renderle immediatamente disponibili alla visita successiva. Non utilizziamo gli indirizzi IP o i cookie per identificare personalmente gli utenti. Utilizziamo il sistema di analisi web al fine di incrementare l&#39;efficienza del nostro portale.<br />\n	<br />\n	Per avere maggiori informazioni sui cookies vi suggeriamo di visitare il sito www.allaboutcookies.org che vi fornir&agrave; indicazioni su come gestire secondo le vostre preferenze, ed eventualmente cancellare i cookies in funzione del browser che state utilizzando.<br />\n	In questo sito web utilizziamo il sistema di analisi Google Analytics per misurare e analizzare le visite al nostro sito. Utilizziamo gli indirizzi IP al fine di raccogliere dati sul traffico Internet, sul browser e sul computer degli utenti. Tali informazioni vengono esaminate unicamente per fini statistici. L&#39;anonimato dell&#39;utente viene rispettato. Informazioni sul funzionamento del software open source di analisi web Google Analytics.<br />\n	<br />\n	Ribadiamo che sul sito sono operativi esclusivamente cookies tecnici (come quelli sopra elencati) necessari per navigare e che essenziali quali autenticazione, validazione, gestione di una sessione di navigazione e prevenzione delle frodi e consentono ad esempio: di identificare se l&rsquo;utente ha avuto regolarmente accesso alle aree del sito che richiedono la preventiva autenticazione oppure la validazione dell&rsquo;utente e la gestione delle sessioni relative ai vari servizi e applicazioni oppure la conservazione dei dati per l&rsquo;accesso in modalit&agrave; sicura oppure le funzioni di controllo e prevenzione delle frodi.<br />\n	Non &egrave; obbligatorio acquisire il consenso alla operativit&agrave; dei soli cookies tecnici o di terze parti o analitici assimilati ai cookies tecnici. La loro disattivazione e/o il diniego alla loro operativit&agrave; comporter&agrave; l&rsquo;impossibilit&agrave; di una corretta navigazione sul Sito e/o la impossibilit&agrave; di fruire dei servizi, delle pagine, delle funzionalit&agrave; o dei contenuti ivi disponibili.<br />\n	<br />\n	Tutti i dati inseriti dai nostri utenti all&#39;interno di moduli verranno utilizzati esclusivamente per rispondere alle loro richieste. Per questo motivo sono obbligati a fornire i dati necessari alla compilazione dei moduli richiesti, in caso contrario non sar&agrave; possibile utilizzare il nostro servizio.<br />\n	In ogni caso confermiamo che i dati utilizzati e memorizzati al fine del funzionamento del servizio non verranno mai e in nessun caso ceduti a terzi per nessun tipo di finalit&agrave; od utilizzo.</p>\n', '', 1),
(7, 'HOME', 'Original Party', '', '', '', 2),
(8, 'CONTACTS', 'Contacts and infos', 'You can contact La porta di MaKa in many different ways: email, phone and socials.', '', '', 2),
(9, 'PRIVACY', 'Privacy', 'Find out how we treat your sensitive data, use cookies, and sessions data.', '<h3>\n	Cookie policy</h3>\n<div style=\"text-align: justify;\">\n	This site uses some technical cookies that are used for navigation and to provide a service already requested by the user. They are not used for further purposes and are normally installed on most websites. A cookie is a small piece of information that is saved on the device of the user who visits a website. The cookie does not contain personal data and can not be used to identify you within other websites, including the analytics provider&#39;s website. Cookies can also be used to store the preferred settings, such as language and country, in order to make them immediately available on the next visit. We do not use IP addresses or cookies to personally identify users. We use the web analysis system in order to increase the efficiency of our portal.</div>\n<div style=\"text-align: justify;\">\n	&nbsp;</div>\n<div style=\"text-align: justify;\">\n	For more information on cookies we suggest you visit www.allaboutcookies.org which will provide you with information on how to manage according to your preferences, and possibly delete cookies depending on the browser you are using. On this website, we use the Google Analytics analysis system to measure and analyze visits to our site. We use IP addresses to collect data on Internet traffic, browser and users&#39; computers. This information is only examined for statistical purposes. The anonymity of the user is respected. Information on the operation of open source web analytics software Google Analytics.&nbsp;</div>\n<div style=\"text-align: justify;\">\n	&nbsp;</div>\n<div style=\"text-align: justify;\">\n	We reiterate that on the site are operating only technical cookies (such as those listed above) necessary to navigate and essential such as authentication, validation, management of a browsing session and prevention of fraud and allow for example: to identify if the user has had regularly access to areas of the site that require prior authentication or user validation and management of sessions related to the various services and applications or the storage of data for access in secure mode or the functions of control and prevention of fraud.</div>\n<div style=\"text-align: justify;\">\n	It is not mandatory to acquire the consent to the operation of only technical or third-party cookies or analytics similar to technical cookies. Their deactivation and / or denial to their operation will make it impossible to properly browse the site and / or the inability to use the services, pages, features or content available there.</div>\n<div style=\"text-align: justify;\">\n	&nbsp;</div>\n<div style=\"text-align: justify;\">\n	All data entered by our users within modules will be used exclusively to respond to their requests. For this reason they are obliged to provide the data necessary to fill in the required forms, otherwise it will not be possible to use our service.</div>\n<div style=\"text-align: justify;\">\n	In any case, we confirm that the data used and stored for the purpose of the service will never be transferred to third parties under any circumstances for any purpose or use.</div>\n', '', 2),
(10, 'SHOP', 'MaKa World', '', '', '', 2),
(11, 'ABOUT', 'About us', 'La porta di MaKa was born in Rome from ideas of Martina and Katy', '<p>\n	La porta di MaKa was born in Rome from ideas of Martina and Katy</p>\n<h3>\n	continua con il testo</h3>\n', 'about.jpg', 2),
(12, 'GALLERY', 'Images gallery', 'This is our best images gallery.', '', '', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti`
--

CREATE TABLE `prodotti` (
  `id_prodotti` int(11) NOT NULL,
  `id_tipo_prodotto` int(11) DEFAULT NULL,
  `codice` varchar(100) DEFAULT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `url_immagine` varchar(250) DEFAULT NULL,
  `stato` tinyint(1) NOT NULL,
  `ordine` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti`
--

INSERT INTO `prodotti` (`id_prodotti`, `id_tipo_prodotto`, `codice`, `nome`, `url_immagine`, `stato`, `ordine`) VALUES
(1, 2, 'la-mia-ragnatela', 'La mia ragnatela è fantasmagorica', '3ef31-prodotto_ragna.png', 1, 1),
(2, 5, 'truccabimbi', 'Truccabimbi', '8f864-img-20180609-wa0025.jpg', 1, 2),
(3, 1, 'festa-harry-potter', 'Festa Harry Potter', '1858a-festa-alice_103.jpg', 1, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_categorie`
--

CREATE TABLE `prodotti_categorie` (
  `id_prodotti_categorie` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti_categorie`
--

INSERT INTO `prodotti_categorie` (`id_prodotti_categorie`, `id_prodotto`, `id_categoria`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_traduzioni`
--

CREATE TABLE `prodotti_traduzioni` (
  `id_prodotti_traduzioni` int(11) NOT NULL,
  `id_prodotti` int(11) NOT NULL,
  `nome_prodotto_trad` varchar(255) NOT NULL,
  `descrizione` text NOT NULL,
  `descrizione_breve` varchar(250) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti_traduzioni`
--

INSERT INTO `prodotti_traduzioni` (`id_prodotti_traduzioni`, `id_prodotti`, `nome_prodotto_trad`, `descrizione`, `descrizione_breve`, `lingua_traduzione_id`) VALUES
(1, 1, 'La mia ragnatela è fantasmagorica', 'Questo è un laboratorio di halloween', 'Questo è un laboratorio di halloween', 1),
(2, 1, 'My halloween web', 'Halloween lab', 'Halloween lab', 2),
(3, 2, 'Truccabimbi', 'Questo è il trucca bimbi di MaKa', 'Questo è il trucca bimbi di MaKa', 1),
(4, 2, 'Face make-up', 'This is MaKa\'s face make-up', ' This is MaKa\'s face make-up', 2),
(5, 3, 'Festa Harry Potter', 'Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico Festa Harry Potter di MaKa, Un vero divertimento magico', 'Festa Harry Potter di MaKa', 1),
(6, 3, 'Harry Potter Party', 'Harry Potter Party MaKa', 'Harry Potter Party MaKa', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `staff_name` varchar(255) NOT NULL,
  `staff_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `staff`
--

INSERT INTO `staff` (`staff_id`, `staff_name`, `staff_image`) VALUES
(1, 'Martina', '21f3b-martina.jpg'),
(2, 'Katy', '71586-katy.jpg'),
(3, 'Eleonora', 'b82da-eleonora.jpg'),
(4, 'Nicla', 'cd818-nicla.jpg');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_descrizione`
--

CREATE TABLE `stato_descrizione` (
  `id_stato_descrizione` int(11) NOT NULL,
  `testo_stato_descrizione` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_descrizione`
--

INSERT INTO `stato_descrizione` (`id_stato_descrizione`, `testo_stato_descrizione`) VALUES
(1, 'ATTIVO'),
(2, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_prodotti`
--

CREATE TABLE `stato_prodotti` (
  `stato_prodotti_id` int(11) NOT NULL,
  `stato_prodotti_desc` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_prodotti`
--

INSERT INTO `stato_prodotti` (`stato_prodotti_id`, `stato_prodotti_desc`) VALUES
(1, 'ATTIVO'),
(2, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_prodotto`
--

CREATE TABLE `tipo_prodotto` (
  `id_tipo_prodotto` int(11) NOT NULL,
  `descrizione_tipo_prodotto` varchar(30) NOT NULL,
  `css_class` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_prodotto`
--

INSERT INTO `tipo_prodotto` (`id_tipo_prodotto`, `descrizione_tipo_prodotto`, `css_class`) VALUES
(1, 'LABEL_TP_SALE', 'text-danger'),
(2, 'LABEL_TP_BESTSELLER', 'text-warning'),
(5, 'LABEL_TP_NEW', 'text-success');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_template`
--

CREATE TABLE `tipo_template` (
  `id_tipo_template` int(11) NOT NULL,
  `desc_tipo_template` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_template`
--

INSERT INTO `tipo_template` (`id_tipo_template`, `desc_tipo_template`) VALUES
(1, 'CONTATTO'),
(2, 'NEWSLETTER'),
(3, 'CUSTOM');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `stato_utente` varchar(15) NOT NULL,
  `user_avatar_url` varchar(250) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `stato_utente`, `user_avatar_url`, `role`) VALUES
(1, '127.0.0.1', 'roberto.rossi77@gmail.com', '$2y$08$/adbu2Mu2zqZ9b4mWnEZb.oiGeGeyC6cFGv/AWEMLAbbaRKfzkv4a', '', 'roberto.rossi77@gmail.com', '', NULL, NULL, NULL, 1268889823, 1543315706, 1, 'Roberto', 'Rossi', 'ADMIN', '4234234', 'ATTIVO', 'ce611-logo_2018_sfondo_rosso.jpg', 0),
(2, '', 'Admin', '$2y$08$5Um.zUMha3YfGi9RGsYy2.t2CD4BFqYr7Y7PkpikjRUwXFuQDRLgu', NULL, 'info@laportadimaka.com', NULL, NULL, NULL, NULL, 0, 1542019560, 1, 'Admin', 'Maka', NULL, NULL, '', 'd512d-avatar2.jpg', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` mediumint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 0),
(2, 1, 1);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id_categorie`);

--
-- Indici per le tabelle `categorie_gallery`
--
ALTER TABLE `categorie_gallery`
  ADD PRIMARY KEY (`id_categoria_gallery`);

--
-- Indici per le tabelle `categorie_gallery_traduzioni`
--
ALTER TABLE `categorie_gallery_traduzioni`
  ADD PRIMARY KEY (`id_categorie_gallery_traduzioni`);

--
-- Indici per le tabelle `categorie_traduzioni`
--
ALTER TABLE `categorie_traduzioni`
  ADD PRIMARY KEY (`id_categoria_traduzioni`);

--
-- Indici per le tabelle `colori_classi`
--
ALTER TABLE `colori_classi`
  ADD PRIMARY KEY (`id_colore_classe`);

--
-- Indici per le tabelle `constants_framework`
--
ALTER TABLE `constants_framework`
  ADD PRIMARY KEY (`id_cf`);

--
-- Indici per le tabelle `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  ADD PRIMARY KEY (`id_contatto`);

--
-- Indici per le tabelle `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  ADD PRIMARY KEY (`id_contatto_newsletter`);

--
-- Indici per le tabelle `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id_template`);

--
-- Indici per le tabelle `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `home_slider`
--
ALTER TABLE `home_slider`
  ADD PRIMARY KEY (`hs_id`);

--
-- Indici per le tabelle `immagini_gallery`
--
ALTER TABLE `immagini_gallery`
  ADD PRIMARY KEY (`id_ig`);

--
-- Indici per le tabelle `immagini_gallery_traduzioni`
--
ALTER TABLE `immagini_gallery_traduzioni`
  ADD PRIMARY KEY (`id_immagini_gallery_traduzioni`);

--
-- Indici per le tabelle `lingue`
--
ALTER TABLE `lingue`
  ADD PRIMARY KEY (`id_lingue`);

--
-- Indici per le tabelle `lingue_labels_lang`
--
ALTER TABLE `lingue_labels_lang`
  ADD PRIMARY KEY (`id_lingue_labels_lang`);

--
-- Indici per le tabelle `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `pagine`
--
ALTER TABLE `pagine`
  ADD PRIMARY KEY (`id_pagina`);

--
-- Indici per le tabelle `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  ADD PRIMARY KEY (`id_pc`);

--
-- Indici per le tabelle `prodotti`
--
ALTER TABLE `prodotti`
  ADD PRIMARY KEY (`id_prodotti`);

--
-- Indici per le tabelle `prodotti_categorie`
--
ALTER TABLE `prodotti_categorie`
  ADD PRIMARY KEY (`id_prodotti_categorie`);

--
-- Indici per le tabelle `prodotti_traduzioni`
--
ALTER TABLE `prodotti_traduzioni`
  ADD PRIMARY KEY (`id_prodotti_traduzioni`);

--
-- Indici per le tabelle `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indici per le tabelle `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  ADD PRIMARY KEY (`id_stato_descrizione`);

--
-- Indici per le tabelle `stato_prodotti`
--
ALTER TABLE `stato_prodotti`
  ADD PRIMARY KEY (`stato_prodotti_id`);

--
-- Indici per le tabelle `tipo_prodotto`
--
ALTER TABLE `tipo_prodotto`
  ADD PRIMARY KEY (`id_tipo_prodotto`);

--
-- Indici per le tabelle `tipo_template`
--
ALTER TABLE `tipo_template`
  ADD PRIMARY KEY (`id_tipo_template`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id_categorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `categorie_gallery`
--
ALTER TABLE `categorie_gallery`
  MODIFY `id_categoria_gallery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `categorie_gallery_traduzioni`
--
ALTER TABLE `categorie_gallery_traduzioni`
  MODIFY `id_categorie_gallery_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `categorie_traduzioni`
--
ALTER TABLE `categorie_traduzioni`
  MODIFY `id_categoria_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `colori_classi`
--
ALTER TABLE `colori_classi`
  MODIFY `id_colore_classe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `constants_framework`
--
ALTER TABLE `constants_framework`
  MODIFY `id_cf` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT per la tabella `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  MODIFY `id_contatto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  MODIFY `id_contatto_newsletter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id_template` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `home_slider`
--
ALTER TABLE `home_slider`
  MODIFY `hs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `immagini_gallery`
--
ALTER TABLE `immagini_gallery`
  MODIFY `id_ig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `immagini_gallery_traduzioni`
--
ALTER TABLE `immagini_gallery_traduzioni`
  MODIFY `id_immagini_gallery_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT per la tabella `lingue`
--
ALTER TABLE `lingue`
  MODIFY `id_lingue` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `lingue_labels_lang`
--
ALTER TABLE `lingue_labels_lang`
  MODIFY `id_lingue_labels_lang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=415;

--
-- AUTO_INCREMENT per la tabella `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `pagine`
--
ALTER TABLE `pagine`
  MODIFY `id_pagina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT per la tabella `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  MODIFY `id_pc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT per la tabella `prodotti`
--
ALTER TABLE `prodotti`
  MODIFY `id_prodotti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `prodotti_categorie`
--
ALTER TABLE `prodotti_categorie`
  MODIFY `id_prodotti_categorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `prodotti_traduzioni`
--
ALTER TABLE `prodotti_traduzioni`
  MODIFY `id_prodotti_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  MODIFY `id_stato_descrizione` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `tipo_prodotto`
--
ALTER TABLE `tipo_prodotto`
  MODIFY `id_tipo_prodotto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `tipo_template`
--
ALTER TABLE `tipo_template`
  MODIFY `id_tipo_template` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
