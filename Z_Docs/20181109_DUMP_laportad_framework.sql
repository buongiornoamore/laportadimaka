-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Nov 09, 2018 alle 07:06
-- Versione del server: 10.1.33-MariaDB
-- Versione PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laportad_framework`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello`
--

CREATE TABLE `carrello` (
  `id_carrello` int(11) NOT NULL,
  `id_variante` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `id_sessione_utente` varchar(250) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `data_creazione` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `qty` int(11) NOT NULL,
  `taglia` varchar(10) NOT NULL,
  `taglia_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `carrello`
--

INSERT INTO `carrello` (`id_carrello`, `id_variante`, `id_prodotto`, `id_sessione_utente`, `id_cliente`, `data_creazione`, `qty`, `taglia`, `taglia_id`) VALUES
(2, 429, 87, '89983b0435139e53768745af4f68b642', 0, '2018-07-26 12:54:40', 1, '30x60', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie`
--

CREATE TABLE `categorie` (
  `id_categorie` int(11) NOT NULL,
  `url_categorie` varchar(25) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `descrizione` varchar(100) DEFAULT NULL,
  `immagine` varchar(150) NOT NULL,
  `label_color_class` int(11) NOT NULL,
  `stato` int(11) NOT NULL,
  `ordine` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `url_categorie`, `nome`, `descrizione`, `immagine`, `label_color_class`, `stato`, `ordine`) VALUES
(1, 'tank-top', 'Tank Top', 'Canotta giromanica', 'tt.jpg', 1, 1, 2),
(2, 'crop-top', 'Crop Top', 'Top corto giromanica', 'ct.jpg', 2, 3, 5),
(3, 'cap-sleeve-top', 'Cap Sleeve Top', 'Cap Sleeve Top', 'cst.jpg', 3, 2, 8),
(4, 'fashion-fit-t-shirt', 'Fashion Fit T-Shirt', 'T-Shirt fashion attillate', 'fft.jpg', 1, 1, 7),
(5, 'love-story-masks', 'Love Story Masks', 'T-Shirt con maschere', 'cf2db-lsm_un_nuovo_inizio_black_f.jpg', 2, 1, 7),
(6, 'dresses', 'Dresses', 'Abiti', 'md.jpg', 3, 1, 0),
(7, 'racerback-tank-top', 'Racerback Tank Top', 'Canotte stile racer', 'rtt.jpg', 1, 3, 3),
(8, 'short-sleeve-t-shirt', 'Short Sleeve T-Shirt', 'T-Shirt manica accorciata', 'ssl.jpg', 2, 1, 10),
(9, 'long-sleeve-t-shirt', 'Long sleeve T-Shirt', 'Maglia semplice a maniche lunghe', '7a343-lsl_blucat_black_f.jpg', 1, 1, 10),
(10, 'woman', 'Woman', 'Prodotti da donna', '32d41-women.jpg', 3, 1, 10),
(12, 'new-collection', 'New collection', 'T-shirt donna', '435d2-ssl_colored_cats_black_f.jpg', 1, 1, 0),
(13, 'yoga-leggings', 'Yoga leggings', 'Leggings', '5bb59-lgg_masks_black_f.jpg', 1, 1, 7),
(14, 'black-friday', 'Black Friday', 'Promozione prodotti in Black Friday', '5761c-bf.jpg', 1, 1, 1),
(15, 'mugs_tazze', 'mugs', 'tazze in ceramica', '34e8b-cm_color_tree_red_f.jpg', 1, 1, 7),
(16, 'promo', 'Promo', 'articoli in saldo', '0274a-fft_yellowcat_white_f.jpg', 1, 1, 15),
(17, 'beach-towel', 'Beach towel', 'Teli mare', 'ba1ee-bt_flowers_b.jpg', 1, 1, 1),
(18, 'beach-bags', 'Beach bags', 'Borse mare', 'c04e1-bb_yang_blue_f.png', 1, 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_gallery`
--

CREATE TABLE `categorie_gallery` (
  `id_categoria_gallery` int(11) NOT NULL,
  `nome_categoria_gallery` varchar(250) NOT NULL,
  `stato_categoria_gallery` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_gallery`
--

INSERT INTO `categorie_gallery` (`id_categoria_gallery`, `nome_categoria_gallery`, `stato_categoria_gallery`) VALUES
(1, 'Prodotti', 1),
(2, 'Su di noi', 1),
(3, 'Coming soon', 2),
(4, 'Ma Chlò world', 1),
(5, 'Dipinti', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_gallery_traduzioni`
--

CREATE TABLE `categorie_gallery_traduzioni` (
  `id_categorie_gallery_traduzioni` int(11) NOT NULL,
  `id_categoria_gallery` int(11) NOT NULL,
  `descrizione_categoria_gallery` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_gallery_traduzioni`
--

INSERT INTO `categorie_gallery_traduzioni` (`id_categorie_gallery_traduzioni`, `id_categoria_gallery`, `descrizione_categoria_gallery`, `id_lingua`) VALUES
(1, 1, 'Prodotti', 1),
(2, 1, 'Products', 2),
(3, 2, 'Su di noi', 1),
(4, 2, 'About us', 2),
(5, 3, 'Novità', 1),
(6, 3, 'Coming soon', 2),
(9, 4, 'Ma Chlò world', 2),
(10, 4, 'Ma Chlò world', 1),
(11, 5, 'Dipinti', 1),
(12, 5, 'Artworks', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_traduzioni`
--

CREATE TABLE `categorie_traduzioni` (
  `id_categoria_traduzioni` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `nome_categoria_trad` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_traduzioni`
--

INSERT INTO `categorie_traduzioni` (`id_categoria_traduzioni`, `id_categoria`, `nome_categoria_trad`, `id_lingua`) VALUES
(1, 6, 'Abiti', 1),
(2, 6, 'Dresses', 2),
(3, 8, 'T-shirt manica corta', 1),
(4, 8, 'Short sleeve t-shirt', 2),
(5, 15, 'Mugs', 2),
(6, 15, 'Tazze', 1),
(7, 10, ' Woman', 2),
(8, 10, 'Donna', 1),
(9, 12, 'New collection', 2),
(10, 12, 'Novità', 1),
(11, 9, 'Maglie Manica Lunga', 1),
(12, 9, 'Long Sleeve T-shirt', 2),
(13, 1, 'Tank Top', 2),
(14, 1, 'Top', 1),
(15, 7, 'Racerback Tank Top', 1),
(16, 7, 'Canotte', 1),
(17, 13, 'Yoga Leggings', 2),
(18, 13, 'Yoga Leggings', 1),
(19, 16, 'Special price', 2),
(20, 16, 'Promozioni', 1),
(21, 2, 'Crop Top', 2),
(22, 2, 'Crop Top', 1),
(23, 5, 'Love Story Masks ', 2),
(24, 5, 'Love Story masks', 1),
(25, 17, 'Beach Towels', 2),
(26, 17, 'Teli mare', 1),
(27, 18, 'Beach Bags', 2),
(28, 18, 'Borse mare', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `clienti`
--

CREATE TABLE `clienti` (
  `id_cliente` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `cognome` varchar(250) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `partita_iva` varchar(11) CHARACTER SET latin1 DEFAULT NULL,
  `codice_fiscale` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `newsletter` tinyint(4) NOT NULL DEFAULT '0',
  `punti` bigint(20) NOT NULL DEFAULT '0',
  `id_lingua` int(11) NOT NULL DEFAULT '1',
  `id_indirizzo_fatturazione` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `clienti`
--

INSERT INTO `clienti` (`id_cliente`, `user_id`, `nome`, `cognome`, `email`, `telefono`, `partita_iva`, `codice_fiscale`, `newsletter`, `punti`, `id_lingua`, `id_indirizzo_fatturazione`) VALUES
(1, 7, 'Maurizio', 'Custodi', 'maurizio.1@gmail.com', '0622334455', NULL, NULL, 0, 0, 1, 1),
(2, 8, 'Stefania', 'Laconi', 'stefania.laconi@tiscali.it', '3386819197', '', 'LCNSFN80R47G203G', 0, 0, 1, 4),
(3, 0, 'Katia', 'Piredda', 'pireddakatia@gmail.com', '3889270867', '', '', 1, 0, 1, 5),
(4, 9, 'katia', 'piredda', '', NULL, NULL, NULL, 0, 0, 1, 0),
(5, 0, 'Tiziana', 'Laconi', 'tizivero@alice.it', '3337306938', NULL, NULL, 1, 0, 1, 6),
(6, 0, 'Roberto', 'Rossi', 'roberto.rossi77@gmail.com', '23423423423423', '', '', 1, 0, 1, 9),
(7, 0, 'Test', 'TesCogn', 'roberto.rossi77@gmail.com', '34324234324', '', '', 1, 0, 1, 10);

-- --------------------------------------------------------

--
-- Struttura della tabella `colori_classi`
--

CREATE TABLE `colori_classi` (
  `id_colore_classe` int(11) NOT NULL,
  `colore_classe` varchar(100) NOT NULL,
  `colore_classe_nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `colori_classi`
--

INSERT INTO `colori_classi` (`id_colore_classe`, `colore_classe`, `colore_classe_nome`) VALUES
(1, 'text-danger', 'Rosso'),
(2, 'text-success', 'Verde'),
(3, 'text-warning', 'Arancio');

-- --------------------------------------------------------

--
-- Struttura della tabella `colori_prodotti`
--

CREATE TABLE `colori_prodotti` (
  `id_colori_prodotti` int(11) NOT NULL,
  `nome_colore` varchar(50) NOT NULL,
  `codice_colore` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `colori_prodotti`
--

INSERT INTO `colori_prodotti` (`id_colori_prodotti`, `nome_colore`, `codice_colore`) VALUES
(1, 'Black', '000000'),
(2, 'White', 'ffffff'),
(3, 'Aqua', '43a9d1'),
(4, 'Berry', 'd2528f'),
(5, 'Blue', '6680b3'),
(6, 'Coral', 'ce474d'),
(7, 'Creamy', 'e5ded8'),
(8, 'Navy', '26273b'),
(9, 'Purple', '523756'),
(10, 'Pink', 'eba3b9'),
(11, 'Raspberry', 'a91671'),
(12, 'Jade', '9cc3c0'),
(13, 'Red', 'ae2f38'),
(14, 'Yellow', 'f3dc5d'),
(15, 'Green', '357249'),
(16, 'Darkgrey', '45413e'),
(17, 'Hotpink', 'db2c77'),
(18, 'Royal', '3e609d'),
(19, 'Strawberry', 'a11927'),
(20, 'Silver', 'c6c6c6'),
(21, 'Darkgreen', '35734a'),
(22, 'Grey', '474340'),
(23, 'White', 'e5e6e1'),
(24, 'White', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `constants_framework`
--

CREATE TABLE `constants_framework` (
  `id_cf` int(11) NOT NULL,
  `cf_name` varchar(100) NOT NULL,
  `cf_value` varchar(250) NOT NULL,
  `cf_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `constants_framework`
--

INSERT INTO `constants_framework` (`id_cf`, `cf_name`, `cf_value`, `cf_description`) VALUES
(1, 'SITE_URL_PATH', 'https://www.laportadimaka.com', 'Indirizzo online del sito'),
(2, 'SITE_TITLE_NAME', 'La porta di MaKa', 'Titolo del sito online'),
(3, 'SMTP_HOST_CUSTOM', 'mail.laportadimaka.com', 'Indirizzo del server SMTP'),
(4, 'SMTP_USER_CUSTOM', 'info@laportadimaka.com', 'Utente del server SMTP'),
(5, 'SMTP_PASS_CUSTOM', 'info18@Maka', 'Password del server SMTP'),
(6, 'DB_HOSTNAME', '37.60.232.114', 'Indirizzo del DB'),
(7, 'DB_USERNAME', 'laportad', 'Utente del DB'),
(8, 'DB_PASSWORD', 'maka18@Cpanel', 'Password del DB'),
(9, 'DB_DATABASE', 'laportad_framework', 'Nome del DB'),
(10, 'COMPANY_NAME', 'La porta di MaKa', 'Nome della società/ditta'),
(11, 'COMPANY_EMAIL', 'info@laportadimaka.com', 'Email della società/ditta'),
(12, 'COMPANY_COPYRIGHT', 'Copyright © 2018 La porta di MaKa', 'Testo footer copyright'),
(13, 'COMPANY_PHONE', '+39 3286105475', 'Telefono società/ditta'),
(14, 'COMPANY_ADDRESS', 'Roma', 'Indirizzo società/ditta'),
(15, 'GPLUS_LINK', '', 'Link social G+'),
(16, 'YOUTUBE_LINK', '', 'Link social YOUTUBE'),
(17, 'PINTEREST_LINK', '', 'Link social PINTEREST'),
(18, 'TWITTER_LINK', '', 'Link social TWITTER'),
(19, 'FACEBOOK_LINK', 'https://www.facebook.com/laportadimaka/', 'Link social FACEBOOK'),
(20, 'INSTAGRAM_LINK', 'https://www.instagram.com/la_porta_di_maka/', 'Link social INSTAGRAM'),
(21, 'GOOGLE_ANALITYCS_ID', '', 'UID Google Analitycs'),
(23, 'COMNINGSOON_LOGO', 'logo.png', 'File logo del COMING SOON'),
(24, 'COMNINGSOON_BACK_IMAGE', 'default.jpg', 'File immagine di sfondo Coming soon'),
(25, 'COMNINGSOON_BTN_COLOR', 'EE2D20', 'Colore del pulsante Coming soon'),
(34, 'SMARTSUPP_ID', '277c5c3c3b2380408d64637427267b0a9ec9bc2e', 'Chiave (ID KEY) per il collegamento a Smartsupp Chat');

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_moduli`
--

CREATE TABLE `contatti_moduli` (
  `id_contatto` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `messaggio` text NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_moduli`
--

INSERT INTO `contatti_moduli` (`id_contatto`, `nome`, `email`, `telefono`, `messaggio`, `data`, `stato_contatto`, `data_unsubscribe`, `id_lingua`) VALUES
(1, '0', '0', '0', '0', '2018-05-11 13:43:19', 1, NULL, 1),
(2, 'Stefania Laconi', 'stefania.laconi@tiscali.it', '3386819197', 'Ciao Stefania,\nil tuo ordine #1 è stato spedito!\nDi seguito il link che ti consentirà il tracking del tuo ordine RP013592545LV\n\nhttps://t.17track.net/en#nums=RP013592545LV&fc=12021', '0000-00-00 00:00:00', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_newsletter`
--

CREATE TABLE `contatti_newsletter` (
  `id_contatto_newsletter` int(11) NOT NULL,
  `email_contatto` varchar(250) NOT NULL,
  `data_contatto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_newsletter`
--

INSERT INTO `contatti_newsletter` (`id_contatto_newsletter`, `email_contatto`, `data_contatto`, `stato_contatto`, `data_unsubscribe`, `lingua_traduzione_id`) VALUES
(1, '0', '2018-04-27 22:52:11', 1, NULL, 1),
(2, 'stef.laconi@gmail.com', '2018-05-03 16:13:59', 1, NULL, 1),
(3, 'stefania.laconi@tiscali.it', '2018-05-03 16:14:17', 1, NULL, 1),
(4, 'sick_girl@live.it', '2018-05-22 22:19:59', 1, NULL, 1),
(5, 'pireddakatia@gmail.com', '2018-05-23 22:00:00', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `countries`
--

CREATE TABLE `countries` (
  `country_name` varchar(255) NOT NULL,
  `country_code` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `countries`
--

INSERT INTO `countries` (`country_name`, `country_code`) VALUES
('Andorra', 'AD'),
('United Arab Emirates', 'AE'),
('Afghanistan', 'AF'),
('Antigua and Barbuda', 'AG'),
('Anguilla', 'AI'),
('Albania', 'AL'),
('Armenia', 'AM'),
('Netherlands Antilles', 'AN'),
('Angola', 'AO'),
('Antarctica', 'AQ'),
('Argentina', 'AR'),
('American Samoa', 'AS'),
('Austria', 'AT'),
('Australia', 'AU'),
('Aruba', 'AW'),
('Azerbaijan', 'AZ'),
('Bosnia and Herzegovina', 'BA'),
('Barbados', 'BB'),
('Bangladesh', 'BD'),
('Belgium', 'BE'),
('Burkina Faso', 'BF'),
('Bulgaria', 'BG'),
('Bahrain', 'BH'),
('Burundi', 'BI'),
('Benin', 'BJ'),
('Bermuda', 'BM'),
('Brunei Darussalam', 'BN'),
('Bolivia', 'BO'),
('Brazil', 'BR'),
('Bahamas', 'BS'),
('Bhutan', 'BT'),
('Bouvet Island', 'BV'),
('Botswana', 'BW'),
('Belarus', 'BY'),
('Belize', 'BZ'),
('Canada', 'CA'),
('Cocos (Keeling) Islands', 'CC'),
('Congo, the Democratic Republic of the', 'CD'),
('Central African Republic', 'CF'),
('Congo', 'CG'),
('Switzerland', 'CH'),
('Cote D\'Ivoire', 'CI'),
('Cook Islands', 'CK'),
('Chile', 'CL'),
('Cameroon', 'CM'),
('China', 'CN'),
('Colombia', 'CO'),
('Costa Rica', 'CR'),
('Cape Verde', 'CV'),
('Curacao', 'CW'),
('Christmas Island', 'CX'),
('Cyprus', 'CY'),
('Czech Republic', 'CZ'),
('Germany', 'DE'),
('Djibouti', 'DJ'),
('Denmark', 'DK'),
('Dominica', 'DM'),
('Dominican Republic', 'DO'),
('Algeria', 'DZ'),
('Ecuador', 'EC'),
('Estonia', 'EE'),
('Egypt', 'EG'),
('Western Sahara', 'EH'),
('Eritrea', 'ER'),
('Spain', 'ES'),
('Ethiopia', 'ET'),
('Finland', 'FI'),
('Fiji', 'FJ'),
('Falkland Islands (Malvinas)', 'FK'),
('Micronesia, Federated States of', 'FM'),
('Faroe Islands', 'FO'),
('France', 'FR'),
('Gabon', 'GA'),
('United Kingdom (UK)', 'GB'),
('Grenada', 'GD'),
('Georgia', 'GE'),
('French Guiana', 'GF'),
('Guernsey', 'GG'),
('Ghana', 'GH'),
('Gibraltar', 'GI'),
('Greenland', 'GL'),
('Gambia', 'GM'),
('Guinea', 'GN'),
('Guadeloupe', 'GP'),
('Equatorial Guinea', 'GQ'),
('Greece', 'GR'),
('South Georgia and the South Sandwich Islands', 'GS'),
('Guatemala', 'GT'),
('Guam', 'GU'),
('Guinea-Bissau', 'GW'),
('Guyana', 'GY'),
('Hong Kong', 'HK'),
('Heard Island and Mcdonald Islands', 'HM'),
('Honduras', 'HN'),
('Croatia', 'HR'),
('Haiti', 'HT'),
('Hungary', 'HU'),
('Indonesia', 'ID'),
('Ireland', 'IE'),
('Israel', 'IL'),
('Isle of Man', 'IM'),
('India', 'IN'),
('British Indian Ocean Territory', 'IO'),
('Iraq', 'IQ'),
('Iceland', 'IS'),
('Italy', 'IT'),
('Jersey', 'JE'),
('Jamaica', 'JM'),
('Jordan', 'JO'),
('Japan', 'JP'),
('Kenya', 'KE'),
('Kyrgyzstan', 'KG'),
('Cambodia', 'KH'),
('Kiribati', 'KI'),
('Comoros', 'KM'),
('Saint Kitts and Nevis', 'KN'),
('Korea, Republic of', 'KR'),
('Kuwait', 'KW'),
('Cayman Islands', 'KY'),
('Kazakhstan', 'KZ'),
('Lao People\'s Democratic Republic', 'LA'),
('Lebanon', 'LB'),
('Saint Lucia', 'LC'),
('Liechtenstein', 'LI'),
('Sri Lanka', 'LK'),
('Liberia', 'LR'),
('Lesotho', 'LS'),
('Lithuania', 'LT'),
('Luxembourg', 'LU'),
('Latvia', 'LV'),
('Libyan Arab Jamahiriya', 'LY'),
('Morocco', 'MA'),
('Monaco', 'MC'),
('Moldova, Republic of', 'MD'),
('Montenegro', 'ME'),
('Sint Maarten', 'MF'),
('Madagascar', 'MG'),
('Marshall Islands', 'MH'),
('Macedonia, the Former Yugoslav Republic of', 'MK'),
('Mali', 'ML'),
('Myanmar', 'MM'),
('Mongolia', 'MN'),
('Macao', 'MO'),
('Northern Mariana Islands', 'MP'),
('Martinique', 'MQ'),
('Mauritania', 'MR'),
('Montserrat', 'MS'),
('Malta', 'MT'),
('Mauritius', 'MU'),
('Maldives', 'MV'),
('Malawi', 'MW'),
('Mexico', 'MX'),
('Malaysia', 'MY'),
('Mozambique', 'MZ'),
('Namibia', 'NA'),
('New Caledonia', 'NC'),
('Niger', 'NE'),
('Norfolk Island', 'NF'),
('Nigeria', 'NG'),
('Nicaragua', 'NI'),
('Netherlands', 'NL'),
('Norway', 'NO'),
('Nepal', 'NP'),
('Nauru', 'NR'),
('Niue', 'NU'),
('New Zealand', 'NZ'),
('Oman', 'OM'),
('Panama', 'PA'),
('Peru', 'PE'),
('French Polynesia', 'PF'),
('Papua New Guinea', 'PG'),
('Philippines', 'PH'),
('Pakistan', 'PK'),
('Poland', 'PL'),
('Saint Pierre and Miquelon', 'PM'),
('Pitcairn', 'PN'),
('Puerto Rico', 'PR'),
('Palestinian Territory, Occupied', 'PS'),
('Portugal', 'PT'),
('Palau', 'PW'),
('Paraguay', 'PY'),
('Qatar', 'QA'),
('Reunion', 'RE'),
('Romania', 'RO'),
('Serbia', 'RS'),
('Russian Federation', 'RU'),
('Rwanda', 'RW'),
('Saudi Arabia', 'SA'),
('Solomon Islands', 'SB'),
('Seychelles', 'SC'),
('Sudan', 'SD'),
('Sweden', 'SE'),
('Singapore', 'SG'),
('Saint Helena', 'SH'),
('Slovenia', 'SI'),
('Svalbard and Jan Mayen', 'SJ'),
('Slovakia', 'SK'),
('Sierra Leone', 'SL'),
('San Marino', 'SM'),
('Senegal', 'SN'),
('Somalia', 'SO'),
('Suriname', 'SR'),
('Sao Tome and Principe', 'ST'),
('El Salvador', 'SV'),
('Syrian Arab Republic', 'SY'),
('Swaziland', 'SZ'),
('Turks and Caicos Islands', 'TC'),
('Chad', 'TD'),
('French Southern Territories', 'TF'),
('Togo', 'TG'),
('Thailand', 'TH'),
('Tajikistan', 'TJ'),
('Tokelau', 'TK'),
('Timor-Leste', 'TL'),
('Turkmenistan', 'TM'),
('Tunisia', 'TN'),
('Tonga', 'TO'),
('Turkey', 'TR'),
('Trinidad and Tobago', 'TT'),
('Tuvalu', 'TV'),
('Taiwan', 'TW'),
('Tanzania', 'TZ'),
('Ukraine', 'UA'),
('Uganda', 'UG'),
('United States Minor Outlying Islands', 'UM'),
('United States (USA)', 'US'),
('Uruguay', 'UY'),
('Uzbekistan', 'UZ'),
('Vatican City', 'VA'),
('Saint Vincent and the Grenadines', 'VC'),
('Venezuela', 'VE'),
('Virgin Islands, British', 'VG'),
('Virgin Islands, U.S.', 'VI'),
('Vietnam', 'VN'),
('Vanuatu', 'VU'),
('Wallis and Futuna', 'WF'),
('Samoa', 'WS'),
('Yemen', 'YE'),
('Mayotte', 'YT'),
('South Africa', 'ZA'),
('Zambia', 'ZM'),
('Zimbabwe', 'ZW');

-- --------------------------------------------------------

--
-- Struttura della tabella `coupon`
--

CREATE TABLE `coupon` (
  `id_coupon` int(11) NOT NULL,
  `data_scadenza_coupon` datetime DEFAULT NULL,
  `importo_coupon` double NOT NULL,
  `percentuale_coupon` int(5) NOT NULL,
  `codice_coupon` varchar(250) NOT NULL,
  `stato_coupon` int(11) NOT NULL,
  `tipo_coupon` int(11) NOT NULL,
  `utilizzatore_coupon` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `coupon`
--

INSERT INTO `coupon` (`id_coupon`, `data_scadenza_coupon`, `importo_coupon`, `percentuale_coupon`, `codice_coupon`, `stato_coupon`, `tipo_coupon`, `utilizzatore_coupon`) VALUES
(1, '2018-06-30 00:00:00', 0, 15, 'BYVFLBSH', 3, 2, 'roberto.rossi77@gmail.com'),
(3, '2018-06-30 00:00:00', 0, 10, 'VWUIKTTH', 1, 4, ''),
(4, '2018-06-30 00:00:00', 0, 20, 'DLDHSSQD', 1, 4, ''),
(5, '2018-06-30 00:00:00', 0, 30, 'YGOJTVYE', 1, 2, '');

-- --------------------------------------------------------

--
-- Struttura della tabella `email_templates`
--

CREATE TABLE `email_templates` (
  `id_template` int(11) NOT NULL,
  `nome_template` varchar(250) NOT NULL,
  `subject_template` varchar(250) NOT NULL,
  `titolo_template` varchar(250) NOT NULL,
  `testo_template` text NOT NULL,
  `id_tipo_template` int(11) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `email_templates`
--

INSERT INTO `email_templates` (`id_template`, `nome_template`, `subject_template`, `titolo_template`, `testo_template`, `id_tipo_template`, `lingua_traduzione_id`) VALUES
(1, 'Ordini ', 'Ma Chlò order', 'Ma Chlò order', 'Grazie per il tuo ordine e benvenuto nel mondo Ma Chlò!\nStiamo elaborando la tua richiesta e riceverai presto il tracking con il dettaglio di spedizione.\nGrazie.\n\nMa Chlò Team', 3, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'customers', 'Customer of ecommerce shop');

-- --------------------------------------------------------

--
-- Struttura della tabella `home_slider`
--

CREATE TABLE `home_slider` (
  `hs_id` int(11) NOT NULL,
  `hs_image` varchar(250) NOT NULL,
  `hs_order` int(11) NOT NULL,
  `hs_title` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `home_slider`
--

INSERT INTO `home_slider` (`hs_id`, `hs_image`, `hs_order`, `hs_title`, `id_lingua`) VALUES
(1, '02.jpg', 1, 'Colora le emozioni con<br>Ma Chlò', 1),
(2, '03.jpg', 2, '', 1),
(3, '02_en.jpg', 1, 'Paint emotions with<br>Ma Chlò', 2),
(4, '01.jpg', 3, '', 1),
(5, '03_en.jpg', 2, '', 2),
(6, '01_en.jpg', 3, '', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `immagini_gallery`
--

CREATE TABLE `immagini_gallery` (
  `id_ig` int(11) NOT NULL,
  `id_categoria_ig` int(11) NOT NULL,
  `nome_ig` varchar(250) NOT NULL,
  `stato_ig` tinyint(4) NOT NULL,
  `immagine_thumb_ig` varchar(250) NOT NULL,
  `immagine_ig` varchar(250) NOT NULL,
  `ordine_ig` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `immagini_gallery`
--

INSERT INTO `immagini_gallery` (`id_ig`, `id_categoria_ig`, `nome_ig`, `stato_ig`, `immagine_thumb_ig`, `immagine_ig`, `ordine_ig`) VALUES
(1, 1, 'Uno', 2, '', '', 0),
(2, 2, 'Due', 2, '', '', 0),
(3, 3, 'Tre', 2, '', '', 2),
(4, 1, 'Quattro', 2, '', '', 4),
(5, 4, 'Masks t-shirt', 1, 'e2b4b-masks_thumbs.jpg', '4821d-masks.jpg', 5),
(6, 5, 'Il bacio', 1, 'e39d0-il_bacio_thumbs.jpg', '91ab5-il_bacio.jpg', 67),
(7, 5, 'Il pensiero', 1, '4e001-il_pensiero_thumbs.jpg', 'd8a1f-il_pensiero.jpg', 68),
(8, 5, 'Il silenzio', 1, 'cfea9-il_silenzio_thumbs.jpg', '4a5d6-il_silenzio.jpg', 64),
(9, 5, 'Incontro', 1, '423c4-incontro_thumbs.jpg', '0de31-incontro.jpg', 70),
(10, 5, 'Infinito', 1, 'eb615-infinito_thumbs.jpg', '8772b-infinito.jpg', 65),
(11, 5, 'La separazione', 1, '57b01-la_separazione_thumbs.jpg', '872d0-la_separazione.jpg', 63),
(12, 5, 'Lo sguardo', 1, 'bfc64-lo_sguardo_thumbs.jpg', '3f39f-lo_sguardo.jpg', 69),
(13, 5, 'Un nuovo inizio', 1, 'bcd37-un_nuovo_inizio_thumbs.jpg', '20947-un_nuovo_inizio.jpg', 66),
(14, 2, 'Stefania e Francesca', 1, 'dcf66-stefania_e_francesca_thumbs.jpg', '9011d-stefania_e_francesca.jpg', 5),
(15, 4, 'Girls', 1, '33725-girls3_thumbs.jpg', '9f1d8-girls3.jpg', 4),
(16, 5, 'Colored tree', 1, '74c28-colored_tree_thumbs.jpg', '75e15-colored_tree.jpg', 19),
(17, 5, 'Dreaming on a colored tree', 1, 'a3af3-dreaming_on_a_colored_tree_thumbs.jpg', 'e8d08-dreaming_on_a_colored_tree.jpg', 18),
(18, 5, 'Life o a tree', 1, '70a75-life_of_a_tree_thumbs.jpg', 'bf81e-life_of_a_tree.jpg', 17),
(19, 5, 'Flowers', 1, '24fe6-flowers_thumbs.jpg', '5c9c8-flowers.jpg', 23),
(20, 5, 'Monkeys into the jungle', 1, '9130f-monkeys_into_the_jungle_thumbs.jpg', 'b9e80-monkeys_into_the_jungle.jpg', 21),
(21, 5, 'Reaching for the moon', 1, 'dda19-reaching_for_the_moon_thumbs.jpg', 'c8c87-reaching_for_the_moon.jpg', 16),
(22, 1, 'Mug', 1, '9a7c2-a_monkey_playing_with_a_snake_thumbs.jpg', '50a7c-a_monkey_playing_with_a_snake.jpg', 0),
(23, 4, 'Jungle flowers dress', 1, '6ecd8-jungle_flowers_dress_thumbs.jpg', '13d3d-jungle_flowers_dress.jpg', 3),
(24, 4, 'Ma Chlò', 1, '4ed09-machlo_thumbs.jpg', 'b23ae-machlo.jpg', 5),
(25, 4, 'Lips t-shirt', 1, '7d661-lips_tshirt_thumbs.jpg', 'b51f7-lips_tshirt.jpg', 4),
(26, 5, 'a woman with a monkey', 1, '8cdda-a_woman_with_a_monkey_thumbs.jpg', '36eff-a_woman_with_a_monkey.jpg', 20),
(27, 5, 'A monkey playing with a snake', 1, '3cbcb-a_monkey_playing_a_snake_thumbs.jpg', '0d177-a_monkey_playing_with_a_snake1.jpg', 22),
(28, 5, 'Gallinella sarda', 1, 'd7dc8-gallinella_sarda_thumbs.jpg', '2a1c2-gallinella_sarda.jpg', 42),
(29, 5, 'Snakes', 1, 'e5773-snakes_thumbs.jpg', '1a470-snakes.jpg', 40),
(30, 5, 'Colorful town', 1, 'd8a8d-colorful_town_thumbs.jpg', '56766-colorful_town.jpg', 30),
(31, 5, 'Wild girl', 1, '79bcc-wild_girl_thumbs.jpg', '292c9-wild_girl.jpg', 29),
(32, 5, 'Talking', 1, '710aa-talking_thumbs.jpg', '7ebb2-talking.jpg', 18),
(33, 5, 'Sleeping mask', 1, '5f62b-sleeping_mask_thumbs.jpg', '881da-sleeping_mask.jpg', 51),
(34, 5, 'geometric', 1, '762ff-geometric_thumbs.jpg', '2b416-geometric.jpg', 22),
(35, 5, 'Eclipse', 1, 'c4580-eclipse_thumbs.jpg', '11f66-eclipse.jpg', 62),
(36, 5, 'Jellyfish', 1, '38a22-jellyfish_thumbs.jpg', 'ab602-jellyfish.jpg', 61),
(37, 5, 'Japan', 1, 'b0921-japan_thumbs.jpg', 'b3042-japan.jpg', 60),
(38, 5, 'Explosion', 1, 'eedac-explosion_thumbs.jpg', 'a5716-explosion.jpg', 59),
(39, 5, 'Kaos', 1, '8f7cc-kaos_thumbs.jpg', 'ce2c1-kaos.jpg', 58),
(40, 5, 'Rain', 1, '208d8-rain_thumbs.jpg', 'bcdd1-rain.jpg', 57),
(41, 5, 'Stand by', 1, '8c5ca-standby_thumbs.jpg', '9c3e8-standby.jpg', 56),
(42, 5, 'Storm', 1, 'a9516-storm_thumbs.jpg', '9b09f-storm.jpg', 55),
(43, 5, 'Yang', 1, '6c2da-yang_thumbs.jpg', '94d1e-yang.jpg', 54),
(44, 5, 'Universe', 1, '73e62-universe_thumbs.jpg', '7fe16-universe.jpg', 53),
(45, 1, 'Lips dress', 1, 'ed97d-lips_dress_thumbs-2-.jpg', 'e8a69-lips_dress.jpg', 0),
(46, 1, 'Masks', 1, 'd5915-tshirt_masks_thumbs.jpg', '1dcc2-tshirt_masks.jpg', 2),
(47, 1, 'Yoga leggings', 1, '056de-yoga_leggings_thumbs.jpg', 'f230e-yoga_leggings.jpg', 5),
(48, 4, 'Jumping', 1, 'eb42e-jumping_thumbs.jpg', '36987-jumping.jpg', 8),
(49, 1, 'Love story masks', 1, '18daa-un_nuovo_inizio_tshirt_thumbs-2-.jpg', '119d0-un_nuovo_inizio_tshirt.jpg', 10);

-- --------------------------------------------------------

--
-- Struttura della tabella `immagini_gallery_traduzioni`
--

CREATE TABLE `immagini_gallery_traduzioni` (
  `id_immagini_gallery_traduzioni` int(11) NOT NULL,
  `titolo_ig` varchar(250) NOT NULL,
  `testo_ig` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `id_ig` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `immagini_gallery_traduzioni`
--

INSERT INTO `immagini_gallery_traduzioni` (`id_immagini_gallery_traduzioni`, `titolo_ig`, `testo_ig`, `id_lingua`, `id_ig`) VALUES
(1, 'Titolo 1', 'TEsto 1', 1, 1),
(2, 'Titolo 1', 'Text 1', 2, 1),
(3, 'Titolo 2', 'Testo 2', 1, 2),
(4, 'Title 2', 'Text 3', 2, 2),
(5, 'Titolo 3', 'Testo 3', 1, 3),
(6, 'Title 3', 'Text 3', 2, 3),
(7, 'Titolo 4', 'Testo 4', 1, 4),
(8, 'Title 4', 'Text 4', 2, 4),
(10, 'Masks ', 'T-shirt con maschere', 1, 5),
(11, 'Masks', 'Masks t-shirt', 2, 5),
(12, 'Il bacio', 'Dipinto il bacio', 1, 6),
(13, 'Il bacio', 'Il bacio painting', 2, 6),
(14, 'Il pensiero', 'Dipinto il pensiero', 1, 7),
(15, 'Il pensiero', 'Il pensiero painting', 2, 7),
(16, 'Il silenzio', 'Dipinto il silenzio', 1, 8),
(17, 'Il silenzio', 'Il silenzio painting', 2, 8),
(18, 'Incontro', 'Dipinto incontro', 1, 9),
(19, 'Incontro', 'Incontro painting', 2, 9),
(20, 'Infinito', 'Dipinto infinito', 1, 10),
(21, 'Infinito', 'Infinito painting', 2, 10),
(22, 'La separazione', 'Dipinto la separazione', 1, 11),
(23, 'La separazione', 'La separazione painting', 2, 11),
(24, 'Lo sguardo', 'Dipinto lo sguardo', 1, 12),
(25, 'Lo sguardo', 'Lo sguardo painting', 2, 12),
(26, 'Un nuovo inizio', 'Dipinto un nuovo inizio', 1, 13),
(27, 'Un nuovo inizio', 'Un nuovo inizio painting', 2, 13),
(28, 'Stefania e Francesca', 'Ritratto di Stefania e Francesca', 1, 14),
(29, 'Stefania e Francesca', 'Stefania and Francesca portrait', 2, 14),
(30, 'Girls', 'Ritratto Ma Chlò girls', 1, 15),
(31, 'Girls', 'Ma Chlò girls', 2, 15),
(32, 'Colored tree', 'Dipinto colored tree', 1, 16),
(33, 'Colored tree', 'Colored tree painting', 2, 16),
(34, 'Dreaming on a colored tree', 'Dipinto dreaming on a colored tree', 1, 17),
(35, 'Dreaming on a colored tree', 'Dreaming on a colored tree painting', 2, 17),
(36, 'Life of a tree', 'Dipinto life of a tree', 1, 18),
(37, 'Life of a tree', 'Life of a tree painting', 2, 18),
(38, 'Flowers', 'Dipinto flowers', 1, 19),
(39, 'Flowers', 'Flowers painting', 2, 19),
(40, 'Monkeys into the jungle', 'Dipinto monkeys into the jungle', 1, 20),
(41, 'Monkeys into the jungle', 'Monkeys into the jungle painting', 2, 20),
(42, 'Reaching for the moon', 'Dipinto reaching for the moon', 1, 21),
(43, 'Reaching for the moon', 'Reaching for the moon painting', 2, 21),
(44, 'Mug', 'tazza in ceramica', 1, 22),
(45, 'Mug', 'Ceramic mug', 2, 22),
(46, 'Jungle flowers', 'Jungle flowers dress', 1, 23),
(47, 'Jungle flowers', 'Jungle flowers dress', 2, 23),
(48, 'Ma Chlò', 'Foto Ma Chlò', 1, 24),
(49, 'Ma Chlò', 'Ma Chlò portrait', 2, 24),
(50, 'Lips ', 'Lips T-shirt', 1, 25),
(51, 'Lips', 'Lips T-shirt', 2, 25),
(52, 'a woman with a monkey', 'Dipinto a woman with a monkey', 1, 26),
(53, 'a woman with a monkey', 'A woman with a monkey painting', 2, 26),
(54, 'A monkey playing with a snake', 'Dipinto a monkey playing with a snake', 1, 27),
(55, 'A monkey playing with a snake', 'A monkey playing with a snake painting', 2, 27),
(56, 'Gallinella sarda', 'Dipinto gallinella sarda', 1, 28),
(57, 'Gallinella sarda', 'Gallinella sarda painting', 2, 28),
(58, 'Snakes', 'Dipinto snakes', 1, 29),
(59, 'Snakes', 'Snakes painting', 2, 29),
(60, 'Colorful town', 'Dipinto colorful town', 1, 30),
(61, 'Colorful town', 'Colorful town painting', 2, 30),
(62, 'Wild girl', 'Dipinto wild girl', 1, 31),
(63, 'Wild girl', 'Wild girl painting', 2, 31),
(64, 'Talking', 'Dipinto talking', 1, 32),
(65, 'Talking', 'Talking painting', 2, 32),
(66, 'Sleeping mask', 'Dipinto sleeping mask', 1, 33),
(67, 'Sleeping mask', 'Sleeping mask painting', 2, 33),
(68, 'Geometric', 'Dipinto geometric', 1, 34),
(69, 'Geometric', 'Geometric painting', 2, 34),
(70, 'Eclipse', 'Dipinto eclipse', 1, 35),
(71, 'Eclipse', 'Eclipse painting', 2, 35),
(72, 'Jellyfish', 'Dipinto jellyfish', 1, 36),
(73, 'Jellyfish', 'Jellyfish painting', 2, 36),
(74, 'Japan', 'Dipinto japan', 1, 37),
(75, 'Japan', 'Japan painting', 2, 37),
(76, 'Explosion', 'Dipinto explosion', 1, 38),
(77, 'Explosion', 'Explosion painting', 2, 38),
(78, 'Kaos', 'Dipinto kaos', 1, 39),
(79, 'Kaos', 'Kaos painting', 2, 39),
(80, 'Rain', 'Dipinto rain', 1, 40),
(81, 'Rain', 'Rain painting', 2, 40),
(82, 'Stand by', 'Dipinto stand by', 1, 41),
(83, 'Stand by', 'Stand by painting', 2, 41),
(84, 'Storm', 'Dipinto storm', 1, 42),
(85, 'Storm', 'Storm painting', 2, 42),
(86, 'Yang', 'Dipinto yang', 1, 43),
(87, 'Yang', 'Yang painting', 2, 43),
(88, 'Universe', 'Dipinto universe', 1, 44),
(89, 'Universe', 'Universe painting', 2, 44),
(91, 'Lips', 'Abito lips', 1, 45),
(92, 'Lips', 'Lips dress', 2, 45),
(93, 'Masks', 'T-shirt con maschere', 1, 46),
(94, 'Masks', 'Masks t-shirt', 2, 46),
(95, 'Yoga leggings', 'Yoga leggings eclipse', 1, 47),
(96, 'Yoga leggings', 'Eclipse yoga leggings', 2, 47),
(97, 'Jumping', 'Lips t-shirt', 1, 48),
(98, 'Jumping', 'Lips t-shirt', 2, 48),
(99, 'Love story masks', 'T-shirt love story masks', 1, 49),
(100, 'Love story masks', 'Love story masks t-shirt', 2, 49);

-- --------------------------------------------------------

--
-- Struttura della tabella `indirizzo_fatturazione`
--

CREATE TABLE `indirizzo_fatturazione` (
  `id_indirizzo_fatturazione` bigint(20) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `indirizzo_fatt` varchar(255) DEFAULT NULL,
  `civico_fatt` varchar(15) DEFAULT NULL,
  `cap_fatt` varchar(10) DEFAULT NULL,
  `nazione_fatt` varchar(50) DEFAULT NULL,
  `citta_fatt` varchar(50) DEFAULT NULL,
  `riferimento_fatt` varchar(250) DEFAULT NULL,
  `note_fatt` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `indirizzo_fatturazione`
--

INSERT INTO `indirizzo_fatturazione` (`id_indirizzo_fatturazione`, `id_cliente`, `indirizzo_fatt`, `civico_fatt`, `cap_fatt`, `nazione_fatt`, `citta_fatt`, `riferimento_fatt`, `note_fatt`) VALUES
(1, 1, 'Via Valentino Mazzola,  2', '', '00142', 'Italia', 'Roma', '', 'Via Valentino Mazzola'),
(2, 0, '0', '0', '0', '0', '0', '0', '0'),
(3, 0, '0', '0', '0', '0', '0', '0', '0'),
(4, 2, 'via Agrigento', '38/b', '07026', 'IT', 'Olbia', '', ''),
(5, 3, 'via bramante', '64', '07026', 'IT', 'olbia', 'katia piredda', ''),
(6, 5, 'Via Milano', '9', '07020', 'IT', 'Monti', 'Tiziana Laconi', ''),
(7, 0, 'via agrigento', '38/b', '07026', 'IT', 'olbia', 'stefania laconi', ''),
(8, 0, 'via agrigento', '38/b', '07026', 'IT', 'olbia', 'stefania laconi', ''),
(9, 6, 'Viale Mani dal Naso', '114', '00166', 'MH', 'Roma', 'Roberto', ''),
(10, 7, 'Via Algida', '47', '558', 'IS', 'Helsinki', 'TEst Con', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `indirizzo_spedizione`
--

CREATE TABLE `indirizzo_spedizione` (
  `id_indirizzo_spedizione` bigint(20) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `indirizzo_sped` varchar(255) DEFAULT NULL,
  `civico_sped` varchar(15) DEFAULT NULL,
  `cap_sped` varchar(10) DEFAULT NULL,
  `nazione_sped` varchar(50) DEFAULT NULL,
  `citta_sped` varchar(50) DEFAULT NULL,
  `riferimento_sped` varchar(250) DEFAULT NULL,
  `note_sped` varchar(100) DEFAULT NULL,
  `flag_predefinito_sped` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue`
--

CREATE TABLE `lingue` (
  `id_lingue` int(11) NOT NULL,
  `nome_lingue` varchar(50) NOT NULL,
  `abbr_lingue` varchar(10) NOT NULL,
  `locale_paypal_lingue` varchar(10) NOT NULL,
  `codice_ci` varchar(150) NOT NULL,
  `stato_lingua` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue`
--

INSERT INTO `lingue` (`id_lingue`, `nome_lingue`, `abbr_lingue`, `locale_paypal_lingue`, `codice_ci`, `stato_lingua`) VALUES
(1, 'ITALIANO', 'IT', 'it_IT', 'italian', 1),
(2, 'ENGLISH', 'EN', 'en_GB', 'english', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue_labels_lang`
--

CREATE TABLE `lingue_labels_lang` (
  `id_lingue_labels_lang` int(11) NOT NULL,
  `lingue_labels_lang_label` varchar(250) NOT NULL,
  `lingue_labels_lang_value` text NOT NULL,
  `lingue_labels_lang_desc` varchar(250) NOT NULL,
  `lingue_labels_lang_type` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue_labels_lang`
--

INSERT INTO `lingue_labels_lang` (`id_lingue_labels_lang`, `lingue_labels_lang_label`, `lingue_labels_lang_value`, `lingue_labels_lang_desc`, `lingue_labels_lang_type`, `id_lingua`) VALUES
(1, 'HOME_CATEGORY_TITLE', 'Scopri i nostri prodotti', 'Intestazione sezione categorie home page', 'frontend', 1),
(2, 'HOME_SHOW_ALL_PRODUCTS', 'Vedi tutti i prodotti', 'Testo pulsante collegamento ai prodotti Home Page', 'frontend', 1),
(3, 'HOME_FEATURE_SHIPPING_TITLE', 'Spedizione in tutto il mondo', 'Testo icona mappa Home Page', 'frontend', 1),
(4, 'HOME_FEATURE_SHIPPING_DESC', 'Spedizione gratuita in 7/10 giorni', 'Descrizione icona mappa Home Page', 'frontend', 1),
(5, 'HOME_FEATURE_QUALITY_TITLE', 'Qualità garantita', 'Testo icona frecce Home Page', 'frontend', 1),
(6, 'HOME_FEATURE_QUALITY_DESC', 'La garanzia dei nostri prodotti', 'Descrizione icona frecce Home Page', 'frontend', 1),
(7, 'HOME_FEATURE_SUPPORT_TITLE', 'Supporto Online', 'Testo icona supporto Home Page', 'frontend', 1),
(8, 'HOME_FEATURE_SUPPORT_DESC', 'Supporto clienti Online', 'Descrizione icona supporto Home Page', 'frontend', 1),
(9, 'HOME_FEATURE_PAYMENTS_TITLE', 'Pagamenti sicuri', 'Testo icona pagamento Home Page', 'frontend', 1),
(10, 'HOME_FEATURE_PAYMENTS_DESC', 'Pagamenti sicuri con protocollo SSL', 'Descrizione icona pagamento Home Page', 'frontend', 1),
(11, 'FOOTER_HELP', 'Hai bisogno di aiuto? Contattaci', 'Testo footer help sinistra', 'frontend', 1),
(12, 'FOOTER_NEWSLTTER_TITLE', 'NEWSLETTER', 'NEWSLETTER', 'frontend', 1),
(13, 'FOOTER_NEWSLTTER_DESC', 'Ricevi le offerte e gli sconti riservati ai clienti.', 'Testo footer newsletter', 'frontend', 1),
(14, 'FOOTER_NEWSLTTER_INPUT', 'Inserisci la tua e-mail', 'Etichetta campo newsletter Footer', 'frontend', 1),
(15, 'FOOTER_PAYMENTS_TITLE', 'METODI DI PAGAMENTO', 'Testo pagamenti footer destra', 'frontend', 1),
(16, 'FOOTER_PAYMENTS_DESC', 'Con noi puoi pagare con i principali metodi di pagamento.', 'Descrizione pagamenti footer destra', 'frontend', 1),
(17, 'LABEL_FILTER', 'Filtra', 'Filtra', 'frontend', 1),
(18, 'LABEL_SEARCH', 'Cerca', 'Cerca', 'frontend', 1),
(19, 'LABEL_ORDER', 'Ordina per', 'Ordina per', 'frontend', 1),
(20, 'LABEL_DEFAULT', 'Default', 'Default', 'frontend', 1),
(21, 'LABEL_SALE', 'Offerta', 'Offerta', 'frontend', 1),
(22, 'LABEL_NEW', 'Novità', 'Novità', 'frontend', 1),
(23, 'LABEL_FEEDBACK', 'Voto medio', 'Voto medio', 'frontend', 1),
(24, 'LABEL_BEST', 'Best seller', 'Best seller', 'frontend', 1),
(25, 'LABEL_ALPHA', 'Alfabetico', 'Alfabetico', 'frontend', 1),
(26, 'LABEL_PRICE', 'Prezzo', 'Prezzo', 'frontend', 1),
(27, 'LABEL_TAGS', 'Tags', 'Tags', 'frontend', 1),
(28, 'LABEL_SEE_MORE', 'Vedi altri', 'Vedi altri', 'frontend', 1),
(29, 'LABEL_ADD_TO_CART', 'Aggiungi', 'Aggiungi', 'frontend', 1),
(30, 'LABEL_ADD_TO_WHISH', 'Aggiungi alla Whishlist', 'Aggiungi alla Whishlist', 'frontend', 1),
(31, 'LABEL_ALL', 'Tutti', 'Tutti', 'frontend', 1),
(32, 'LABEL_MY_ACCOUNT', 'Il mio profilo', 'Il mio profilo', 'frontend', 1),
(33, 'LABEL_USER_ACCOUNT', 'Account utente', 'Account utente', 'frontend', 1),
(34, 'LABEL_USER_REGISTER', 'Registrazione', 'Registrazione', 'frontend', 1),
(35, 'LABEL_USER_SIGN_UP', 'Iscriviti', 'Iscriviti', 'frontend', 1),
(36, 'LABEL_USER_LOGIN', 'Effettua il Login', 'Effettua il Login', 'frontend', 1),
(37, 'LABEL_UPDATE', 'Salva', 'Salva', 'frontend', 1),
(38, 'LABEL_GOT', 'Hai', 'Hai', 'frontend', 1),
(39, 'LABEL_POINTS', 'punti', 'punti', 'frontend', 1),
(40, 'LABEL_POINTS_DESC', 'Puoi utilizzare i tuoi punti per acquistare prodotti dal nostro shop o accedere alle promozioni speciali.', 'Puoi utilizzare i tuoi punti per acquistare prodotti dal nostro shop o accedere alle promozioni speciali.', 'frontend', 1),
(41, 'LABEL_MANAGE_POINTS', 'Gestisci i tuoi punti', 'Gestisci i tuoi punti', 'frontend', 1),
(42, 'LABEL_BACK_SHOP', 'Torna allo Shop', 'Torna allo Shop', 'frontend', 1),
(43, 'LABEL_LOGOUT', 'Logout', 'Logout', 'frontend', 1),
(44, 'LABEL_PROFILE', 'Profilo', 'Profilo', 'frontend', 1),
(45, 'LABEL_ORDERS', 'Ordini', 'Ordini', 'frontend', 1),
(46, 'LABEL_ADDRESSES', 'Indirizzi di spedizione', 'Indirizzi di spedizione', 'frontend', 1),
(47, 'LABEL_WHISHLIST', 'Whishlist', 'Whishlist', 'frontend', 1),
(48, 'LABEL_NAME', 'Nome', 'Nome', 'frontend', 1),
(49, 'LABEL_SURNAME', 'Cognome', 'Cognome', 'frontend', 1),
(50, 'LABEL_EMAIL', 'Email', 'Email', 'frontend', 1),
(51, 'LABEL_PHONE', 'Telefono', 'Telefono', 'frontend', 1),
(52, 'LABEL_COUNTRY', 'Stato/Paese', 'Stato/Paese', 'frontend', 1),
(53, 'LABEL_CITY', 'Città', 'Città', 'frontend', 1),
(54, 'LABEL_ADDRESS', 'Indirizzo', 'Indirizzo', 'frontend', 1),
(55, 'LABEL_ADDRESS_REF', 'Riferimento spedizione c/o (es Mario Rossi)', 'Riferimento spedizione c/o (es Mario Rossi)', 'frontend', 1),
(56, 'LABEL_ADDRESS_REF_FATT', 'Riferimento fatturazione (persona o azienda)', 'Riferimento fatturazione (persona o azienda)', 'frontend', 1),
(57, 'LABEL_CIVICO', 'Civico', 'Civico', 'frontend', 1),
(58, 'LABEL_POSTAL_CODE', 'CAP', 'CAP', 'frontend', 1),
(59, 'LABEL_ADDRESS_NOTES', 'Note indirizzo', 'Note indirizzo', 'frontend', 1),
(60, 'LABEL_ORDER_NOTES', 'Note ordine', 'Note ordine', 'frontend', 1),
(61, 'LABEL_TOTAL_ORDER', 'Totale ordine', 'Totale ordine', 'frontend', 1),
(62, 'LABEL_TOTAL', 'Totale', 'Totale', 'frontend', 1),
(63, 'LABEL_QTY', 'Quantità', 'Quantità', 'frontend', 1),
(64, 'LABEL_SUBTOTAL_ORDER', 'Subtotale', 'Subtotale', 'frontend', 1),
(65, 'LABEL_TOTAL_CART', 'Totale carrello', 'Totale carrello', 'frontend', 1),
(66, 'LABEL_TOTAL_ORDER_NOTES', '* Note: il totale include i costi di spedizione', '* Note: il totale include i costi di spedizione', 'frontend', 1),
(67, 'LABEL_TOTAL_CART_NOTES', '* Note: il totale non include eventuali spese di spedizione. I costi di spedizione o consegna verranno calcolati nel checkout successivo', '* Note: il totale non include eventuali spese di spedizione. I costi di spedizione o consegna verranno calcolati nel checkout successivo', 'frontend', 1),
(68, 'LABEL_CONFIRM', 'CONFERMA', 'CONFERMA', 'frontend', 1),
(69, 'LABEL_REMOVE', 'ELIMINA', 'ELIMINA', 'frontend', 1),
(70, 'LABEL_BACK_TO_CART', 'RITORNA AL CARRELLO', 'RITORNA AL CARRELLO', 'frontend', 1),
(71, 'LABEL_UPDATE_CART', 'AGGIORNA IL CARRELLO', 'AGGIORNA IL CARRELLO', 'frontend', 1),
(72, 'LABEL_BACK_TO_SHOP', 'TORNA ALLO SHOP', 'TORNA ALLO SHOP', 'frontend', 1),
(73, 'LABEL_CART', 'Carrello', 'Carrello', 'frontend', 1),
(74, 'LABEL_CART_EMPTY', 'Il tuo carrello è vuoto !', 'Il tuo carrello è vuoto !', 'frontend', 1),
(75, 'LABEL_CHECKOUT', 'Checkout', 'Checkout', 'frontend', 1),
(76, 'LABEL_PAYMENT_METHOD', 'Modalità di pagamento', 'Modalità di pagamento', 'frontend', 1),
(77, 'LABEL_PAYMENT_METHOD_PAYPAL', 'Paypal', 'Paypal', 'frontend', 1),
(78, 'LABEL_PAYMENT_METHOD_CC', 'Carta di credito / Prepagata', 'Carta di credito / Prepagata', 'frontend', 1),
(79, 'LABEL_MESSAGE', 'Messaggio', 'Messaggio', 'frontend', 1),
(80, 'LABEL_SEND', 'INVIA', 'INVIA', 'frontend', 1),
(81, 'LABEL_PRODUCTS', 'prodotti', 'prodotti', 'frontend', 1),
(82, 'LABEL_CART_INFO_ACTUALLY_1', 'Attualmente ci sono', 'Attualmente ci sono', 'frontend', 1),
(83, 'LABEL_CART_INFO_ACTUALLY_2', 'nel carrello', 'nel carrello', 'frontend', 1),
(84, 'LABEL_SIZE', 'Tg', 'Tg', 'frontend', 1),
(85, 'LABEL_COLOR', 'Colore', 'Colore', 'frontend', 1),
(86, 'LABEL_CATEGORY', 'Categoria', 'Categoria', 'frontend', 1),
(87, 'LABEL_DESCRIPTION', 'Descrizione', 'Descrizione', 'frontend', 1),
(88, 'LABEL_REVIEWS', 'Commenti', 'Commenti', 'frontend', 1),
(89, 'LABEL_ALSO_LIKE', 'Ti potrebbero piacere', 'Ti potrebbero piacere', 'frontend', 1),
(90, 'LABEL_REFCODE', 'Rif', 'Rif', 'frontend', 1),
(91, 'LABEL_DETAIL', 'Dettaglio', 'Dettaglio', 'frontend', 1),
(92, 'LABEL_SHOPPING_CART', 'Carrello', 'Carrello', 'frontend', 1),
(93, 'LABEL_FRONT', 'FRONTE', 'FRONTE', 'frontend', 1),
(94, 'LABEL_BACK', 'RETRO', 'RETRO', 'frontend', 1),
(95, 'LABEL_AVAILABILITY', 'Disponibilità', 'Disponibilità', 'frontend', 1),
(96, 'LABEL_AVAILABILITY_HIGH', 'Alta', 'Alta', 'frontend', 1),
(97, 'LABEL_AVAILABILITY_LOW', 'Bassa', 'Bassa', 'frontend', 1),
(98, 'LABEL_404_MESSAGE', 'Oops.... la pagina richiesta non esiste !', 'Oops.... la pagina richiesta non esiste !', 'frontend', 1),
(99, 'LABEL_404_BTN', 'TORNA ALLA HOME', 'TORNA ALLA HOME', 'frontend', 1),
(100, 'LABEL_ORDER', 'Ordine', 'Ordine', 'frontend', 1),
(101, 'LABEL_YOUR_ACCOUNT', 'Il tuo account', 'Il tuo account', 'frontend', 1),
(102, 'LABEL_USER_ALREADY_REGISTERED', 'Sei già registrato ?', 'Sei già registrato ?', 'frontend', 1),
(103, 'LABEL_USER_NOTREGISTERED', 'Altrimenti puoi ordinare compilando i dati', 'Altrimenti puoi ordinare compilando i dati', 'frontend', 1),
(104, 'LABEL_USER_NOTREGISTERED_POINTS', 'non accumulerai punti e bonus riservati ai clienti registrati', 'non accumulerai punti e bonus riservati ai clienti registrati', 'frontend', 1),
(105, 'LABEL_COUPON', 'COUPON', 'COUPON', 'frontend', 1),
(106, 'LABEL_COUPON_HAVE', 'Hai un codice sconto?', 'Hai un codice sconto?', 'frontend', 1),
(107, 'LABEL_COUPON_INSERT', 'Inserisci il codice coupon', 'Inserisci il codice coupon', 'frontend', 1),
(108, 'LABEL_COUPON_APPLY', 'Applica coupon', 'Applica coupon', 'frontend', 1),
(109, 'LABEL_DISCOUNT', 'Sconto', 'Sconto', 'frontend', 1),
(110, 'LABEL_SHIPPING_THIS', 'Spedisci a questo indirizzo', 'Spedisci a questo indirizzo', 'frontend', 1),
(111, 'LABEL_SHIPPING_OTHER', 'Spedisci ad un altro indirizzo', 'Spedisci ad un altro indirizzo', 'frontend', 1),
(112, 'LABEL_NEW_ADDRESS', 'Nuovo indirizzo', 'Nuovo indirizzo', 'frontend', 1),
(113, 'LABEL_BILLING_ADDRESS', 'Indirizzo di fatturazione', 'Indirizzo di fatturazione', 'frontend', 1),
(114, 'LABEL_SHIPPING_ADDRESS', 'Indirizzo di spedizione', 'Indirizzo di spedizione', 'frontend', 1),
(115, 'LABEL_SHIPPING', 'Spedizione', 'Spedizione', 'frontend', 1),
(116, 'LABEL_CHANGE_PASSWORD', 'Cambio Password', 'Cambio Password', 'frontend', 1),
(117, 'LABEL_ORDER_DATE', 'Data ordine', 'Data ordine', 'frontend', 1),
(118, 'LABEL_STRIPE_DESC', 'Paga in tutta sicurezza con Stripe', 'Paga in tutta sicurezza con Stripe', 'frontend', 1),
(119, 'MSG_SEARCH_INSERT', 'Inserisci il testo da ricercare', 'Inserisci il testo da ricercare', 'frontend', 1),
(120, 'MSG_SAVE_NEWSLETTER', 'Iscrivimi alla newsletter', 'Iscrivimi alla newsletter', 'frontend', 1),
(121, 'MSG_SEND_CONTACT_US', 'Richiesta di contatto inviata con successo!<br/>Grazie.', 'Richiesta di contatto inviata con successo!<br/>Grazie.', 'frontend', 1),
(122, 'MSG_PAYPAL_NOTETOPAYER', 'L\'indirizzo di spedizione resterà quello inserito nel modulo di checkout e non quello indicato nel pagamento PayPal !', 'L\'indirizzo di spedizione resterà quello inserito nel modulo di checkout e non quello indicato nel pagamento PayPal !', 'frontend', 1),
(123, 'MSG_PAYPAL_CANCEL', 'Riprova ad effettuare il pagamemto !', 'Riprova ad effettuare il pagamemto !', 'frontend', 1),
(124, 'MSG_PAYPAL_ERROR', 'Errore durante il pagamento:', 'Errore durante il pagamento:', 'frontend', 1),
(125, 'MSG_NO_RESULT', 'Nessun risultato per ', 'Nessun risultato per ', 'frontend', 1),
(126, 'MSG_NO_RESULT_FILTER', 'Nessun risultato presente per i filtri selezionati.', 'Nessun risultato presente per i filtri selezionati.', 'frontend', 1),
(127, 'MSG_NO_SIZE_FOR_PRODUCTS_COLOR', 'Al momento non ci sono taglie disponibili per questo prodotto/colore', 'Al momento non ci sono taglie disponibili per questo prodotto/colore', 'frontend', 1),
(128, 'MSG_ORDER_SUCCESS', 'inserito con successo ! Grazie.', 'inserito con successo ! Grazie.', 'frontend', 1),
(129, 'MSG_ORDER_PAYMENT_ERROR', 'Errore durante il pagamento ordine. Per favore verifica il tuo ordine e riprova.', 'Errore durante il pagamento ordine. Per favore verifica il tuo ordine e riprova.', 'frontend', 1),
(130, 'MSG_SUCCESS_CONTACT', 'Il tuo messaggio è stato inviato correttamente. Grazie !', 'Il tuo messaggio è stato inviato correttamente. Grazie !', 'frontend', 1),
(131, 'MSG_FAILURE_CONTACT', 'Abbiamo riscontrato un problema nell\'invio del mesaggio. Riprova!', 'Abbiamo riscontrato un problema nell\'invio del mesaggio. Riprova!', 'frontend', 1),
(132, 'MSG_SUCCESS_NEWSLETTER', 'Adesso sei iscritto alla newsletter. Grazie !', 'Adesso sei iscritto alla newsletter. Grazie !', 'frontend', 1),
(133, 'MSG_UNIQUE_NEWSLETTER', 'L\'indirizzo email è già iscritto alla newsletter', 'L\'indirizzo email è già iscritto alla newsletter', 'frontend', 1),
(134, 'MSG_UNSUBSCRIBE_DONE', 'La tua email/iscrizione è stata rimossa. Grazie!', 'La tua email/iscrizione è stata rimossa. Grazie!', 'frontend', 1),
(135, 'MSG_UNSUBSCRIBE_NOTFOUND', 'Il contatto richiesto non è attualmente registrato', 'Il contatto richiesto non è attualmente registrato', 'frontend', 1),
(136, 'MSG_CART_REMOVED', 'Prodotto rimosso dal carrello', 'Prodotto rimosso dal carrello', 'frontend', 1),
(137, 'MSG_CART_ADDED', 'Prodotto inserito nel carrello', 'Prodotto inserito nel carrello', 'frontend', 1),
(138, 'MSG_CART_UPDATED', 'Prodotto aggiornato nel carrello', 'Prodotto aggiornato nel carrello', 'frontend', 1),
(139, 'MSG_SERVICE_FAILURE', 'Si è verificato un errore. Riprova!', 'Si è verificato un errore. Riprova!', 'frontend', 1),
(140, 'MSG_BILLING_ADDRESS_NECESSARY', 'L\'indirizzo di fatturazione è obbligatorio ai fini del processo di acquisto! <br/>Sei sicuro di voler uscire?<br/>Ti sarà comunque chiesto nella fase di acquisto!', 'L\'indirizzo di fatturazione è obbligatorio ai fini del processo di acquisto! <br/>Sei sicuro di voler uscire?<br/>Ti sarà comunque chiesto nella fase di acquisto!', 'frontend', 1),
(141, 'MSG_ALTERNATE_ADDRESS_LOGGED', 'inserisci un nuovo indirizzo o selezionane uno già presente nella lista', 'inserisci un nuovo indirizzo o selezionane uno già presente nella lista', 'frontend', 1),
(142, 'MSG_ALTERNATE_ADDRESS_NOTLOGGED', 'inserisci un nuovo indirizzo per la spedizione', 'inserisci un nuovo indirizzo per la spedizione', 'frontend', 1),
(143, 'MSG_COUPON_INVALID', 'Coupon non valido o scaduto', 'Coupon non valido o scaduto', 'frontend', 1),
(144, 'MSG_COUPON_INVALID_OVER', 'Il valore del Coupon inserito è maggiore del carrello!', 'Il valore del Coupon inserito è maggiore del carrello!', 'frontend', 1),
(145, 'LABEL_TP_SALE', 'Offerta', 'Offerta', 'frontend', 1),
(146, 'LABEL_TP_BESTSELLER', 'Più venduti', 'Più venduti', 'frontend', 1),
(147, 'LABEL_TP_TOPRATED', 'Più votati', 'Più votati', 'frontend', 1),
(148, 'LABEL_TP_STANDARD', 'Standard', 'Standard', 'frontend', 1),
(149, 'Standard', 'Nuovo', 'Nuovo', 'frontend', 1),
(150, 'SEND_AREYOUSURE_BTN', 'SEI SICURO ?', 'SEI SICURO ?', 'frontend', 1),
(151, 'LABEL_UNSUBSCRIBE', 'Cancellati', 'Cancellati', 'frontend', 1),
(152, 'LABEL_MY_ACCOUNT_EMAIL', 'Il mio profilo', 'Il mio profilo', 'email', 1),
(153, 'LABEL_SEE_EMAIL_ONLINE', 'Vedi email online', 'Vedi email online', 'email', 1),
(154, 'LABEL_EMAIL_SALES_TITLE', 'Controlla le nostre ultime offerte!', 'Ultime offerte', 'email', 1),
(155, 'LABEL_DETAIL_EMAIL', 'Vedi i dettagli', 'Vedi i dettagli', 'email', 1),
(156, 'TEXT_EMAIL_FOOTER_USUBSCRIBE', 'cancellati', 'Cancellati footer', 'email', 1),
(157, 'TEXT_EMAIL_FOOTER_RESERVED', 'Tutti i diritti riservati', 'Tutti i diritti riservati', 'email', 1),
(158, 'TEXT_EMAIL_FOOTER_COPYRIGHT', 'Se non vuoi più ricevere queste email per favore', 'Non ricevere più email', 'email', 1),
(159, 'LABEL_EMAIL_SUBJECT_CONTACT', 'Contatto dal sito', 'Contatto dal sito', 'email', 1),
(160, 'LABEL_EMAIL_CONTACT_TITLE', 'Ti ringraziamo per averci contattato!', 'Ti ringraziamo per il contatto', 'email', 1),
(161, 'LABEL_EMAIL_CONTACT_TEXT', 'Abbiamo ricevuto la tua comunicazione e ti invieremo una risposta nel minor tempo possibile. Grazie!', 'Comunicazione ricevuta', 'email', 1),
(162, 'LABEL_EMAIL_SUBJECT_NEWSLETTER', 'Iscrizione alla Newsletter', 'Iscrizione alla Newsletter', 'email', 1),
(163, 'LABEL_EMAIL_NEWSLETTER_TITLE', 'Grazie per esserti iscritto alla nostra newsletter!', 'Grazie newsletter', 'email', 1),
(164, 'LABEL_EMAIL_NEWSLETTER_TEXT', 'Riceverai gli aggiornamenti, le news e le nostre offerte esclusive per restare in contatto con il nostro mondo.', 'Riceverai gli aggiornamenti', 'email', 1),
(165, 'LABEL_EMAIL_SUBJECT_WELCOME', 'Benvenuto su Ma Chlò', 'Benvenuto su', 'email', 1),
(166, 'LABEL_EMAIL_WELCOME_TITLE', 'Benvenuto su Ma Chlò!', 'Benvenuto su TITOLO', 'email', 1),
(167, 'LABEL_EMAIL_WELCOME_TEXT', 'Grazie per esserti registrato! Speriamo che ti piaccia il nostro lavoro. Consulta alcune delle nostre ultime offerte qui sotto o clicca sul pulsante per visualizzare il tuo nuovo account.', 'Grazie per esserti registrato', 'email', 1),
(168, 'LABEL_EMAIL_SUBJECT_NEW_ORDER', 'Nuovo ordine', 'Nuovo ordine', 'email', 1),
(169, 'LABEL_TITLE_INVOICE', 'Riepilogo ordine', 'Riepilogo ordine', 'email', 1),
(170, 'LABEL_INVOICE_THANKS', 'Ti ringraziamo per il tuo ordine!', 'Ti ringraziamo per il tuo ordine!', 'email', 1),
(171, 'LABEL_INVOICE_THANKS_TEXT', 'Ti faremo sapere non appena i tuoi articoli saranno spediti.<br>Per cambiare o visualizzare il tuo ordine, ti preghiamo di visualizzare il tuo account facendo clic sul pulsante qui sotto.', 'Ti faremo sapere USER', 'email', 1),
(172, 'LABEL_INVOICE_THANKS_TEXT_NOUSER', 'Ti faremo sapere non appena i tuoi articoli saranno spediti.', 'Ti faremo sapere NO USER', 'email', 1),
(173, 'LABEL_SHIPPING_ADDRESS_EMAIL', 'Indirizzo di spedizione', 'Indirizzo di spedizione', 'email', 1),
(174, 'LABEL_ORDER_EMAIL', 'Ordine', 'Ordine', 'email', 1),
(175, 'LABEL_ORDER_DATE_EMAIL', 'Data ordine', 'Data ordine', 'email', 1),
(176, 'LABEL_ADDRESS_NOTES_EMAIL', 'Note indirizzo', 'Note indirizzo', 'email', 1),
(177, 'LABEL_ORDER_NOTES_EMAIL', 'Note ordine', 'Note ordine', 'email', 1),
(178, 'LABEL_DESCRIPTION_EMAIL', 'Descrizione', 'Descrizione', 'email', 1),
(179, 'LABEL_QTY_EMAIL', 'Quantità', 'Quantità', 'email', 1),
(180, 'LABEL_SIZE_EMAIL', 'Taglia', 'Taglia', 'email', 1),
(181, 'LABEL_COLOR_EMAIL', 'Colore', 'Colore', 'email', 1),
(182, 'LABEL_SUBTOTAL_ORDER_EMAIL', 'Subtotale', 'Subtotale', 'email', 1),
(183, 'LABEL_COUPON_APPLY_EMAIL', 'Applica coupon', 'Applica coupon', 'email', 1),
(184, 'LABEL_SHIPPING_EMAIL', 'Spedizione', 'Spedizione', 'email', 1),
(185, 'LABEL_TOTAL_EMAIL', 'Totale', 'Totale', 'email', 1),
(186, 'HOME_CATEGORY_TITLE', 'Discover our products', 'Intestazione sezione categorie home page', 'frontend', 2),
(187, 'HOME_SHOW_ALL_PRODUCTS', 'Show all products', 'Testo pulsante collegamento ai prodotti Home Page', 'frontend', 2),
(188, 'HOME_FEATURE_SHIPPING_TITLE', 'Show all products', 'Testo icona mappa Home Page', 'frontend', 2),
(189, 'HOME_FEATURE_SHIPPING_DESC', 'Free shipping on all orders 7/10 days', 'Descrizione icona mappa Home Page', 'frontend', 2),
(190, 'HOME_FEATURE_QUALITY_TITLE', 'Quality Guarantee', 'Testo icona frecce Home Page', 'frontend', 2),
(191, 'HOME_FEATURE_QUALITY_DESC', 'Guarantee on our products', 'Descrizione icona frecce Home Page', 'frontend', 2),
(192, 'HOME_FEATURE_SUPPORT_TITLE', 'Online support', 'Testo icona supporto Home Page', 'frontend', 2),
(193, 'HOME_FEATURE_SUPPORT_DESC', 'Friendly customer support', 'Descrizione icona supporto Home Page', 'frontend', 2),
(194, 'HOME_FEATURE_PAYMENTS_TITLE', 'Secure Online Payments', 'Testo icona pagamento Home Page', 'frontend', 2),
(195, 'HOME_FEATURE_PAYMENTS_DESC', 'We posess SSL / Secure Certificate', 'Descrizione icona pagamento Home Page', 'frontend', 2),
(196, 'FOOTER_HELP', 'Do you need help? Contact us', 'Testo footer help sinistra', 'frontend', 2),
(197, 'FOOTER_NEWSLTTER_TITLE', 'NEWSLETTER', 'NEWSLETTER', 'frontend', 2),
(198, 'FOOTER_NEWSLTTER_DESC', 'To receive latest offers and discounts from the shop.', 'Testo footer newsletter', 'frontend', 2),
(199, 'FOOTER_NEWSLTTER_INPUT', 'Your e-mail', 'Etichetta campo newsletter Footer', 'frontend', 2),
(200, 'FOOTER_PAYMENTS_TITLE', 'PAYMENT METHODS', 'Testo pagamenti footer destra', 'frontend', 2),
(201, 'FOOTER_PAYMENTS_DESC', 'We support one of the following payment methods.', 'Descrizione pagamenti footer destra', 'frontend', 2),
(202, 'LABEL_FILTER', 'Filter', 'Filtra', 'frontend', 2),
(203, 'LABEL_SEARCH', 'Search', 'Cerca', 'frontend', 2),
(204, 'LABEL_ORDER', 'Order by', 'Ordina per', 'frontend', 2),
(205, 'LABEL_DEFAULT', 'Default', 'Default', 'frontend', 2),
(206, 'LABEL_SALE', 'Sale', 'Offerta', 'frontend', 2),
(207, 'LABEL_NEW', 'New', 'Novità', 'frontend', 2),
(208, 'LABEL_FEEDBACK', 'Feedback', 'Voto medio', 'frontend', 2),
(209, 'LABEL_BEST', 'Best seller', 'Best seller', 'frontend', 2),
(210, 'LABEL_ALPHA', 'Alphabetical', 'Alfabetico', 'frontend', 2),
(211, 'LABEL_PRICE', 'Price', 'Prezzo', 'frontend', 2),
(212, 'LABEL_TAGS', 'Tags', 'Tags', 'frontend', 2),
(213, 'LABEL_SEE_MORE', 'See more', 'Vedi altri', 'frontend', 2),
(214, 'LABEL_ADD_TO_CART', 'Add to cart', 'Aggiungi', 'frontend', 2),
(215, 'LABEL_ADD_TO_WHISH', 'Add to Whishlist', 'Aggiungi alla Whishlist', 'frontend', 2),
(216, 'LABEL_ALL', 'All', 'Tutti', 'frontend', 2),
(217, 'LABEL_MY_ACCOUNT', 'My account', 'Il mio profilo', 'frontend', 2),
(218, 'LABEL_USER_ACCOUNT', 'User account', 'Account utente', 'frontend', 2),
(219, 'LABEL_USER_REGISTER', 'Register', 'Registrazione', 'frontend', 2),
(220, 'LABEL_USER_SIGN_UP', 'Sign up', 'Iscriviti', 'frontend', 2),
(221, 'LABEL_USER_LOGIN', 'Please Login', 'Effettua il Login', 'frontend', 2),
(222, 'LABEL_UPDATE', 'Update', 'Salva', 'frontend', 2),
(223, 'LABEL_GOT', 'You have', 'Hai', 'frontend', 2),
(224, 'LABEL_POINTS', 'points', 'punti', 'frontend', 2),
(225, 'LABEL_POINTS_DESC', 'You can use your points to receive special offers and have dedicated discounts.', 'Puoi utilizzare i tuoi punti per acquistare prodotti dal nostro shop o accedere alle promozioni speciali.', 'frontend', 2),
(226, 'LABEL_MANAGE_POINTS', 'Manage your points', 'Gestisci i tuoi punti', 'frontend', 2),
(227, 'LABEL_BACK_SHOP', 'Back to Shop', 'Torna allo Shop', 'frontend', 2),
(228, 'LABEL_LOGOUT', 'Logout', 'Logout', 'frontend', 2),
(229, 'LABEL_PROFILE', 'Profile', 'Profilo', 'frontend', 2),
(230, 'LABEL_ORDERS', 'Orders', 'Ordini', 'frontend', 2),
(231, 'LABEL_ADDRESSES', 'Shipping addresses', 'Indirizzi di spedizione', 'frontend', 2),
(232, 'LABEL_WHISHLIST', 'Whishlist', 'Whishlist', 'frontend', 2),
(233, 'LABEL_NAME', 'Name', 'Nome', 'frontend', 2),
(234, 'LABEL_SURNAME', 'Surname', 'Cognome', 'frontend', 2),
(235, 'LABEL_EMAIL', 'Email', 'Email', 'frontend', 2),
(236, 'LABEL_PHONE', 'Phone', 'Telefono', 'frontend', 2),
(237, 'LABEL_COUNTRY', 'Country/State', 'Stato/Paese', 'frontend', 2),
(238, 'LABEL_CITY', 'City', 'Città', 'frontend', 2),
(239, 'LABEL_ADDRESS', 'Address', 'Indirizzo', 'frontend', 2),
(240, 'LABEL_ADDRESS_REF', 'Reference shipping c/o (ex Tom Brad)', 'Riferimento spedizione c/o (es Mario Rossi)', 'frontend', 2),
(241, 'LABEL_ADDRESS_REF_FATT', 'Reference invoice (person or company)', 'Riferimento fatturazione (persona o azienda)', 'frontend', 2),
(242, 'LABEL_CIVICO', 'Number', 'Civico', 'frontend', 2),
(243, 'LABEL_POSTAL_CODE', 'Postal code', 'CAP', 'frontend', 2),
(244, 'LABEL_ADDRESS_NOTES', 'Address notes', 'Note indirizzo', 'frontend', 2),
(245, 'LABEL_ORDER_NOTES', 'Order notes', 'Note ordine', 'frontend', 2),
(246, 'LABEL_TOTAL_ORDER', 'Total order', 'Totale ordine', 'frontend', 2),
(247, 'LABEL_TOTAL', 'Total', 'Totale', 'frontend', 2),
(248, 'LABEL_QTY', 'Qty', 'Quantità', 'frontend', 2),
(249, 'LABEL_SUBTOTAL_ORDER', 'Subtotal', 'Subtotale', 'frontend', 2),
(250, 'LABEL_TOTAL_CART', 'Total cart', 'Totale carrello', 'frontend', 2),
(251, 'LABEL_TOTAL_ORDER_NOTES', '* Notes: this total include the shipping cost', '* Note: il totale include i costi di spedizione', 'frontend', 2),
(252, 'LABEL_TOTAL_CART_NOTES', '* Notes: the total does not include any shipping charges. Shipping or delivery costs will be calculated in the next checkout', '* Note: il totale non include eventuali spese di spedizione. I costi di spedizione o consegna verranno calcolati nel checkout successivo', 'frontend', 2),
(253, 'LABEL_CONFIRM', 'CONFIRM', 'CONFERMA', 'frontend', 2),
(254, 'LABEL_REMOVE', 'REMOVE', 'ELIMINA', 'frontend', 2),
(255, 'LABEL_BACK_TO_CART', 'BACK TO CART', 'RITORNA AL CARRELLO', 'frontend', 2),
(256, 'LABEL_UPDATE_CART', 'UPDATE CART', 'AGGIORNA IL CARRELLO', 'frontend', 2),
(257, 'LABEL_BACK_TO_SHOP', 'BACK TO SHOP', 'TORNA ALLO SHOP', 'frontend', 2),
(258, 'LABEL_CART', 'Cart', 'Carrello', 'frontend', 2),
(259, 'LABEL_CART_EMPTY', 'Your cart is empty !', 'Il tuo carrello è vuoto !', 'frontend', 2),
(260, 'LABEL_CHECKOUT', 'Checkout', 'Checkout', 'frontend', 2),
(261, 'LABEL_PAYMENT_METHOD', 'Payment method', 'Modalità di pagamento', 'frontend', 2),
(262, 'LABEL_PAYMENT_METHOD_PAYPAL', 'Paypal', 'Paypal', 'frontend', 2),
(263, 'LABEL_PAYMENT_METHOD_CC', 'Credit card / Prepaid card', 'Carta di credito / Prepagata', 'frontend', 2),
(264, 'LABEL_MESSAGE', 'Message', 'Messaggio', 'frontend', 2),
(265, 'LABEL_SEND', 'SEND', 'INVIA', 'frontend', 2),
(266, 'LABEL_PRODUCTS', 'products', 'prodotti', 'frontend', 2),
(267, 'LABEL_CART_INFO_ACTUALLY_1', 'There are currently', 'Attualmente ci sono', 'frontend', 2),
(268, 'LABEL_CART_INFO_ACTUALLY_2', 'in your shopping cart', 'nel carrello', 'frontend', 2),
(269, 'LABEL_SIZE', 'Size', 'Tg', 'frontend', 2),
(270, 'LABEL_COLOR', 'Color', 'Colore', 'frontend', 2),
(271, 'LABEL_CATEGORY', 'Category', 'Categoria', 'frontend', 2),
(272, 'LABEL_DESCRIPTION', 'Description', 'Descrizione', 'frontend', 2),
(273, 'LABEL_REVIEWS', 'Reviews', 'Commenti', 'frontend', 2),
(274, 'LABEL_ALSO_LIKE', 'You May Also Like', 'Ti potrebbero piacere', 'frontend', 2),
(275, 'LABEL_REFCODE', 'Ref', 'Rif', 'frontend', 2),
(276, 'LABEL_DETAIL', 'Detail', 'Dettaglio', 'frontend', 2),
(277, 'LABEL_SHOPPING_CART', 'Shopping cart', 'Carrello', 'frontend', 2),
(278, 'LABEL_FRONT', 'FRONT', 'FRONTE', 'frontend', 2),
(279, 'LABEL_BACK', 'BACK', 'RETRO', 'frontend', 2),
(280, 'LABEL_AVAILABILITY', 'Availability', 'Disponibilità', 'frontend', 2),
(281, 'LABEL_AVAILABILITY_HIGH', 'High', 'Alta', 'frontend', 2),
(282, 'LABEL_AVAILABILITY_LOW', 'Low', 'Bassa', 'frontend', 2),
(283, 'LABEL_404_MESSAGE', 'Oops.... the page requested not exist !', 'Oops.... la pagina richiesta non esiste !', 'frontend', 2),
(284, 'LABEL_404_BTN', 'BACK TO HOME', 'TORNA ALLA HOME', 'frontend', 2),
(285, 'LABEL_ORDER', 'Order', 'Ordine', 'frontend', 2),
(286, 'LABEL_YOUR_ACCOUNT', 'Your account', 'Il tuo account', 'frontend', 2),
(287, 'LABEL_USER_ALREADY_REGISTERED', 'Are you already registered?', 'Sei già registrato ?', 'frontend', 2),
(288, 'LABEL_USER_NOTREGISTERED', 'Otherwise you can order filling in the data', 'Altrimenti puoi ordinare compilando i dati', 'frontend', 2),
(289, 'LABEL_USER_NOTREGISTERED_POINTS', 'you will not accumulate points and bonuses reserved for registered customers', 'non accumulerai punti e bonus riservati ai clienti registrati', 'frontend', 2),
(290, 'LABEL_COUPON', 'COUPON', 'COUPON', 'frontend', 2),
(291, 'LABEL_COUPON_HAVE', 'Do you have a discount code?', 'Hai un codice sconto?', 'frontend', 2),
(292, 'LABEL_COUPON_INSERT', 'Enter the coupon code', 'Inserisci il codice coupon', 'frontend', 2),
(293, 'LABEL_COUPON_APPLY', 'Apply coupons', 'Applica coupon', 'frontend', 2),
(294, 'LABEL_DISCOUNT', 'Discount', 'Sconto', 'frontend', 2),
(295, 'LABEL_SHIPPING_THIS', 'Send to this address', 'Spedisci a questo indirizzo', 'frontend', 2),
(296, 'LABEL_SHIPPING_OTHER', 'Send to another address', 'Spedisci ad un altro indirizzo', 'frontend', 2),
(297, 'LABEL_NEW_ADDRESS', 'New address', 'Nuovo indirizzo', 'frontend', 2),
(298, 'LABEL_BILLING_ADDRESS', 'Billing address', 'Indirizzo di fatturazione', 'frontend', 2),
(299, 'LABEL_SHIPPING_ADDRESS', 'Shipping address', 'Indirizzo di spedizione', 'frontend', 2),
(300, 'LABEL_SHIPPING', 'Shipping', 'Spedizione', 'frontend', 2),
(301, 'LABEL_CHANGE_PASSWORD', 'Change Password', 'Cambio Password', 'frontend', 2),
(302, 'LABEL_ORDER_DATE', 'Order date', 'Data ordine', 'frontend', 2),
(303, 'LABEL_STRIPE_DESC', 'Make secure payments with Stripe', 'Paga in tutta sicurezza con Stripe', 'frontend', 2),
(304, 'MSG_SEARCH_INSERT', 'Enter text to search', 'Inserisci il testo da ricercare', 'frontend', 2),
(305, 'MSG_SAVE_NEWSLETTER', 'Sign up to newsletter', 'Iscrivimi alla newsletter', 'frontend', 2),
(306, 'MSG_SEND_CONTACT_US', 'Contact requests send!<br/>Thank you.', 'Richiesta di contatto inviata con successo!<br/>Grazie.', 'frontend', 2),
(307, 'MSG_PAYPAL_NOTETOPAYER', 'The shipping address will remain in the checkout form and not the one indicated in the PayPal payment!', 'L\'indirizzo di spedizione resterà quello inserito nel modulo di checkout e non quello indicato nel pagamento PayPal !', 'frontend', 2),
(308, 'MSG_PAYPAL_CANCEL', 'Retry with payment to complete the order !', 'Riprova ad effettuare il pagamemto !', 'frontend', 2),
(309, 'MSG_PAYPAL_ERROR', 'Error during payment operation:', 'Errore durante il pagamento:', 'frontend', 2),
(310, 'MSG_NO_RESULT', 'No results found for  ', 'Nessun risultato per ', 'frontend', 2),
(311, 'MSG_NO_RESULT_FILTER', 'No results found with filters selected.', 'Nessun risultato presente per i filtri selezionati.', 'frontend', 2),
(312, 'MSG_NO_SIZE_FOR_PRODUCTS_COLOR', 'No availables sizes at the moment for this product/color', 'Al momento non ci sono taglie disponibili per questo prodotto/colore', 'frontend', 2),
(313, 'MSG_ORDER_SUCCESS', 'saved successfully! Thank you.', 'inserito con successo ! Grazie.', 'frontend', 2),
(314, 'MSG_ORDER_PAYMENT_ERROR', 'Error during order payment. Please che your order and retry.', 'Errore durante il pagamento ordine. Per favore verifica il tuo ordine e riprova.', 'frontend', 2),
(315, 'MSG_SUCCESS_CONTACT', 'Your message has been sent correctly. Thank you !', 'Il tuo messaggio è stato inviato correttamente. Grazie !', 'frontend', 2),
(316, 'MSG_FAILURE_CONTACT', 'Sorry it seems that our mail server is not responding. Please try again later!', 'Abbiamo riscontrato un problema nell\'invio del mesaggio. Riprova!', 'frontend', 2),
(317, 'MSG_SUCCESS_NEWSLETTER', 'Now you are subscribed to the newsletter. Thank you !', 'Adesso sei iscritto alla newsletter. Grazie !', 'frontend', 2),
(318, 'MSG_UNIQUE_NEWSLETTER', 'The email address is already subscribed to the newsletter.', 'L\'indirizzo email è già iscritto alla newsletter', 'frontend', 2),
(319, 'MSG_UNSUBSCRIBE_DONE', 'Your email/subscription was removed. Thank you!', 'La tua email/iscrizione è stata rimossa. Grazie!', 'frontend', 2),
(320, 'MSG_UNSUBSCRIBE_NOTFOUND', 'The required contact is not currently registered', 'Il contatto richiesto non è attualmente registrato', 'frontend', 2),
(321, 'MSG_CART_REMOVED', 'Product removed from cart', 'Prodotto rimosso dal carrello', 'frontend', 2),
(322, 'MSG_CART_ADDED', 'Product added to cart', 'Prodotto inserito nel carrello', 'frontend', 2),
(323, 'MSG_CART_UPDATED', 'Product updated into cart', 'Prodotto aggiornato nel carrello', 'frontend', 2),
(324, 'MSG_SERVICE_FAILURE', 'There was an error. Please retry!', 'Si è verificato un errore. Riprova!', 'frontend', 2),
(325, 'MSG_BILLING_ADDRESS_NECESSARY', 'The billing address is required for the purchase process!<br/>Are you sure you want to leave?<br/>You will still be asked for the purchase phase!', 'L\'indirizzo di fatturazione è obbligatorio ai fini del processo di acquisto! <br/>Sei sicuro di voler uscire?<br/>Ti sarà comunque chiesto nella fase di acquisto!', 'frontend', 2),
(326, 'MSG_ALTERNATE_ADDRESS_LOGGED', 'enter a new address or select one already in the list', 'inserisci un nuovo indirizzo o selezionane uno già presente nella lista', 'frontend', 2),
(327, 'MSG_ALTERNATE_ADDRESS_NOTLOGGED', 'enter a new shipping address', 'inserisci un nuovo indirizzo per la spedizione', 'frontend', 2),
(328, 'MSG_COUPON_INVALID', 'Coupon invalid or expired', 'Coupon non valido o scaduto', 'frontend', 2),
(329, 'MSG_COUPON_INVALID_OVER', 'The value of the inserted Coupon is greater than the cart!', 'Il valore del Coupon inserito è maggiore del carrello!', 'frontend', 2),
(330, 'LABEL_TP_SALE', 'Sale', 'Offerta', 'frontend', 2),
(331, 'LABEL_TP_BESTSELLER', 'Best seller', 'Più venduti', 'frontend', 2),
(332, 'LABEL_TP_TOPRATED', 'Top rated', 'Più votati', 'frontend', 2),
(333, 'LABEL_TP_STANDARD', 'Standard', 'Standard', 'frontend', 2),
(334, 'Standard', 'New', 'Nuovo', 'frontend', 2),
(335, 'SEND_AREYOUSURE_BTN', 'ARE YOU SURE ?', 'SEI SICURO ?', 'frontend', 2),
(336, 'LABEL_UNSUBSCRIBE', 'Unsubscribe', 'Cancellati', 'frontend', 2),
(337, 'LABEL_MY_ACCOUNT_EMAIL', 'My account', 'Il mio profilo', 'email', 2),
(338, 'LABEL_SEE_EMAIL_ONLINE', 'See contents online', 'Vedi email online', 'email', 2),
(339, 'LABEL_EMAIL_SALES_TITLE', 'Check out our newest sales!', 'Ultime offerte', 'email', 2),
(340, 'LABEL_DETAIL_EMAIL', 'See details', 'Vedi i dettagli', 'email', 2),
(341, 'TEXT_EMAIL_FOOTER_USUBSCRIBE', 'unsubscribe', 'Cancellati footer', 'email', 2),
(342, 'TEXT_EMAIL_FOOTER_RESERVED', 'All rights reserved', 'Tutti i diritti riservati', 'email', 2),
(343, 'TEXT_EMAIL_FOOTER_COPYRIGHT', 'If you no longer wish to receive these emails please', 'Non ricevere più email', 'email', 2),
(344, 'LABEL_EMAIL_SUBJECT_CONTACT', 'Contact from', 'Contatto dal sito', 'email', 2),
(345, 'LABEL_EMAIL_CONTACT_TITLE', 'Thank you for contact us!', 'Ti ringraziamo per il contatto', 'email', 2),
(346, 'LABEL_EMAIL_CONTACT_TEXT', 'We have received your communication and will send you an answer as soon as possible. Thank you!', 'Comunicazione ricevuta', 'email', 2),
(347, 'LABEL_EMAIL_SUBJECT_NEWSLETTER', 'Newsletter signup', 'Iscrizione alla Newsletter', 'email', 2),
(348, 'LABEL_EMAIL_NEWSLETTER_TITLE', 'Thank you for signing up to our newsletter!', 'Grazie newsletter', 'email', 2),
(349, 'LABEL_EMAIL_NEWSLETTER_TEXT', 'You will receive updates, news and exclusive offers to stay in touch with our world.', 'Riceverai gli aggiornamenti', 'email', 2),
(350, 'LABEL_EMAIL_SUBJECT_WELCOME', 'Welcome to Ma Chlò!', 'Benvenuto su', 'email', 2),
(351, 'LABEL_EMAIL_WELCOME_TITLE', 'Welcome to Ma Chlò!', 'Benvenuto su TITOLO', 'email', 2),
(352, 'LABEL_EMAIL_WELCOME_TEXT', 'Thank you for signing up! We hope you enjoy your time with us. Check out some of our newest offers below or the button to view your new account.', 'Grazie per esserti registrato', 'email', 2),
(353, 'LABEL_EMAIL_SUBJECT_NEW_ORDER', 'New order', 'Nuovo ordine', 'email', 2),
(354, 'LABEL_TITLE_INVOICE', 'Order invoice', 'Riepilogo ordine', 'email', 2),
(355, 'LABEL_INVOICE_THANKS', 'Thank you for your order!', 'Ti ringraziamo per il tuo ordine!', 'email', 2),
(356, 'LABEL_INVOICE_THANKS_TEXT', 'We\\\'ll let you know as soon as your items have shipped.<br>To change or view your order, please view your account by clicking the button below.', 'Ti faremo sapere USER', 'email', 2),
(357, 'LABEL_INVOICE_THANKS_TEXT_NOUSER', 'We\'ll let you know as soon as your items have shipped.', 'Ti faremo sapere NO USER', 'email', 2),
(358, 'LABEL_SHIPPING_ADDRESS_EMAIL', 'Shipping address', 'Indirizzo di spedizione', 'email', 2),
(359, 'LABEL_ORDER_EMAIL', 'Order', 'Ordine', 'email', 2),
(360, 'LABEL_ORDER_DATE_EMAIL', 'Order date', 'Data ordine', 'email', 2),
(361, 'LABEL_ADDRESS_NOTES_EMAIL', 'Address notes', 'Note indirizzo', 'email', 2),
(362, 'LABEL_ORDER_NOTES_EMAIL', 'Order notes', 'Note ordine', 'email', 2),
(363, 'LABEL_DESCRIPTION_EMAIL', 'Description', 'Descrizione', 'email', 2),
(364, 'LABEL_QTY_EMAIL', 'Qty', 'Quantità', 'email', 2),
(365, 'LABEL_SIZE_EMAIL', 'Size', 'Taglia', 'email', 2),
(366, 'LABEL_COLOR_EMAIL', 'Color', 'Colore', 'email', 2),
(367, 'LABEL_SUBTOTAL_ORDER_EMAIL', 'Subtotal', 'Subtotale', 'email', 2),
(368, 'LABEL_COUPON_APPLY_EMAIL', 'Apply coupon', 'Applica coupon', 'email', 2),
(369, 'LABEL_SHIPPING_EMAIL', 'Shipping', 'Spedizione', 'email', 2),
(370, 'LABEL_TOTAL_EMAIL', 'Total', 'Totale', 'email', 2),
(371, 'LABEL_TP_NEW', 'Nuovo', 'Nuovo prodotto', 'frontend', 1),
(372, 'LABEL_TP_NEW', 'New', 'New product', 'frontend', 2),
(373, 'LABEL_FROM', 'from', 'From', 'frontend', 2),
(374, 'LABEL_FROM', 'da', 'Da', 'frontend', 1),
(375, 'MSG_WISHLIST_INSERTED', 'Prodotto inserito nella Wishlist !!!', 'Prodotto inserito', 'frontend', 1),
(376, 'MSG_WISHLIST_INSERTED', 'Product inserted in the Wishlist !!!', 'Product inserted', 'frontend', 2),
(377, 'MSG_WISHLIST_PRESENT', 'Prodotto già presente nella Wishlist !!!', 'Prodotto già presente', 'frontend', 1),
(378, 'MSG_WISHLIST_PRESENT', 'Product already present in the Wishlist !!!', 'Product already present', 'frontend', 2),
(379, 'MSG_WISHLIST_NOTALLOWED', 'Effettua la login/regitrazione per utilizzare la Wishlist !!!', 'Utente non abilitato alla Wishlist', 'frontend', 1),
(380, 'MSG_WISHLIST_NOTALLOWED', 'Please login / register to use the Wishlist !!!', 'User not allowed to use wishlist', 'frontend', 2),
(381, 'MSG_OPERATION_SUCCESS', 'Operazione eseguita', 'Operazione eseguita', 'frontend', 1),
(382, 'MSG_OPERATION_SUCCESS', 'Operation performed', 'Operation performed', 'frontend', 2),
(383, 'MSG_OPERATION_FAILURE', 'Operazione non eseguita', 'Operazione non eseguita', 'frontend', 1),
(384, 'MSG_OPERATION_FAILURE', 'Operation not performed', 'Operation not performed', 'frontend', 2),
(385, 'LABEL_CANCEL', 'Cancel', 'Cancel operation', 'frontend', 2),
(386, 'LABEL_CANCEL', 'Cancella', 'Cancella operazione', 'frontend', 1),
(387, 'LABEL_CLOSE', 'Close', 'Close', 'frontend', 2),
(388, 'LABEL_CLOSE', 'Chiudi', 'Chiudi', 'frontend', 1),
(389, 'LABEL_DEFAULT_ADDRESS', 'Indirizzo predefinito', 'Indirizzo predefinito', 'frontend', 1),
(390, 'LABEL_DEFAULT_ADDRESS', 'Default address', 'Default address', 'frontend', 2),
(391, 'MSG_OPERATION_SAVED', 'salvato correttamente', 'salvato correttamente', 'frontend', 1),
(392, 'MSG_OPERATION_SAVED', 'saved succesfully', 'saved succesfully', 'frontend', 2),
(393, 'MSG_OPERATION_DELETE', 'deleted succesfully', 'deleted succesfully', 'frontend', 2),
(394, 'MSG_OPERATION_DELETE', 'eliminato correttamente', 'eliminato correttamente', 'frontend', 1),
(395, 'MSG_NO_ELEMENTS', 'Nessun elemento presente', 'Nessun elemento presente', 'frontend', 1),
(396, 'MSG_NO_ELEMENTS', 'No elements found', 'No elements found', 'frontend', 2),
(397, 'LABEL_TRACKING', 'Tracking', 'Tracking', 'frontend', 1),
(398, 'LABEL_TRACKING', 'Tracking', 'Tracking', 'frontend', 2),
(399, 'MSG_ORDER_CONTACT', 'Per informazioni o modifiche al tuo ordine CONTATTACI', 'Per informazioni o modifiche al tuo ordine CONTATTACI', 'frontend', 1),
(400, 'MSG_ORDER_CONTACT', 'For information or changes to your order CONTACT US', 'For information or changes to your order CONTACT US', 'frontend', 2),
(401, 'MSG_COOKIE_LAW', 'Questo sito utilizza i cookie per migliorare la tua esperienza di navigazione.', 'Questo sito utilizza i cookie per migliorare la tua esperienza di navigazione.', 'frontend', 1),
(402, 'MSG_COOKIE_LAW', 'This website uses cookies to ensure you get the best experience on our website.', 'Questo sito utilizza i cookie per migliorare la tua esperienza di navigazione.', 'frontend', 2),
(403, 'MSG_COOKIE_LAW_BTN', 'Ho capito!', 'Pulsante ok (ho capito)', 'frontend', 1),
(404, 'MSG_COOKIE_LAW_BTN', 'Got it!', 'Pulsante ok (ho capito)', 'frontend', 2),
(405, 'MSG_COOKIE_LAW_INFO', 'leggi di più', 'Per saperne di più', 'frontend', 1),
(406, 'MSG_COOKIE_LAW_INFO', 'learn more', 'Per saperne di più', 'frontend', 2),
(407, 'LABEL_INVOICE_CC_PAYMENT_FAIL', 'Il pagamento relativo al tuo ordine non risulta effettuato. Procedi al pagamento utilizzando il pulsante di pagamento.', 'Messaggio pagamento non effettuato', 'frontend', 1),
(408, 'LABEL_INVOICE_CC_PAYMENT_FAIL', 'The payment for your order is not made. Proceed to payment using the payment button.', 'Messaggio pagamento non effettuato', 'frontend', 2),
(409, 'LABEL_PAY_NOW', 'Paga adesso', 'Etichetta bottone paga adesso', 'frontend', 1),
(410, 'LABEL_PAY_NOW', 'Pay now', 'Etichetta bottone paga adesso', 'frontend', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE `ordini` (
  `id_ordine` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `data_ordine` date DEFAULT NULL,
  `totale_ordine` double DEFAULT NULL,
  `note_ordine` text NOT NULL,
  `tipo_pagamento` int(11) NOT NULL,
  `stato_pagamento` int(11) NOT NULL,
  `token_pagamento` varchar(250) NOT NULL,
  `stato_ordine` tinyint(4) NOT NULL DEFAULT '1',
  `id_indirizzo_spedizione` int(11) DEFAULT NULL,
  `id_indirizzo_fatturazione_spedizione` int(11) DEFAULT NULL,
  `punti` int(11) NOT NULL DEFAULT '0',
  `coupon_code` varchar(250) NOT NULL,
  `coupon_value` double NOT NULL,
  `printful_status` varchar(50) NOT NULL,
  `printful_id` int(11) NOT NULL,
  `printful_last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ordini`
--

INSERT INTO `ordini` (`id_ordine`, `id_cliente`, `data_ordine`, `totale_ordine`, `note_ordine`, `tipo_pagamento`, `stato_pagamento`, `token_pagamento`, `stato_ordine`, `id_indirizzo_spedizione`, `id_indirizzo_fatturazione_spedizione`, `punti`, `coupon_code`, `coupon_value`, `printful_status`, `printful_id`, `printful_last_update`) VALUES
(1, 3, '2018-05-24', 7.95, '', 1, 1, 'paypal', 3, 0, 5, 0, '', 0, '', 0, '0000-00-00 00:00:00'),
(2, 6, '2018-06-02', 36.62, '', 2, 1, 'tok_1CsThJF2tw3BJl6OtUM88IOo', 3, 0, 6, 0, 'OBBNMXBR', 0, 'draft', 8979165, '2018-06-02 10:43:10'),
(3, 2, '2018-06-03', 3.9, '', 1, 1, 'paypal', 1, 0, NULL, 0, '', 0, '', 0, '0000-00-00 00:00:00'),
(4, 2, '2018-06-03', 3.9, '', 1, 1, 'paypal', 1, 0, NULL, 0, '', 0, '', 0, '0000-00-00 00:00:00'),
(5, 6, '2018-06-06', 34.58, 'sdfdsfddsf', 2, 1, 'tok_1Ca07tF2tw3BJl6O87AfRvPT', 1, 0, 9, 3, 'BYVFLBSH', 6.1035, '', 0, '0000-00-00 00:00:00'),
(6, 7, '2018-07-26', 43.95, 'Piano -1', 2, 3, '', 1, 0, 10, 4, '', 0, '', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine`
--

CREATE TABLE `pagine` (
  `id_pagina` int(11) NOT NULL,
  `nome_pagina` varchar(50) NOT NULL,
  `url_pagina` varchar(50) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `controller` varchar(250) NOT NULL,
  `tipo_pagina` varchar(25) NOT NULL,
  `trad_code` varchar(25) NOT NULL,
  `label_page_url` varchar(250) NOT NULL,
  `ordine_menu` int(5) NOT NULL,
  `nome_menu` varchar(255) NOT NULL,
  `testo_menu` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine`
--

INSERT INTO `pagine` (`id_pagina`, `nome_pagina`, `url_pagina`, `id_lingua`, `controller`, `tipo_pagina`, `trad_code`, `label_page_url`, `ordine_menu`, `nome_menu`, `testo_menu`) VALUES
(1, 'default page', 'default_page', 1, 'Comingsoon', 'statica', '', '', 0, '', ''),
(2, 'Home', 'it/home', 1, 'frontend/Home', 'statica', '', 'PAGE_HOME_URL', 1, 'MENU_HOME', 'Home'),
(3, 'Negozio', 'it/negozio', 1, 'frontend/Home/shop', 'dinamica', 'negozio', 'PAGE_SHOP_URL', 2, 'MENU_SHOP', 'Negozio'),
(4, 'Negozio', 'it/negozio/(:any)', 1, 'frontend/Home/shop/$1', 'dinamica', 'negozio', '', 0, '', ''),
(5, 'Chi siamo', 'it/chisiamo', 1, 'frontend/Home/about', 'statica', '', 'PAGE_ABOUT_URL', 3, 'MENU_ABOUT', 'Chi siamo'),
(6, 'Spedizioni', 'it/spedizioni', 1, 'frontend/Home/shipping', 'statica', '', 'PAGE_SHIPPING_URL', 4, 'MENU_SHIPPING', 'Spedizioni'),
(7, 'Contatti', 'it/contatti', 1, 'frontend/Home/contacts', 'statica', '', 'PAGE_CONTACTS_URL', 8, 'MENU_CONTACTS', 'Contatti'),
(8, 'Regolamento', 'it/regolamento', 1, 'frontend/Home/rules', 'statica', '', 'PAGE_RULES_URL', 5, 'MENU_RULES', 'Regolamento'),
(9, 'Prodotti', 'it/prodotti/(:any)/(:any)', 1, 'frontend/Products/detailcode/$1', 'dinamica', 'prodotti', '', 0, '', ''),
(10, 'Varianti', 'it/prodotti/(:any)/(:any)/(:any)', 1, 'frontend/Products/detailcode/$1/$2', 'dinamica', 'varianti', '', 0, '', ''),
(11, 'Account', 'it/account', 1, 'frontend/Account', 'statica', '', 'PAGE_ACCOUNT_URL', 0, '', ''),
(12, 'Logout', 'it/logout', 1, 'frontend/Account/logout', 'statica', '', 'PAGE_LOGOUT_URL', 0, '', ''),
(13, 'Login', 'it/login', 1, 'frontend/Account/login', 'statica', '', 'PAGE_LOGIN_URL', 0, '', ''),
(14, 'Registrati', 'it/registrati', 1, 'frontend/Account/register', 'statica', '', '', 0, '', ''),
(15, 'Salva account', 'it/salva_account', 1, 'frontend/Account/salvaDatiProfilo', 'statica', '', '', 0, '', ''),
(16, 'Carrello', 'it/carrello', 1, 'frontend/Cart', 'statica', '', 'PAGE_CART_URL', 0, '', ''),
(17, 'Checkout', 'it/checkout', 1, 'frontend/Cart/checkout', 'statica', '', 'PAGE_CHECKOUT_URL', 0, '', ''),
(18, 'Privacy', 'it/privacy', 1, 'frontend/Home/privacy', 'statica', '', 'PAGE_PRIVACY_URL', 7, 'MENU_PRIVACY', 'Privacy'),
(19, 'Home', 'en/home', 2, 'frontend/Home', 'statica', '', 'PAGE_HOME_URL', 1, 'MENU_HOME', 'Home'),
(20, 'Shop', 'en/shop', 2, 'frontend/Home/shop', 'dinamica', 'negozio', 'PAGE_SHOP_URL', 2, 'MENU_SHOP', 'Shop'),
(21, 'Shop', 'en/shop/(:any)', 2, 'frontend/Home/shop/$1', 'dinamica', 'negozio', '', 0, '', ''),
(22, 'About', 'en/about', 2, 'frontend/Home/about', 'statica', '', 'PAGE_ABOUT_URL', 3, 'MENU_ABOUT', 'About us'),
(23, 'Shipping', 'en/shipping', 2, 'frontend/Home/shipping', 'statica', '', 'PAGE_SHIPPING_URL', 4, 'MENU_SHIPPING', 'Shipping'),
(24, 'Contacts', 'en/contacts', 2, 'frontend/Home/contacts', 'statica', '', 'PAGE_CONTACTS_URL', 8, 'MENU_CONTACTS', 'Contacts'),
(25, 'Rules', 'en/rules', 2, 'frontend/Home/rules', 'statica', '', 'PAGE_RULES_URL', 5, 'MENU_RULES', 'Rules'),
(26, 'Products', 'en/products/(:any)/(:any)', 2, 'frontend/Products/detailcode/$1', 'dinamica', 'prodotti', '', 0, '', ''),
(27, 'Variants', 'en/products/(:any)/(:any)/(:any)', 2, 'frontend/Products/detailcode/$1/$2', 'dinamica', 'varianti', '', 0, '', ''),
(28, 'Account', 'en/account', 2, 'frontend/Account', 'statica', '', 'PAGE_ACCOUNT_URL', 0, '', ''),
(29, 'Logout', 'en/logout', 2, 'frontend/Account/logout', 'statica', '', 'PAGE_LOGOUT_URL', 0, '', ''),
(30, 'Login', 'en/login', 2, 'frontend/Account/login', 'statica', '', 'PAGE_LOGIN_URL', 0, '', ''),
(31, 'Register', 'en/register', 2, 'frontend/Account/register', 'statica', '', '', 0, '', ''),
(32, 'Save account', 'en/save_account', 2, 'frontend/Account/salvaDatiProfilo', 'statica', '', '', 0, '', ''),
(33, 'Shopping cart', 'en/cart', 2, 'frontend/Cart', 'statica', '', 'PAGE_CART_URL', 0, '', ''),
(34, 'Checkout', 'en/checkout', 2, 'frontend/Cart/checkout', 'statica', '', 'PAGE_CHECKOUT_URL', 0, '', ''),
(35, 'Privacy', 'en/privacy', 2, 'frontend/Home/privacy', 'statica', '', 'PAGE_PRIVACY_URL', 7, 'MENU_PRIVACY', 'Privacy'),
(36, 'Gallery', 'it/gallery', 1, 'frontend/Home/gallery', 'statica', '', 'PAGE_GALLERY_URL', 6, 'MENU_GALLERY', 'Gallery'),
(37, 'Gallery', 'en/gallery', 2, 'frontend/Home/gallery', 'statica', '', 'PAGE_GALLERY_URL', 6, 'MENU_GALLERY', 'Gallery'),
(38, 'Prodotti', 'it/prodotti', 1, 'frontend/Products', 'dinamica', 'prodotti', 'PAGE_PRODUCTS_URL', 0, '', ''),
(39, 'Products', 'en/products', 2, 'frontend/Products', 'dinamica', 'prodotti', 'PAGE_PRODUCTS_URL', 0, '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine_contenuti`
--

CREATE TABLE `pagine_contenuti` (
  `id_pc` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `meta_description` text NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine_contenuti`
--

INSERT INTO `pagine_contenuti` (`id_pc`, `code`, `title`, `meta_description`, `description`, `image`, `id_lingua`) VALUES
(1, 'HOME', 'Abbigliamento artistico', 'Benvenuti sul sito di Ma Chlò vendita di abbigliamento di qualità basato su lavori artistici con spedizione in tutto il mondo.', '', '', 1),
(2, 'SHOP', 'Negozio', 'Qui puoi scoprire tutti i nostri prodotti, le taglie e i colori disponibili per portare le nostre opere a casa tua.', '', '', 1),
(3, 'ABOUT', 'Chi siamo', 'Ma Chlò nasce in Sardegna nel 2017 dall’idea e dalla collaborazione di Stefania, Simona e Francesca che hanno voluto riprodurre su capi di abbigliamento i quadri di una delle fondatrici da sempre appassionata al disegno e all’arte.', 'Ma Chlò nasce in Sardegna nel 2017 dall’idea e dalla collaborazione di Stefania, Simona e Francesca che hanno voluto riprodurre su capi di abbigliamento i quadri di una delle fondatrici da sempre appassionata al disegno e all’arte. Ma Chlò, infatti, non propone delle stampe qualsiasi ma delle vere e proprie opere d’arte uniche e originali che con i loro soggetti e fantasie raffigurano la natura in tutte le sue forme e colori, qualcosa di unico che prende ispirazione anche dai profumi, dalla magia e dai paesaggi della Sardegna. Ogni quadro è ispirato ad un viaggio, un vissuto, un’emozione, un’esperienza che l’artista ha voluto esprimere su tela e nella quale ogni donna può immedesimarsi.<br><br>\r\nOltre alla natura sotto forma di animali, alberi, fiori, Ma Chlò propone come linea principale “Le maschere” che rappresentano le varie tappe di una storia d’amore. «L’idea delle maschere» racconta l’artista «nasce da una passeggiata lungo l’ultimo chilometro e mezzo rimasto in piedi del muro di Berlino.<br><br>Rimasi affascinata da un murales che rappresentava due facce divise da qualcosa di importante, decisi di riportarle su tela giocando con i colori e dopo il primo disegno capii che quella doveva diventare una storia, la storia di un amore che nessun muro avrebbe più potuto fermare». Una storia, dunque, articolata in diverse fasi, l’incontro, lo sguardo, il bacio, la separazione, il silenzio, la rottura, l’infinito, un nuovo inizio che Ma Chlò vuole far indossare ad ogni donna come espressione del proprio vissuto.<br><br>\r\nPer iniziare Ma Chlò propone nella sua linea di abbigliamento t-shirt e vestiti con l’obiettivo di ampliare la sua gamma di prodotti in un’ottica di crescita continua.<h3>Mission</h3>La nostra mission è creare un punto di incontro tra moda e arte.<br>Ciò che ci contraddistingue è l’originalità ed il significato delle stampe.', 'about.jpg', 1),
(4, 'SHIPPING', 'Spedizioni e consegne', 'La spedizione dei prodotti Ma Chlò è gratuita, i tempi di consegna sono di circa 7/10 giorni lavorativi dall’evasione dell’ordine.', '<h3>Regole di spedizione e costi</h3>\r\nLa spedizione dei prodotti Ma Chlò è gratuita, i tempi di consegna sono di circa 7/10 giorni lavorativi dall’evasione dell’ordine, se invece si desidera una spedizione espressa i tempi di consegna sono di 3/5 giorni lavorativi con un costo a carico del cliente di 20 euro.', '99fc4-blog.jpg', 1),
(5, 'RULES', 'Regolamento e condizioni', 'Ma Chlò è marchio registrato, scopri come funziona e quale è il regolamento del servizio.', 'Ma Chlò è marchio registrato, tutti i suoi contenuti, quali, a titolo esemplificativo, le immagini, le fotografie, le opere, i disegni, le figure, i loghi ed ogni altro materiale, in qualsiasi formato, pubblicato su machlo.com , compresi i menu, le pagine web, la grafica, i colori, gli schemi, gli strumenti, i caratteri ed il design del sito web, il layouts, i metodi, i processi, le funzioni ed il software che fanno parte di Ma Chlò , sono protetti dal diritto d\\\'autore e da ogni altro diritto di proprietà intellettuale del Gestore e degli altri titolari dei diritti. È vietata la riproduzione, in tutto o in parte, in qualsiasi forma, di Ma Chlò senza il consenso espresso in forma scritta del Gestore.<br><br> Ma Chlò garantisce un utilizzo dei dati personali strettamente legato all’erogazione dei propri servizi, alla gestione del sito e all’evasione degli ordini e non verranno in alcun modo venduti a terzi.<br><br> Ma Chlò non può garantire ai propri utenti che le misure adottate per la sicurezza del sito e della trasmissione dei dati e delle informazioni sul sito siano in grado di limitare o escludere qualsiasi rischio di accesso non consentito o di dispersione dei dati da parte di dispositivi di pertinenza dell’utente. Per tale motivo, suggeriamo agli utenti del sito di assicurarsi che il proprio computer sia dotato di software adeguati per la protezione della trasmissione in rete di dati (ad esempio antivirus aggiornati) e che il proprio Internet provider abbia adottato misure idonee per la sicurezza della trasmissione di dati in rete.<br><br>\r\n<h3>Pagamenti</h3>\r\nIl cliente puo’ scegliere il metodo di pagamento indicato al momento dell’acquisto nel modulo d’ordine.<br> Tutti i nostri prodotti sono made in Usa, i prezzi e le transazioni sono in Euro, eventuali cambi valuta, dazi doganali, commissioni su pagamenti con carta di credito e Paypal sono a carico del cliente.<br><br> Le informazioni relative all’esecuzione della transazione saranno inviate agli enti responsabili ( Circuito Visa/Mastercard, PayPal )  tramite protocollo crittografato, senza che terzi possano aver accesso in alcun modo. Le informazioni non saranno mai visualizzate o memorizzate da parte di Ma Chlò.<br><br>\r\n<h3>Reso</h3>\r\nIl cliente ha 14 giorni per esercitare il diritto di recesso dalla data di ricezione della merce e la spedizione di restituzione  sarà a carico del cliente.<br><br> Non è  possibile cambiare il prodotto scelto con un altro.<br> Per richiedere l’autorizzazione al reso, accedere alla sezione contatti e scrivere un messaggio di posta elettronica  all’indirizzo info@machlo.com  con indicazioni del prodotto.<br><br> Il Diritto di Recesso si intende esercitato correttamente qualora siano interamente rispettate anche le seguenti condizioni:<br> •	I prodotti non devono essere stati danneggiati, indossati, lavati e non devono presentare nessun segno d’uso.<br> •	I resi devono essere spediti all’interno della confezione Ma Chlò entro 14 giorni dalla data di comunicazione, da parte del cliente, del diritto di recesso.<br><br> Il rimborso sarà eseguito con lo stesso mezzo da te utilizzato per il pagamento dopo aver ricevuto il prodotto reso.<br> Ma Chlò si riserva inoltre il diritto di rifiutare resi non autorizzati o comunque non conformi a tutte le condizioni previste.', '', 1),
(6, 'PRIVACY', 'Privacy', 'Scopri in che modo vengo trattati i tuoi dati sensibili, l\'utilizzo dei cookie e dei dati di sessione.', '<h3>Cookie policy</h3>\r\nIn questo sito vengono utilizzati alcuni cookie tecnici che servono per la navigazione e per fornire un servizio già richiesto dall\'utente come il carrello degli acquisti. Non vengono utilizzati per scopi ulteriori e sono normalmente installati nella maggior parte dei siti web. Un cookie è una piccola particella di informazioni che viene salvata sul dispositivo dell\'utente che visita un sito web. Il cookie non contiene dati personali e non può essere utilizzato per identificare l\\\'utente all\\\'interno di altri siti web, compreso il sito web del provider di analisi. I cookie possono inoltre essere utilizzati per memorizzare le impostazioni preferite, come lingua e paese, in modo da renderle immediatamente disponibili alla visita successiva. Non utilizziamo gli indirizzi IP o i cookie per identificare personalmente gli utenti. Utilizziamo il sistema di analisi web al fine di incrementare l\\\'efficienza del nostro portale.<br><br>Per avere maggiori informazioni sui cookies vi suggeriamo di visitare il sito www.allaboutcookies.org che vi fornirà indicazioni su come gestire secondo le vostre preferenze, ed eventualmente cancellare i cookies in funzione del browser che state utilizzando.<br> In questo sito web utilizziamo il sistema di analisi Google Analytics per misurare e analizzare le visite al nostro sito. Utilizziamo gli indirizzi IP al fine di raccogliere dati sul traffico Internet, sul browser e sul computer degli utenti. Tali informazioni vengono esaminate unicamente per fini statistici. L\\\'anonimato dell\\\'utente viene rispettato. Informazioni sul funzionamento del software open source di analisi web Google Analytics.<br><br> Ribadiamo che sul sito sono operativi esclusivamente cookies tecnici (come quelli sopra elencati) necessari per navigare e che essenziali quali autenticazione, validazione, gestione di una sessione di navigazione e prevenzione delle frodi e consentono ad esempio: di identificare se l’utente ha avuto regolarmente accesso alle aree del sito che richiedono la preventiva autenticazione oppure la validazione dell’utente e la gestione delle sessioni relative ai vari servizi e applicazioni oppure la conservazione dei dati per l’accesso in modalità sicura oppure le funzioni di controllo e prevenzione delle frodi.<br> Non è obbligatorio acquisire il consenso alla operatività dei soli cookies tecnici o di terze parti o analitici assimilati ai cookies tecnici. La loro disattivazione e/o il diniego alla loro operatività comporterà l’impossibilità di una corretta navigazione sul Sito e/o la impossibilità di fruire dei servizi, delle pagine, delle funzionalità o dei contenuti ivi disponibili.<br><br> Tutti i dati inseriti dai nostri clienti all\\\'interno di moduli, carrello, ordini e procedure di pagamento verranno utilizzati esclusivamente per gli ordini e le consegne degli stessi. Per questo motivo i clienti sono obbligati a fornire i dati necessari alla compilazione dei moduli richiesti, in caso contrario non sarà possibile utilizzare il nostro servizio.<br> In particolare i dati utilizzati per i pagamenti online non verranno assolutamente trattati e/o memorizzati sui nostri sistemi in quanto passati direttamente ai servizi di pagamento Stripe e/o Paypal tramite transazione sicura SSL (HTTPS).<br>Al momento dell\\\'ordine, della richiesta di preventivo o informazioni, l\\\'indirizzo email dell\\\'utente verrà inserito nel nostro sistema di newsletter e informazioni ai clienti da cui sarà sempre possibile disiscriversi facilmente tramite il link presente in ogni comunicazione inviata.<br> In ogni caso confermiamo che i dati utilizzati e memorizzati al fine del funzionamento del servizio non verranno mai e in nessun caso ceduti a terzi per nessun tipo di finalità od utilizzo.', '', 1),
(7, 'CONTACTS', 'Contatti e recapiti', 'Puoi contattare Ma Chlò in tantissimi modi diversi: email, telefono e socials.', '', '', 1),
(8, 'GALLERY', 'Galleria immagini', 'Ecco una galleria delle nostre immagini più belle.', '', '', 1),
(9, 'HOME', 'Artistic apparel', 'Welcome to Ma Chlò\' s website, selling quality garments based on artwork with worldwide shipping.', '', '', 2),
(10, 'SHOP', 'Our products', 'Here you can find all of our products, sizes and colors available to bring our works to your home.', '', '', 2),
(11, 'ABOUT', 'About us', 'Ma Chlò was born in Sardinia in 2017 by the idea and collaboration of Stefania, Simona and Francesca who wanted to reproduce on dresses the paintings of one of the founders who have always been fond of drawing and art.', 'Ma Chlò was founded in Sardinia in 2017 by the collaboration of Stefania, Simona and Francesca. One of our founders has always been interested in art and we wanted to display her paintings on our clothing. Ma Chlò doesn’t use prints but original and real works of art which represent nature in all its shapes and colors and include different subjects and their imaginations. Our unique offering also takes inspiration from the scents, magic and landscape of Sardinia. Each picture is inspired by a trip, an emotion or an experience that the artist wants to express in her art and which every woman who wears our clothes can feel.<br><br>Apart from nature in the form of animals, trees and flowers, Ma Chlò offers our customers the clothing line: \"The Masks\" that represents the evolution of a love story. \"The idea of the masks,\" the artist tells us, “arose during a walk along the last kilometre and a half of the remaining section of the Berlin Wall. I was fascinated by a painting on this famous wall that represented two faces which had been divided from each other. After playing with the colours I understood after the first sketch that this was a story, a love story, that no barrier could ever stop.\" Different phases of the story are represented and include “the meeting”, ”the look”, “the kiss”, “the separation”, “the silence”, “the breakup”, “eternity” and “a new beginning” that Ma Chlò wants every woman to wear as an expression of her experiences. Ma Chlò currently offers t-shirts and dresses and plans to expand its range of products as we continue to grow.<h3>Mission</h3>We would like to create a meeting point between fashion and art.  We believe that our originality and the meaning of our paintings distinguishes us from other clothing companies.', 'about.jpg', 2),
(12, 'SHIPPING', 'Shipping and delivery', 'Welcome to Ma Chlò\\\'s website, selling quality garments based on artwork with worldwide shipping.', '<h3>Shipping rules and price</h3>\r\nMa Chlò shipping is free, the delivery time is within 7/10 business days once the order will be fulfilled, or you can choose an express shipping for 20€', '99fc4-blog.jpg', 2),
(13, 'RULES', 'Rules and conditions', 'Ma Chlò is a registered trademark, find out how it works and what the service policy is.', 'Ma Chlò is a registered trade mark, all of its contents, like images, photos, works, sketches, figures, logos and every other material, in any format, published on machlo.com, included menus, web pages, graphics, colors, schemes, tools, characters and the design of the web site, layouts, methods, trials, functions and the software that make part of Ma Chlò, are protected by copyright and by all other intellectual rights of the Owner and the other holders of the rights. All reproduction, in whole or in part, in any form, of Ma Chlò, without written permission of the Owner, is forbidden.<br/><br/> Ma Chlò tightly guarantees an use of the personal data tied up to its own services, to the management of the site and the escape of the orders and they won\'t come in some way sold to third.<br/><br/> Ma Chlò cannot guarantee to its own consumers that the measures adopted for the safety of the site and the transmission of the data and the information on the site is able to limit or to exclude any risk of access not allowed or of dispersion of the data from devices of pertinence of the consumer. For such motive, we suggest to the consumers of the site to make sure that his/her own computer both endowed with suitable software for the protection of the transmission online of data (for instance adjourned antivirus) and that the proper Internet provider has adopted online fit measures for the safety of the data transmission.<br>\r\n<h3>Payments</h3>\r\nThe consumer is able to choose the method of suitable payment during the purchase in the form of order. All of our products are made in Usa, the prices and the transactions are in Euro, possible changes currency, customs, errands on payments with credit card and Paypal are to load of the consumer. The financial information (Encircled Visa / Mastercard PayPal) will be cryptographically forwarded , without third parties being able to access said information in any way . Information will never be visualized or you memorize from Ma Chlò.<br/><br/>\r\n<h3>Refund</h3>\r\nThe consumer has the right to withdraw from the contract concluded with the Vendor, within 14 working days from the day of receiving the products purchased on “Ma Chlò”.<br/> Return will be a customer\'s charge.<br/> An item cannot be exchanged for another one.<br/><br/> To ask for the authorization to theproduct, you can contact us by our web site on the section “contact” writing an e-mail message to the address info@machlo.com with indications of the product.<br/> The Right of Return is considered correctly followed when the following conditions are also completely met:<br/><br/> •	The products must not have been damaged, worn, washed and must not show any sign of use.<br/> •	the products must be returned in their original packaging to Ma Chlò within 14 days from the date of communication, from the consumer, of the right of recess.<br/><br/> The refund will be performed with the same metod used for the payment after having received the product.<br/><br/> Ma Chlò reserves the right to refuse non authorized refunds or however you doesn\'t conform to all the anticipated conditions.', '', 2),
(14, 'PRIVACY', 'Privacy', 'Find out how we treat your sensitive data, use cookies, and sessions data.', '<h3>Cookie policy</h3>\r\nThis site uses some technical cookies that are used to navigate and provide a service already required by the user as the shopping cart. They are not used for further purposes and are normally installed on most websites. A cookie is a small piece of information that is saved on the user\\\'s device that visits a website. The cookie does not contain personal data and can not be used to identify the user inside other websites, including the analyst\\\'s website. Cookies can also be used to store your favorite settings, such as your language and country, so that you can make them available immediately to your next visit. We do not use IP addresses or cookies to personally identify users. We use the web analytics system to increase the efficiency of our portal. <br> <br> For more information on cookies, please visit www.allaboutcookies.org to provide you with directions on how to handle your cookies Your preferences, and possibly delete cookies as a browser you are using. <br> On this website we use the Google Analytics analysis system to measure and analyze visits to our site. We use IP addresses to collect data on Internet traffic, browsers, and user computers. This information is only examined for statistical purposes. The user\\\'s anonymity is respected. About the operation of the open source Google Analytics web analytics software. <br> <br> We reiterate that technical cookies (such as those listed above) are required to navigate and essential, such as authentication, validation, management of a session of Navigation and fraud prevention and allow, for example: to identify whether the user has regularly accessed areas of the site that require prior authentication or user validation and session management for various services and applications or the retention of Data for secure access or the control and prevention of fraud. <br> It is not compulsory to acquire the consent of operating only technical or third-party cookies or analytics similar to technical cookies. Their deactivation and / or denial of their operation will result in the inability to properly navigate the Site and / or the inability to access the services, pages, features or content available there. The data entered by our customers inside forms, shopping carts, orders and payment procedures will only be used for orders and deliveries. For this reason, customers are required to provide the data required to complete the required forms, otherwise we will not be able to use our service. In particular, the data used for online payments will not be treated and / or stored on Our systems as they have passed directly to the Stripe and / or Paypal payment services via Secure SSL Transaction (HTTPS). <br> At the time of ordering the quote or information, the user\\\'s email address will come Inserted in our newsletter system and customer information from which it will always be possible to easily disagree with the link in any communication sent. <br> In any case, we confirm that the data used and stored for the purpose of service will never and No case has been transferred to third parties for any purpose or use.', '', 2),
(15, 'CONTACTS', 'Contacts and infos', 'You can contact Ma Chlò in many different ways: email, phone and socials.', '', '', 2),
(16, 'GALLERY', 'Images gallery', 'This is our best images gallery.', '', '', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti`
--

CREATE TABLE `prodotti` (
  `id_prodotti` int(11) NOT NULL,
  `id_tipo_prodotto` int(11) DEFAULT NULL,
  `codice` varchar(100) DEFAULT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `prezzo` double DEFAULT NULL,
  `url_img_piccola` varchar(250) DEFAULT NULL,
  `url_img_grande` varchar(250) DEFAULT NULL,
  `stato` tinyint(1) NOT NULL,
  `varianti` int(11) NOT NULL,
  `ordine` int(11) NOT NULL DEFAULT '0',
  `sync_status` int(2) NOT NULL,
  `sync_time` datetime NOT NULL,
  `tipo_stampa` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti`
--

INSERT INTO `prodotti` (`id_prodotti`, `id_tipo_prodotto`, `codice`, `nome`, `prezzo`, `url_img_piccola`, `url_img_grande`, `stato`, `varianti`, `ordine`, `sync_status`, `sync_time`, `tipo_stampa`) VALUES
(1, 5, '5aa5a386dd8547', 'Blue cat', 47.95, '5aa5a386dd8547.png', '5aa5a386dd8547.png', 1, 8, 0, 2, '2018-06-04 02:30:01', 1),
(4, 2, '5aa3013293e052', 'Lips t-shirt', 40.69, '5aa3013293e052.png', '5aa3013293e052.png', 1, 35, 1, 2, '2018-06-04 02:30:01', 0),
(5, 2, '5aa2fe0e98fe41', 'Flowers', 39.96, '5aa2fe0e98fe41.png', '5aa2fe0e98fe41.png', 1, 8, 1, 2, '2018-06-04 02:30:01', 1),
(6, 3, '5aa2fb8b66a114', 'Masks', 39.98, '5aa2fb8b66a114.png', '5aa2fb8b66a114.png', 1, 8, 25, 2, '2018-06-04 02:30:01', 1),
(7, 2, '5aa2f943e9f697', 'Yellow cat', 38.36, '5aa2f943e9f697.png', '5aa2f943e9f697.png', 1, 15, 1, 2, '2018-06-04 02:30:01', 1),
(8, 5, '5aa2f81d3552c7', 'Gallinella sarda', 45.97, '5aa2f81d3552c7.png', '5aa2f81d3552c7.png', 1, 8, 10, 2, '2018-06-04 02:30:01', 1),
(9, 4, '5aa2a4314a7da4', 'Yellow cat', 24.97, '5aa2a4314a7da4.png', '5aa2a4314a7da4.png', 1, 1, 1, 2, '2018-06-04 02:30:01', 0),
(10, 5, '5aa2a3db357648', 'Yang', 7.95, '5aa2a3db357648.png', '5aa2a3db357648.png', 1, 1, 1, 2, '2018-06-04 02:30:01', 0),
(11, 4, '5aa2a35b45f316', 'Universe', 24.97, '5aa2a35b45f316.png', '5aa2a35b45f316.png', 1, 1, 1, 2, '2018-06-04 02:30:01', 2),
(12, 4, '5aa2a2dc902565', 'Standby', 24.97, '5aa2a2dc902565.png', '5aa2a2dc902565.png', 1, 1, 1, 2, '2018-06-04 02:30:01', 2),
(13, 4, '5aa2a2244efd56', 'Ma Chlò', 24.97, '5aa2a2244efd56.png', '5aa2a2244efd56.png', 1, 1, 5, 2, '2018-06-04 02:30:01', 0),
(14, 4, '5aa29e4923eeb6', 'La separazione', 24.97, '5aa29e4923eeb6.png', '5aa29e4923eeb6.png', 1, 1, 1, 2, '2018-06-04 02:30:01', 2),
(15, 4, '5aa29dff07e579', 'Japan', 24.97, '5aa29dff07e579.png', '5aa29dff07e579.png', 1, 1, 9, 2, '2018-06-04 02:30:01', 2),
(16, 4, '5aa29cee9d00e1', 'Il silenzio', 24.97, '5aa29cee9d00e1.png', '5aa29cee9d00e1.png', 1, 1, 1, 2, '2018-06-04 02:30:01', 0),
(17, 4, '5aa29cb78a18b1', 'Un nuovo inizio', 24.97, '5aa29cb78a18b1.png', '5aa29cb78a18b1.png', 1, 1, 7, 2, '2018-06-04 02:30:01', 2),
(18, 4, '5aa29c681a45f2', 'Il bacio', 24.97, '5aa29c681a45f2.png', '5aa29c681a45f2.png', 1, 1, 1, 2, '2018-06-04 02:30:01', 2),
(19, 3, '5aa29c1e763eb7', 'Il pensiero', 24.97, '5aa29c1e763eb7.png', '5aa29c1e763eb7.png', 1, 1, 11, 2, '2018-06-04 02:30:01', 2),
(20, 2, '5aa29bd9d57cd5', 'Jellyfish', 24.97, '5aa29bd9d57cd5.png', '5aa29bd9d57cd5.png', 1, 1, 16, 2, '2018-06-04 02:30:01', 2),
(21, 4, '5aa29b851d86a1', 'Color tree', 24.97, '5aa29b851d86a1.png', '5aa29b851d86a1.png', 1, 1, 1, 2, '2018-06-04 02:30:01', 2),
(22, 4, '5aa29b26787987', 'Colorful town', 24.97, '5aa29b26787987.png', '5aa29b26787987.png', 1, 1, 1, 2, '2018-06-04 02:30:01', 2),
(23, 3, '5aa29ab9c856c2', 'A monkey playing with a snake', 24.97, '5aa29ab9c856c2.png', '5aa29ab9c856c2.png', 1, 1, 12, 2, '2018-06-04 02:30:01', 0),
(24, 4, '5aa29a3e69b2c4', 'Feel good', 24.97, '5aa29a3e69b2c4.png', '5aa29a3e69b2c4.png', 1, 1, 8, 2, '2018-06-04 02:30:01', 2),
(25, 2, '5aa299f1a4ad82', 'Talking', 24.97, '5aa299f1a4ad82.png', '5aa299f1a4ad82.png', 1, 1, 17, 2, '2018-06-04 02:30:01', 2),
(26, 3, '5aa298b3ec6ad5', 'Japan', 54.35, '5aa298b3ec6ad5.png', '5aa298b3ec6ad5.png', 1, 5, 3, 2, '2018-06-04 02:30:01', 2),
(27, 1, '5aa297d0cfdd44', 'Eclipse', 54.35, '5aa297d0cfdd44.png', '5aa297d0cfdd44.png', 1, 5, 3, 2, '2018-06-04 02:30:01', 2),
(28, 2, '5aa27b23a9f4a8', 'Yang', 54.35, '5aa27b23a9f4a8.png', '5aa27b23a9f4a8.png', 1, 5, 7, 2, '2018-06-04 02:30:01', 2),
(29, 1, '5aa27a84d1e238', 'Standby', 54.35, '5aa27a84d1e238.png', '5aa27a84d1e238.png', 1, 5, 6, 2, '2018-06-04 02:30:01', 2),
(30, 2, '5aa27a09f005c3', 'Universe', 54.35, '5aa27a09f005c3.png', '5aa27a09f005c3.png', 1, 5, 4, 2, '2018-06-04 02:30:01', 2),
(31, 1, '5aa27975242e84', 'Colored cats', 54.35, '5aa27975242e84.png', '5aa27975242e84.png', 1, 5, 3, 2, '2018-06-04 02:30:01', 2),
(32, 1, '5aa275790bd952', 'Geometric', 54.35, '5aa275790bd952.png', '5aa275790bd952.png', 1, 5, 1, 2, '2018-06-04 02:30:01', 2),
(33, 1, '5aa010e062b998', 'Il pensiero', 60.32, '5aa010e062b998.png', '5aa010e062b998.png', 1, 5, 1, 2, '2018-06-04 02:30:01', 2),
(34, 1, '5aa00f5f1c42e5', 'Jungle flowers dress black', 60.32, '5aa00f5f1c42e5.png', '5aa00f5f1c42e5.png', 1, 5, 2, 2, '2018-06-04 02:30:01', 2),
(35, 2, '5aa00eb9a04005', 'Jungle flowers dress', 60.32, '5aa00eb9a04005.png', '5aa00eb9a04005.png', 1, 5, 1, 2, '2018-06-04 02:30:01', 2),
(36, 1, '5aa00df8cceb58', 'La separazione', 60.32, '5aa00df8cceb58.png', '5aa00df8cceb58.png', 1, 5, 1, 2, '2018-06-04 02:30:01', 2),
(37, 3, '5aa00db6bb9d33', 'Life of a tree', 60.32, '5aa00db6bb9d33.png', '5aa00db6bb9d33.png', 1, 5, 6, 2, '2018-06-04 02:30:01', 2),
(38, 2, '5aa00c3e2e6184', 'Lips dress', 69.95, '5aa00c3e2e6184.png', '5aa00c3e2e6184.png', 1, 5, 19, 2, '2018-06-04 02:30:01', 0),
(39, 1, '5aa00bc3dc48f8', 'Monkeys into the jungle', 60.32, '5aa00bc3dc48f8.png', '5aa00bc3dc48f8.png', 1, 5, 1, 2, '2018-06-04 02:30:01', 2),
(40, 1, '5aa00b9b1fc403', 'Reaching for the moon', 60.32, '5aa00b9b1fc403.png', '5aa00b9b1fc403.png', 1, 5, 1, 2, '2018-06-04 02:30:01', 2),
(41, 3, '5aa009916da013', 'Un nuovo inizio', 60.32, '5aa009916da013.png', '5aa009916da013.png', 1, 5, 8, 2, '2018-06-04 02:30:01', 2),
(42, 1, '5aa0058e449ec3', 'Woman\'s lips', 60.32, '5aa0058e449ec3.png', '5aa0058e449ec3.png', 1, 5, 1, 2, '2018-06-04 02:30:01', 2),
(43, 3, '5aa00071b87794', 'Wild girl', 60.32, '5aa00071b87794.png', '5aa00071b87794.png', 1, 5, 6, 2, '2018-06-04 02:30:01', 2),
(44, 1, '5aa0001c6a2461', 'Snakes dress', 60.32, '5aa0001c6a2461.png', '5aa0001c6a2461.png', 1, 5, 6, 2, '2018-06-04 02:30:01', 2),
(45, 1, '5a9fffa5692b77', 'Colorful town', 60.32, '5a9fffa5692b77.png', '5a9fffa5692b77.png', 1, 5, 4, 2, '2018-06-04 02:30:01', 2),
(47, 5, '5a9e7dbe639f93', 'Gallinella sarda t-shirt', 49.95, '5a9e7dbe639f93.png', '5a9e7dbe639f93.png', 1, 4, 14, 2, '2018-06-04 02:30:01', 0),
(49, 5, '5a9e7cfa97eb11', 'Dreaming of you t-shirt', 49.95, '5a9e7cfa97eb11.png', '5a9e7cfa97eb11.png', 1, 4, 12, 2, '2018-06-04 02:30:01', 0),
(50, 1, '5a9e7be6ef2c59', 'Il bacio t-shirt', 49.27, '5a9e7be6ef2c59.png', '5a9e7be6ef2c59.png', 1, 6, 10, 2, '2018-06-04 02:30:01', 2),
(51, 1, '5a9e7b0f14d151', 'Il pensiero t-shirt', 49.27, '5a9e7b0f14d151.png', '5a9e7b0f14d151.png', 1, 6, 9, 2, '2018-06-04 02:30:01', 2),
(52, 1, '5a9e7a457b0155', 'Il silenzio t-shirt', 49.27, '5a9e7a457b0155.png', '5a9e7a457b0155.png', 1, 6, 3, 2, '2018-06-04 02:30:01', 2),
(53, 1, '5a9e7938145986', 'Infinito t-shirt', 49.27, '5a9e7938145986.png', '5a9e7938145986.png', 1, 6, 4, 2, '2018-06-04 02:30:01', 2),
(54, 1, '5a9e784dd4fc14', 'La separazione t-shirt', 49.27, '5a9e784dd4fc14.png', '5a9e784dd4fc14.png', 1, 6, 5, 2, '2018-06-04 02:30:01', 2),
(55, 1, '5a9e76e177cf98', 'Lo sguardo t-shirt', 49.27, '5a9e76e177cf98.png', '5a9e76e177cf98.png', 1, 6, 7, 2, '2018-06-04 02:30:01', 2),
(56, 1, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt', 49.27, '5a8ed76cdfac23.png', '5a8ed76cdfac23.png', 1, 6, 20, 2, '2018-06-04 02:30:01', 2),
(57, 1, '5a8ed67932c228', 'L\'incontro T-shirt', 49.27, '5a8ed67932c228.png', '5a8ed67932c228.png', 1, 6, 1, 2, '2018-06-04 02:30:01', 2),
(61, 5, '5ac78fc799f779', '\"Dreaming of you\" black dress', 69.95, '5ac78fc799f779.png', '5ac78fc799f779.png', 1, 5, 18, 2, '2018-06-04 02:30:01', 2),
(62, 5, '5ac78f5a022b03', 'Dreaming of you Red', 69.95, '5ac78f5a022b03.png', '5ac78f5a022b03.png', 1, 5, 19, 2, '2018-06-04 02:30:01', 2),
(63, 5, '5ac78eeb5ed556', 'Jellyfish', 69.95, '5ac78eeb5ed556.png', '5ac78eeb5ed556.png', 1, 5, 30, 2, '2018-06-04 02:30:01', 2),
(64, 5, '5ac7a04fbb0094', '\"Japan\" dress', 69.95, '5ac7a04fbb0094.png', '5ac7a04fbb0094.png', 1, 5, 15, 2, '2018-06-04 02:30:01', 2),
(65, 5, '5ac79fa0e200e6', '\"Lips\" black dress', 69.95, '5ac79fa0e200e6.png', '5ac79fa0e200e6.png', 1, 5, 17, 2, '2018-06-04 02:30:01', 2),
(66, 5, '5ac79f41a6a458', '\"Lips\" violet dress', 69.95, '5ac79f41a6a458.png', '5ac79f41a6a458.png', 1, 5, 18, 2, '2018-06-04 02:30:01', 2),
(67, 5, '5ac89b9c141e76', '\"Colorful cats\" ', 42.95, '5ac89b9c141e76.png', '5ac89b9c141e76.png', 1, 5, 6, 2, '2018-06-04 02:30:01', 1),
(68, 5, '5acd1cefea6059', 'Ocean purple', 69.95, '5acd1cefea6059.png', '5acd1cefea6059.png', 1, 5, 28, 2, '2018-06-04 02:30:01', 2),
(69, 5, '5ace35879dac75', '\" Fluo \" Tank Top', 49.95, '5ace35879dac75.png', '5ace35879dac75.png', 1, 5, 19, 2, '2018-06-04 02:30:01', 2),
(70, 5, '5ace34e242d3a1', '\" Colored cats \" tank top', 49.95, '5ace34e242d3a1.png', '5ace34e242d3a1.png', 1, 5, 0, 2, '2018-06-04 02:30:01', 2),
(71, 5, '5ace33a14f8df1', '\" Colorful town \" tank top', 49.95, '5ace33a14f8df1.png', '5ace33a14f8df1.png', 1, 5, 15, 2, '2018-06-04 02:30:01', 2),
(72, 5, '5ace560ed99ae6', '\"Sleeping mask \" Tank Top', 39.95, '5ace560ed99ae6.png', '5ace560ed99ae6.png', 1, 4, 17, 2, '2018-06-04 02:30:01', 1),
(73, 5, '5ae4ae11af8f51', '\"Mindyoursoul\" T-shirt', 15.5, '5ae4ae11af8f51.png', '5ae4ae11af8f51.png', 1, 4, 1, 2, '2018-06-04 02:30:01', 0),
(75, 5, '5ae4ad0da64437', '\"Il pensiero\" Tank top', 46.95, '5ae4ad0da64437.png', '5ae4ad0da64437.png', 1, 6, 10, 2, '2018-06-04 02:30:01', 2),
(76, 5, '5ae4ac4dd579a8', '\"Lo sguardo\" Tank top', 46.95, '5ae4ac4dd579a8.png', '5ae4ac4dd579a8.png', 1, 6, 16, 2, '2018-06-04 02:30:01', 2),
(78, 5, '5ae4aad40b2c44', '\"Bonds\" Tank top', 46.95, '5ae4aad40b2c44.png', '5ae4aad40b2c44.png', 1, 6, 20, 2, '2018-06-04 02:30:01', 2),
(79, 5, '5b01cfb9a57859', '\"Infinito\" Beach Towel', 39.99, '5b01cfb9a57859.png', '5b01cfb9a57859.png', 1, 1, 20, 2, '2018-06-04 02:30:01', 2),
(80, 5, '5b0157f0abdc04', '\"Tinge\" Beach Towel', 39.99, '5b0157f0abdc04.png', '5b0157f0abdc04.png', 1, 1, 33, 2, '2018-06-04 02:30:01', 2),
(81, 5, '5b0157a39d1385', '\"Summer flowers\" Beach Towel', 39.99, '5b0157a39d1385.png', '5b0157a39d1385.png', 1, 1, 24, 2, '2018-06-04 02:30:01', 2),
(82, 5, '5b0155db8ec4b7', '\"Bonds\" Beach Towel', 39.99, '5b0155db8ec4b7.png', '5b0155db8ec4b7.png', 1, 1, 34, 2, '2018-06-04 02:30:01', 2),
(83, 5, '5b0155800b1581', '\"Colorful tree\" Beach Towel', 39.99, '5b0155800b1581.png', '5b0155800b1581.png', 1, 1, 26, 2, '2018-06-04 02:30:01', 2),
(84, 5, '5b015504d39408', '\"Flowers\" Beach Towel', 39.99, '5b015504d39408.png', '5b015504d39408.png', 1, 1, 32, 2, '2018-06-04 02:30:01', 2),
(85, 5, '5b0154ab6a0ab6', '\"A woman with a monkey\" Beach Towel', 39.99, '5b0154ab6a0ab6.png', '5b0154ab6a0ab6.png', 1, 1, 10, 2, '2018-06-04 02:30:01', 2),
(86, 5, '5b01542043e103', '\"Yang\" Beach Towel', 39.99, '5b01542043e103.png', '5b01542043e103.png', 1, 1, 38, 2, '2018-06-04 02:30:01', 2),
(87, 5, '5b0153c5c24c51', '\"Fluo\" Beach Towel', 39.99, '5b0153c5c24c51.png', '5b0153c5c24c51.png', 1, 1, 25, 2, '2018-06-04 02:30:01', 2),
(88, 5, '5b0c42cfb530a8', '\"Explosion\" Beach Bag', 43.95, '5b0c42cfb530a8.png', '5b0c42cfb530a8.png', 1, 1, 12, 2, '2018-06-04 02:30:01', 2),
(89, 5, '5b0c418f41dd81', '\"Jellyfish\" Beach Bag', 43.95, '5b0c418f41dd81.png', '5b0c418f41dd81.png', 1, 1, 8, 2, '2018-06-04 02:30:01', 2),
(90, 5, '5b0c36a3794d15', '\"Flowers\" Beach Bag', 43.95, '5b0c36a3794d15.png', '5b0c36a3794d15.png', 1, 1, 1, 2, '2018-06-04 02:30:01', 2),
(91, 5, '5b0c35e96a9326', '\"Yang\" Beach Bag', 43.95, '5b0c35e96a9326.png', '5b0c35e96a9326.png', 1, 1, 9, 2, '2018-06-04 02:30:01', 2),
(92, 5, '5b0c3534992b15', '\"White bonds\" Beach Bag', 43.95, '5b0c3534992b15.png', '5b0c3534992b15.png', 1, 1, 11, 2, '2018-06-04 02:30:01', 2),
(93, 5, '5b0c34b4a6f9c4', '\"Bonds\" Beach Bag', 43.95, '5b0c34b4a6f9c4.png', '5b0c34b4a6f9c4.png', 1, 1, 10, 2, '2018-06-04 02:30:01', 2),
(94, 5, '5b0c33e33e5128', '\"Colored tree\" Beach Bag', 43.95, '5b0c33e33e5128.png', '5b0c33e33e5128.png', 1, 1, 1, 2, '2018-06-04 02:30:01', 2),
(95, 5, '5b0c330931f679', '\"Il silenzio\" Beach Bag', 43.95, '5b0c330931f679.png', '5b0c330931f679.png', 1, 1, 5, 2, '2018-06-04 02:30:01', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_categorie`
--

CREATE TABLE `prodotti_categorie` (
  `id_prodotti_categorie` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti_categorie`
--

INSERT INTO `prodotti_categorie` (`id_prodotti_categorie`, `id_prodotto`, `id_categoria`) VALUES
(1, 59, 15),
(3, 58, 16),
(4, 4, 16),
(15, 50, 5),
(16, 57, 5),
(17, 56, 5),
(18, 55, 5),
(19, 54, 5),
(20, 53, 5),
(21, 52, 5),
(22, 51, 5),
(42, 25, 15),
(43, 24, 15),
(44, 23, 15),
(45, 22, 15),
(46, 21, 15),
(47, 20, 15),
(48, 19, 15),
(49, 18, 15),
(50, 17, 15),
(51, 16, 15),
(52, 15, 15),
(53, 14, 15),
(54, 13, 15),
(55, 12, 15),
(56, 11, 15),
(57, 9, 15),
(58, 32, 13),
(59, 31, 13),
(60, 30, 13),
(61, 29, 13),
(62, 28, 13),
(63, 27, 13),
(64, 26, 13),
(71, 63, 12),
(72, 63, 6),
(73, 47, 9),
(74, 49, 9),
(75, 49, 12),
(76, 47, 12),
(77, 60, 6),
(78, 45, 6),
(79, 44, 6),
(80, 43, 6),
(81, 42, 6),
(82, 41, 6),
(83, 40, 6),
(84, 39, 6),
(85, 38, 6),
(86, 37, 6),
(87, 36, 6),
(88, 35, 6),
(89, 34, 6),
(90, 33, 6),
(91, 62, 6),
(92, 62, 12),
(93, 61, 6),
(94, 61, 12),
(95, 8, 12),
(96, 8, 8),
(97, 7, 16),
(98, 66, 6),
(99, 66, 12),
(100, 65, 6),
(101, 65, 12),
(102, 64, 6),
(103, 64, 12),
(104, 6, 16),
(105, 1, 8),
(106, 5, 16),
(107, 67, 8),
(108, 68, 6),
(109, 68, 12),
(110, 71, 12),
(111, 71, 1),
(112, 70, 12),
(113, 70, 1),
(114, 69, 12),
(115, 69, 1),
(116, 72, 12),
(117, 72, 1),
(118, 78, 12),
(119, 78, 1),
(120, 76, 12),
(121, 76, 1),
(122, 75, 12),
(123, 75, 1),
(124, 7, 8),
(125, 6, 8),
(126, 4, 8),
(127, 87, 17),
(128, 86, 17),
(129, 85, 17),
(130, 84, 17),
(131, 83, 17),
(132, 82, 17),
(133, 81, 17),
(134, 80, 17),
(135, 79, 17),
(137, 95, 18),
(138, 94, 18),
(139, 93, 18),
(140, 92, 18),
(141, 91, 18),
(142, 90, 18),
(143, 89, 18),
(144, 88, 18);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_traduzioni`
--

CREATE TABLE `prodotti_traduzioni` (
  `id_prodotti_traduzioni` int(11) NOT NULL,
  `id_prodotti` int(11) NOT NULL,
  `descrizione` text NOT NULL,
  `descrizione_breve` varchar(250) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti_traduzioni`
--

INSERT INTO `prodotti_traduzioni` (`id_prodotti_traduzioni`, `id_prodotti`, `descrizione`, `descrizione_breve`, `lingua_traduzione_id`) VALUES
(5, 4, '- 100% cotone pettinato ad eccezione del grigio melange ( 10% poliestere ), grigio melange scuro e blue melange ( 65% poliestere ) \n- Silhouette classica semi-sagomata con cucitura laterale\n- Taglio elegante e leggero, tessuto morbido che resisterà a ripetuti lavaggi\n- Maniche con doppio ago e orlo inferiore\n- Particolare del logo Ma Chlò \n- Disponibile nelle taglie S - 2XL\n\nCura del capo: Lavaggio in lavatrice a freddo, ciclo delicato con detergente delicato e colori simili. Non stirare la decorazione e non lavare a secco.', 'T-shirt con labbra stampate', 1),
(6, 4, '- Made of 100% combed ring-spun cotton, except for heather grey (contains 10% polyester), dark heather grey and heather blue (65% polyester).\n- Classic semi-contoured silhouette with side seam\n- Double-needle stitched sleeves and bottom hem\n- Ma Chlò logo on the back\n- Available in sizes S - 2XL\n\nCare instructions: Machine wash cold, inside-out, gentle cycle with mild detergent and similar colors. Tumble dry low, or hang-dry for longest life. Cool iron inside-out if necessary. Do \n                                 not iron decoration. Do not dry clean.', 'Lips T-shirt', 2),
(7, 57, '- 96% poliestere / 4% elastan\n- Tessuto elasticizzato con vestibilità regolare\n- realizzata in jersey di poliestere di cotone super liscio e confortevole che non si sbiadirà dopo il lavaggio\n- Il capo è stampato a sublimazione, tagliato e cucito a mano \n- Particolare del dipinto originale \" L\'incontro \" Love Story Masks con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - 2XL\n\nCura del capo:  lavaggio ciclo delicati max 30°', 'T-shirt con maschere colorate', 1),
(8, 57, '- Made from super smooth and comfortable cotton touch polyester jersey that won’t fade after washing \n- Each garment is sublimation printed, cut, and hand-sewn\n- 96% polyester/4% elastane\n- Regular fit\n- Original artwork detail from \" L\'incontro \" Love Story Masks, Ma Chlò logo on the back\n- Available in sizes XS - 2XL\n\nCare instructions: delicate wash max 30°', 'Love Story masks T-shirt', 2),
(9, 56, '- 96% poliestere / 4% elastan\n- Tessuto elasticizzato con vestibilità regolare\n- realizzata in jersey di poliestere di cotone super liscio e confortevole che non si sbiadirà dopo il lavaggio\n- Il capo è stampato a sublimazione, tagliato e cucito a mano \n- Particolare del dipinto originale \" Un nuovo inizio \" Love Story Masks con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - 2XL\n\nCura del capo:  lavaggio ciclo delicati max 30°', 'T-shirt con maschere colorate', 1),
(10, 56, '- Made from super smooth and comfortable cotton touch polyester jersey that won’t fade after washing \n- Each garment is sublimation printed, cut, and hand-sewn\n- 96% polyester/4% elastane\n- Regular fit\n- Original artwork detail from \" Un nuovo inizio \" Love Story Masks, Ma Chlò logo on the back\n- Available in sizes XS - 2XL\n\nCare instructions: delicate wash max 30°', 'Love Story masks t-shirt', 2),
(11, 55, '- 96% poliestere / 4% elastan\n- Tessuto elasticizzato con vestibilità regolare\n- realizzata in jersey di poliestere di cotone super liscio e confortevole che non si sbiadirà dopo il lavaggio\n- Il capo è stampato a sublimazione, tagliato e cucito a mano \n- Particolare del dipinto originale \" Lo sguardo \" Love Story Masks con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - 2XL\n\nCura del capo:  lavaggio ciclo delicati max 30°', 'T-shirt con maschere colorate', 1),
(12, 55, '- Made from super smooth and comfortable cotton touch polyester jersey that won’t fade after washing \n- Each garment is sublimation printed, cut, and hand-sewn\n- 96% polyester/4% elastane\n- Regular fit\n- Original artwork detail from \" Lo sguardo \" Love Story Masks, Ma Chlò logo on the back\n- Available in sizes XS - 2XL\n\nCare instructions: delicate wash max 30°', 'Love story masks t-shirt', 2),
(13, 54, '- 96% poliestere / 4% elastan\n- Tessuto elasticizzato con vestibilità regolare\n- realizzata in jersey di poliestere di cotone super liscio e confortevole che non si sbiadirà dopo il lavaggio\n- Il capo è stampato a sublimazione, tagliato e cucito a mano \n- Particolare del dipinto originale \" La separazione \" Love Story Masks con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - 2XL\n\nCura del capo:  lavaggio ciclo delicati max 30°', 'T-shirt con maschere colorate', 1),
(14, 54, '- Made from super smooth and comfortable cotton touch polyester jersey that won’t fade after washing \n- Each garment is sublimation printed, cut, and hand-sewn\n- 96% polyester/4% elastane\n- Regular fit\n- Original artwork detail from \" La separazione \" Love Story Masks, Ma Chlò logo on the back\n- Available in sizes XS - 2XL\n\nCare instructions: delicate wash max 30°', 'Love story masks t-shirt', 2),
(15, 53, '- 96% poliestere / 4% elastan\n- Tessuto elasticizzato con vestibilità regolare\n- realizzata in jersey di poliestere di cotone super liscio e confortevole che non si sbiadirà dopo il lavaggio\n- Il capo è stampato a sublimazione, tagliato e cucito a mano \n- Particolare del dipinto originale \" L\'Infinito \" Love Story Masks con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - 2XL\n\nCura del capo:  lavaggio ciclo delicati max 30°', 'T-shirt con maschere colorate', 1),
(16, 53, '- Made from super smooth and comfortable cotton touch polyester jersey that won’t fade after washing \n- Each garment is sublimation printed, cut, and hand-sewn\n- 96% polyester/4% elastane\n- Regular fit\n- Original artwork detail from \" L\'Infinito \" Love Story Masks, Ma Chlò logo on the back\n- Available in sizes XS - 2XL\n\nCare instructions: delicate wash max 30°', 'Love story masks t-shirt', 2),
(17, 52, '- 96% poliestere / 4% elastan\n- Tessuto elasticizzato con vestibilità regolare\n- realizzata in jersey di poliestere di cotone super liscio e confortevole che non si sbiadirà dopo il lavaggio\n- Il capo è stampato a sublimazione, tagliato e cucito a mano \n- Particolare del dipinto originale \" Il silenzio \" Love Story Masks con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - 2XL\n\nCura del capo:  lavaggio ciclo delicati max 30°', 'T-shirt con maschere colorate', 1),
(18, 52, '- Made from super smooth and comfortable cotton touch polyester jersey that won’t fade after washing \n- Each garment is sublimation printed, cut, and hand-sewn\n- 96% polyester/4% elastane\n- Regular fit\n- Original artwork detail from \" Il silenzio \" Love Story Masks, Ma Chlò logo on the back\n- Available in sizes XS - 2XL\n\nCare instructions: delicate wash max 30°', 'Love story masks t-shirt', 2),
(19, 51, '- 96% poliestere / 4% elastan\n- Tessuto elasticizzato con vestibilità regolare\n- realizzata in jersey di poliestere di cotone super liscio e confortevole che non si sbiadirà dopo il lavaggio\n- Il capo è stampato a sublimazione, tagliato e cucito a mano \n- Particolare del dipinto originale \" Il pensiero \" Love Story Masks con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - 2XL\n\nCura del capo:  lavaggio ciclo delicati max 30°', 't-shirt con maschere colorate', 1),
(20, 51, '- Made from super smooth and comfortable cotton touch polyester jersey that won’t fade after washing \n- Each garment is sublimation printed, cut, and hand-sewn\n- 96% polyester/4% elastane\n- Regular fit\n- Original artwork detail from \" Il pensiero \" Love Story Masks, Ma Chlò logo on the back\n- Available in sizes XS - 2XL\n\nCare instructions: delicate wash max 30°', 'Love story masks t-shirt', 2),
(21, 50, '- 96% poliestere / 4% elastan\n- Tessuto elasticizzato con vestibilità regolare\n- realizzata in jersey di poliestere di cotone super liscio e confortevole che non si sbiadirà dopo il lavaggio\n- Il capo è stampato a sublimazione, tagliato e cucito a mano \n- Particolare del dipinto originale \" Il bacio \" Love Story Masks con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - 2XL\n\nCura del capo:  lavaggio ciclo delicati max 30°', 'T-shirt con maschere colorate', 1),
(22, 50, '- Made from super smooth and comfortable cotton touch polyester jersey that won’t fade after washing \n- Each garment is sublimation printed, cut, and hand-sewn\n- 96% polyester/4% elastane\n- Regular fit\n- Original artwork detail from \" Il bacio \" Love Story Masks, Ma Chlò logo on the back\n- Available in sizes XS - 2XL\n\nCare instructions: delicate wash max 30°', 'Love story masks t-shirt', 2),
(23, 34, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Flowers \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'abito donna con fiori colorati', 1),
(24, 34, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Flowers \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'women\'s flowers print dress', 2),
(25, 49, '- 65% poliestere, 35% viscosa\n- Maglietta a maniche lunghe con orlo inferiore curvo, cuciture laterali, vestibilità comoda e drappeggiata con maniche strette\n- Particolare del dipinto originale \" Sleeping mask \" con logo Ma Chlò\n- Disponibile nelle taglie S - XL\n\nCura del capo:  Lavaggio in lavatrice a freddo, ciclo delicato con detergente delicato e colori simili. Non stirare la decorazione e non lavare a secco.', 'Maglietta a maniche lunghe con maschera', 1),
(26, 49, '- 65% polyester, 35% viscose\n- Curved bottom hem, side seams, relaxed, drapey fit and tight sleeves\n- Original artwork detail from \" Sleeping mask \" with Ma Chlò logo\n- Available in sizes S - XL\n\nCare instructions: Machine wash cold, gentle cycle with mild detergent and similar colors. Tumble dry low, or hang-dry for longest life. Do not iron decoration. Do not dry clean.\n\n\n', 'Long sleeve mask t-shirt', 2),
(27, 47, '- 65% poliestere, 35% viscosa\n- Maglietta a maniche lunghe con orlo inferiore curvo, cuciture laterali, vestibilità comoda e drappeggiata con maniche strette\n- Particolare del dipinto originale \" Gallinella sarda \" con logo Ma Chlò\n- Disponibile nelle taglie S - XL\n\nCura del capo:  Lavaggio in lavatrice a freddo, ciclo delicato con detergente delicato e colori simili. Non stirare la decorazione e non lavare a secco.', 'Maglietta a maniche lunghe con gallinella sarda', 1),
(28, 47, '- 65% polyester, 35% viscose\n- Curved bottom hem, side seams, relaxed, drapey fit and tight sleeves\n- Original artwork detail from \" Gallinella sarda \" with Ma Chlò logo\n- Available in sizes S - XL\n\nCare instructions: Machine wash cold, gentle cycle with mild detergent and similar colors. Tumble dry low, or hang-dry for longest life. Do not iron decoration. Do not dry clean.', 'Long sleeve t-shirt with sardinian hen', 2),
(29, 45, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Colorful town \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°\n', 'Abito donna colorato', 1),
(30, 45, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Colorful town \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'women\'s colorful dress', 2),
(31, 44, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Snakes \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°\n', 'Abito donna con serpenti colorati', 1),
(32, 44, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Snakes \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'women\'s colorful snakes dress', 2),
(33, 43, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Wild girl \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'Abito donna con stampe colorate', 1),
(34, 43, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Wild girl \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'women\'s colorful print dress', 2),
(35, 42, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" A woman with a monkey \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'Abito donna con disegno stampato', 1),
(36, 42, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" A woman with a monkey \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'woman\'s dress with artwork print', 2),
(37, 41, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Un nuovo inizio \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'Abito donna con maschere colorate', 1),
(38, 41, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Un nuovo inizio \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'woman\'s dress with masks artwork print', 2),
(39, 39, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Monkeys into the jungle \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'abito donna con dipinto colorato stampato', 1),
(40, 39, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Monkeys into the jungle \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'women\'s dress with artwork print', 2),
(41, 40, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Reaching for the moon \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'abito donna con dipinto stampato', 1),
(42, 40, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Reaching for the moon \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'woman\'s dress with artwork print', 2),
(43, 38, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del logo Ma Chlò \n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'abito donna con labbra stampate', 1),
(44, 38, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Particular detail from Ma Chlò logo \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'woman\'s lips dress print', 2),
(45, 37, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Life of a tree \" logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'abito donna con albero colorato stampato', 1),
(46, 37, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Life of a tree \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'women\'s dress with artwork print', 2),
(47, 36, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" La separazione \" logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'abito donna con maschere stampate', 1),
(48, 36, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" La separazione \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'women\'s dress with masks artwork print', 2),
(49, 33, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Il pensiero \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'abito donna con maschere stampate', 1),
(50, 33, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Il pensiero \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'women\'s masks print dress', 2),
(51, 25, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" Talking \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie\n', 'tazza in ceramica colorata', 1),
(52, 25, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" Talking \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic mug', 2),
(53, 24, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" Eclipse \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie\n', 'tazza in ceramica colorata', 1),
(54, 24, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" Eclipse \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic colorful mug', 2),
(55, 23, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" A monkey playing with a snake \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie\n', 'tazza in ceramica colorata', 1),
(56, 23, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" A monkey playing with a snake \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic mug ', 2),
(57, 22, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" Colorful town \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie\n', 'tazza in ceramica colorata', 1),
(58, 22, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" Colorful town \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic colorful mug', 2),
(59, 21, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" Colored tree \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic colorful mug', 2),
(60, 21, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" Color tree \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie\n', 'tazza in ceramica', 1),
(61, 20, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" Jellyfish \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie\n', 'tazza in ceramica colorata', 1),
(62, 20, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" Jellyfish \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic colorful mug', 2),
(63, 19, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" Il pensiero \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic mug', 2),
(64, 19, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" Il pensiero \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie', 'tazza in ceramica colorata', 1),
(65, 18, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" Il bacio \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie', 'tazza in ceramica colorata', 1),
(66, 18, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" Il bacio \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic colorful mug', 2),
(67, 17, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" un nuovo inizio \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic mug', 2),
(68, 17, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" un nuovo inizio \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie', 'tazza in ceramica colorata', 1),
(69, 16, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" Il silenzio \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie', 'tazza in ceramica', 1),
(70, 16, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" Il silenzio \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic mug', 2),
(71, 15, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" Japan \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie', 'tazza in ceramic', 1),
(72, 15, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" Japan \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic mug', 2),
(73, 14, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" La separazione \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic mug', 2),
(74, 14, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" La separazione \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie', 'tazza in ceramica', 1),
(75, 13, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie', 'tazza in ceramica', 1),
(76, 13, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nParticular detail from Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic mug', 2),
(77, 12, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" Standby \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic mug', 2),
(78, 12, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" Standby \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie', 'tazza in ceramica', 1),
(79, 11, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" Universe \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie', 'tazza in ceramica colorata', 1),
(80, 11, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" Japan \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic mug', 2),
(81, 9, 'This sturdy mug is perfect for your morning coffee, afternoon tea, or whatever hot beverage you enjoy. It\'s glossy white and yields vivid prints that retain their quality when dish-washed and microwaved.\nOriginal artwork detail from \" Colored cats \" with Ma Chlò logo\nCeramic\nAvailable in 11oz (325ml) and 15oz (444ml) sizes\nDishwasher and microwave safe\nMade in China', 'ceramic mug', 2),
(82, 9, 'Tazza in ceramica cinese lucida, ideale per qualsiasi bevanda calda,\ncapienza 325 ml\nparticolare del dipinto originale \" Colored cats \" con logo Ma Chlò\nutilizzabile a microonde e lavabile in lavastavoglie', 'tazza in ceramica', 1),
(83, 32, '- 82% poliestere , 18% spandex\n- Stampato, tagliato e cucito a mano \n- Realizzato con un filato in microfibra liscio e confortevole\n- Morbido elastico in vita per un maggiore comfort\n- Cavallo a tassello triangolare\n- Particolare del dipinto originale \" Geometric \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'Yoga leggings colorati', 1),
(84, 32, '- 82% polyester, 18% spandex\n- Printed, cut, and hand-sewn\n- Made with a smooth and comfortable micro-fiber yarn\n- Soft, 0.25\" wide, clear elastic in waistband for extra comfort\n- Original artwork detail from \" Geometric \" with Ma Chlò logo on the back\n- Available in sizes XS - XL\n\nCare instructions :  delicate washing cycle, max. 30 °C\n', 'yoga leggings with artwork print', 2),
(85, 31, '- 82% poliestere , 18% spandex\n- Stampato, tagliato e cucito a mano \n- Realizzato con un filato in microfibra liscio e confortevole\n- Morbido elastico in vita per un maggiore comfort\n- Cavallo a tassello triangolare\n- Particolare del dipinto originale \" Colored cats \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'yoga leggings colorati', 1),
(86, 31, '- 82% polyester, 18% spandex\n- Printed, cut, and hand-sewn\n- Made with a smooth and comfortable micro-fiber yarn\n- Soft, 0.25\" wide, clear elastic in waistband for extra comfort\n- Original artwork detail from \" Colored cats \" with Ma Chlò logo on the back\n- Available in sizes XS - XL\n\nCare instructions :  delicate washing cycle, max. 30 °C', 'yoga leggings with artwork print', 2),
(87, 30, '- 82% poliestere , 18% spandex\n- Stampato, tagliato e cucito a mano \n- Realizzato con un filato in microfibra liscio e confortevole\n- Morbido elastico in vita per un maggiore comfort\n- Cavallo a tassello triangolare\n- Particolare del dipinto originale \" Universe \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'yoga leggings colorati', 1),
(88, 30, '- 82% polyester, 18% spandex\n- Printed, cut, and hand-sewn\n- Made with a smooth and comfortable micro-fiber yarn\n- Soft, 0.25\" wide, clear elastic in waistband for extra comfort\n- Original artwork detail from \" Universe \" with Ma Chlò logo on the back\n- Available in sizes XS - XL\n\nCare instructions :  delicate washing cycle, max. 30 °C', 'yoga leggings with artwork print', 2),
(89, 29, '- 82% polyester, 18% spandex\n- Printed, cut, and hand-sewn\n- Made with a smooth and comfortable micro-fiber yarn\n- Soft, 0.25\" wide, clear elastic in waistband for extra comfort\n- Original artwork detail from \" Standby \" with Ma Chlò logo on the back\n- Available in sizes XS - XL\n\nCare instructions :  delicate washing cycle, max. 30 °C', 'yoga leggings with artwork print', 2),
(90, 29, '- 82% poliestere , 18% spandex\n- Stampato, tagliato e cucito a mano \n- Realizzato con un filato in microfibra liscio e confortevole\n- Morbido elastico in vita per un maggiore comfort\n- Cavallo a tassello triangolare\n- Particolare del dipinto originale \" Standby \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'yoga leggings colorati', 1),
(91, 28, '- 82% poliestere , 18% spandex\n- Stampato, tagliato e cucito a mano \n- Realizzato con un filato in microfibra liscio e confortevole\n- Morbido elastico in vita per un maggiore comfort\n- Cavallo a tassello triangolare\n- Particolare del dipinto originale \" Yang \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'yoga leggings colorati', 1),
(92, 28, '- 82% polyester, 18% spandex\n- Printed, cut, and hand-sewn\n- Made with a smooth and comfortable micro-fiber yarn\n- Soft, 0.25\" wide, clear elastic in waistband for extra comfort\n- Original artwork detail from \" Yang \" with Ma Chlò logo on the back\n- Available in sizes XS - XL\n\nCare instructions :  delicate washing cycle, max. 30 °C', 'yoga leggings with artwork print', 2),
(93, 27, '- 82% polyester, 18% spandex\n- Printed, cut, and hand-sewn\n- Made with a smooth and comfortable micro-fiber yarn\n- Soft, 0.25\" wide, clear elastic in waistband for extra comfort\n- Original artwork detail from \" Eclipse \" with Ma Chlò logo on the back\n- Available in sizes XS - XL\n\nCare instructions :  delicate washing cycle, max. 30 °C', 'Yoga leggings with artwork print', 2),
(94, 27, '- 82% poliestere , 18% spandex\n- Stampato, tagliato e cucito a mano \n- Realizzato con un filato in microfibra liscio e confortevole\n- Morbido elastico in vita per un maggiore comfort\n- Cavallo a tassello triangolare\n- Particolare del dipinto originale \" Eclipse \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'yoga leggings colorati', 1),
(95, 26, '- 82% poliestere , 18% spandex\n- Stampato, tagliato e cucito a mano \n- Realizzato con un filato in microfibra liscio e confortevole\n- Morbido elastico in vita per un maggiore comfort\n- Cavallo a tassello triangolare\n- Particolare del dipinto originale \" Japan \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'yoga leggings colorati', 1),
(96, 26, '- 82% polyester, 18% spandex\n- Printed, cut, and hand-sewn\n- Made with a smooth and comfortable micro-fiber yarn\n- Soft, 0.25\" wide, clear elastic in waistband for extra comfort\n- Original artwork detail from \" Japan \" with Ma Chlò logo on the back\n- Available in sizes XS - XL\n\nCare instructions :  delicate washing cycle, max. 30 °C', 'yoga leggings with artwork print', 2),
(97, 63, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Jellyfish \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°\n', 'abito donna colorato', 1),
(98, 63, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \"Jellyfish \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'women\'s colorful dress', 2),
(99, 62, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Sleeping mask \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'women\'s dress with mask print', 2),
(100, 62, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \"Sleeping mask\" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°\n', 'abito donna con maschera stampata', 1),
(101, 61, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \"Sleeping mask\" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°\n', 'abito donna con maschera stampata', 1),
(102, 61, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Sleeping mask \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'women\'s dress with print mask', 2),
(103, 8, '- 100 % cotone pettinato\n- Cuciture laterali per una vestibilità aderente\n- E\' consigliabile una taglia in più per una vestibilità più comoda\n- Particolare del dipinto \"Gallinella sarda\" con logo Ma Chlò sul retro\n- Disponibile nelle taglie S - XL\n\nCura del capo: Lavaggio in lavatrice a freddo, ciclo delicato con detergente delicato e colori simili. Non stirare la decorazione e non lavare a secco.', 'maglia donna a maniche corte con disegno stampato', 1),
(104, 8, '- 100% combed ring-spun cotton\n- Side seams for a slim fit, longer body length\n- This shirt tends to run sumall – order a size up if you want a looser fit\n- Original artwork detail from \"Gallinella sarda\" with Ma Chlò logo on the back\n- Available in sizes S - XL\n\nCare instructions: machine wash cold, inside-out, gentle cycle with mild detergent and similar colors. Tumble dry low, or hang-dry for longest life. Cool iron inside-out if necessary. Do not iron decoration. Do not dry clean.', 'women\'s t-shirt with artwork print', 2),
(105, 7, '- 100% cotone pettinato ad eccezione del grigio melange scuro ( 65% poliestere ) \n- Silhouette classica semi-sagomata con cucitura laterale\n- Taglio elegante e leggero, tessuto morbido che resisterà a ripetuti lavaggi\n- Maniche con doppio ago e orlo inferiore\n- Particolare del dipinto originale \"Colored cats\"\n- Disponibile nelle taglie S - 2XL\n\nCura del capo: Lavaggio in lavatrice a freddo, ciclo delicato con detergente delicato e colori simili. Non stirare la decorazione e non lavare a secco.', 'maglia donna a maniche corte con gatto stampato', 1),
(106, 7, '- Made of 100% combed ring-spun cotton, except for heather grey (65% polyester).\n- Classic semi-contoured silhouette with side seam\n- Double-needle stitched sleeves and bottom hem\n- Original artwork detail from \" Colored cats \"\n- Available in sizes S - 2XL\n\nCare instructions: Machine wash cold, inside-out, gentle cycle with mild detergent and similar colors. Tumble dry low, or hang-dry for longest life. Cool iron inside-out if necessary. Do \n                                 not iron decoration. Do not dry clean.', 'yellow cat t-shirt', 2),
(107, 66, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del logo Ma Chlò \n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'abito donna con labbra stampate', 1),
(108, 66, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Particular detail from Ma Chlò logo \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'woman\'s lips dress print', 2),
(109, 65, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del logo Ma Chlò \n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'abito donna con labbra stampate', 1),
(110, 65, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Particular detail from Ma Chlò logo \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'woman\'s lips dress print', 2),
(111, 64, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \"Japan\" with Ma Chlò logo on the back\n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'watercolor artwork dress print', 2),
(112, 64, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Japan \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'abito donna con dipinto stampato', 1),
(113, 6, '- 100 % cotone pettinato\n- Cuciture laterali per una vestibilità aderente\n- E\' consigliabile una taglia in più per una vestibilità più comoda\n- Particolare del dipinto \"Il silenzio\" con logo Ma Chlò\n- Disponibile nelle taglie S - XL\n\nCura del capo: Lavaggio in lavatrice a freddo, ciclo delicato con detergente delicato e colori simili. Non stirare la decorazione e non lavare a secco.', 'maglietta maniche corte con maschere stampate', 1),
(114, 6, '- 100% combed ring-spun cotton\n- Side seams for a slim fit, longer body length\n- This shirt tends to run sumall – order a size up if you want a looser fit\n- Original artwork detail from \"Il silenzio\" with Ma Chlò logo \n- Available in sizes S - XL\n\nCare instructions: machine wash cold, inside-out, gentle cycle with mild detergent and similar colors. Tumble dry low, or hang-dry for longest life. Cool iron inside-out if necessary. Do not iron decoration. Do not dry clean.', 't-shirt with masks print', 2),
(115, 1, '- 100 % cotone pettinato\n- Cuciture laterali per una vestibilità aderente\n- E\' consigliabile una taglia in più per una vestibilità più comoda\n- Particolare del dipinto \"Colored cats\" con logo Ma Chlò\n- Disponibile nelle taglie S - XL\n\nCura del capo: Lavaggio in lavatrice a freddo, ciclo delicato con detergente delicato e colori simili. Non stirare la decorazione e non lavare a secco.', 'maglietta manica corta con gatto stampato', 1),
(116, 1, '- 100% combed ring-spun cotton\n- Side seams for a slim fit, longer body length\n- This shirt tends to run sumall – order a size up if you want a looser fit\n- Original artwork detail from \"Colored cats\" with Ma Chlò logo \n- Available in sizes S - XL\n\nCare instructions: machine wash cold, inside-out, gentle cycle with mild detergent and similar colors. Tumble dry low, or hang-dry for longest life. Cool iron inside-out if necessary. Do not iron decoration. Do not dry clean.', 'woman\'s t-shirt with cats print', 2),
(117, 5, '- 100 % cotone pettinato\n- Cuciture laterali per una vestibilità aderente\n- E\' consigliabile una taglia in più per una vestibilità più comoda\n- Particolare del dipinto \"Flowers\" con logo Ma Chlò sul retro\n- Disponibile nelle taglie S - XL\n\nCura del capo: Lavaggio in lavatrice a freddo, ciclo delicato con detergente delicato e colori simili. Non stirare la decorazione e non lavare a secco.', 'maglietta manica corta con fiori colorati', 1),
(118, 5, '- 100% combed ring-spun cotton\n- Side seams for a slim fit, longer body length\n- This shirt tends to run sumall – order a size up if you want a looser fit\n- Original artwork detail from \"Flowers\" with Ma Chlò logo on the back\n- Available in sizes S - XL\n\nCare instructions: machine wash cold, inside-out, gentle cycle with mild detergent and similar colors. Tumble dry low, or hang-dry for longest life. Cool iron inside-out if necessary. Do not iron decoration. Do not dry clean.', 'flowers print t-shirt', 2),
(119, 67, '- 100% cotone pettinato ad eccezione del grigio melange scuro ( 65% poliestere ) \n- Silhouette classica semi-sagomata con cucitura laterale\n- Taglio elegante e leggero, tessuto morbido che resisterà a ripetuti lavaggi\n- Maniche con doppio ago e orlo inferiore\n- Particolare del dipinto originale \"Colored cats\" con logo Ma Chlò\n- Disponibile nelle taglie S - 2XL\n\nCura del capo: Lavaggio in lavatrice a freddo, ciclo delicato con detergente delicato e colori simili. Non stirare la decorazione e non lavare a secco.', 'maglietta manica corta con gatti colorati stampati', 1),
(120, 67, '- Made of 100% combed ring-spun cotton, except for heather grey (65% polyester).\n- Classic semi-contoured silhouette with side seam\n- Double-needle stitched sleeves and bottom hem\n- Original artwork detail from \" Colored cats \" with Ma Chlò logo\n- Available in sizes S - 2XL\n\nCare instructions: Machine wash cold, inside-out, gentle cycle with mild detergent and similar colors. Tumble dry low, or hang-dry for longest life. Cool iron inside-out if necessary. Do \n                                 not iron decoration. Do not dry clean.', 'cats print t-shirt', 2),
(121, 68, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Ocean purple \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'abito donna con dipinto stampato', 1),
(122, 68, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Ocean purple \" with Ma Chlò logo on the back\n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'women\'s artwork print dress', 2),
(123, 71, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Colorful Town \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'Tank Top con dipinto stampato', 1),
(124, 71, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Colorful Town \" with Ma Chlò logo on the back\n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'woman\'s dress', 2),
(125, 70, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Colored cats \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'Tank top con dipinto stampato', 1),
(126, 70, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Colored cats \" with Ma Chlò logo on the back\n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'Tank top artwork print', 2),
(127, 69, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Fluo \" with Ma Chlò logo on the back\n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'Tank Top con dipinto stampato', 2),
(128, 69, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Fluo \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'Tank top con dipinto stampato', 1),
(129, 72, '-  65% poliestere, 35% viscosa\n- Canotta morbida con giromanica basso per una vestibilità drappeggiata\n- Vestibilità morbida, giromanica taglio basso e cuciture laterali\n- Particolare del dipinto originale \" Sleeping Mask \" con logo Ma Chlò\n- Disponibile nelle taglie S - 2XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'Tank top con maschera stampata                                                                      ', 1),
(130, 72, '- 65% polyester, 35% viscose\n- Low cut armholes, curved bottom hem\n- Relaxed fit, side seams\n- Original artwork detail from \" Sleeping Mask \" with Ma Chlò logo\n- Available in sizes S - 2XL\n- Care instructions :  delicate washing cycle, max. 30 °C\n', 'artwork tank top print', 2),
(131, 35, '- 82% poliestere 18% spandex\n- Tagliato e cucito a mano dopo la stampa\n- Realizzato con filato in microfibra liscio e confortevole\n- Particolare del dipinto originale \" Flowers \" con logo Ma Chlò sul retro\n- Disponibile nelle taglie XS - XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'abito donna con fiori colorati', 1),
(132, 35, '- 82% polyester , 18% spandex\n- Printed, cut, and hand-sewn \n- Made with smooth, comfortable microfiber yarn\n- Original artwork detail from \" Flowers \" with Ma Chlò logo on the back \n- Available in sizes XS - XL\n- Care instructions :  delicate washing cycle, max. 30 °C', 'woman\'s flowers print dress', 2),
(133, 78, '- 96% poliestere, 4% elastan\n- Stampato, tagliato e cucito a mano \n- Tessuto morbido e confortevole\n- Particolare del dipinto originale \"Bonds\" con logo Ma Chlò\n- Disponibile nelle taglie  XS - 2XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'Tank top con stampe colorate', 1),
(134, 78, '- 96% polyester, 4% elastane\n- Printed, cut, and hand-sewn \n- Four-way stretch material stretches and recovers on the cross and lengthwise grains\n- Originale artwork detail from \"Bonds\" with Ma Chlò logo\n-Available in sizes XS - 2XL\n\n Care instructions :  delicate washing cycle, max. 30 °C\n', 'Tank top with artwork print', 2);
INSERT INTO `prodotti_traduzioni` (`id_prodotti_traduzioni`, `id_prodotti`, `descrizione`, `descrizione_breve`, `lingua_traduzione_id`) VALUES
(135, 76, '- 96% poliestere, 4% elastan\n- Stampato, tagliato e cucito a mano \n- Tessuto morbido e confortevole\n- Particolare del dipinto originale \"Lo sguardo\" con logo Ma Chlò sul retro\n- Disponibile nelle taglie  XS - 2XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'Tank top con maschere stampate', 1),
(136, 76, '- 96% polyester, 4% elastane\n- Printed, cut, and hand-sewn \n- Four-way stretch material stretches and recovers on the cross and lengthwise grains\n- Originale artwork detail from \"Lo sguardo\" with Ma Chlò logo on the back\n-Available in sizes XS - 2XL\n\n Care instructions :  delicate washing cycle, max. 30 °C\n', 'Masks Tank top print', 2),
(137, 75, '- 96% polyester, 4% elastane\n- Printed, cut, and hand-sewn \n- Four-way stretch material stretches and recovers on the cross and lengthwise grains\n- Originale artwork detail from \"Il pensiero\" with Ma Chlò logo on the back\n-Available in sizes XS - 2XL\n\n Care instructions :  delicate washing cycle, max. 30 °C\n', 'Masks tank top print', 2),
(138, 75, '- 96% poliestere, 4% elastan\n- Stampato, tagliato e cucito a mano \n- Tessuto morbido e confortevole\n- Particolare del dipinto originale \"Il pensiero\" con logo Ma Chlò sul retro\n- Disponibile nelle taglie  XS - 2XL\n\nCura del capo: lavaggio ciclo delicati max 30°', 'Tank top con maschere colorate', 1),
(139, 87, '- Stampato solo su un lato\n- 52% cotone, 48% poliestere\n- Il lato non stampato è realizzato in tessuto spugna morbido, rendendo il telo mare più assorbente\n- Disponibile nella misura 74.5  X 152 cm\n- Particolare del dipinto originale \"Fluo\" con logo Ma Chlò\n\nCura del capo:  lavaggio ciclo delicati max 30°\n\n', 'Telo mare con dipinto stampato', 1),
(140, 87, '- Printed on one side only\n- 52% cotton, 48% polyester\n- The non-printed side is made of terry fabric, making the towel more water-absorbent\n- Available in size 74.5  X 152 cm\n- Original artwork detail from \"Fluo\" with Ma Chlò logo\n\nCare instructions :  delicate washing cycle, max 30 °C', 'Beach towel with artwork print', 2),
(141, 86, '- Stampato solo su un lato\n- 52% cotone, 48% poliestere\n- Il lato non stampato è realizzato in tessuto spugna morbido, rendendo il telo mare più assorbente\n- Disponibile nella misura 74.5 X 152 cm\n- Particolare del dipinto originale \"Yang\" con logo Ma Chlò\n\nCura del capo:  lavaggio ciclo delicati max 30°\n\n', 'Telo mare con dipinto stampato', 1),
(142, 86, '- Printed on one side only\n- 52% cotton, 48% polyester\n- The non-printed side is made of terry fabric, making the towel more water-absorbent\n- Available in size 74.5  X 152 cm\n- Original artwork detail from \"Yang\" with Ma Chlò logo\n\nCare instructions :  delicate washing cycle, max 30 °C', 'Beach towel with artwork print', 2),
(143, 85, '- Stampato solo su un lato\n- 52% cotone, 48% poliestere\n- Il lato non stampato è realizzato in tessuto spugna morbido, rendendo il telo mare più assorbente\n- Disponibile nella misura 74.5 X 152 cm\n- Particolare del dipinto originale \"A woman with a monkey\" con logo Ma Chlò\n\nCura del capo:  lavaggio ciclo delicati max 30°\n', 'Telo mare con dipinto stampato', 1),
(144, 85, '- Printed on one side only\n- 52% cotton, 48% polyester\n- The non-printed side is made of terry fabric, making the towel more water-absorbent\n- Available in size 74.5  X 152 cm\n- Original artwork detail from \"A woman with a monkey\" with Ma Chlò logo\n\nCare instructions :  delicate washing cycle, max 30 °C', 'Beach towel with artwork print', 2),
(145, 84, '- Printed on one side only\n- 52% cotton, 48% polyester\n- The non-printed side is made of terry fabric, making the towel more water-absorbent\n- Available in size 74.5  X 152 cm\n- Original artwork detail from \"Flowers\" with Ma Chlò logo\n\nCare instructions :  delicate washing cycle, max 30 °C', 'Beach towel with flowers print', 2),
(146, 84, '- Stampato solo su un lato\n- 52% cotone, 48% poliestere\n- Il lato non stampato è realizzato in tessuto spugna morbido, rendendo il telo mare più assorbente\n- Disponibile nella misura 74.5 X 152 cm\n- Particolare del dipinto originale \"Flowers\" con logo Ma Chlò\n\nCura del capo:  lavaggio ciclo delicati max 30°\n', 'Telo mare con fiori colorati stampati', 1),
(147, 83, '- Stampato solo su un lato\n- 52% cotone, 48% poliestere\n- Il lato non stampato è realizzato in tessuto spugna morbido, rendendo il telo mare più assorbente\n- Disponibile nella misura 74.5 X 152 cm\n- Particolare del dipinto originale \"Colored tree\" con logo Ma Chlò\n\nCura del capo:  lavaggio ciclo delicati max 30°\n', 'Telo mare con dipinto stampato', 1),
(148, 83, '- Printed on one side only\n- 52% cotton, 48% polyester\n- The non-printed side is made of terry fabric, making the towel more water-absorbent\n- Available in size 74.5  X 152 cm\n- Original artwork detail from \"Colored tree\" with Ma Chlò logo\n\nCare instructions :  delicate washing cycle, max 30 °C', 'Beach towel with artwork print', 2),
(149, 82, '- Stampato solo su un lato\n- 52% cotone, 48% poliestere\n- Il lato non stampato è realizzato in tessuto spugna morbido, rendendo il telo mare più assorbente\n- Disponibile nella misura 74.5 X 152 cm\n- Particolare del dipinto originale \"Bonds\" con logo Ma Chlò\n\nCura del capo:  lavaggio ciclo delicati max 30°\n', 'Telo mare con dipinto stampato', 1),
(150, 82, '- Printed on one side only\n- 52% cotton, 48% polyester\n- The non-printed side is made of terry fabric, making the towel more water-absorbent\n- Available in size 74.5  X 152 cm\n- Original artwork detail from \"Bonds\" with Ma Chlò logo\n\nCare instructions :  delicate washing cycle, max 30 °C', 'Beach towel with artwork print', 2),
(151, 81, '- Printed on one side only\n- 52% cotton, 48% polyester\n- The non-printed side is made of terry fabric, making the towel more water-absorbent\n- Available in size 74.5  X 152 cm\n- Original artwork detail from \"Flowers\" with Ma Chlò logo\n\nCare instructions :  delicate washing cycle, max 30 °C', 'Beach towel with flowers print', 2),
(152, 81, '- Stampato solo su un lato\n- 52% cotone, 48% poliestere\n- Il lato non stampato è realizzato in tessuto spugna morbido, rendendo il telo mare più assorbente\n- Disponibile nella misura 76.5 X 152 cm\n- Particolare del dipinto originale \"Flowers\" con logo Ma Chlò\n\nCura del capo:  lavaggio ciclo delicati max 30°', 'Telo mare con fiori colorati stampati', 1),
(153, 80, '- Stampato solo su un lato\n- 52% cotone, 48% poliestere\n- Il lato non stampato è realizzato in tessuto spugna morbido, rendendo il telo mare più assorbente\n- Disponibile nella misura 76.5 X 152 cm\n- Particolare del dipinto originale \"Tinge\" con logo Ma Chlò\n\nCura del capo:  lavaggio ciclo delicati max 30°', 'Telo mare con dipinto stampato', 1),
(154, 80, '- Printed on one side only\n- 52% cotton, 48% polyester\n- The non-printed side is made of terry fabric, making the towel more water-absorbent\n- Available in size 74.5  X 152 cm\n- Original artwork detail from \"Tinge\" with Ma Chlò logo\n\nCare instructions :  delicate washing cycle, max 30 °C', 'Beach towel with artwork print', 2),
(155, 79, '- Printed on one side only\n- 52% cotton, 48% polyester\n- The non-printed side is made of terry fabric, making the towel more water-absorbent\n- Available in size 74.5  X 152 cm\n- Original artwork detail from \"Infinito\" with Ma Chlò logo\n\nCare instructions :  delicate washing cycle, max 30 °C', 'Beach towel with colorful masks print', 2),
(156, 79, '- Stampato solo su un lato\n- 52% cotone, 48% poliestere\n- Il lato non stampato è realizzato in tessuto spugna morbido, rendendo il telo mare più assorbente\n- Disponibile nella misura 76.5 X 152 cm\n- Particolare del dipinto originale \"Infinito\" con logo Ma Chlò\n\nCura del capo:  lavaggio ciclo delicati max 30°', 'Telo mare con maschere colorate stampate', 1),
(157, 95, '- 100 % poliestere\n- Ampia tasca interna, tagliato e cucito a mano \n- Comode maniglie in cotone\n- Colori vibranti che non svaniranno \n- Particolare del dipinto originale \"Il silenzio\" con logo Ma Chlò\n- Disponibile nella taglia 40 X 50 cm', 'Borsa mare con dipinto stampato', 1),
(158, 95, '- 100% polyester fabric\n- Large inside pocket, printed, cut, and hand-sewn  \n- Comfortable cotton webbing handles\n- Vibrant colors that won\'t fade\n- Original artwork detail from \"Il silenzio\" with Ma Chlò logo\n- Available in one size 40 X 50 cm', 'Beach bag with artwork print', 2),
(159, 94, '- 100 % poliestere\n- Ampia tasca interna, tagliato e cucito a mano \n- Comode maniglie in cotone\n- Colori vibranti che non svaniranno \n- Particolare del dipinto originale \"Colorful tree\" con logo Ma Chlò\n- Disponibile nella taglia 40 X 50 cm', 'Borsa mare con albero colorato stampato', 1),
(160, 94, '- 100% polyester fabric\n- Large inside pocket, printed, cut, and hand-sewn  \n- Comfortable cotton webbing handles\n- Vibrant colors that won\'t fade\n- Original artwork detail from \"Colored tree\" with Ma Chlò logo\n- Available in one size 40 X 50 cm', 'Beach bag with artwork print', 2),
(161, 93, '- 100% polyester fabric\n- Large inside pocket, printed, cut, and hand-sewn  \n- Comfortable cotton webbing handles\n- Vibrant colors that won\'t fade\n- Original artwork detail from \"Bonds\" with Ma Chlò logo\n- Available in one size 40 X 50 cm', 'Beach bag with artwork print', 2),
(162, 93, '- 100 % poliestere\n- Ampia tasca interna, tagliato e cucito a mano \n- Comode maniglie in cotone\n- Colori vibranti che non svaniranno \n- Particolare del dipinto originale \"Bonds\" con logo Ma Chlò\n- Disponibile nella taglia 40 X 50 cm', 'Borsa mare con dipinto stampato', 1),
(163, 92, '- 100 % poliestere\n- Ampia tasca interna, tagliato e cucito a mano \n- Comode maniglie in cotone\n- Colori vibranti che non svaniranno \n- Particolare del dipinto originale \"Bonds\" con logo Ma Chlò\n- Disponibile nella taglia 40 X 50 cm', 'Borsa mare con dipinto stampato', 1),
(164, 92, '- 100% polyester fabric\n- Large inside pocket, printed, cut, and hand-sewn  \n- Comfortable cotton webbing handles\n- Vibrant colors that won\'t fade\n- Original artwork detail from \"Bonds\" with Ma Chlò logo\n- Available in one size 40 X 50 cm', 'Beach bag with artwork print', 2),
(165, 91, '- 100% polyester fabric\n- Large inside pocket, printed, cut, and hand-sewn  \n- Comfortable cotton webbing handles\n- Vibrant colors that won\'t fade\n- Original artwork detail from \"Yang\" with Ma Chlò logo\n- Available in one size 40 X 50 cm', 'Beach bag with artwork print', 2),
(166, 91, '- 100 % poliestere\n- Ampia tasca interna, tagliato e cucito a mano \n- Comode maniglie in cotone\n- Colori vibranti che non svaniranno \n- Particolare del dipinto originale \"Yang\" con logo Ma Chlò\n- Disponibile nella taglia 40 X 50 cm', 'Borsa mare con dipinto stampato', 1),
(167, 90, '- 100 % poliestere\n- Ampia tasca interna, tagliato e cucito a mano \n- Comode maniglie in cotone\n- Colori vibranti che non svaniranno \n- Particolare del dipinto originale \"Flowers\" con logo Ma Chlò\n- Disponibile nella taglia 40 X 50 cm', 'Borsa mare con fiori stampati', 1),
(168, 90, '- 100% polyester fabric\n- Large inside pocket, printed, cut, and hand-sewn  \n- Comfortable cotton webbing handles\n- Vibrant colors that won\'t fade\n- Original artwork detail from \"Flowers\" with Ma Chlò logo\n- Available in one size 40 X 50 cm', 'Beach bag with flowers print', 2),
(169, 89, '- 100% polyester fabric\n- Large inside pocket, printed, cut, and hand-sewn  \n- Comfortable cotton webbing handles\n- Vibrant colors that won\'t fade\n- Original artwork detail from \"Jellyfish\" with Ma Chlò logo\n- Available in one size 40 X 50 cm', 'Beach bag with artwork print', 2),
(170, 89, '- 100 % poliestere\n- Ampia tasca interna, tagliato e cucito a mano \n- Comode maniglie in cotone\n- Colori vibranti che non svaniranno \n- Particolare del dipinto originale \"Jellyfish\" con logo Ma Chlò\n- Disponibile nella taglia 40 X 50 cm', 'Borsa mare con dipinto stampato', 1),
(171, 88, '- 100 % poliestere\n- Ampia tasca interna, tagliato e cucito a mano \n- Comode maniglie in cotone\n- Colori vibranti che non svaniranno \n- Particolare del dipinto originale \"Explosion\" con logo Ma Chlò\n- Disponibile nella taglia 40 X 50 cm', 'Borsa mare con dipinto stampato', 1),
(172, 88, '- 100% polyester fabric\n- Large inside pocket, printed, cut, and hand-sewn  \n- Comfortable cotton webbing handles\n- Vibrant colors that won\'t fade\n- Original artwork detail from \"Explosion\" with Ma Chlò logo\n- Available in one size 40 X 50 cm', 'Beach bag with artwork print', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_taglia`
--

CREATE TABLE `prodotto_taglia` (
  `fk_prodotto` int(11) NOT NULL,
  `fk_taglia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `shipments`
--

CREATE TABLE `shipments` (
  `ship_id` int(11) NOT NULL,
  `id_ordine` int(11) NOT NULL,
  `ship_printful_id` int(11) NOT NULL,
  `ship_type` varchar(150) NOT NULL,
  `carrier` varchar(250) NOT NULL,
  `service` varchar(250) NOT NULL,
  `tracking_number` varchar(250) NOT NULL,
  `tracking_url` varchar(250) NOT NULL,
  `created` varchar(250) NOT NULL,
  `ship_date` date NOT NULL,
  `reshipment` tinyint(1) NOT NULL,
  `location` varchar(50) NOT NULL,
  `items` int(5) NOT NULL,
  `last_update_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_coupon`
--

CREATE TABLE `stato_coupon` (
  `id_stato_coupon` int(11) NOT NULL,
  `desc_stato_coupon` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_coupon`
--

INSERT INTO `stato_coupon` (`id_stato_coupon`, `desc_stato_coupon`) VALUES
(1, 'ATTIVO'),
(2, 'SOSPESO'),
(3, 'UTILIZZATO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_descrizione`
--

CREATE TABLE `stato_descrizione` (
  `id_stato_descrizione` int(11) NOT NULL,
  `testo_stato_descrizione` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_descrizione`
--

INSERT INTO `stato_descrizione` (`id_stato_descrizione`, `testo_stato_descrizione`) VALUES
(1, 'ATTIVO'),
(2, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_ordine`
--

CREATE TABLE `stato_ordine` (
  `id_stato_ordine` int(11) NOT NULL,
  `desc_stato_ordine` varchar(100) NOT NULL,
  `class_stato_ordine` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_ordine`
--

INSERT INTO `stato_ordine` (`id_stato_ordine`, `desc_stato_ordine`, `class_stato_ordine`) VALUES
(1, 'IN LAVORAZIONE', 'text-warning'),
(2, 'IN CONSEGNA', 'text-top-rated'),
(3, 'CONSEGNATO', 'text-success'),
(4, 'ANNULLATO', 'text-danger'),
(5, 'SPEDITO', 'text-warning');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_pagamento`
--

CREATE TABLE `stato_pagamento` (
  `id_stato_pagamento` int(11) NOT NULL,
  `desc_stato_pagamento` varchar(100) NOT NULL,
  `class_stato_pagamento` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_pagamento`
--

INSERT INTO `stato_pagamento` (`id_stato_pagamento`, `desc_stato_pagamento`, `class_stato_pagamento`) VALUES
(1, 'ACCETTATO', 'text-success'),
(2, 'SOSPESO', 'text-warning'),
(3, 'RIFIUTATO', 'text-danger'),
(4, 'ANNULLATO', 'text-danger');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_prodotti`
--

CREATE TABLE `stato_prodotti` (
  `stato_prodotti_id` int(11) NOT NULL,
  `stato_prodotti_desc` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_prodotti`
--

INSERT INTO `stato_prodotti` (`stato_prodotti_id`, `stato_prodotti_desc`) VALUES
(1, 'ATTIVO'),
(2, 'CANCELLATO'),
(3, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `storico_carrello`
--

CREATE TABLE `storico_carrello` (
  `id_storico_carrello` int(11) NOT NULL,
  `id_ordine` int(11) DEFAULT NULL,
  `id_variante` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `data_storicizzazione` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `qty` int(11) NOT NULL,
  `taglia` varchar(15) NOT NULL,
  `tipo_prodotto` varchar(250) NOT NULL,
  `codice_prodotto` varchar(100) NOT NULL,
  `codice_variante` varchar(100) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `prezzo` double NOT NULL DEFAULT '0',
  `prezzo_scontato` double NOT NULL DEFAULT '0',
  `url_immagine` varchar(250) NOT NULL,
  `colore_prodotto` varchar(150) DEFAULT NULL,
  `colore_prodotto_codice` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `storico_carrello`
--

INSERT INTO `storico_carrello` (`id_storico_carrello`, `id_ordine`, `id_variante`, `id_prodotto`, `data_storicizzazione`, `qty`, `taglia`, `tipo_prodotto`, `codice_prodotto`, `codice_variante`, `nome`, `prezzo`, `prezzo_scontato`, `url_immagine`, `colore_prodotto`, `colore_prodotto_codice`) VALUES
(1, 1, 113, 11, '2018-05-24 11:42:48', 1, '11oz', 'LABEL_TP_STANDARD', '5aa2a35b45f316', '5aa2a35b45fc52', 'Universe', 7.95, 0, '5aa2a35b45fc52_f.png', 'White', ''),
(2, 2, 37, 4, '2018-06-02 06:34:12', 1, 'S', 'LABEL_TP_BESTSELLER', '5aa3013293e052', '5aa3013293e907', 'Lips t-shirt', 50.87, 40.69, '5aa3013293e907_f.png', 'White', 'e5e6e1'),
(3, 5, 37, 4, '2018-06-06 11:48:32', 1, 'S', 'LABEL_TP_BESTSELLER', '5aa3013293e052', '5aa3013293e907', 'Lips t-shirt', 50.87, 40.69, '5aa3013293e907_f.png', 'White', 'e5e6e1'),
(4, 6, 434, 92, '2018-07-26 12:52:14', 1, '16×20', 'LABEL_TP_NEW', '5b0c3534992b15', '5b0c3534993426', '\"White bonds\" Beach Bag', 43.95, 0, '5b0c3534993426_f.png', 'Red', 'c5262a');

-- --------------------------------------------------------

--
-- Struttura della tabella `storico_clienti`
--

CREATE TABLE `storico_clienti` (
  `id_storico_clienti` int(11) NOT NULL,
  `id_ordine` int(11) NOT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `cognome` varchar(250) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` int(11) DEFAULT NULL,
  `indirizzo_fatturazione` varchar(250) DEFAULT NULL,
  `indirizzo_spedizione` varchar(250) NOT NULL,
  `note_indirizzo_spedizione` varchar(250) NOT NULL,
  `note_indirizzo_fatturazione` varchar(250) NOT NULL,
  `partita_iva` varchar(11) CHARACTER SET latin1 DEFAULT NULL,
  `codice_fiscale` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `storico_clienti`
--

INSERT INTO `storico_clienti` (`id_storico_clienti`, `id_ordine`, `nome`, `cognome`, `email`, `telefono`, `indirizzo_fatturazione`, `indirizzo_spedizione`, `note_indirizzo_spedizione`, `note_indirizzo_fatturazione`, `partita_iva`, `codice_fiscale`, `id_lingua`) VALUES
(1, 1, 'Katia', 'Piredda', 'pireddakatia@gmail.com', 2147483647, 'katia piredda | via bramante, 64 - 07026 olbia (IT)', 'katia piredda | via bramante, 64 - 07026 olbia (IT)', '', '', '', '', 1),
(2, 2, 'Roberto', 'Rossi', 'roberto.rossi77@gmail.com', 2147483647, 'Roberto | Viale Mani dal Naso, 114 - 00166 Roma (MH)', 'Roberto | Viale Mani dal Naso, 114 - 00166 Roma (MH)', '', '', '', '', 1),
(3, 5, 'Roberto', 'Rossi', 'roberto.rossi77@gmail.com', 2147483647, 'Roberto | Viale Mani dal Naso, 114 - 00166 Roma (MH)', 'Roberto | Viale Mani dal Naso, 114 - 00166 Roma (MH)', '', '', '', '', 1),
(4, 6, 'Test', 'TesCogn', 'roberto.rossi77@gmail.com', 2147483647, 'TEst Con | Via Algida, 47 - 558 Helsinki (IS)', 'TEst Con | Via Algida, 47 - 558 Helsinki (IS)', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `sync_status`
--

CREATE TABLE `sync_status` (
  `id_sync_status` int(11) NOT NULL,
  `desc_sync_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `sync_status`
--

INSERT INTO `sync_status` (`id_sync_status`, `desc_sync_status`) VALUES
(0, 'RIMOSSO'),
(1, 'NUOVO'),
(2, 'AGGIORNATO'),
(3, 'FORZATO');

-- --------------------------------------------------------

--
-- Struttura della tabella `sync_test`
--

CREATE TABLE `sync_test` (
  `id_sync_test` int(11) NOT NULL,
  `time_sync_test` datetime NOT NULL,
  `sync_type` varchar(15) NOT NULL,
  `sync_status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `sync_test`
--

INSERT INTO `sync_test` (`id_sync_test`, `time_sync_test`, `sync_type`, `sync_status`) VALUES
(1, '2018-06-04 10:00:01', 'sync_variants', 'COMPLETED'),
(2, '2018-04-06 16:30:02', 'orders', 'PROGRESS');

-- --------------------------------------------------------

--
-- Struttura della tabella `taglie`
--

CREATE TABLE `taglie` (
  `id_taglia` int(11) NOT NULL,
  `codice` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `descrizione` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `separatore` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `taglie`
--

INSERT INTO `taglie` (`id_taglia`, `codice`, `descrizione`, `separatore`) VALUES
(1, 'XS', 'Extra Small', '-'),
(2, 'S', 'Small', '-'),
(3, 'M', 'Medium', '-'),
(4, 'L', 'Large', '-'),
(5, 'XL', 'Extra Large', '-'),
(6, '2XL', '2 Extra Large', '-'),
(7, 'One Size', 'Taglia unica', '-');

-- --------------------------------------------------------

--
-- Struttura della tabella `tags`
--

CREATE TABLE `tags` (
  `id_tag` int(11) NOT NULL,
  `nome_tag` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tags`
--

INSERT INTO `tags` (`id_tag`, `nome_tag`) VALUES
(1, 'Design'),
(2, 'Estate'),
(3, 'Elegante'),
(4, 'Manica corta'),
(5, 'Smanicato');

-- --------------------------------------------------------

--
-- Struttura della tabella `tags_prodotti`
--

CREATE TABLE `tags_prodotti` (
  `id_tags_prodotti` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tags_prodotti`
--

INSERT INTO `tags_prodotti` (`id_tags_prodotti`, `id_tag`, `id_prodotto`) VALUES
(1, 1, 59),
(2, 2, 60),
(3, 4, 4),
(4, 4, 56),
(5, 4, 55),
(6, 4, 54),
(7, 4, 53),
(8, 4, 52),
(9, 4, 51),
(10, 4, 50),
(11, 2, 34),
(12, 1, 49),
(13, 1, 47),
(14, 1, 45),
(15, 1, 44),
(16, 1, 43),
(17, 1, 41),
(18, 1, 40),
(19, 2, 38),
(20, 1, 36),
(21, 1, 33),
(22, 1, 25),
(23, 1, 23),
(24, 1, 22),
(25, 1, 21),
(26, 1, 20),
(27, 1, 19),
(28, 1, 18),
(29, 1, 17),
(30, 1, 16),
(31, 1, 15),
(32, 1, 14),
(33, 1, 12),
(34, 1, 11),
(35, 1, 9),
(36, 1, 32),
(37, 1, 31),
(38, 1, 30),
(39, 1, 29),
(40, 1, 27),
(41, 1, 26),
(42, 1, 63),
(43, 1, 62),
(44, 1, 61),
(45, 2, 8),
(46, 2, 7),
(47, 2, 66),
(48, 2, 65),
(49, 1, 64),
(50, 1, 6),
(51, 4, 1),
(52, 1, 5),
(53, 1, 67),
(54, 2, 68),
(55, 1, 71),
(56, 1, 70),
(57, 1, 69),
(58, 2, 72),
(59, 1, 35),
(60, 2, 78),
(61, 1, 76),
(62, 2, 87),
(63, 2, 86),
(64, 2, 85),
(65, 2, 84),
(66, 2, 83),
(67, 2, 82),
(68, 2, 81),
(69, 2, 80),
(70, 2, 79),
(71, 1, 95),
(72, 2, 94),
(73, 2, 93),
(74, 2, 92),
(75, 1, 91),
(76, 2, 90),
(77, 1, 89),
(78, 2, 88);

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_coupon`
--

CREATE TABLE `tipo_coupon` (
  `id_tipo_coupon` int(11) NOT NULL,
  `desc_tipo_coupon` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_coupon`
--

INSERT INTO `tipo_coupon` (`id_tipo_coupon`, `desc_tipo_coupon`) VALUES
(1, 'Sconto sul carrello utilizzo singolo'),
(2, 'Sconto % sul carrello utilizzo singolo'),
(3, 'Sconto sul carrello utilizzo multiplo'),
(4, 'Sconto % sul carrello utilizzo multiplo');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_pagamento`
--

CREATE TABLE `tipo_pagamento` (
  `id_tipo_pagamento` int(11) NOT NULL,
  `desc_tipo_pagamento` varchar(100) NOT NULL,
  `icon_tipo_pagamento` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_pagamento`
--

INSERT INTO `tipo_pagamento` (`id_tipo_pagamento`, `desc_tipo_pagamento`, `icon_tipo_pagamento`) VALUES
(1, 'Paypal', 'fa-paypal'),
(2, 'Stripe', 'fa-cc-stripe');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_prodotto`
--

CREATE TABLE `tipo_prodotto` (
  `id_tipo_prodotto` int(11) NOT NULL,
  `descrizione_tipo_prodotto` varchar(30) NOT NULL,
  `css_class` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_prodotto`
--

INSERT INTO `tipo_prodotto` (`id_tipo_prodotto`, `descrizione_tipo_prodotto`, `css_class`) VALUES
(1, 'LABEL_TP_SALE', 'text-danger'),
(2, 'LABEL_TP_BESTSELLER', 'text-warning'),
(3, 'LABEL_TP_TOPRATED', 'top-rated'),
(4, 'LABEL_TP_STANDARD', ''),
(5, 'LABEL_TP_NEW', 'text-success');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_stampa`
--

CREATE TABLE `tipo_stampa` (
  `id_tipo_stampa` int(11) NOT NULL,
  `desc_tipo_stampa` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_stampa`
--

INSERT INTO `tipo_stampa` (`id_tipo_stampa`, `desc_tipo_stampa`) VALUES
(1, 'STANDARD'),
(2, 'ALL OVER');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_template`
--

CREATE TABLE `tipo_template` (
  `id_tipo_template` int(11) NOT NULL,
  `desc_tipo_template` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_template`
--

INSERT INTO `tipo_template` (`id_tipo_template`, `desc_tipo_template`) VALUES
(1, 'CONTATTO'),
(2, 'NEWSLETTER'),
(3, 'CUSTOM');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'roberto.rossi77@gmail.com', '$2y$08$thYAVQ43pc.dHmdNDhQz/evLOaAKHbthgR/dkfc7MIjpEwCcDYdMy', '', 'roberto.rossi77@gmail.com', '', NULL, NULL, NULL, 1268889823, 1541586301, 1, 'Roberto', 'Rossi', 'ADMIN', '4234234'),
(2, '127.0.0.1', 'info@machlo.com', '$2y$08$VYAfiskG1BgPgTUswBIoaejDT6w6OCenxk9duml7x/hP6ArsaAdb2', NULL, 'info@machlo.com', NULL, NULL, NULL, NULL, 1505121761, 1528047773, 1, 'Stefania', 'Laconi', 'machlo', '060606'),
(5, '93.34.88.220', 'maurizio.maui@gmail.com', '$2y$08$IJj7OXsyviRMup4oDOUIIehrno/frnZSVfmRrOJjPC7yiZBHRKuUy', NULL, 'maurizio.maui@gmail.com', NULL, NULL, NULL, NULL, 1507237096, 1522935655, 1, 'Maurizio', 'Custodi', '0', '0'),
(6, '82.49.28.174', 'stef.laconi@gmail.com', '$2y$08$MXI4PCsr9Dv.wCuVHeJ9buNREPmDsbb43gib47zUFTUFTxo0HggbG', NULL, 'stef.laconi@gmail.com', NULL, NULL, NULL, NULL, 1515429629, 1515429675, 1, 'Stefania', 'Laconi', '0', '0'),
(7, '93.34.88.220', 'maurizio.1@gmail.com', '$2y$08$80tMdnqKSZiylwih.FNKCeEcvdinrhxipd2jE1MFn9I8UEe3ZqI0a', NULL, 'maurizio.1@gmail.com', NULL, NULL, NULL, NULL, 1522405884, 1522405908, 1, 'Maurizio', 'Custodi', '0', '0'),
(8, '82.63.23.166', 'stefania.laconi@tiscali.it', '$2y$08$I8zisJvNmPdUXt9RazQ41uNfMP1WcN5UiILDrgXn4p50vmWCsAY42', NULL, 'stefania.laconi@tiscali.it', NULL, NULL, NULL, NULL, 1525371009, 1525371207, 1, 'Stefania', 'Laconi', '0', '0'),
(9, '176.246.113.105', 'pireddakatia@gmail.com', '$2y$08$LcQhY5prGM9oTHYpXLUkSe/7oZsdqJIr8t34ZgcTtX6eoujpi1FyC', NULL, 'pireddakatia@gmail.com', NULL, NULL, NULL, NULL, 1527170005, 1527170007, 1, 'katia', 'piredda', '0', '0');

-- --------------------------------------------------------

--
-- Struttura della tabella `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(6, 5, 2),
(11, 6, 2),
(16, 7, 2),
(17, 8, 2),
(18, 9, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `variante_taglia`
--

CREATE TABLE `variante_taglia` (
  `fk_variante` int(11) NOT NULL,
  `fk_taglia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `varianti_prodotti`
--

CREATE TABLE `varianti_prodotti` (
  `id_variante` int(11) NOT NULL,
  `codice_prodotto` varchar(100) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `colore` varchar(15) NOT NULL,
  `colore_codice` varchar(15) NOT NULL,
  `taglia` varchar(10) NOT NULL,
  `codice` varchar(100) NOT NULL,
  `prezzo` double NOT NULL,
  `prezzo_scontato` double NOT NULL,
  `url_img_piccola` varchar(250) NOT NULL,
  `url_img_grande` varchar(250) NOT NULL,
  `url_img_grande_retro` varchar(250) NOT NULL,
  `stato` tinyint(1) NOT NULL,
  `in_stock` tinyint(4) NOT NULL,
  `sync_status` int(2) NOT NULL,
  `sync_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `varianti_prodotti`
--

INSERT INTO `varianti_prodotti` (`id_variante`, `codice_prodotto`, `nome`, `colore`, `colore_codice`, `taglia`, `codice`, `prezzo`, `prezzo_scontato`, `url_img_piccola`, `url_img_grande`, `url_img_grande_retro`, `stato`, `in_stock`, `sync_status`, `sync_time`) VALUES
(1, '5aa5a386dd8547', 'Blue cat - Black / S', 'Black', '050505', 'S', '5aa5a386dd8f31', 47.95, 0, '5aa5a386dd8f31.png', '5aa5a386dd8f31_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(2, '5aa5a386dd8547', 'Blue cat - Black / M', 'Black', '050505', 'M', '5aa5a386dd96d4', 47.95, 0, '5aa5a386dd96d4.png', '5aa5a386dd96d4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(3, '5aa5a386dd8547', 'Blue cat - Black / L', 'Black', '050505', 'L', '5aa5a386dd9e61', 47.95, 0, '5aa5a386dd9e61.png', '5aa5a386dd9e61_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(4, '5aa5a386dd8547', 'Blue cat - Black / XL', 'Black', '050505', 'XL', '5aa5a386dda604', 47.95, 0, '5aa5a386dda604.png', '5aa5a386dda604_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(5, '5aa5a386dd8547', 'Blue cat - White / S', 'White', 'fdfdff', 'S', '5aa5a386ddada7', 47.95, 0, '5aa5a386ddada7.png', '5aa5a386ddada7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(6, '5aa5a386dd8547', 'Blue cat - White / M', 'White', 'fdfdff', 'M', '5aa5a386ddb535', 47.95, 0, '5aa5a386ddb535.png', '5aa5a386ddb535_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(7, '5aa5a386dd8547', 'Blue cat - White / L', 'White', 'fdfdff', 'L', '5aa5a386ddbcc4', 47.95, 0, '5aa5a386ddbcc4.png', '5aa5a386ddbcc4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(8, '5aa5a386dd8547', 'Blue cat - White / XL', 'White', 'fdfdff', 'XL', '5aa5a386ddc456', 47.95, 0, '5aa5a386ddc456.png', '5aa5a386ddc456_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(37, '5aa3013293e052', 'Lips t-shirt - White / S', 'White', 'e5e6e1', 'S', '5aa3013293e907', 50.87, 40.69, '5aa3013293e907.png', '5aa3013293e907_f.png', 'a2f7a-fft_lips_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(38, '5aa3013293e052', 'Lips t-shirt - White / M', 'White', 'e5e6e1', 'M', '5aa3013293f098', 50.87, 40.69, '5aa3013293f098.png', '5aa3013293f098_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(39, '5aa3013293e052', 'Lips t-shirt - White / L', 'White', 'e5e6e1', 'L', '5aa3013293f802', 50.87, 40.69, '5aa3013293f802.png', '5aa3013293f802_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(40, '5aa3013293e052', 'Lips t-shirt - White / XL', 'White', 'e5e6e1', 'XL', '5aa3013293ff78', 50.87, 40.69, '5aa3013293ff78.png', '5aa3013293ff78_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(41, '5aa3013293e052', 'Lips t-shirt - White / 2XL', 'White', 'e5e6e1', '2XL', '5aa301329406e2', 50.87, 40.69, '5aa301329406e2.png', '5aa301329406e2_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(42, '5aa3013293e052', 'Lips t-shirt - Black / S', 'Black', '232227', 'S', '5aa30132940e68', 50.87, 40.69, '5aa30132940e68.png', '5aa30132940e68_f.png', '251ee-fft_lips_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(43, '5aa3013293e052', 'Lips t-shirt - Black / M', 'Black', '232227', 'M', '5aa301329415d1', 50.87, 40.69, '5aa301329415d1.png', '5aa301329415d1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(44, '5aa3013293e052', 'Lips t-shirt - Black / L', 'Black', '232227', 'L', '5aa30132941d34', 50.87, 40.69, '5aa30132941d34.png', '5aa30132941d34_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(45, '5aa3013293e052', 'Lips t-shirt - Black / XL', 'Black', '232227', 'XL', '5aa301329424a7', 50.87, 40.69, '5aa301329424a7.png', '5aa301329424a7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(46, '5aa3013293e052', 'Lips t-shirt - Black / 2XL', 'Black', '232227', '2XL', '5aa30132942c01', 50.87, 40.69, '5aa30132942c01.png', '5aa30132942c01_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(47, '5aa3013293e052', 'Lips t-shirt - Heather Dark Grey / S', 'Heather Dark Gr', '262f36', 'S', '5aa30132943362', 50.87, 40.69, '5aa30132943362.png', '5aa30132943362_f.png', 'd1430-fft_lips_darkgrey_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(48, '5aa3013293e052', 'Lips t-shirt - Heather Dark Grey / M', 'Heather Dark Gr', '262f36', 'M', '5aa30132943ac3', 50.87, 40.69, '5aa30132943ac3.png', '5aa30132943ac3_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(49, '5aa3013293e052', 'Lips t-shirt - Heather Dark Grey / L', 'Heather Dark Gr', '262f36', 'L', '5aa30132944212', 50.87, 40.69, '5aa30132944212.png', '5aa30132944212_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(50, '5aa3013293e052', 'Lips t-shirt - Heather Dark Grey / XL', 'Heather Dark Gr', '262f36', 'XL', '5aa30132944958', 50.87, 40.69, '5aa30132944958.png', '5aa30132944958_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(51, '5aa3013293e052', 'Lips t-shirt - Heather Dark Grey / 2XL', 'Heather Dark Gr', '262f36', '2XL', '5aa301329450b4', 50.87, 40.69, '5aa301329450b4.png', '5aa301329450b4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(52, '5aa3013293e052', 'Lips t-shirt - Heather Blue / S', 'Heather Blue', '4a5684', 'S', '5aa301329457f2', 50.87, 40.69, '5aa301329457f2.png', '5aa301329457f2_f.png', '5f77e-fft_lips_blue_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(53, '5aa3013293e052', 'Lips t-shirt - Heather Blue / M', 'Heather Blue', '4a5684', 'M', '5aa30132945f44', 50.87, 40.69, '5aa30132945f44.png', '5aa30132945f44_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(54, '5aa3013293e052', 'Lips t-shirt - Heather Blue / L', 'Heather Blue', '4a5684', 'L', '5aa30132946696', 50.87, 40.69, '5aa30132946696.png', '5aa30132946696_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(55, '5aa3013293e052', 'Lips t-shirt - Heather Blue / XL', 'Heather Blue', '4a5684', 'XL', '5aa30132946de4', 50.87, 40.69, '5aa30132946de4.png', '5aa30132946de4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(56, '5aa3013293e052', 'Lips t-shirt - Heather Blue / 2XL', 'Heather Blue', '4a5684', '2XL', '5aa30132947536', 50.87, 40.69, '5aa30132947536.png', '5aa30132947536_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(57, '5aa3013293e052', 'Lips t-shirt - Heather Grey / S', 'Heather Grey', '928d92', 'S', '5aa30132947c83', 50.87, 40.69, '5aa30132947c83.png', '5aa30132947c83_f.png', '0e4d8-fft_lips_grey_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(58, '5aa3013293e052', 'Lips t-shirt - Heather Grey / M', 'Heather Grey', '928d92', 'M', '5aa301329483f1', 50.87, 40.69, '5aa301329483f1.png', '5aa301329483f1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(59, '5aa3013293e052', 'Lips t-shirt - Heather Grey / L', 'Heather Grey', '928d92', 'L', '5aa30132948b54', 50.87, 40.69, '5aa30132948b54.png', '5aa30132948b54_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(60, '5aa3013293e052', 'Lips t-shirt - Heather Grey / XL', 'Heather Grey', '928d92', 'XL', '5aa301329492c3', 50.87, 40.69, '5aa301329492c3.png', '5aa301329492c3_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(61, '5aa3013293e052', 'Lips t-shirt - Heather Grey / 2XL', 'Heather Grey', '928d92', '2XL', '5aa30132949a35', 50.87, 40.69, '5aa30132949a35.png', '5aa30132949a35_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(62, '5aa3013293e052', 'Lips t-shirt - Caribbean Blue / S', 'Caribbean Blue', '008fb7', 'S', '5aa3013294a198', 50.87, 40.69, '5aa3013294a198.png', '5aa3013294a198_f.png', 'd1261-fft_lips_caribbean_blue_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(63, '5aa3013293e052', 'Lips t-shirt - Caribbean Blue / M', 'Caribbean Blue', '008fb7', 'M', '5aa3013294a8e3', 50.87, 40.69, '5aa3013294a8e3.png', '5aa3013294a8e3_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(64, '5aa3013293e052', 'Lips t-shirt - Caribbean Blue / L', 'Caribbean Blue', '008fb7', 'L', '5aa3013294b048', 50.87, 40.69, '5aa3013294b048.png', '5aa3013294b048_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(65, '5aa3013293e052', 'Lips t-shirt - Caribbean Blue / XL', 'Caribbean Blue', '008fb7', 'XL', '5aa3013294b7c8', 50.87, 40.69, '5aa3013294b7c8.png', '5aa3013294b7c8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(66, '5aa3013293e052', 'Lips t-shirt - Caribbean Blue / 2XL', 'Caribbean Blue', '008fb7', '2XL', '5aa3013294bf45', 50.87, 40.69, '5aa3013294bf45.png', '5aa3013294bf45_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(67, '5aa3013293e052', 'Lips t-shirt - Red / S', 'Red', 'd00f3c', 'S', '5aa3013294c6b1', 50.87, 40.69, '5aa3013294c6b1.png', '5aa3013294c6b1_f.png', 'efa20-ftt_lips_red_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(68, '5aa3013293e052', 'Lips t-shirt - Red / M', 'Red', 'd00f3c', 'M', '5aa3013294ce36', 50.87, 40.69, '5aa3013294ce36.png', '5aa3013294ce36_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(69, '5aa3013293e052', 'Lips t-shirt - Red / L', 'Red', 'd00f3c', 'L', '5aa3013294d5b3', 50.87, 40.69, '5aa3013294d5b3.png', '5aa3013294d5b3_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(70, '5aa3013293e052', 'Lips t-shirt - Red / XL', 'Red', 'd00f3c', 'XL', '5aa3013294dd22', 50.87, 40.69, '5aa3013294dd22.png', '5aa3013294dd22_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(71, '5aa3013293e052', 'Lips t-shirt - Red / 2XL', 'Red', 'd00f3c', '2XL', '5aa3013294e4c5', 50.87, 40.69, '5aa3013294e4c5.png', '5aa3013294e4c5_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(72, '5aa2fe0e98fe41', 'Flowers - Black / S', 'Black', '050505', 'S', '5aa2fe0e990725', 49.95, 39.96, '5aa2fe0e990725.png', '5aa2fe0e990725_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(73, '5aa2fe0e98fe41', 'Flowers - Black / M', 'Black', '050505', 'M', '5aa2fe0e990ed6', 49.95, 39.96, '5aa2fe0e990ed6.png', '5aa2fe0e990ed6_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(74, '5aa2fe0e98fe41', 'Flowers - Black / L', 'Black', '050505', 'L', '5aa2fe0e991669', 49.95, 39.96, '5aa2fe0e991669.png', '5aa2fe0e991669_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(75, '5aa2fe0e98fe41', 'Flowers - Black / XL', 'Black', '050505', 'XL', '5aa2fe0e991de9', 49.95, 39.96, '5aa2fe0e991de9.png', '5aa2fe0e991de9_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(76, '5aa2fe0e98fe41', 'Flowers - White / S', 'White', 'fdfdff', 'S', '5aa2fe0e992565', 49.95, 39.96, '5aa2fe0e992565.png', '5aa2fe0e992565_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(77, '5aa2fe0e98fe41', 'Flowers - White / M', 'White', 'fdfdff', 'M', '5aa2fe0e992ce5', 49.95, 39.96, '5aa2fe0e992ce5.png', '5aa2fe0e992ce5_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(78, '5aa2fe0e98fe41', 'Flowers - White / L', 'White', 'fdfdff', 'L', '5aa2fe0e993445', 49.95, 39.96, '5aa2fe0e993445.png', '5aa2fe0e993445_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(79, '5aa2fe0e98fe41', 'Flowers - White / XL', 'White', 'fdfdff', 'XL', '5aa2fe0e993bc8', 49.95, 39.96, '5aa2fe0e993bc8.png', '5aa2fe0e993bc8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(80, '5aa2fb8b66a114', 'Masks - Black / S', 'Black', '050505', 'S', '5aa2fb8b66aa28', 49.97, 39.98, '5aa2fb8b66aa28.png', '5aa2fb8b66aa28_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(81, '5aa2fb8b66a114', 'Masks - Black / M', 'Black', '050505', 'M', '5aa2fb8b66b257', 49.97, 39.98, '5aa2fb8b66b257.png', '5aa2fb8b66b257_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(82, '5aa2fb8b66a114', 'Masks - Black / L', 'Black', '050505', 'L', '5aa2fb8b66ba53', 49.97, 39.98, '5aa2fb8b66ba53.png', '5aa2fb8b66ba53_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(83, '5aa2fb8b66a114', 'Masks - Black / XL', 'Black', '050505', 'XL', '5aa2fb8b66c259', 49.97, 39.98, '5aa2fb8b66c259.png', '5aa2fb8b66c259_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(84, '5aa2fb8b66a114', 'Masks - White / S', 'White', 'fdfdff', 'S', '5aa2fb8b66ca43', 49.97, 39.98, '5aa2fb8b66ca43.png', '5aa2fb8b66ca43_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(85, '5aa2fb8b66a114', 'Masks - White / M', 'White', 'fdfdff', 'M', '5aa2fb8b66d237', 49.97, 39.98, '5aa2fb8b66d237.png', '5aa2fb8b66d237_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(86, '5aa2fb8b66a114', 'Masks - White / L', 'White', 'fdfdff', 'L', '5aa2fb8b66d9f5', 49.97, 39.98, '5aa2fb8b66d9f5.png', '5aa2fb8b66d9f5_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(87, '5aa2fb8b66a114', 'Masks - White / XL', 'White', 'fdfdff', 'XL', '5aa2fb8b66e1c9', 49.97, 39.98, '5aa2fb8b66e1c9.png', '5aa2fb8b66e1c9_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(88, '5aa2f943e9f697', 'Yellow cat - White / S', 'White', 'e5e6e1', 'S', '5aa2f943e9ffa8', 47.95, 38.36, '5aa2f943e9ffa8.png', '5aa2f943e9ffa8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(89, '5aa2f943e9f697', 'Yellow cat - White / M', 'White', 'e5e6e1', 'M', '5aa2f943ea07a7', 47.95, 38.36, '5aa2f943ea07a7.png', '5aa2f943ea07a7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(90, '5aa2f943e9f697', 'Yellow cat - White / L', 'White', 'e5e6e1', 'L', '5aa2f943ea0fd8', 47.95, 38.36, '5aa2f943ea0fd8.png', '5aa2f943ea0fd8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(91, '5aa2f943e9f697', 'Yellow cat - White / XL', 'White', 'e5e6e1', 'XL', '5aa2f943ea1822', 47.95, 38.36, '5aa2f943ea1822.png', '5aa2f943ea1822_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(92, '5aa2f943e9f697', 'Yellow cat - White / 2XL', 'White', 'e5e6e1', '2XL', '5aa2f943ea2041', 47.95, 38.36, '5aa2f943ea2041.png', '5aa2f943ea2041_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(93, '5aa2f943e9f697', 'Yellow cat - Black / S', 'Black', '232227', 'S', '5aa2f943ea2826', 47.95, 38.36, '5aa2f943ea2826.png', '5aa2f943ea2826_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(94, '5aa2f943e9f697', 'Yellow cat - Black / M', 'Black', '232227', 'M', '5aa2f943ea3033', 47.95, 38.36, '5aa2f943ea3033.png', '5aa2f943ea3033_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(95, '5aa2f943e9f697', 'Yellow cat - Black / L', 'Black', '232227', 'L', '5aa2f943ea3855', 47.95, 38.36, '5aa2f943ea3855.png', '5aa2f943ea3855_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(96, '5aa2f943e9f697', 'Yellow cat - Black / XL', 'Black', '232227', 'XL', '5aa2f943ea4088', 47.95, 38.36, '5aa2f943ea4088.png', '5aa2f943ea4088_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(97, '5aa2f943e9f697', 'Yellow cat - Black / 2XL', 'Black', '232227', '2XL', '5aa2f943ea48d3', 47.95, 38.36, '5aa2f943ea48d3.png', '5aa2f943ea48d3_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(98, '5aa2f943e9f697', 'Yellow cat - Heather Dark Grey / S', 'Heather Dark Gr', '262f36', 'S', '5aa2f943ea5112', 47.95, 38.36, '5aa2f943ea5112.png', '5aa2f943ea5112_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(99, '5aa2f943e9f697', 'Yellow cat - Heather Dark Grey / M', 'Heather Dark Gr', '262f36', 'M', '5aa2f943ea5978', 47.95, 38.36, '5aa2f943ea5978.png', '5aa2f943ea5978_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(100, '5aa2f943e9f697', 'Yellow cat - Heather Dark Grey / L', 'Heather Dark Gr', '262f36', 'L', '5aa2f943ea6195', 47.95, 38.36, '5aa2f943ea6195.png', '5aa2f943ea6195_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(101, '5aa2f943e9f697', 'Yellow cat - Heather Dark Grey / XL', 'Heather Dark Gr', '262f36', 'XL', '5aa2f943ea69a9', 47.95, 38.36, '5aa2f943ea69a9.png', '5aa2f943ea69a9_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(102, '5aa2f943e9f697', 'Yellow cat - Heather Dark Grey / 2XL', 'Heather Dark Gr', '262f36', '2XL', '5aa2f943ea71c1', 47.95, 38.36, '5aa2f943ea71c1.png', '5aa2f943ea71c1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(103, '5aa2f81d3552c7', 'Gallinella sarda - Black / S', 'Black', '050505', 'S', '5aa2f81d355ad6', 45.97, 0, '5aa2f81d355ad6.png', '5aa2f81d355ad6_f.png', '639ca-slt_gallinella_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(104, '5aa2f81d3552c7', 'Gallinella sarda - Black / M', 'Black', '050505', 'M', '5aa2f81d356202', 45.97, 0, '5aa2f81d356202.png', '5aa2f81d356202_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(105, '5aa2f81d3552c7', 'Gallinella sarda - Black / L', 'Black', '050505', 'L', '5aa2f81d356937', 45.97, 0, '5aa2f81d356937.png', '5aa2f81d356937_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(106, '5aa2f81d3552c7', 'Gallinella sarda - Black / XL', 'Black', '050505', 'XL', '5aa2f81d357062', 45.97, 0, '5aa2f81d357062.png', '5aa2f81d357062_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(107, '5aa2f81d3552c7', 'Gallinella sarda - White / S', 'White', 'fdfdff', 'S', '5aa2f81d357781', 45.97, 0, '5aa2f81d357781.png', '5aa2f81d357781_f.png', '9bb31-slt_gallinella_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(108, '5aa2f81d3552c7', 'Gallinella sarda - White / M', 'White', 'fdfdff', 'M', '5aa2f81d357ea1', 45.97, 0, '5aa2f81d357ea1.png', '5aa2f81d357ea1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(109, '5aa2f81d3552c7', 'Gallinella sarda - White / L', 'White', 'fdfdff', 'L', '5aa2f81d3585b2', 45.97, 0, '5aa2f81d3585b2.png', '5aa2f81d3585b2_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(110, '5aa2f81d3552c7', 'Gallinella sarda - White / XL', 'White', 'fdfdff', 'XL', '5aa2f81d358cc9', 45.97, 0, '5aa2f81d358cc9.png', '5aa2f81d358cc9_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(111, '5aa2a4314a7da4', 'Yellow cat', 'White', '', '11oz', '5aa2a4314a85e1', 24.97, 0, '5aa2a4314a85e1.png', '5aa2a4314a85e1_f.png', 'a1ab1-cm_yellow_cat_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(112, '5aa2a3db357648', 'Yang', 'White', '', '11oz', '5aa2a3db357f84', 7.95, 0, '5aa2a3db357f84.png', '5aa2a3db357f84_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(113, '5aa2a35b45f316', 'Universe', 'White', '', '11oz', '5aa2a35b45fc52', 24.97, 0, '5aa2a35b45fc52.png', '5aa2a35b45fc52_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(114, '5aa2a2dc902565', 'Standby', 'White', '', '11oz', '5aa2a2dc903193', 24.97, 0, '5aa2a2dc903193.png', '5aa2a2dc903193_f.png', '3cfc6-cm_standby_purple_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(115, '5aa2a2244efd56', 'Ma Chlò', 'White', '', '11oz', '5aa2a2244f0586', 24.97, 0, '5aa2a2244f0586.png', '5aa2a2244f0586_f.png', '8e6b1-cm_machlo_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(116, '5aa29e4923eeb6', 'La separazione', 'White', '', '11oz', '5aa29e4923f7c6', 24.97, 0, '5aa29e4923f7c6.png', '5aa29e4923f7c6_f.png', 'a4c63-cm_la_separazione_blue_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(117, '5aa29dff07e579', 'Japan', 'White', '', '11oz', '5aa29dff07ee65', 24.97, 0, '5aa29dff07ee65.png', '5aa29dff07ee65_f.png', '28173-cm_japan_yellow_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(118, '5aa29cee9d00e1', 'Il silenzio', 'White', '', '11oz', '5aa29cee9d08c7', 24.97, 0, '5aa29cee9d08c7.png', '5aa29cee9d08c7_f.png', '82450-cm_il_silenzio_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(119, '5aa29cb78a18b1', 'Un nuovo inizio', 'White', '', '11oz', '5aa29cb78a1f97', 24.97, 0, '5aa29cb78a1f97.png', '5aa29cb78a1f97_f.png', '0897a-cm_black_masks_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(120, '5aa29c681a45f2', 'Il bacio', 'White', '', '11oz', '5aa29c681a4e66', 24.97, 0, '5aa29c681a4e66.png', '5aa29c681a4e66_f.png', 'b9272-cm_il_bacio_green_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(121, '5aa29c1e763eb7', 'Il pensiero', 'White', '', '11oz', '5aa29c1e764934', 24.97, 0, '5aa29c1e764934.png', '5aa29c1e764934_f.png', '6dd2f-cm_il_pensiero_red_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(122, '5aa29bd9d57cd5', 'Jellyfish', 'White', '', '11oz', '5aa29bd9d584c6', 24.97, 0, '5aa29bd9d584c6.png', '5aa29bd9d584c6_f.png', 'f10d7-cm_jellyfish_green_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(123, '5aa29b851d86a1', 'Color tree', 'White', '', '11oz', '5aa29b851d8f67', 24.97, 0, '5aa29b851d8f67.png', '5aa29b851d8f67_f.png', 'c1e29-cm_colortree_red_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(124, '5aa29b26787987', 'Colorful town', 'White', '', '11oz', '5aa29b26788174', 24.97, 0, '5aa29b26788174.png', '5aa29b26788174_f.png', '341a7-cm_colorful_town_blue_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(125, '5aa29ab9c856c2', 'A monkey playing with a snake', 'White', '', '11oz', '5aa29ab9c86045', 24.97, 0, '5aa29ab9c86045.png', '5aa29ab9c86045_f.png', 'e20e6-cm_a_monkey_playing_with_a_snake_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(126, '5aa29a3e69b2c4', 'Feel good', 'White', '', '11oz', '5aa29a3e69bad6', 24.97, 0, '5aa29a3e69bad6.png', '5aa29a3e69bad6_f.png', 'e9eef-cm_eclipse_orange_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(127, '5aa299f1a4ad82', 'Talking', 'White', '', '11oz', '5aa299f1a4b653', 24.97, 0, '5aa299f1a4b653.png', '5aa299f1a4b653_f.png', '2f3e2-cm_talking_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(128, '5aa298b3ec6ad5', 'Japan - XS', 'White', 'ffffff', 'XS', '5aa298b3ec73a4', 63.95, 54.35, '5aa298b3ec73a4.png', '5aa298b3ec73a4_f.png', 'ca412-lgg_japan_yellow_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(129, '5aa298b3ec6ad5', 'Japan - S', 'White', 'ffffff', 'S', '5aa298b3ec7b32', 63.95, 54.35, '5aa298b3ec7b32.png', '5aa298b3ec7b32_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(130, '5aa298b3ec6ad5', 'Japan - M', 'White', 'ffffff', 'M', '5aa298b3ec82a3', 63.95, 54.35, '5aa298b3ec82a3.png', '5aa298b3ec82a3_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(131, '5aa298b3ec6ad5', 'Japan - L', 'White', 'ffffff', 'L', '5aa298b3ec8a01', 63.95, 54.35, '5aa298b3ec8a01.png', '5aa298b3ec8a01_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(132, '5aa298b3ec6ad5', 'Japan - XL', 'White', 'ffffff', 'XL', '5aa298b3ec9183', 63.95, 54.35, '5aa298b3ec9183.png', '5aa298b3ec9183_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(133, '5aa297d0cfdd44', 'Eclipse - XS', 'White', 'ffffff', 'XS', '5aa297d0cfe558', 63.95, 54.35, '5aa297d0cfe558.png', '5aa297d0cfe558_f.png', 'd5fc6-lgg_eclipse_lilac_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(134, '5aa297d0cfdd44', 'Eclipse - S', 'White', 'ffffff', 'S', '5aa297d0cfec74', 63.95, 54.35, '5aa297d0cfec74.png', '5aa297d0cfec74_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(135, '5aa297d0cfdd44', 'Eclipse - M', 'White', 'ffffff', 'M', '5aa297d0cff377', 63.95, 54.35, '5aa297d0cff377.png', '5aa297d0cff377_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(136, '5aa297d0cfdd44', 'Eclipse - L', 'White', 'ffffff', 'L', '5aa297d0cffa85', 63.95, 54.35, '5aa297d0cffa85.png', '5aa297d0cffa85_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(137, '5aa297d0cfdd44', 'Eclipse - XL', 'White', 'ffffff', 'XL', '5aa297d0d00173', 63.95, 54.35, '5aa297d0d00173.png', '5aa297d0d00173_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(138, '5aa27b23a9f4a8', 'Yang - XS', 'White', 'ffffff', 'XS', '5aa27b23a9fca1', 65.95, 54.35, '5aa27b23a9fca1.png', '5aa27b23a9fca1_f.png', '2fdef-lgg_yang_aqua_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(139, '5aa27b23a9f4a8', 'Yang - S', 'White', 'ffffff', 'S', '5aa27b23aa03d7', 65.95, 54.35, '5aa27b23aa03d7.png', '5aa27b23aa03d7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(140, '5aa27b23a9f4a8', 'Yang - M', 'White', 'ffffff', 'M', '5aa27b23aa0b05', 65.95, 54.35, '5aa27b23aa0b05.png', '5aa27b23aa0b05_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(141, '5aa27b23a9f4a8', 'Yang - L', 'White', 'ffffff', 'L', '5aa27b23aa1215', 65.95, 54.35, '5aa27b23aa1215.png', '5aa27b23aa1215_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(142, '5aa27b23a9f4a8', 'Yang - XL', 'White', 'ffffff', 'XL', '5aa27b23aa1932', 65.95, 54.35, '5aa27b23aa1932.png', '5aa27b23aa1932_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(143, '5aa27a84d1e238', 'Standby - XS', 'White', 'ffffff', 'XS', '5aa27a84d1ea75', 63.95, 54.35, '5aa27a84d1ea75.png', '5aa27a84d1ea75_f.png', 'b5de5-lgg_standby_purple_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(144, '5aa27a84d1e238', 'Standby - S', 'White', 'ffffff', 'S', '5aa27a84d1f193', 63.95, 54.35, '5aa27a84d1f193.png', '5aa27a84d1f193_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(145, '5aa27a84d1e238', 'Standby - M', 'White', 'ffffff', 'M', '5aa27a84d1f892', 63.95, 54.35, '5aa27a84d1f892.png', '5aa27a84d1f892_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(146, '5aa27a84d1e238', 'Standby - L', 'White', 'ffffff', 'L', '5aa27a84d1ff84', 63.95, 54.35, '5aa27a84d1ff84.png', '5aa27a84d1ff84_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(147, '5aa27a84d1e238', 'Standby - XL', 'White', 'ffffff', 'XL', '5aa27a84d20686', 63.95, 54.35, '5aa27a84d20686.png', '5aa27a84d20686_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(148, '5aa27a09f005c3', 'Universe - XS', 'White', 'ffffff', 'XS', '5aa27a09f00db6', 63.95, 54.35, '5aa27a09f00db6.png', '5aa27a09f00db6_f.png', '0da11-lgg_universe_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(149, '5aa27a09f005c3', 'Universe - S', 'White', 'ffffff', 'S', '5aa27a09f014e8', 63.95, 54.35, '5aa27a09f014e8.png', '5aa27a09f014e8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(150, '5aa27a09f005c3', 'Universe - M', 'White', 'ffffff', 'M', '5aa27a09f01bf8', 63.95, 54.35, '5aa27a09f01bf8.png', '5aa27a09f01bf8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(151, '5aa27a09f005c3', 'Universe - L', 'White', 'ffffff', 'L', '5aa27a09f022f1', 63.95, 54.35, '5aa27a09f022f1.png', '5aa27a09f022f1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(152, '5aa27a09f005c3', 'Universe - XL', 'White', 'ffffff', 'XL', '5aa27a09f029f5', 63.95, 54.35, '5aa27a09f029f5.png', '5aa27a09f029f5_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(153, '5aa27975242e84', 'Colored cats - XS', 'White', 'ffffff', 'XS', '5aa27975243692', 63.95, 54.35, '5aa27975243692.png', '5aa27975243692_f.png', '93ecc-lgg_colored_cats_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(154, '5aa27975242e84', 'Colored cats - S', 'White', 'ffffff', 'S', '5aa27975243cf6', 63.95, 54.35, '5aa27975243cf6.png', '5aa27975243cf6_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(155, '5aa27975242e84', 'Colored cats - M', 'White', 'ffffff', 'M', '5aa27975244473', 63.95, 54.35, '5aa27975244473.png', '5aa27975244473_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(156, '5aa27975242e84', 'Colored cats - L', 'White', 'ffffff', 'L', '5aa27975244b92', 63.95, 54.35, '5aa27975244b92.png', '5aa27975244b92_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(157, '5aa27975242e84', 'Colored cats - XL', 'White', 'ffffff', 'XL', '5aa279752452b5', 63.95, 54.35, '5aa279752452b5.png', '5aa279752452b5_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(158, '5aa275790bd952', 'Geometric - XS', 'White', 'ffffff', 'XS', '5aa275790be221', 63.95, 54.35, '5aa275790be221.png', '5aa275790be221_f.png', 'ab44b-lgg_geometric_green_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(159, '5aa275790bd952', 'Geometric - S', 'White', 'ffffff', 'S', '5aa275790bea14', 63.95, 54.35, '5aa275790bea14.png', '5aa275790bea14_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(160, '5aa275790bd952', 'Geometric - M', 'White', 'ffffff', 'M', '5aa275790bf1e4', 63.95, 54.35, '5aa275790bf1e4.png', '5aa275790bf1e4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(161, '5aa275790bd952', 'Geometric - L', 'White', 'ffffff', 'L', '5aa275790bf9a4', 63.95, 54.35, '5aa275790bf9a4.png', '5aa275790bf9a4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(162, '5aa275790bd952', 'Geometric - XL', 'White', 'ffffff', 'XL', '5aa275790c0193', 63.95, 54.35, '5aa275790c0193.png', '5aa275790c0193_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(163, '5aa010e062b998', 'Il pensiero - XS', 'White', 'ffffff', 'XS', '5aa010e062c288', 70.97, 60.32, '5aa010e062c288.png', '5aa010e062c288_f.png', 'f2dd3-md_il_pensiero_red_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(164, '5aa010e062b998', 'Il pensiero - S', 'White', 'ffffff', 'S', '5aa010e062ca38', 70.97, 60.32, '5aa010e062ca38.png', '5aa010e062ca38_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(165, '5aa010e062b998', 'Il pensiero - M', 'White', 'ffffff', 'M', '5aa010e062d1c4', 70.97, 60.32, '5aa010e062d1c4.png', '5aa010e062d1c4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(166, '5aa010e062b998', 'Il pensiero - L', 'White', 'ffffff', 'L', '5aa010e062d942', 70.97, 60.32, '5aa010e062d942.png', '5aa010e062d942_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(167, '5aa010e062b998', 'Il pensiero - XL', 'White', 'ffffff', 'XL', '5aa010e062e0c3', 70.97, 60.32, '5aa010e062e0c3.png', '5aa010e062e0c3_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(168, '5aa00f5f1c42e5', 'Jungle flowers dress black - XS', 'White', 'ffffff', 'XS', '5aa00f5f1c4b17', 70.97, 60.32, '5aa00f5f1c4b17.png', '5aa00f5f1c4b17_f.png', '05e09-md_jungle_flowers_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(169, '5aa00f5f1c42e5', 'Jungle flowers dress black - S', 'White', 'ffffff', 'S', '5aa00f5f1c5258', 70.97, 60.32, '5aa00f5f1c5258.png', '5aa00f5f1c5258_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(170, '5aa00f5f1c42e5', 'Jungle flowers dress black - M', 'White', 'ffffff', 'M', '5aa00f5f1c5981', 70.97, 60.32, '5aa00f5f1c5981.png', '5aa00f5f1c5981_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(171, '5aa00f5f1c42e5', 'Jungle flowers dress black - L', 'White', 'ffffff', 'L', '5aa00f5f1c6091', 70.97, 60.32, '5aa00f5f1c6091.png', '5aa00f5f1c6091_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(172, '5aa00f5f1c42e5', 'Jungle flowers dress black - XL', 'White', 'ffffff', 'XL', '5aa00f5f1c67a6', 70.97, 60.32, '5aa00f5f1c67a6.png', '5aa00f5f1c67a6_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(173, '5aa00eb9a04005', 'Jungle flowers dress - XS', 'White', 'ffffff', 'XS', '5aa00eb9a04839', 70.97, 60.32, '5aa00eb9a04839.png', '5aa00eb9a04839_f.png', '83b21-md_jungle_flowers_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(174, '5aa00eb9a04005', 'Jungle flowers dress - S', 'White', 'ffffff', 'S', '5aa00eb9a04f68', 70.97, 60.32, '5aa00eb9a04f68.png', '5aa00eb9a04f68_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(175, '5aa00eb9a04005', 'Jungle flowers dress - M', 'White', 'ffffff', 'M', '5aa00eb9a05671', 70.97, 60.32, '5aa00eb9a05671.png', '5aa00eb9a05671_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(176, '5aa00eb9a04005', 'Jungle flowers dress - L', 'White', 'ffffff', 'L', '5aa00eb9a05d84', 70.97, 60.32, '5aa00eb9a05d84.png', '5aa00eb9a05d84_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(177, '5aa00eb9a04005', 'Jungle flowers dress - XL', 'White', 'ffffff', 'XL', '5aa00eb9a06499', 70.97, 60.32, '5aa00eb9a06499.png', '5aa00eb9a06499_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(178, '5aa00df8cceb58', 'La separazione - XS', 'White', 'ffffff', 'XS', '5aa00df8ccf352', 70.97, 60.32, '5aa00df8ccf352.png', '5aa00df8ccf352_f.png', '17c84-md_la_separazione_blue_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(179, '5aa00df8cceb58', 'La separazione - S', 'White', 'ffffff', 'S', '5aa00df8ccfa88', 70.97, 60.32, '5aa00df8ccfa88.png', '5aa00df8ccfa88_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(180, '5aa00df8cceb58', 'La separazione - M', 'White', 'ffffff', 'M', '5aa00df8cd01a4', 70.97, 60.32, '5aa00df8cd01a4.png', '5aa00df8cd01a4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(181, '5aa00df8cceb58', 'La separazione - L', 'White', 'ffffff', 'L', '5aa00df8cd08b1', 70.97, 60.32, '5aa00df8cd08b1.png', '5aa00df8cd08b1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(182, '5aa00df8cceb58', 'La separazione - XL', 'White', 'ffffff', 'XL', '5aa00df8cd0fc9', 70.97, 60.32, '5aa00df8cd0fc9.png', '5aa00df8cd0fc9_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(183, '5aa00db6bb9d33', 'Life of a tree - XS', 'White', 'ffffff', 'XS', '5aa00db6bba5c1', 70.97, 60.32, '5aa00db6bba5c1.png', '5aa00db6bba5c1_f.png', 'd36ce-md_life_of_a_tree_yellow_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(184, '5aa00db6bb9d33', 'Life of a tree - S', 'White', 'ffffff', 'S', '5aa00db6bbad24', 70.97, 60.32, '5aa00db6bbad24.png', '5aa00db6bbad24_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(185, '5aa00db6bb9d33', 'Life of a tree - M', 'White', 'ffffff', 'M', '5aa00db6bbb435', 70.97, 60.32, '5aa00db6bbb435.png', '5aa00db6bbb435_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(186, '5aa00db6bb9d33', 'Life of a tree - L', 'White', 'ffffff', 'L', '5aa00db6bbbb41', 70.97, 60.32, '5aa00db6bbbb41.png', '5aa00db6bbbb41_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(187, '5aa00db6bb9d33', 'Life of a tree - XL', 'White', 'ffffff', 'XL', '5aa00db6bbc254', 70.97, 60.32, '5aa00db6bbc254.png', '5aa00db6bbc254_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(188, '5aa00c3e2e6184', 'Lips dress - XS', 'White', 'ffffff', 'XS', '5aa00c3e2e6a34', 69.95, 0, '5aa00c3e2e6a34.png', '5aa00c3e2e6a34_f.png', '6b60c-md_lips_dress_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(189, '5aa00c3e2e6184', 'Lips dress - S', 'White', 'ffffff', 'S', '5aa00c3e2e71c5', 69.95, 0, '5aa00c3e2e71c5.png', '5aa00c3e2e71c5_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(190, '5aa00c3e2e6184', 'Lips dress - M', 'White', 'ffffff', 'M', '5aa00c3e2e7937', 69.95, 0, '5aa00c3e2e7937.png', '5aa00c3e2e7937_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(191, '5aa00c3e2e6184', 'Lips dress - L', 'White', 'ffffff', 'L', '5aa00c3e2e80b4', 69.95, 0, '5aa00c3e2e80b4.png', '5aa00c3e2e80b4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(192, '5aa00c3e2e6184', 'Lips dress - XL', 'White', 'ffffff', 'XL', '5aa00c3e2e8853', 69.95, 0, '5aa00c3e2e8853.png', '5aa00c3e2e8853_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(193, '5aa00bc3dc48f8', 'Monkeys into the jungle - XS', 'White', 'ffffff', 'XS', '5aa00bc3dc51d5', 70.97, 60.32, '5aa00bc3dc51d5.png', '5aa00bc3dc51d5_f.png', '35d56-md_monkeys_into_the_jungle_blue_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(194, '5aa00bc3dc48f8', 'Monkeys into the jungle - S', 'White', 'ffffff', 'S', '5aa00bc3dc59a7', 70.97, 60.32, '5aa00bc3dc59a7.png', '5aa00bc3dc59a7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(195, '5aa00bc3dc48f8', 'Monkeys into the jungle - M', 'White', 'ffffff', 'M', '5aa00bc3dc6206', 70.97, 60.32, '5aa00bc3dc6206.png', '5aa00bc3dc6206_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(196, '5aa00bc3dc48f8', 'Monkeys into the jungle - L', 'White', 'ffffff', 'L', '5aa00bc3dc6838', 70.97, 60.32, '5aa00bc3dc6838.png', '5aa00bc3dc6838_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(197, '5aa00bc3dc48f8', 'Monkeys into the jungle - XL', 'White', 'ffffff', 'XL', '5aa00bc3dc6fe1', 70.97, 60.32, '5aa00bc3dc6fe1.png', '5aa00bc3dc6fe1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(198, '5aa00b9b1fc403', 'Reaching for the moon - XS', 'White', 'ffffff', 'XS', '5aa00b9b1fccd2', 70.97, 60.32, '5aa00b9b1fccd2.png', '5aa00b9b1fccd2_f.png', '4b4b3-md_reaching_for_the_moon_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(199, '5aa00b9b1fc403', 'Reaching for the moon - S', 'White', 'ffffff', 'S', '5aa00b9b1fd4b1', 70.97, 60.32, '5aa00b9b1fd4b1.png', '5aa00b9b1fd4b1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(200, '5aa00b9b1fc403', 'Reaching for the moon - M', 'White', 'ffffff', 'M', '5aa00b9b1fdc61', 70.97, 60.32, '5aa00b9b1fdc61.png', '5aa00b9b1fdc61_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(201, '5aa00b9b1fc403', 'Reaching for the moon - L', 'White', 'ffffff', 'L', '5aa00b9b1fe416', 70.97, 60.32, '5aa00b9b1fe416.png', '5aa00b9b1fe416_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(202, '5aa00b9b1fc403', 'Reaching for the moon - XL', 'White', 'ffffff', 'XL', '5aa00b9b1fed33', 70.97, 60.32, '5aa00b9b1fed33.png', '5aa00b9b1fed33_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(203, '5aa009916da013', 'Un nuovo inizio - XS', 'White', 'ffffff', 'XS', '5aa009916da824', 70.97, 60.32, '5aa009916da824.png', '5aa009916da824_f.png', '71165-md_un_nuovo_inizio_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(204, '5aa009916da013', 'Un nuovo inizio - S', 'White', 'ffffff', 'S', '5aa009916daf57', 70.97, 60.32, '5aa009916daf57.png', '5aa009916daf57_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(205, '5aa009916da013', 'Un nuovo inizio - M', 'White', 'ffffff', 'M', '5aa009916db669', 70.97, 60.32, '5aa009916db669.png', '5aa009916db669_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(206, '5aa009916da013', 'Un nuovo inizio - L', 'White', 'ffffff', 'L', '5aa009916dbd75', 70.32, 60.32, '5aa009916dbd75.png', '5aa009916dbd75_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(207, '5aa009916da013', 'Un nuovo inizio - XL', 'White', 'ffffff', 'XL', '5aa009916dc482', 70.97, 60.32, '5aa009916dc482.png', '5aa009916dc482_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(208, '5aa0058e449ec3', 'Woman\'s lips - XS', 'White', 'ffffff', 'XS', '5aa0058e44a6c2', 70.97, 60.32, '5aa0058e44a6c2.png', '5aa0058e44a6c2_f.png', '136b6-md_woman_lips_red_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(209, '5aa0058e449ec3', 'Woman\'s lips - S', 'White', 'ffffff', 'S', '5aa0058e44ae05', 70.97, 60.32, '5aa0058e44ae05.png', '5aa0058e44ae05_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(210, '5aa0058e449ec3', 'Woman\'s lips - M', 'White', 'ffffff', 'M', '5aa0058e44b513', 70.97, 60.32, '5aa0058e44b513.png', '5aa0058e44b513_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(211, '5aa0058e449ec3', 'Woman\'s lips - L', 'White', 'ffffff', 'L', '5aa0058e44bc27', 70.97, 60.32, '5aa0058e44bc27.png', '5aa0058e44bc27_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(212, '5aa0058e449ec3', 'Woman\'s lips - XL', 'White', 'ffffff', 'XL', '5aa0058e44c331', 70.97, 60.32, '5aa0058e44c331.png', '5aa0058e44c331_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(213, '5aa00071b87794', 'Wild girl - XS', 'White', 'ffffff', 'XS', '5aa00071b87f97', 70.97, 60.32, '5aa00071b87f97.png', '5aa00071b87f97_f.png', '6d97e-md_wild_girl_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(214, '5aa00071b87794', 'Wild girl - S', 'White', 'ffffff', 'S', '5aa00071b886d1', 70.97, 60.32, '5aa00071b886d1.png', '5aa00071b886d1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(215, '5aa00071b87794', 'Wild girl - M', 'White', 'ffffff', 'M', '5aa00071b88e02', 70.97, 60.32, '5aa00071b88e02.png', '5aa00071b88e02_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(216, '5aa00071b87794', 'Wild girl - L', 'White', 'ffffff', 'L', '5aa00071b89516', 70.97, 60.32, '5aa00071b89516.png', '5aa00071b89516_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(217, '5aa00071b87794', 'Wild girl - XL', 'White', 'ffffff', 'XL', '5aa00071b89c38', 70.97, 60.32, '5aa00071b89c38.png', '5aa00071b89c38_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(218, '5aa0001c6a2461', 'Snakes dress - XS', 'White', 'ffffff', 'XS', '5aa0001c6a2cc6', 70.97, 60.32, '5aa0001c6a2cc6.png', '5aa0001c6a2cc6_f.png', '16de6-md_snakes_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(219, '5aa0001c6a2461', 'Snakes dress - S', 'White', 'ffffff', 'S', '5aa0001c6a3457', 70.97, 60.32, '5aa0001c6a3457.png', '5aa0001c6a3457_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(220, '5aa0001c6a2461', 'Snakes dress - M', 'White', 'ffffff', 'M', '5aa0001c6a3b96', 70.97, 60.32, '5aa0001c6a3b96.png', '5aa0001c6a3b96_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(221, '5aa0001c6a2461', 'Snakes dress - L', 'White', 'ffffff', 'L', '5aa0001c6a42b2', 70.97, 60.32, '5aa0001c6a42b2.png', '5aa0001c6a42b2_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(222, '5aa0001c6a2461', 'Snakes dress - XL', 'White', 'ffffff', 'XL', '5aa0001c6a49c1', 70.97, 60.32, '5aa0001c6a49c1.png', '5aa0001c6a49c1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(223, '5a9fffa5692b77', 'Colorful town - XS', 'White', 'ffffff', 'XS', '5a9fffa56934f4', 70.97, 60.32, '5a9fffa56934f4.png', '5a9fffa56934f4_f.png', 'f2cb8-md_coloful_town_blue_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(224, '5a9fffa5692b77', 'Colorful town - S', 'White', 'ffffff', 'S', '5a9fffa5693c79', 70.97, 60.32, '5a9fffa5693c79.png', '5a9fffa5693c79_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(225, '5a9fffa5692b77', 'Colorful town - M', 'White', 'ffffff', 'M', '5a9fffa5694389', 70.97, 60.32, '5a9fffa5694389.png', '5a9fffa5694389_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(226, '5a9fffa5692b77', 'Colorful town - L', 'White', 'ffffff', 'L', '5a9fffa5694a81', 70.97, 60.32, '5a9fffa5694a81.png', '5a9fffa5694a81_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(227, '5a9fffa5692b77', 'Colorful town - XL', 'White', 'ffffff', 'XL', '5a9fffa5695178', 70.97, 60.32, '5a9fffa5695178.png', '5a9fffa5695178_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(232, '5a9e7dbe639f93', 'Gallinella sarda t-shirt - S', 'Black', '2F2E33', 'S', '5a9e7dbe63a8c7', 49.95, 0, '5a9e7dbe63a8c7.png', '5a9e7dbe63a8c7_f.png', 'c8a90-lsl_gallinella_sarda_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(233, '5a9e7dbe639f93', 'Gallinella sarda t-shirt - M', 'Black', '2F2E33', 'M', '5a9e7dbe63b125', 49.95, 0, '5a9e7dbe63b125.png', '5a9e7dbe63b125_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(234, '5a9e7dbe639f93', 'Gallinella sarda t-shirt - L', 'Black', '2F2E33', 'L', '5a9e7dbe63b975', 49.95, 0, '5a9e7dbe63b975.png', '5a9e7dbe63b975_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(235, '5a9e7dbe639f93', 'Gallinella sarda t-shirt - XL', 'Black', '2F2E33', 'XL', '5a9e7dbe63c1e7', 49.95, 0, '5a9e7dbe63c1e7.png', '5a9e7dbe63c1e7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(240, '5a9e7cfa97eb11', 'Dreaming of you t-shirt - S', 'Black', '2F2E33', 'S', '5a9e7cfa97f343', 49.95, 0, '5a9e7cfa97f343.png', '5a9e7cfa97f343_f.png', 'ccd00-lsl_dreaming_of_you_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(241, '5a9e7cfa97eb11', 'Dreaming of you t-shirt - M', 'Black', '2F2E33', 'M', '5a9e7cfa97fa83', 49.95, 0, '5a9e7cfa97fa83.png', '5a9e7cfa97fa83_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(242, '5a9e7cfa97eb11', 'Dreaming of you t-shirt - L', 'Black', '2F2E33', 'L', '5a9e7cfa9801b8', 49.95, 0, '5a9e7cfa9801b8.png', '5a9e7cfa9801b8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(243, '5a9e7cfa97eb11', 'Dreaming of you t-shirt - XL', 'Black', '2F2E33', 'XL', '5a9e7cfa9808e7', 49.95, 0, '5a9e7cfa9808e7.png', '5a9e7cfa9808e7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(244, '5a9e7be6ef2c59', 'Il bacio t-shirt - XS', 'White', 'ffffff', 'XS', '5a9e7be6ef3494', 59.97, 49.27, '5a9e7be6ef3494.png', '5a9e7be6ef3494_f.png', 'bcda4-lsm_il_bacio_lime_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(245, '5a9e7be6ef2c59', 'Il bacio t-shirt - S', 'White', 'ffffff', 'S', '5a9e7be6ef3bf1', 59.97, 49.27, '5a9e7be6ef3bf1.png', '5a9e7be6ef3bf1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(246, '5a9e7be6ef2c59', 'Il bacio t-shirt - M', 'White', 'ffffff', 'M', '5a9e7be6ef4326', 59.97, 49.27, '5a9e7be6ef4326.png', '5a9e7be6ef4326_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(247, '5a9e7be6ef2c59', 'Il bacio t-shirt - L', 'White', 'ffffff', 'L', '5a9e7be6ef4a88', 59.97, 49.27, '5a9e7be6ef4a88.png', '5a9e7be6ef4a88_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(248, '5a9e7be6ef2c59', 'Il bacio t-shirt - XL', 'White', 'ffffff', 'XL', '5a9e7be6ef51a2', 59.97, 49.27, '5a9e7be6ef51a2.png', '5a9e7be6ef51a2_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(249, '5a9e7be6ef2c59', 'Il bacio t-shirt - 2XL', 'White', 'ffffff', '2XL', '5a9e7be6ef58c5', 59.97, 49.27, '5a9e7be6ef58c5.png', '5a9e7be6ef58c5_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(250, '5a9e7b0f14d151', 'Il pensiero t-shirt - XS', 'White', 'ffffff', 'XS', '5a9e7b0f14dae2', 59.97, 49.27, '5a9e7b0f14dae2.png', '5a9e7b0f14dae2_f.png', 'b5bf8-lsm_il_pensiero_red_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(251, '5a9e7b0f14d151', 'Il pensiero t-shirt - S', 'White', 'ffffff', 'S', '5a9e7b0f14e213', 59.97, 49.27, '5a9e7b0f14e213.png', '5a9e7b0f14e213_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(252, '5a9e7b0f14d151', 'Il pensiero t-shirt - M', 'White', 'ffffff', 'M', '5a9e7b0f14eb92', 59.97, 49.27, '5a9e7b0f14eb92.png', '5a9e7b0f14eb92_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(253, '5a9e7b0f14d151', 'Il pensiero t-shirt - L', 'White', 'ffffff', 'L', '5a9e7b0f14f167', 59.97, 49.27, '5a9e7b0f14f167.png', '5a9e7b0f14f167_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(254, '5a9e7b0f14d151', 'Il pensiero t-shirt - XL', 'White', 'ffffff', 'XL', '5a9e7b0f14f8e1', 59.97, 49.27, '5a9e7b0f14f8e1.png', '5a9e7b0f14f8e1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(255, '5a9e7b0f14d151', 'Il pensiero t-shirt - 2XL', 'White', 'ffffff', '2XL', '5a9e7b0f14fff4', 59.97, 49.27, '5a9e7b0f14fff4.png', '5a9e7b0f14fff4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(256, '5a9e7a457b0155', 'Il silenzio t-shirt - XS', 'White', 'ffffff', 'XS', '5a9e7a457b0983', 59.97, 49.27, '5a9e7a457b0983.png', '5a9e7a457b0983_f.png', 'd527a-lsm_il_silenzio_purple_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(257, '5a9e7a457b0155', 'Il silenzio t-shirt - S', 'White', 'ffffff', 'S', '5a9e7a457b10c5', 59.97, 49.27, '5a9e7a457b10c5.png', '5a9e7a457b10c5_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(258, '5a9e7a457b0155', 'Il silenzio t-shirt - M', 'White', 'ffffff', 'M', '5a9e7a457b17f5', 59.97, 49.27, '5a9e7a457b17f5.png', '5a9e7a457b17f5_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(259, '5a9e7a457b0155', 'Il silenzio t-shirt - L', 'White', 'ffffff', 'L', '5a9e7a457b1f11', 59.97, 49.27, '5a9e7a457b1f11.png', '5a9e7a457b1f11_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(260, '5a9e7a457b0155', 'Il silenzio t-shirt - XL', 'White', 'ffffff', 'XL', '5a9e7a457b2628', 59.97, 49.27, '5a9e7a457b2628.png', '5a9e7a457b2628_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(261, '5a9e7a457b0155', 'Il silenzio t-shirt - 2XL', 'White', 'ffffff', '2XL', '5a9e7a457b2d32', 59.97, 49.27, '5a9e7a457b2d32.png', '5a9e7a457b2d32_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(262, '5a9e7938145986', 'Infinito t-shirt - XS', 'White', 'ffffff', 'XS', '5a9e7938146198', 59.97, 49.27, '5a9e7938146198.png', '5a9e7938146198_f.png', '2b80a-lsm_infinito_blue_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(263, '5a9e7938145986', 'Infinito t-shirt - S', 'White', 'ffffff', 'S', '5a9e79381468c5', 59.97, 49.27, '5a9e79381468c5.png', '5a9e79381468c5_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(264, '5a9e7938145986', 'Infinito t-shirt - M', 'White', 'ffffff', 'M', '5a9e7938146fd8', 59.97, 49.27, '5a9e7938146fd8.png', '5a9e7938146fd8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(265, '5a9e7938145986', 'Infinito t-shirt - L', 'White', 'ffffff', 'L', '5a9e79381476d7', 59.97, 49.27, '5a9e79381476d7.png', '5a9e79381476d7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(266, '5a9e7938145986', 'Infinito t-shirt - XL', 'White', 'ffffff', 'XL', '5a9e7938147de8', 59.97, 49.27, '5a9e7938147de8.png', '5a9e7938147de8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(267, '5a9e7938145986', 'Infinito t-shirt - 2XL', 'White', 'ffffff', '2XL', '5a9e79381484e8', 59.97, 49.27, '5a9e79381484e8.png', '5a9e79381484e8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(268, '5a9e784dd4fc14', 'La separazione t-shirt - XS', 'White', 'ffffff', 'XS', '5a9e784dd50507', 59.97, 49.27, '5a9e784dd50507.png', '5a9e784dd50507_f.png', '771d6-lsm_la_separazione_blue_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(269, '5a9e784dd4fc14', 'La separazione t-shirt - S', 'White', 'ffffff', 'S', '5a9e784dd50d08', 59.97, 49.27, '5a9e784dd50d08.png', '5a9e784dd50d08_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(270, '5a9e784dd4fc14', 'La separazione t-shirt - M', 'White', 'ffffff', 'M', '5a9e784dd514f3', 59.97, 49.27, '5a9e784dd514f3.png', '5a9e784dd514f3_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(271, '5a9e784dd4fc14', 'La separazione t-shirt - L', 'White', 'ffffff', 'L', '5a9e784dd51cd2', 59.97, 49.27, '5a9e784dd51cd2.png', '5a9e784dd51cd2_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(272, '5a9e784dd4fc14', 'La separazione t-shirt - XL', 'White', 'ffffff', 'XL', '5a9e784dd524b2', 59.97, 49.27, '5a9e784dd524b2.png', '5a9e784dd524b2_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(273, '5a9e784dd4fc14', 'La separazione t-shirt - 2XL', 'White', 'ffffff', '2XL', '5a9e784dd52c88', 59.97, 49.27, '5a9e784dd52c88.png', '5a9e784dd52c88_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(274, '5a9e76e177cf98', 'Lo sguardo t-shirt - XS', 'White', 'ffffff', 'XS', '5a9e76e177d7a7', 59.97, 49.27, '5a9e76e177d7a7.png', '5a9e76e177d7a7_f.png', '10194-lsm_lo_sguardo_darkgreen_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(275, '5a9e76e177cf98', 'Lo sguardo t-shirt - S', 'White', 'ffffff', 'S', '5a9e76e177df18', 59.97, 49.27, '5a9e76e177df18.png', '5a9e76e177df18_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(276, '5a9e76e177cf98', 'Lo sguardo t-shirt - M', 'White', 'ffffff', 'M', '5a9e76e177e636', 59.97, 49.27, '5a9e76e177e636.png', '5a9e76e177e636_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(277, '5a9e76e177cf98', 'Lo sguardo t-shirt - L', 'White', 'ffffff', 'L', '5a9e76e177ed45', 59.97, 49.27, '5a9e76e177ed45.png', '5a9e76e177ed45_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(278, '5a9e76e177cf98', 'Lo sguardo t-shirt - XL', 'White', 'ffffff', 'XL', '5a9e76e177f555', 59.97, 49.27, '5a9e76e177f555.png', '5a9e76e177f555_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(279, '5a9e76e177cf98', 'Lo sguardo t-shirt - 2XL', 'White', 'ffffff', '2XL', '5a9e76e177fcd1', 59.97, 49.27, '5a9e76e177fcd1.png', '5a9e76e177fcd1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(280, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt - XS', 'White', 'ffffff', 'XS', '5a8ed76cdfb427', 57.97, 49.27, '5a8ed76cdfb427.png', '5a8ed76cdfb427_f.png', '340da-lsm_un_nuovo_inizio_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(281, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt - S', 'White', 'ffffff', 'S', '5a8ed76cdfbb44', 57.97, 49.27, '5a8ed76cdfbb44.png', '5a8ed76cdfbb44_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(282, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt - M', 'White', 'ffffff', 'M', '5a8ed76cdfc265', 57.97, 49.27, '5a8ed76cdfc265.png', '5a8ed76cdfc265_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(283, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt - L', 'White', 'ffffff', 'L', '5a8ed76cdfc973', 59.97, 49.27, '5a8ed76cdfc973.png', '5a8ed76cdfc973_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(284, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt - XL', 'White', 'ffffff', 'XL', '5a8ed76cdfd073', 59.97, 49.27, '5a8ed76cdfd073.png', '5a8ed76cdfd073_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(285, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt - 2XL', 'White', 'ffffff', '2XL', '5a8ed76cdfd766', 59.97, 49.27, '5a8ed76cdfd766.png', '5a8ed76cdfd766_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(286, '5a8ed67932c228', 'L\'incontro T-shirt - XS', 'White', 'ffffff', 'XS', '5a8ed67932ca34', 57.97, 49.27, '5a8ed67932ca34.png', '5a8ed67932ca34_f.png', '39f26-lsm_incontro_yellow_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(287, '5a8ed67932c228', 'L\'incontro T-shirt - S', 'White', 'ffffff', 'S', '5a8ed67932d184', 57.97, 49.27, '5a8ed67932d184.png', '5a8ed67932d184_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(288, '5a8ed67932c228', 'L\'incontro T-shirt - M', 'White', 'ffffff', 'M', '5a8ed67932d8a7', 57.97, 49.27, '5a8ed67932d8a7.png', '5a8ed67932d8a7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(289, '5a8ed67932c228', 'L\'incontro T-shirt - L', 'White', 'ffffff', 'L', '5a8ed67932dfc7', 57.97, 49.27, '5a8ed67932dfc7.png', '5a8ed67932dfc7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(290, '5a8ed67932c228', 'L\'incontro T-shirt - XL', 'White', 'ffffff', 'XL', '5a8ed67932e715', 57.97, 49.27, '5a8ed67932e715.png', '5a8ed67932e715_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(291, '5a8ed67932c228', 'L\'incontro T-shirt - 2XL', 'White', 'ffffff', '2XL', '5a8ed67932eef9', 57.97, 49.27, '5a8ed67932eef9.png', '5a8ed67932eef9_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(328, '5ac78fc799f779', '\"Dreaming of you\" black dress - XS', 'White', 'ffffff', 'XS', '5ac78fc799fe68', 69.95, 0, '5ac78fc799fe68.png', '5ac78fc799fe68_f.png', 'ba11b-md_dreaming_of_you_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(329, '5ac78fc799f779', '\"Dreaming of you\" black dress - S', 'White', 'ffffff', 'S', '5ac78fc79a0652', 69.95, 0, '5ac78fc79a0652.png', '5ac78fc79a0652_f.png', '', 1, 1, 2, '2018-06-04 10:00:01');
INSERT INTO `varianti_prodotti` (`id_variante`, `codice_prodotto`, `nome`, `colore`, `colore_codice`, `taglia`, `codice`, `prezzo`, `prezzo_scontato`, `url_img_piccola`, `url_img_grande`, `url_img_grande_retro`, `stato`, `in_stock`, `sync_status`, `sync_time`) VALUES
(330, '5ac78fc799f779', '\"Dreaming of you\" black dress - M', 'White', 'ffffff', 'M', '5ac78fc79a0ed8', 69.95, 0, '5ac78fc79a0ed8.png', '5ac78fc79a0ed8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(331, '5ac78fc799f779', '\"Dreaming of you\" black dress - L', 'White', 'ffffff', 'L', '5ac78fc79a1694', 69.95, 0, '5ac78fc79a1694.png', '5ac78fc79a1694_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(332, '5ac78fc799f779', '\"Dreaming of you\" black dress - XL', 'White', 'ffffff', 'XL', '5ac78fc79a1de2', 69.95, 0, '5ac78fc79a1de2.png', '5ac78fc79a1de2_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(333, '5ac78f5a022b03', 'Dreaming of you Red - XS', 'White', 'ffffff', 'XS', '5ac78f5a023447', 69.95, 0, '5ac78f5a023447.png', '5ac78f5a023447_f.png', 'e0a4d-md_dreaming_of_you_red_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(334, '5ac78f5a022b03', 'Dreaming of you Red - S', 'White', 'ffffff', 'S', '5ac78f5a023b86', 69.95, 0, '5ac78f5a023b86.png', '5ac78f5a023b86_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(335, '5ac78f5a022b03', 'Dreaming of you Red - M', 'White', 'ffffff', 'M', '5ac78f5a0243e6', 69.95, 0, '5ac78f5a0243e6.png', '5ac78f5a0243e6_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(336, '5ac78f5a022b03', 'Dreaming of you Red - L', 'White', 'ffffff', 'L', '5ac78f5a024c21', 69.95, 0, '5ac78f5a024c21.png', '5ac78f5a024c21_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(337, '5ac78f5a022b03', 'Dreaming of you Red - XL', 'White', 'ffffff', 'XL', '5ac78f5a025476', 69.95, 0, '5ac78f5a025476.png', '5ac78f5a025476_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(338, '5ac78eeb5ed556', 'Jellyfish - XS', 'White', 'ffffff', 'XS', '5ac78eeb5edec1', 69.95, 0, '5ac78eeb5edec1.png', '5ac78eeb5edec1_f.png', '89163-md_jellyfish_aqua_b.jpg', 1, 1, 2, '2018-06-04 10:00:01'),
(339, '5ac78eeb5ed556', 'Jellyfish - S', 'White', 'ffffff', 'S', '5ac78eeb5ee628', 69.95, 0, '5ac78eeb5ee628.png', '5ac78eeb5ee628_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(340, '5ac78eeb5ed556', 'Jellyfish - M', 'White', 'ffffff', 'M', '5ac78eeb5eed52', 69.95, 0, '5ac78eeb5eed52.png', '5ac78eeb5eed52_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(341, '5ac78eeb5ed556', 'Jellyfish - L', 'White', 'ffffff', 'L', '5ac78eeb5ef472', 69.95, 0, '5ac78eeb5ef472.png', '5ac78eeb5ef472_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(342, '5ac78eeb5ed556', 'Jellyfish - XL', 'White', 'ffffff', 'XL', '5ac78eeb5efb98', 69.95, 0, '5ac78eeb5efb98.png', '5ac78eeb5efb98_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(343, '5ac7a04fbb0094', '\"Japan\" dress - XS', 'White', 'ffffff', 'XS', '5ac7a04fbb08c8', 69.95, 0, '5ac7a04fbb08c8.png', '5ac7a04fbb08c8_f.png', 'ccde2-md_japan_yellow_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(344, '5ac7a04fbb0094', '\"Japan\" dress - S', 'White', 'ffffff', 'S', '5ac7a04fbb1021', 69.95, 0, '5ac7a04fbb1021.png', '5ac7a04fbb1021_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(345, '5ac7a04fbb0094', '\"Japan\" dress - M', 'White', 'ffffff', 'M', '5ac7a04fbb1752', 69.95, 0, '5ac7a04fbb1752.png', '5ac7a04fbb1752_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(346, '5ac7a04fbb0094', '\"Japan\" dress - L', 'White', 'ffffff', 'L', '5ac7a04fbb1e74', 69.95, 0, '5ac7a04fbb1e74.png', '5ac7a04fbb1e74_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(347, '5ac7a04fbb0094', '\"Japan\" dress - XL', 'White', 'ffffff', 'XL', '5ac7a04fbb25b2', 69.95, 0, '5ac7a04fbb25b2.png', '5ac7a04fbb25b2_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(348, '5ac79fa0e200e6', '\"Lips\" black dress - XS', 'White', 'ffffff', 'XS', '5ac79fa0e20911', 69.95, 0, '5ac79fa0e20911.png', '5ac79fa0e20911_f.png', '3e5cb-md_lips_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(349, '5ac79fa0e200e6', '\"Lips\" black dress - S', 'White', 'ffffff', 'S', '5ac79fa0e21067', 69.95, 0, '5ac79fa0e21067.png', '5ac79fa0e21067_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(350, '5ac79fa0e200e6', '\"Lips\" black dress - M', 'White', 'ffffff', 'M', '5ac79fa0e21794', 69.95, 0, '5ac79fa0e21794.png', '5ac79fa0e21794_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(351, '5ac79fa0e200e6', '\"Lips\" black dress - L', 'White', 'ffffff', 'L', '5ac79fa0e21ec1', 69.95, 0, '5ac79fa0e21ec1.png', '5ac79fa0e21ec1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(352, '5ac79fa0e200e6', '\"Lips\" black dress - XL', 'White', 'ffffff', 'XL', '5ac79fa0e225e2', 69.95, 0, '5ac79fa0e225e2.png', '5ac79fa0e225e2_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(353, '5ac79f41a6a458', '\"Lips\" violet dress - XS', 'White', 'ffffff', 'XS', '5ac79f41a6acb8', 69.95, 0, '5ac79f41a6acb8.png', '5ac79f41a6acb8_f.png', '87316-md_lips_violet_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(354, '5ac79f41a6a458', '\"Lips\" violet dress - S', 'White', 'ffffff', 'S', '5ac79f41a6b401', 69.95, 0, '5ac79f41a6b401.png', '5ac79f41a6b401_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(355, '5ac79f41a6a458', '\"Lips\" violet dress - M', 'White', 'ffffff', 'M', '5ac79f41a6bb38', 69.95, 0, '5ac79f41a6bb38.png', '5ac79f41a6bb38_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(356, '5ac79f41a6a458', '\"Lips\" violet dress - L', 'White', 'ffffff', 'L', '5ac79f41a6c252', 69.95, 0, '5ac79f41a6c252.png', '5ac79f41a6c252_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(357, '5ac79f41a6a458', '\"Lips\" violet dress - XL', 'White', 'ffffff', 'XL', '5ac79f41a6c976', 69.95, 0, '5ac79f41a6c976.png', '5ac79f41a6c976_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(358, '5ac89b9c141e76', '\"Colorful cats\"  - S', 'White', 'e5e6e1', 'S', '5ac89b9c142684', 42.95, 0, '5ac89b9c142684.png', '5ac89b9c142684_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(359, '5ac89b9c141e76', '\"Colorful cats\"  - M', 'White', 'e5e6e1', 'M', '5ac89b9c142dc2', 42.95, 0, '5ac89b9c142dc2.png', '5ac89b9c142dc2_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(360, '5ac89b9c141e76', '\"Colorful cats\"  - L', 'White', 'e5e6e1', 'L', '5ac89b9c1434e9', 42.95, 0, '5ac89b9c1434e9.png', '5ac89b9c1434e9_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(361, '5ac89b9c141e76', '\"Colorful cats\"  - XL', 'White', 'e5e6e1', 'XL', '5ac89b9c143bf7', 42.95, 0, '5ac89b9c143bf7.png', '5ac89b9c143bf7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(362, '5ac89b9c141e76', '\"Colorful cats\"  - 2XL', 'White', 'e5e6e1', '2XL', '5ac89b9c144304', 42.95, 0, '5ac89b9c144304.png', '5ac89b9c144304_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(363, '5acd1cefea6059', 'Ocean purple - XS', 'White', 'ffffff', 'XS', '5acd1cefea6a38', 69.95, 0, '5acd1cefea6a38.png', '5acd1cefea6a38_f.png', 'a57ad-md_ocean_purple_purple_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(364, '5acd1cefea6059', 'Ocean purple - S', 'White', 'ffffff', 'S', '5acd1cefea72b8', 69.95, 0, '5acd1cefea72b8.png', '5acd1cefea72b8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(365, '5acd1cefea6059', 'Ocean purple - M', 'White', 'ffffff', 'M', '5acd1cefea7b03', 69.95, 0, '5acd1cefea7b03.png', '5acd1cefea7b03_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(366, '5acd1cefea6059', 'Ocean purple - L', 'White', 'ffffff', 'L', '5acd1cefea8341', 69.95, 0, '5acd1cefea8341.png', '5acd1cefea8341_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(367, '5acd1cefea6059', 'Ocean purple - XL', 'White', 'ffffff', 'XL', '5acd1cefea8b84', 69.95, 0, '5acd1cefea8b84.png', '5acd1cefea8b84_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(368, '5ace35879dac75', '\" Fluo \" Tank Top - XS', 'White', 'ffffff', 'XS', '5ace35879db4b8', 49.95, 0, '5ace35879db4b8.png', '5ace35879db4b8_f.png', 'eff69-tt_fluo_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(369, '5ace35879dac75', '\" Fluo \" Tank Top - S', 'White', 'ffffff', 'S', '5ace35879dbc04', 49.95, 0, '5ace35879dbc04.png', '5ace35879dbc04_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(370, '5ace35879dac75', '\" Fluo \" Tank Top - M', 'White', 'ffffff', 'M', '5ace35879dc345', 49.95, 0, '5ace35879dc345.png', '5ace35879dc345_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(371, '5ace35879dac75', '\" Fluo \" Tank Top - L', 'White', 'ffffff', 'L', '5ace35879dca88', 49.95, 0, '5ace35879dca88.png', '5ace35879dca88_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(372, '5ace35879dac75', '\" Fluo \" Tank Top - XL', 'White', 'ffffff', 'XL', '5ace35879dd1b4', 49.95, 0, '5ace35879dd1b4.png', '5ace35879dd1b4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(373, '5ace34e242d3a1', '\" Colored cats \" tank top - XS', 'White', 'ffffff', 'XS', '5ace34e242dce6', 49.95, 0, '5ace34e242dce6.png', '5ace34e242dce6_f.png', 'a1162-tt_colored_cats_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(374, '5ace34e242d3a1', '\" Colored cats \" tank top - S', 'White', 'ffffff', 'S', '5ace34e242e4e8', 49.95, 0, '5ace34e242e4e8.png', '5ace34e242e4e8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(375, '5ace34e242d3a1', '\" Colored cats \" tank top - M', 'White', 'ffffff', 'M', '5ace34e242ec34', 49.95, 0, '5ace34e242ec34.png', '5ace34e242ec34_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(376, '5ace34e242d3a1', '\" Colored cats \" tank top - L', 'White', 'ffffff', 'L', '5ace34e242f363', 49.95, 0, '5ace34e242f363.png', '5ace34e242f363_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(377, '5ace34e242d3a1', '\" Colored cats \" tank top - XL', 'White', 'ffffff', 'XL', '5ace34e242faf7', 49.95, 0, '5ace34e242faf7.png', '5ace34e242faf7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(378, '5ace33a14f8df1', '\" Colorful town \" tank top - XS', 'White', 'ffffff', 'XS', '5ace33a14f96c2', 49.95, 0, '5ace33a14f96c2.png', '5ace33a14f96c2_f.png', '25a44-tt_colorful_town_black_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(379, '5ace33a14f8df1', '\" Colorful town \" tank top - S', 'White', 'ffffff', 'S', '5ace33a14f9ef5', 49.95, 0, '5ace33a14f9ef5.png', '5ace33a14f9ef5_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(380, '5ace33a14f8df1', '\" Colorful town \" tank top - M', 'White', 'ffffff', 'M', '5ace33a14fa743', 49.95, 0, '5ace33a14fa743.png', '5ace33a14fa743_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(381, '5ace33a14f8df1', '\" Colorful town \" tank top - L', 'White', 'ffffff', 'L', '5ace33a14fafb2', 49.95, 0, '5ace33a14fafb2.png', '5ace33a14fafb2_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(382, '5ace33a14f8df1', '\" Colorful town \" tank top - XL', 'White', 'ffffff', 'XL', '5ace33a14fb788', 49.95, 0, '5ace33a14fb788.png', '5ace33a14fb788_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(383, '5ace560ed99ae6', '\"Sleeping mask \" Tank Top - S', 'White', 'D9DAD5', 'S', '5ace560ed9a2f5', 39.95, 0, '5ace560ed9a2f5.png', '5ace560ed9a2f5_f.png', 'ba773-tt_sleeping_mask_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(384, '5ace560ed99ae6', '\"Sleeping mask \" Tank Top - M', 'White', 'D9DAD5', 'M', '5ace560ed9aa37', 39.95, 0, '5ace560ed9aa37.png', '5ace560ed9aa37_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(385, '5ace560ed99ae6', '\"Sleeping mask \" Tank Top - L', 'White', 'D9DAD5', 'L', '5ace560ed9b157', 39.95, 0, '5ace560ed9b157.png', '5ace560ed9b157_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(386, '5ace560ed99ae6', '\"Sleeping mask \" Tank Top - XL', 'White', 'D9DAD5', 'XL', '5ace560ed9b867', 39.95, 0, '5ace560ed9b867.png', '5ace560ed9b867_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(387, '5ae4ae11af8f51', '\"Mindyoursoul\" T-shirt - S', 'Charcoal black ', '202020', 'S', '5ae4ae11af9866', 15.5, 0, '5ae4ae11af9866.png', '5ae4ae11af9866_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(388, '5ae4ae11af8f51', '\"Mindyoursoul\" T-shirt - M', 'Charcoal black ', '202020', 'M', '5ae4ae11afa097', 15.5, 0, '5ae4ae11afa097.png', '5ae4ae11afa097_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(389, '5ae4ae11af8f51', '\"Mindyoursoul\" T-shirt - L', 'Charcoal black ', '202020', 'L', '5ae4ae11afa8a1', 15.5, 0, '5ae4ae11afa8a1.png', '5ae4ae11afa8a1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(390, '5ae4ae11af8f51', '\"Mindyoursoul\" T-shirt - XL', 'Charcoal black ', '202020', 'XL', '5ae4ae11afb0a1', 15.5, 0, '5ae4ae11afb0a1.png', '5ae4ae11afb0a1_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(395, '5ae4ad0da64437', '\"Il pensiero\" Tank top - XS', 'White', 'ffffff', 'XS', '5ae4ad0da64de6', 46.95, 0, '5ae4ad0da64de6.png', '5ae4ad0da64de6_f.png', 'b683c-tt_il_pensiero_red_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(396, '5ae4ad0da64437', '\"Il pensiero\" Tank top - S', 'White', 'ffffff', 'S', '5ae4ad0da65603', 46.95, 0, '5ae4ad0da65603.png', '5ae4ad0da65603_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(397, '5ae4ad0da64437', '\"Il pensiero\" Tank top - M', 'White', 'ffffff', 'M', '5ae4ad0da65db4', 46.95, 0, '5ae4ad0da65db4.png', '5ae4ad0da65db4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(398, '5ae4ad0da64437', '\"Il pensiero\" Tank top - L', 'White', 'ffffff', 'L', '5ae4ad0da66558', 46.95, 0, '5ae4ad0da66558.png', '5ae4ad0da66558_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(399, '5ae4ad0da64437', '\"Il pensiero\" Tank top - XL', 'White', 'ffffff', 'XL', '5ae4ad0da66ce2', 46.95, 0, '5ae4ad0da66ce2.png', '5ae4ad0da66ce2_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(400, '5ae4ad0da64437', '\"Il pensiero\" Tank top - 2XL', 'White', 'ffffff', '2XL', '5ae4ad0da67471', 46.95, 0, '5ae4ad0da67471.png', '5ae4ad0da67471_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(401, '5ae4ac4dd579a8', '\"Lo sguardo\" Tank top - XS', 'White', 'ffffff', 'XS', '5ae4ac4dd58282', 46.95, 0, '5ae4ac4dd58282.png', '5ae4ac4dd58282_f.png', 'f1bf4-tt_lo_sguardo_black.png', 1, 1, 2, '2018-06-04 10:00:01'),
(402, '5ae4ac4dd579a8', '\"Lo sguardo\" Tank top - S', 'White', 'ffffff', 'S', '5ae4ac4dd58a29', 46.95, 0, '5ae4ac4dd58a29.png', '5ae4ac4dd58a29_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(403, '5ae4ac4dd579a8', '\"Lo sguardo\" Tank top - M', 'White', 'ffffff', 'M', '5ae4ac4dd591b7', 46.95, 0, '5ae4ac4dd591b7.png', '5ae4ac4dd591b7_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(404, '5ae4ac4dd579a8', '\"Lo sguardo\" Tank top - L', 'White', 'ffffff', 'L', '5ae4ac4dd59923', 46.95, 0, '5ae4ac4dd59923.png', '5ae4ac4dd59923_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(405, '5ae4ac4dd579a8', '\"Lo sguardo\" Tank top - XL', 'White', 'ffffff', 'XL', '5ae4ac4dd5a0b5', 46.95, 0, '5ae4ac4dd5a0b5.png', '5ae4ac4dd5a0b5_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(406, '5ae4ac4dd579a8', '\"Lo sguardo\" Tank top - 2XL', 'White', 'ffffff', '2XL', '5ae4ac4dd5a846', 46.95, 0, '5ae4ac4dd5a846.png', '5ae4ac4dd5a846_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(415, '5ae4aad40b2c44', '\"Bonds\" Tank top - XS', 'White', 'ffffff', 'XS', '5ae4aad40b34f7', 46.95, 0, '5ae4aad40b34f7.png', '5ae4aad40b34f7_f.png', '733d2-tt_bonds_black.png', 1, 1, 2, '2018-06-04 10:00:01'),
(416, '5ae4aad40b2c44', '\"Bonds\" Tank top - S', 'White', 'ffffff', 'S', '5ae4aad40b3ca8', 46.95, 0, '5ae4aad40b3ca8.png', '5ae4aad40b3ca8_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(417, '5ae4aad40b2c44', '\"Bonds\" Tank top - M', 'White', 'ffffff', 'M', '5ae4aad40b4438', 46.95, 0, '5ae4aad40b4438.png', '5ae4aad40b4438_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(418, '5ae4aad40b2c44', '\"Bonds\" Tank top - L', 'White', 'ffffff', 'L', '5ae4aad40b4c28', 46.95, 0, '5ae4aad40b4c28.png', '5ae4aad40b4c28_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(419, '5ae4aad40b2c44', '\"Bonds\" Tank top - XL', 'White', 'ffffff', 'XL', '5ae4aad40b53d4', 46.95, 0, '5ae4aad40b53d4.png', '5ae4aad40b53d4_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(420, '5ae4aad40b2c44', '\"Bonds\" Tank top - 2XL', 'White', 'ffffff', '2XL', '5ae4aad40b5b01', 46.95, 0, '5ae4aad40b5b01.png', '5ae4aad40b5b01_f.png', '', 1, 1, 2, '2018-06-04 10:00:01'),
(421, '5b01cfb9a57859', '\"Infinito\" Beach Towel', 'White', 'ffffff', '30x60', '5b01cfb9a58148', 39.99, 0, '5b01cfb9a58148.png', '5b01cfb9a58148_f.png', '0f9ec-bt_infinito_red_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(422, '5b0157f0abdc04', '\"Tinge\" Beach Towel', 'White', 'ffffff', '30x60', '5b0157f0abe524', 39.99, 0, '5b0157f0abe524.png', '5b0157f0abe524_f.png', 'c1364-bt_tinge_blue_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(423, '5b0157a39d1385', '\"Summer flowers\" Beach Towel', 'White', 'ffffff', '30x60', '5b0157a39d1bb7', 39.99, 0, '5b0157a39d1bb7.png', '5b0157a39d1bb7_f.png', '28e3b-bt_summer_flowers_violet_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(424, '5b0155db8ec4b7', '\"Bonds\" Beach Towel', 'White', 'ffffff', '30x60', '5b0155db8ecca4', 39.99, 0, '5b0155db8ecca4.png', '5b0155db8ecca4_f.png', '5bf55-bt_bonds_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(425, '5b0155800b1581', '\"Colorful tree\" Beach Towel', 'White', 'ffffff', '30x60', '5b0155800b1db4', 39.99, 0, '5b0155800b1db4.png', '5b0155800b1db4_f.png', 'aa280-bt_colorful_tree_red_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(426, '5b015504d39408', '\"Flowers\" Beach Towel', 'White', 'ffffff', '30x60', '5b015504d39c28', 39.99, 0, '5b015504d39c28.png', '5b015504d39c28_f.png', 'a5b18-bt_flowers_purple_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(427, '5b0154ab6a0ab6', '\"A woman with a monkey\" Beach Towel', 'White', 'ffffff', '30x60', '5b0154ab6a12f5', 39.99, 0, '5b0154ab6a12f5.png', '5b0154ab6a12f5_f.png', 'f3be9-bt_a_woman_playing_with_a_monkey_red_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(428, '5b01542043e103', '\"Yang\" Beach Towel', 'White', 'ffffff', '30x60', '5b01542043e951', 39.99, 0, '5b01542043e951.png', '5b01542043e951_f.png', '2038a-bt_yang_blue_b-2-.png', 1, 1, 2, '2018-06-04 10:00:01'),
(429, '5b0153c5c24c51', '\"Fluo\" Beach Towel', 'White', 'ffffff', '30x60', '5b0153c5c255b6', 39.99, 0, '5b0153c5c255b6.png', '5b0153c5c255b6_f.png', '4ad26-bt_fluo_green_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(430, '5b0c42cfb530a8', '\"Explosion\" Beach Bag', 'Black', '000000', '16×20', '5b0c42cfb538d5', 43.95, 0, '5b0c42cfb538d5.png', '5b0c42cfb538d5_f.png', 'a4bf9-bb_explosion_orange_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(431, '5b0c418f41dd81', '\"Jellyfish\" Beach Bag', 'Black', '000000', '16×20', '5b0c418f41e6a2', 43.95, 0, '5b0c418f41e6a2.png', '5b0c418f41e6a2_f.png', 'b4842-bb_jellyfish_green_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(432, '5b0c36a3794d15', '\"Flowers\" Beach Bag', 'Black', '000000', '16×20', '5b0c36a3795678', 43.95, 0, '5b0c36a3795678.png', '5b0c36a3795678_f.png', '9ce08-bb_flowers_purple_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(433, '5b0c35e96a9326', '\"Yang\" Beach Bag', 'Black', '000000', '16×20', '5b0c35e96a9b36', 43.95, 0, '5b0c35e96a9b36.png', '5b0c35e96a9b36_f.png', '9d187-bb_yang_blue_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(434, '5b0c3534992b15', '\"White bonds\" Beach Bag', 'Red', 'c5262a', '16×20', '5b0c3534993426', 43.95, 0, '5b0c3534993426.png', '5b0c3534993426_f.png', '2662b-bb_white_bonds_white_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(435, '5b0c34b4a6f9c4', '\"Bonds\" Beach Bag', 'Yellow', 'de9219', '16×20', '5b0c34b4a70298', 43.95, 0, '5b0c34b4a70298.png', '5b0c34b4a70298_f.png', 'd7551-bb_bonds_violet_b_png.png', 1, 1, 2, '2018-06-04 10:00:01'),
(436, '5b0c33e33e5128', '\"Colored tree\" Beach Bag', 'Black', '000000', '16×20', '5b0c33e33e5874', 43.95, 0, '5b0c33e33e5874.png', '5b0c33e33e5874_f.png', '55798-bb_colored_tree_blue_b.png', 1, 1, 2, '2018-06-04 10:00:01'),
(437, '5b0c330931f679', '\"Il silenzio\" Beach Bag', 'Red', 'c5262a', '16×20', '5b0c330931feb4', 43.95, 0, '5b0c330931feb4.png', '5b0c330931feb4_f.png', '89382-bb_il_silenzio_red_b.png', 1, 1, 2, '2018-06-04 10:00:01');

-- --------------------------------------------------------

--
-- Struttura della tabella `wishlist`
--

CREATE TABLE `wishlist` (
  `id_wishlist` int(11) NOT NULL,
  `codice_variante` varchar(250) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `data_creazione` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `wishlist`
--

INSERT INTO `wishlist` (`id_wishlist`, `codice_variante`, `id_cliente`, `data_creazione`) VALUES
(1, '5acd1cefea6a38', NULL, '2018-04-17 15:40:50'),
(2, '5ac79fa0e20911', NULL, '2018-04-17 15:41:19');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `carrello`
--
ALTER TABLE `carrello`
  ADD PRIMARY KEY (`id_carrello`);

--
-- Indici per le tabelle `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id_categorie`);

--
-- Indici per le tabelle `categorie_gallery`
--
ALTER TABLE `categorie_gallery`
  ADD PRIMARY KEY (`id_categoria_gallery`);

--
-- Indici per le tabelle `categorie_gallery_traduzioni`
--
ALTER TABLE `categorie_gallery_traduzioni`
  ADD PRIMARY KEY (`id_categorie_gallery_traduzioni`);

--
-- Indici per le tabelle `categorie_traduzioni`
--
ALTER TABLE `categorie_traduzioni`
  ADD PRIMARY KEY (`id_categoria_traduzioni`);

--
-- Indici per le tabelle `clienti`
--
ALTER TABLE `clienti`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indici per le tabelle `colori_classi`
--
ALTER TABLE `colori_classi`
  ADD PRIMARY KEY (`id_colore_classe`);

--
-- Indici per le tabelle `colori_prodotti`
--
ALTER TABLE `colori_prodotti`
  ADD PRIMARY KEY (`id_colori_prodotti`);

--
-- Indici per le tabelle `constants_framework`
--
ALTER TABLE `constants_framework`
  ADD PRIMARY KEY (`id_cf`);

--
-- Indici per le tabelle `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  ADD PRIMARY KEY (`id_contatto`);

--
-- Indici per le tabelle `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  ADD PRIMARY KEY (`id_contatto_newsletter`);

--
-- Indici per le tabelle `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_code`);

--
-- Indici per le tabelle `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id_coupon`);

--
-- Indici per le tabelle `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id_template`);

--
-- Indici per le tabelle `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `home_slider`
--
ALTER TABLE `home_slider`
  ADD PRIMARY KEY (`hs_id`);

--
-- Indici per le tabelle `immagini_gallery`
--
ALTER TABLE `immagini_gallery`
  ADD PRIMARY KEY (`id_ig`);

--
-- Indici per le tabelle `immagini_gallery_traduzioni`
--
ALTER TABLE `immagini_gallery_traduzioni`
  ADD PRIMARY KEY (`id_immagini_gallery_traduzioni`);

--
-- Indici per le tabelle `indirizzo_fatturazione`
--
ALTER TABLE `indirizzo_fatturazione`
  ADD PRIMARY KEY (`id_indirizzo_fatturazione`);

--
-- Indici per le tabelle `indirizzo_spedizione`
--
ALTER TABLE `indirizzo_spedizione`
  ADD PRIMARY KEY (`id_indirizzo_spedizione`),
  ADD KEY `fk_cliente_idx` (`id_cliente`);

--
-- Indici per le tabelle `lingue`
--
ALTER TABLE `lingue`
  ADD PRIMARY KEY (`id_lingue`);

--
-- Indici per le tabelle `lingue_labels_lang`
--
ALTER TABLE `lingue_labels_lang`
  ADD PRIMARY KEY (`id_lingue_labels_lang`);

--
-- Indici per le tabelle `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`id_ordine`),
  ADD KEY `fk_cliente_idx` (`id_cliente`);

--
-- Indici per le tabelle `pagine`
--
ALTER TABLE `pagine`
  ADD PRIMARY KEY (`id_pagina`);

--
-- Indici per le tabelle `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  ADD PRIMARY KEY (`id_pc`);

--
-- Indici per le tabelle `prodotti`
--
ALTER TABLE `prodotti`
  ADD PRIMARY KEY (`id_prodotti`);

--
-- Indici per le tabelle `prodotti_categorie`
--
ALTER TABLE `prodotti_categorie`
  ADD PRIMARY KEY (`id_prodotti_categorie`);

--
-- Indici per le tabelle `prodotti_traduzioni`
--
ALTER TABLE `prodotti_traduzioni`
  ADD PRIMARY KEY (`id_prodotti_traduzioni`);

--
-- Indici per le tabelle `prodotto_taglia`
--
ALTER TABLE `prodotto_taglia`
  ADD PRIMARY KEY (`fk_prodotto`,`fk_taglia`);

--
-- Indici per le tabelle `shipments`
--
ALTER TABLE `shipments`
  ADD PRIMARY KEY (`ship_id`);

--
-- Indici per le tabelle `stato_coupon`
--
ALTER TABLE `stato_coupon`
  ADD PRIMARY KEY (`id_stato_coupon`);

--
-- Indici per le tabelle `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  ADD PRIMARY KEY (`id_stato_descrizione`);

--
-- Indici per le tabelle `stato_ordine`
--
ALTER TABLE `stato_ordine`
  ADD PRIMARY KEY (`id_stato_ordine`);

--
-- Indici per le tabelle `stato_pagamento`
--
ALTER TABLE `stato_pagamento`
  ADD PRIMARY KEY (`id_stato_pagamento`);

--
-- Indici per le tabelle `stato_prodotti`
--
ALTER TABLE `stato_prodotti`
  ADD PRIMARY KEY (`stato_prodotti_id`);

--
-- Indici per le tabelle `storico_carrello`
--
ALTER TABLE `storico_carrello`
  ADD PRIMARY KEY (`id_storico_carrello`);

--
-- Indici per le tabelle `storico_clienti`
--
ALTER TABLE `storico_clienti`
  ADD PRIMARY KEY (`id_storico_clienti`);

--
-- Indici per le tabelle `sync_status`
--
ALTER TABLE `sync_status`
  ADD PRIMARY KEY (`id_sync_status`);

--
-- Indici per le tabelle `sync_test`
--
ALTER TABLE `sync_test`
  ADD PRIMARY KEY (`id_sync_test`);

--
-- Indici per le tabelle `taglie`
--
ALTER TABLE `taglie`
  ADD PRIMARY KEY (`id_taglia`);

--
-- Indici per le tabelle `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id_tag`);

--
-- Indici per le tabelle `tags_prodotti`
--
ALTER TABLE `tags_prodotti`
  ADD PRIMARY KEY (`id_tags_prodotti`);

--
-- Indici per le tabelle `tipo_coupon`
--
ALTER TABLE `tipo_coupon`
  ADD PRIMARY KEY (`id_tipo_coupon`);

--
-- Indici per le tabelle `tipo_pagamento`
--
ALTER TABLE `tipo_pagamento`
  ADD PRIMARY KEY (`id_tipo_pagamento`);

--
-- Indici per le tabelle `tipo_prodotto`
--
ALTER TABLE `tipo_prodotto`
  ADD PRIMARY KEY (`id_tipo_prodotto`);

--
-- Indici per le tabelle `tipo_stampa`
--
ALTER TABLE `tipo_stampa`
  ADD PRIMARY KEY (`id_tipo_stampa`);

--
-- Indici per le tabelle `tipo_template`
--
ALTER TABLE `tipo_template`
  ADD PRIMARY KEY (`id_tipo_template`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indici per le tabelle `variante_taglia`
--
ALTER TABLE `variante_taglia`
  ADD PRIMARY KEY (`fk_variante`,`fk_taglia`);

--
-- Indici per le tabelle `varianti_prodotti`
--
ALTER TABLE `varianti_prodotti`
  ADD PRIMARY KEY (`id_variante`),
  ADD KEY `fk_prodotti` (`codice_prodotto`),
  ADD KEY `fk_colore` (`colore`);

--
-- Indici per le tabelle `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id_wishlist`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `carrello`
--
ALTER TABLE `carrello`
  MODIFY `id_carrello` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id_categorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT per la tabella `categorie_gallery`
--
ALTER TABLE `categorie_gallery`
  MODIFY `id_categoria_gallery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `categorie_gallery_traduzioni`
--
ALTER TABLE `categorie_gallery_traduzioni`
  MODIFY `id_categorie_gallery_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT per la tabella `categorie_traduzioni`
--
ALTER TABLE `categorie_traduzioni`
  MODIFY `id_categoria_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT per la tabella `clienti`
--
ALTER TABLE `clienti`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `colori_classi`
--
ALTER TABLE `colori_classi`
  MODIFY `id_colore_classe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `colori_prodotti`
--
ALTER TABLE `colori_prodotti`
  MODIFY `id_colori_prodotti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT per la tabella `constants_framework`
--
ALTER TABLE `constants_framework`
  MODIFY `id_cf` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT per la tabella `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  MODIFY `id_contatto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  MODIFY `id_contatto_newsletter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id_coupon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id_template` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `home_slider`
--
ALTER TABLE `home_slider`
  MODIFY `hs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `immagini_gallery`
--
ALTER TABLE `immagini_gallery`
  MODIFY `id_ig` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT per la tabella `immagini_gallery_traduzioni`
--
ALTER TABLE `immagini_gallery_traduzioni`
  MODIFY `id_immagini_gallery_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT per la tabella `indirizzo_fatturazione`
--
ALTER TABLE `indirizzo_fatturazione`
  MODIFY `id_indirizzo_fatturazione` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT per la tabella `indirizzo_spedizione`
--
ALTER TABLE `indirizzo_spedizione`
  MODIFY `id_indirizzo_spedizione` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `lingue`
--
ALTER TABLE `lingue`
  MODIFY `id_lingue` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `lingue_labels_lang`
--
ALTER TABLE `lingue_labels_lang`
  MODIFY `id_lingue_labels_lang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=411;

--
-- AUTO_INCREMENT per la tabella `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `ordini`
--
ALTER TABLE `ordini`
  MODIFY `id_ordine` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `pagine`
--
ALTER TABLE `pagine`
  MODIFY `id_pagina` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT per la tabella `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  MODIFY `id_pc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT per la tabella `prodotti`
--
ALTER TABLE `prodotti`
  MODIFY `id_prodotti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT per la tabella `prodotti_categorie`
--
ALTER TABLE `prodotti_categorie`
  MODIFY `id_prodotti_categorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT per la tabella `prodotti_traduzioni`
--
ALTER TABLE `prodotti_traduzioni`
  MODIFY `id_prodotti_traduzioni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;

--
-- AUTO_INCREMENT per la tabella `shipments`
--
ALTER TABLE `shipments`
  MODIFY `ship_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `stato_coupon`
--
ALTER TABLE `stato_coupon`
  MODIFY `id_stato_coupon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  MODIFY `id_stato_descrizione` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `stato_ordine`
--
ALTER TABLE `stato_ordine`
  MODIFY `id_stato_ordine` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `stato_pagamento`
--
ALTER TABLE `stato_pagamento`
  MODIFY `id_stato_pagamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `storico_carrello`
--
ALTER TABLE `storico_carrello`
  MODIFY `id_storico_carrello` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `storico_clienti`
--
ALTER TABLE `storico_clienti`
  MODIFY `id_storico_clienti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `sync_status`
--
ALTER TABLE `sync_status`
  MODIFY `id_sync_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `sync_test`
--
ALTER TABLE `sync_test`
  MODIFY `id_sync_test` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `taglie`
--
ALTER TABLE `taglie`
  MODIFY `id_taglia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `tags`
--
ALTER TABLE `tags`
  MODIFY `id_tag` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `tags_prodotti`
--
ALTER TABLE `tags_prodotti`
  MODIFY `id_tags_prodotti` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT per la tabella `tipo_coupon`
--
ALTER TABLE `tipo_coupon`
  MODIFY `id_tipo_coupon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `tipo_pagamento`
--
ALTER TABLE `tipo_pagamento`
  MODIFY `id_tipo_pagamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `tipo_prodotto`
--
ALTER TABLE `tipo_prodotto`
  MODIFY `id_tipo_prodotto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `tipo_stampa`
--
ALTER TABLE `tipo_stampa`
  MODIFY `id_tipo_stampa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `tipo_template`
--
ALTER TABLE `tipo_template`
  MODIFY `id_tipo_template` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT per la tabella `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT per la tabella `varianti_prodotti`
--
ALTER TABLE `varianti_prodotti`
  MODIFY `id_variante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=438;

--
-- AUTO_INCREMENT per la tabella `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id_wishlist` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
